/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.business;

import com.google.common.collect.Lists;
import com.google.inject.AbstractModule;
import org.assertj.core.api.Assertions;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.business.apitests.AllServicesInjector;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDemandDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.material.AddMaterialRequest;
import regiec.knast.erp.api.dto.material.MaterialDTO;
import regiec.knast.erp.api.dto.material.RemoveMaterialRequest;
import regiec.knast.erp.api.dto.materialtemplates.AddMaterialTemplateRequest;
import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTO;
import regiec.knast.erp.api.dto.materialtypes.AddMaterialTypeRequest;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypeDTO;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.exceptions.RemoveEntityException;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.util.TestBinderBuilder;
import regiec.knast.erp.api.util.TestGuiceBuilder;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MaterialServiceTest extends AllServicesInjector {

    private TestGuiceBuilder objBuilder;
    private MaterialDemandDAOInterface materialDemandDAO;

    private String materialTypetId = null;
    private String materialTemplateId = null;
    private static CuttingSize SIZE = CuttingSizeManager.createOneDimCuttingSize(3.0);

    @Before
    public void setUp() throws ObjectCreateError {
        AbstractModule module = new TestBinderBuilder(true)
                .build();
        objBuilder = new TestGuiceBuilder(module);
        ObjectBuilderFactory.configure(objBuilder);
        super.initServices();
        materialDemandDAO = objBuilder.create(MaterialDemandDAOInterface.class);

        String materialTypeName = "Profile";
        String materialTemplateCode = "material1";

        // add material1 to database        
        MaterialTemplateDTO mtDTO1 = addMaterialTemplate(materialTypeName, materialTemplateCode);
        materialTypetId = mtDTO1.getMaterialTypeId();
        materialTemplateId = mtDTO1.getId();
        // validate material template exists
        MaterialTemplateDTO mtDTO = materialTemplateService.getMaterialTemplate(new BasicGetRequest(mtDTO1.getId()));
        Assertions.assertThat(mtDTO).isNotNull();
        Assertions.assertThat(mtDTO.getMaterialType().getName()).isEqualTo(materialTypeName);
        Assertions.assertThat(mtDTO.getMaterialCode()).isEqualTo(materialTemplateCode);
        Assertions.assertThat(mtDTO.getMaterialType().getId()).isEqualTo(mtDTO.getMaterialTypeId());

    }

    @Test
    public void shall_removeMaterial_fromDB() throws ObjectCreateError {
        AddMaterialRequest addRequest = new AddMaterialRequest(materialTemplateId, SIZE, 2, AddMaterialRequest.CreationContext.MANUAL);
        MaterialDTO materialDto = materialService.addMaterial(addRequest);

        RemoveResponse removeResult = materialService.removeMaterial(new RemoveMaterialRequest(materialDto.getId(), true, false));
        MaterialDTO get = materialService.get(new BasicGetRequest(materialDto.getId()));
        Assertions.assertThat(get.getRemoved()).isTrue();
        
        RemoveResponse removeResult2= materialService.removeMaterial(new RemoveMaterialRequest(materialDto.getId(), true, false));
        MaterialDAOInterface dao = objBuilder.create(MaterialDAOInterface.class);
        MaterialEntity removed = dao.get(materialDto.getId());
        Assertions.assertThat(removed).isNull();
    }

    @Test
    public void shall_notRemoveMaterial_whenIsInMaterialDemand() {

        AddMaterialRequest addRequest = new AddMaterialRequest(materialTemplateId, SIZE, 2, AddMaterialRequest.CreationContext.MANUAL);
        MaterialDTO materialDto = materialService.addMaterial(addRequest);
        MaterialDemandEntity mdEntity = new MaterialDemandEntity();
        mdEntity.setUsedMaterialsIds(Lists.newArrayList(materialDto.getId()));
        materialDemandDAO.add(mdEntity);

        Assertions.assertThatThrownBy(() -> {
            materialService.removeMaterial(new RemoveMaterialRequest(materialDto.getId(), true, false));
        }).isInstanceOf(RemoveEntityException.class);
    }

    private MaterialTemplateDTO addMaterialTemplate(String name, String code) {
        MaterialTypeDTO materialType = addMaterialType(name);
        AddMaterialTemplateRequest request = new AddMaterialTemplateRequest();

        request.setMaterialCode(code);
        request.setMaterialTypeId(materialType.getId());
        request.setParticularDimension("0,3x0.45");
        MaterialTemplateDTO materialTemplate = materialTemplateService.addMaterialTemplate(request);
        return materialTemplate;
    }

    private MaterialTypeDTO addMaterialType(String name) {
        AddMaterialTypeRequest request = new AddMaterialTypeRequest();
        request.setName(name);
        request.setBaseDimensionTemplate(Dimension.AxB);
        request.setBaseDimensionUnit("m x m");
        request.setCuttingDimensionTemplate(Dimension.dł);
        request.setCuttingDimensionUnit("m");

        MaterialTypeDTO materialType = materialTypeService.addMaterialType(request);
        return materialType;
    }
}
