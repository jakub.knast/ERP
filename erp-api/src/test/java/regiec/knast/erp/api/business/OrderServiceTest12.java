/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.business;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ThrowableAssert;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import regiec.knast.erp.api.business.apitests.AllServicesInjector;
import regiec.knast.erp.api.dao.interfaces.OrderDAOInterface;
import regiec.knast.erp.api.dto.order.*;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.exceptions.ErpStateValidateionException;
import regiec.knast.erp.api.model.*;
import regiec.knast.erp.api.model.bases.RecipientBaseBrief;
import regiec.knast.erp.api.util.TestBinderBuilder;
import regiec.knast.erp.api.util.TestGuiceBuilder;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class OrderServiceTest12 extends AllServicesInjector {

    private static final boolean runWithFakeMongo = true;

    private static final String PRODUCT_NO_1 = "product_no_1";
    private static final String RECIPIENT_1_ID = "000000000000000000000000";
    private static final String RECIPIENT_1_NAME = "recipient_1_name";
    private MoneyERP money = MoneyERP.create(new BigDecimal(1.0), "PLN");
    private static MoneyERP price = MoneyERP.create(new BigDecimal(12.0), "PLN");
    private static final List<SalesOffer> SALE_OFFERS = Lists.newArrayList(new SalesOffer(new RecipientBaseBrief(RECIPIENT_1_NAME, RECIPIENT_1_ID), price, 0.0));
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private TestGuiceBuilder objBuilder;
    private OrderDAOInterface orderDAO;

    @Before
    public void setUp() throws ObjectCreateError {
        orderDAO = Mockito.mock(OrderDAOInterface.class);
        AbstractModule module = new TestBinderBuilder(runWithFakeMongo)
                .override(OrderDAOInterface.class, orderDAO)
                .build();
        objBuilder = new TestGuiceBuilder(module);
        ObjectBuilderFactory.configure(objBuilder);
        super.initServices();
    }

    @Test
    public void shall_notGenerateProductionOrdersIfOrderIsFramework() throws ObjectCreateError {

        // given
        final String orderId = "orderId123123";
        String orderJson = getJson();
        OrderEntity order = super.g.fromJson(orderJson, OrderEntity.class);
        Mockito.when(orderDAO.get(orderId)).thenReturn(order);
        Assertions.assertThatThrownBy(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                productionOrderService.generateProductionOrder(new GenerateProductionOrderRequest(orderId));
            }
        }).isInstanceOf(ErpStateValidateionException.class);

    }
    
    @Test
    public void shall_notGenerateDispositionIfOrderIsNotFramework() throws ObjectCreateError {

        // given
        final String orderId = "orderId123123";
        String orderJson = getJson();
        OrderEntity order = super.g.fromJson(orderJson, OrderEntity.class);
        order.setFrameworkContractOrder(false);
        Mockito.when(orderDAO.get(orderId)).thenReturn(order);
        Assertions.assertThatThrownBy(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                productionOrderService.generateProductionOrderDisposition(new GenerateProductionOrderDispositionRequest(orderId, 2, new Date()));
            }
        }).isInstanceOf(ErpStateValidateionException.class);

    }

    public String getJson() {
        return "{\n"
                + "  \"id\": \"5b8cc48546fbed08747e4eed\",\n"
                + "  \"recipient\": {\n"
                + "    \"id\": \"5975c9984b13ec08e472ddc3\",\n"
                + "    \"name\": \"PROMAG SA\",\n"
                + "    \"nip\": \"778-00-40-905\",\n"
                + "    \"street\": \"ul.Romana Maya 11\",\n"
                + "    \"postalCode\": \"61-371\",\n"
                + "    \"city\": \"Poznań\",\n"
                + "    \"email\": \"M.SIKORSKI@promag.pl\",\n"
                + "    \"phone\": \"+48 663 772 289\",\n"
                + "    \"daysForPayment\": 30,\n"
                + "    \"state\": \"NORMAL\",\n"
                + "    \"creationDate\": \"2017-07-24T10:19:04.857Z\",\n"
                + "    \"lastModificationDate\": \"2017-07-24T10:19:04.857Z\"\n"
                + "  },\n"
                + "  \"orderPositions\": [\n"
                + "    {\n"
                + "      \"product\": {\n"
                + "        \"id\": \"5b8cc4a446fbed08747e4eef\",\n"
                + "        \"productTemplate\": {\n"
                + "          \"id\": \"5b75c6176f3098228c0ea92d\",\n"
                + "          \"partCard\": {\n"
                + "            \"materialParts\": [\n"
                + "              {\n"
                + "                \"positionId\": \"5b75c6396f3098228c0ea92e\",\n"
                + "                \"materialTemplate\": {\n"
                + "                  \"id\": \"590c39eb71a24a08c8e3fd2c\",\n"
                + "                  \"materialType\": {\n"
                + "                    \"id\": \"58b73eb94c1926221c03a215\",\n"
                + "                    \"name\": \"Profile\",\n"
                + "                    \"cuttingDimensionTemplate\": \"dł\",\n"
                + "                    \"baseDimensionTemplate\": \"AxBxGr\",\n"
                + "                    \"cuttingDimensionUnit\": \"mb\",\n"
                + "                    \"baseDimensionUnit\": \"mm x mm x mm\",\n"
                + "                    \"state\": \"NORMAL\",\n"
                + "                    \"creationDate\": \"2017-05-16T20:03:34.363Z\",\n"
                + "                    \"lastModificationDate\": \"2017-05-25T09:09:05.105Z\"\n"
                + "                  },\n"
                + "                  \"materialCode\": \"10004753\",\n"
                + "                  \"materialKind\": {\n"
                + "                    \"materialKind\": \"stal\"\n"
                + "                  },\n"
                + "                  \"steelGrade\": {\n"
                + "                    \"steelGrade\": \"S355J2H\"\n"
                + "                  },\n"
                + "                  \"particularDimension\": \"110x110x4\",\n"
                + "                  \"unit\": \"mm\",\n"
                + "                  \"materialUsages\": [\n"
                + "                    \"numer: PG130127, nazwa: Kształtownik\",\n"
                + "                    \"numer: profilek, nazwa: profilek\",\n"
                + "                    \"numer: dupa, nazwa: dupa\"\n"
                + "                  ],\n"
                + "                  \"state\": \"NORMAL\",\n"
                + "                  \"creationDate\": \"2017-05-15T18:47:21.644Z\",\n"
                + "                  \"lastModificationDate\": \"2017-05-25T09:09:05.105Z\"\n"
                + "                },\n"
                + "                \"cuttedSectionSize\": \"2.9\",\n"
                + "                \"cuttedSectionSizeWithBorders\": \"3.0\",\n"
                + "                \"count\": 2\n"
                + "              },\n"
                + "              {\n"
                + "                \"positionId\": \"5b76f70d47a48624d89db470\",\n"
                + "                \"materialTemplate\": {\n"
                + "                  \"id\": \"590c39eb71a24a08c8e3fd2c\",\n"
                + "                  \"materialType\": {\n"
                + "                    \"id\": \"58b73eb94c1926221c03a215\",\n"
                + "                    \"name\": \"Profile\",\n"
                + "                    \"cuttingDimensionTemplate\": \"dł\",\n"
                + "                    \"baseDimensionTemplate\": \"AxBxGr\",\n"
                + "                    \"cuttingDimensionUnit\": \"mb\",\n"
                + "                    \"baseDimensionUnit\": \"mm x mm x mm\",\n"
                + "                    \"state\": \"NORMAL\",\n"
                + "                    \"creationDate\": \"2017-05-16T20:03:34.363Z\",\n"
                + "                    \"lastModificationDate\": \"2017-05-25T09:09:05.105Z\"\n"
                + "                  },\n"
                + "                  \"materialCode\": \"10004753\",\n"
                + "                  \"materialKind\": {\n"
                + "                    \"materialKind\": \"stal\"\n"
                + "                  },\n"
                + "                  \"steelGrade\": {\n"
                + "                    \"steelGrade\": \"S355J2H\"\n"
                + "                  },\n"
                + "                  \"particularDimension\": \"110x110x4\",\n"
                + "                  \"unit\": \"mm\",\n"
                + "                  \"materialUsages\": [\n"
                + "                    \"numer: PG130127, nazwa: Kształtownik\",\n"
                + "                    \"numer: profilek, nazwa: profilek\",\n"
                + "                    \"numer: dupa, nazwa: dupa\"\n"
                + "                  ],\n"
                + "                  \"state\": \"NORMAL\",\n"
                + "                  \"creationDate\": \"2017-05-15T18:47:21.644Z\",\n"
                + "                  \"lastModificationDate\": \"2017-05-25T09:09:05.105Z\"\n"
                + "                },\n"
                + "                \"cuttedSectionSize\": \"1.1\",\n"
                + "                \"cuttedSectionSizeWithBorders\": \"2\",\n"
                + "                \"count\": 1\n"
                + "              },\n"
                + "              {\n"
                + "                \"positionId\": \"5b76f71c47a48624d89db471\",\n"
                + "                \"materialTemplate\": {\n"
                + "                  \"id\": \"590c39eb71a24a08c8e3fd2c\",\n"
                + "                  \"materialType\": {\n"
                + "                    \"id\": \"58b73eb94c1926221c03a215\",\n"
                + "                    \"name\": \"Profile\",\n"
                + "                    \"cuttingDimensionTemplate\": \"dł\",\n"
                + "                    \"baseDimensionTemplate\": \"AxBxGr\",\n"
                + "                    \"cuttingDimensionUnit\": \"mb\",\n"
                + "                    \"baseDimensionUnit\": \"mm x mm x mm\",\n"
                + "                    \"state\": \"NORMAL\",\n"
                + "                    \"creationDate\": \"2017-05-16T20:03:34.363Z\",\n"
                + "                    \"lastModificationDate\": \"2017-05-25T09:09:05.105Z\"\n"
                + "                  },\n"
                + "                  \"materialCode\": \"10004753\",\n"
                + "                  \"materialKind\": {\n"
                + "                    \"materialKind\": \"stal\"\n"
                + "                  },\n"
                + "                  \"steelGrade\": {\n"
                + "                    \"steelGrade\": \"S355J2H\"\n"
                + "                  },\n"
                + "                  \"particularDimension\": \"110x110x4\",\n"
                + "                  \"unit\": \"mm\",\n"
                + "                  \"materialUsages\": [\n"
                + "                    \"numer: PG130127, nazwa: Kształtownik\",\n"
                + "                    \"numer: profilek, nazwa: profilek\",\n"
                + "                    \"numer: dupa, nazwa: dupa\"\n"
                + "                  ],\n"
                + "                  \"state\": \"NORMAL\",\n"
                + "                  \"creationDate\": \"2017-05-15T18:47:21.644Z\",\n"
                + "                  \"lastModificationDate\": \"2017-05-25T09:09:05.105Z\"\n"
                + "                },\n"
                + "                \"cuttedSectionSize\": \"2.9\",\n"
                + "                \"cuttedSectionSizeWithBorders\": \"3.4\",\n"
                + "                \"count\": 1\n"
                + "              }\n"
                + "            ],\n"
                + "            \"wareParts\": [],\n"
                + "            \"semiproductParts\": []\n"
                + "          },\n"
                + "          \"productionPlan\": [\n"
                + "            {\n"
                + "              \"positionId\": \"5b75c6876f3098228c0ea92f\",\n"
                + "              \"operationName\": {\n"
                + "                \"id\": \"5acef2a35bc1dcb3bf2b1612\",\n"
                + "                \"name\": \"Cięcie\",\n"
                + "                \"state\": \"NORMAL\"\n"
                + "              },\n"
                + "              \"professionGroup\": {\n"
                + "                \"id\": \"5acef2a45bc1dcb3bf2b1626\",\n"
                + "                \"allowOperations\": [\n"
                + "                  {\n"
                + "                    \"id\": \"5acef2a35bc1dcb3bf2b1612\",\n"
                + "                    \"name\": \"Cięcie\",\n"
                + "                    \"state\": \"NORMAL\"\n"
                + "                  }\n"
                + "                ],\n"
                + "                \"code\": \"01\",\n"
                + "                \"name\": \"Krajacz\",\n"
                + "                \"label\": \"01-Krajacz\"\n"
                + "              },\n"
                + "              \"transportTime\": 1.0,\n"
                + "              \"preparationTime\": 1.0,\n"
                + "              \"operationTime\": 1.0,\n"
                + "              \"operationNumber\": 10\n"
                + "            }\n"
                + "          ],\n"
                + "          \"validationStatus\": \"tak\",\n"
                + "          \"validationMessage\": [],\n"
                + "          \"ptUsages\": [],\n"
                + "          \"totalBuildingTime\": 2.0,\n"
                + "          \"buildingTime\": 2.0,\n"
                + "          \"buildingTimeTJ\": 1.0,\n"
                + "          \"buildingTimeTPZ\": 1.0,\n"
                + "          \"totalBuildingTimeTJ\": 1.0,\n"
                + "          \"totalBuildingTimeTPZ\": 1.0,\n"
                + "          \"buildingTimeStr\": \"2.0 (1.0 + 1.0)\",\n"
                + "          \"totalBuildingTimeStr\": \"2.0 (1.0 + 1.0)\",\n"
                + "          \"productNo\": \"profilek\",\n"
                + "          \"name\": \"profilek\",\n"
                + "          \"comments\": \"\\u003cp\\u003esdfsdf\\u003c/p\\u003e\",\n"
                + "          \"type\": \"SALE\",\n"
                + "          \"salesOffers\": [\n"
                + "            {\n"
                + "              \"recipient\": {\n"
                + "                \"name\": \"PROMAG SA\",\n"
                + "                \"id\": \"5975c9984b13ec08e472ddc3\"\n"
                + "              },\n"
                + "              \"price\": {\n"
                + "                \"amount\": 23,\n"
                + "                \"currency\": \"PLN\"\n"
                + "              },\n"
                + "              \"discount\": 10.0\n"
                + "            },\n"
                + "            {\n"
                + "              \"recipient\": {\n"
                + "                \"name\": \"HOMAG POLSKA  Sp. z o.o\",\n"
                + "                \"id\": \"5975cc934b13ec08e472ddc4\"\n"
                + "              },\n"
                + "              \"price\": {\n"
                + "                \"amount\": 100,\n"
                + "                \"currency\": \"PLN\"\n"
                + "              },\n"
                + "              \"discount\": 0.0\n"
                + "            }\n"
                + "          ],\n"
                + "          \"state\": \"NORMAL\",\n"
                + "          \"creationDate\": \"2018-08-16T18:44:39.090Z\",\n"
                + "          \"lastModificationDate\": \"2018-09-03T05:20:36.559Z\"\n"
                + "        },\n"
                + "        \"recipient\": {\n"
                + "          \"id\": \"5975c9984b13ec08e472ddc3\",\n"
                + "          \"name\": \"PROMAG SA\",\n"
                + "          \"nip\": \"778-00-40-905\",\n"
                + "          \"street\": \"ul.Romana Maya 11\",\n"
                + "          \"postalCode\": \"61-371\",\n"
                + "          \"city\": \"Poznań\",\n"
                + "          \"email\": \"M.SIKORSKI@promag.pl\",\n"
                + "          \"phone\": \"+48 663 772 289\",\n"
                + "          \"daysForPayment\": 30,\n"
                + "          \"state\": \"NORMAL\",\n"
                + "          \"creationDate\": \"2017-07-24T10:19:04.857Z\",\n"
                + "          \"lastModificationDate\": \"2017-07-24T10:19:04.857Z\"\n"
                + "        },\n"
                + "        \"count\": 16,\n"
                + "        \"recipientId\": \"5975c9984b13ec08e472ddc3\",\n"
                + "        \"orderNo\": \"ramowe2\",\n"
                + "        \"state\": \"ORDERED\",\n"
                + "        \"creationDate\": \"2018-09-03T05:20:36.559Z\",\n"
                + "        \"lastModificationDate\": \"2018-09-03T05:20:36.559Z\"\n"
                + "      },\n"
                + "      \"orderPositionId\": \"5b8cc4a446fbed08747e4eef\",\n"
                + "      \"positionOnOrder\": \"010\",\n"
                + "      \"count\": 16,\n"
                + "      \"piecePrice\": {\n"
                + "        \"amount\": 23,\n"
                + "        \"currency\": \"PLN\"\n"
                + "      },\n"
                + "      \"piecePriceWithDiscount\": {\n"
                + "        \"amount\": 20.7,\n"
                + "        \"currency\": \"PLN\"\n"
                + "      },\n"
                + "      \"discount\": 10.0,\n"
                + "      \"productionOrderId\": \"5b8cc4c346fbed08747e4ef0\",\n"
                + "      \"disposedCount\": 0,\n"
                + "      \"toDisposeCount\": 16,\n"
                + "      \"dispositionsId\": []\n"
                + "    }\n"
                + "  ],\n"
                + "  \"state\": \"NEW\",\n"
                + "  \"nr\": \"ramowe2\",\n"
                + "  \"comments\": \"\\u003cp\\u003eghjk\\u003c/p\\u003e\",\n"
                + "  \"frameworkContractOrder\": true,\n"
                + "  \"creationDate\": \"2018-09-03T05:20:05.668Z\",\n"
                + "  \"lastModificationDate\": \"2018-09-03T05:21:07.274Z\"\n"
                + "}";
    }
}
