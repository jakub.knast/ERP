/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import java.util.Map;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import org.jaxygen.converters.json.JSONBuilderRegistry;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import org.mockito.invocation.InvocationOnMock;
import regiec.knast.erp.api.ContextInitializer;
import regiec.knast.erp.api.GuiceBuilder;
import regiec.knast.erp.api.MyBinder;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDemandDAOInterface;
import regiec.knast.erp.api.dto.material.MaterialDTO;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandDTO;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.MaterialTypeEntity;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.manager.orders.DeliveryOrderPositionsManager;
import regiec.knast.erp.api.manager.satisfier.OneDimSatisfier;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.states.MaterialDemandState;
import regiec.knast.erp.api.model.states.MaterialState;
import regiec.knast.erp.api.util.TestBinderBuilder;
import regiec.knast.erp.api.util.TestGuiceBuilder;

/**
 *
 * @author fbrzuzka
 */
public class MaterialDemandSatisfierTest {

    private Gson g;
    private OneDimSatisfier oneDimSatisfier;
    private MaterialDAOInterface mMock;
    private MaterialDemandDAOInterface mdMock;
    private ProductionOrderManager pomMock;
    private MaterialTemplateEntity profilTemplate;
    private MaterialTypeEntity profilType;
    private ProductTemplateEntity detaltemplate;
    private OrderEntity order;
    private RecipientEntity recipient;
    private DeliveryOrderPositionsManager deliveryOrderPositionsManagerMock;

    @Before
    public void setUp() throws ObjectCreateError {
        g = JSONBuilderRegistry.getBuilder().build();

        mMock = mock(MaterialDAOInterface.class);
        mdMock = mock(MaterialDemandDAOInterface.class);
        pomMock = mock(ProductionOrderManager.class);
        deliveryOrderPositionsManagerMock = mock(DeliveryOrderPositionsManager.class);
        
        Mockito.when(mdMock.update(Matchers.any())).thenAnswer((InvocationOnMock invocation) -> {
            Object[] arguments = invocation.getArguments();
            MaterialDemandEntity entity = (MaterialDemandEntity)arguments[0];
            return entity;
        });

        profilTemplate = new MaterialTemplateEntity();
        profilType = new MaterialTypeEntity();
        profilType.setName("Profil");
        profilType.setBaseDimensionUnit("mm x mm x mm");
        profilType.setCuttingDimensionUnit("mb");
        profilTemplate.setId("jakiesTamId");
        profilTemplate.setMaterialType(profilType);
        profilTemplate.setMaterialCode("1234");
        profilTemplate.setParticularDimension("1x2x3");

        detaltemplate = new ProductTemplateEntity();
        detaltemplate.setId("111111111111111111111111");
        detaltemplate.setProductNo("12345678");
        detaltemplate.setName("Jakiś tam produkt");

        recipient = new RecipientEntity();
        recipient.setId("222222222222222222222222");
        recipient.setName("Scanclimber");

        order = new OrderEntity();
        order.setId("333333333333333333333333");
        order.setNr("numer zamówienia");
        order.setRecipient(recipient);

        AbstractModule module = new TestBinderBuilder(false)
                .override(MaterialDAOInterface.class, mMock)
                .override(MaterialDemandDAOInterface.class, mdMock)
                .override(ProductionOrderManager.class, pomMock)
                .override(DeliveryOrderPositionsManager.class, deliveryOrderPositionsManagerMock)
                .build();
        TestGuiceBuilder objBuilder = new TestGuiceBuilder(module);

        oneDimSatisfier = (OneDimSatisfier) objBuilder.create(OneDimSatisfier.class);
    }

    @BeforeClass
    public static void beforeClass() {
        ObjectBuilderFactory.configure(GuiceBuilder.getInstance());
        ContextInitializer.getInstance().contextInitialized();
        Map<Class, Class> map = MyBinder.BIND_MAP;
    }

    private MaterialEntity m(int count, double length, double reservedLength, double leftLength) {
        MaterialEntity m = new MaterialEntity();
        m.setCount(count);
        m.setLength(CuttingSizeManager.createOneDimCuttingSize(length));
        m.setReservedLength(CuttingSizeManager.createOneDimCuttingSize(reservedLength));
        m.setLeftLength(CuttingSizeManager.createOneDimCuttingSize(leftLength));
        m.setId("5a14884bc42bb9176cc861d6");
        m.setMaterialTemplate(profilTemplate);
        m.setState(MaterialState.ADDED_MANUALY);
        return m;
    }

    private MaterialDemandEntity md(int count, double length) {
        MaterialDemandEntity md = new MaterialDemandEntity();
        md.setCount(count);
        md.setCuttingSize(CuttingSizeManager.createOneDimCuttingSize(length));
        md.setId("5a14884bc42bb9176cc861d6");
        md.setMaterialTemplate(profilTemplate);
        md.setDetalTemplate(detaltemplate);
        md.setOrder(order);
        md.setProductionOrderId("aaa");
        return md;
    }

    private Map verifyResultObject(Object result) {
        assertThat(result).isInstanceOf(Map.class);
        Map res = (Map) result;
        Object mess = res.get("message");
        assertThat(mess).isNotNull();
        return res;
    }

    private void verifyMaterials(Object material, int count, double length, double reservedLength, double leftLength) {
        assertThat(material).isNotNull();
        assertThat(material).isInstanceOf(MaterialDTO.class);
        assertThat(material).hasFieldOrPropertyWithValue("count", count);
        MaterialDTO dto = (MaterialDTO) material;

        assertThat(dto.getLength()).hasFieldOrPropertyWithValue("cuttingDimension", Dimension.dł);
        assertThat(dto.getReservedLength()).hasFieldOrPropertyWithValue("cuttingDimension", Dimension.dł);
        assertThat(dto.getLeftLength()).hasFieldOrPropertyWithValue("cuttingDimension", Dimension.dł);

        assertThat(dto.getLength()).hasFieldOrPropertyWithValue("isOneDimensial", true);
        assertThat(dto.getReservedLength()).hasFieldOrPropertyWithValue("isOneDimensial", true);
        assertThat(dto.getLeftLength()).hasFieldOrPropertyWithValue("isOneDimensial", true);

        assertThat(dto.getLength()).hasFieldOrPropertyWithValue("length", length);
        assertThat(dto.getReservedLength()).hasFieldOrPropertyWithValue("length", reservedLength);
        assertThat(dto.getLeftLength()).hasFieldOrPropertyWithValue("length", leftLength);
    }

    private void verifyMaterialDemand(Object materialDemand, int count, double length, int satisfied, int unsatisfied, MaterialDemandState mds) {
        assertThat(materialDemand).isNotNull();
        assertThat(materialDemand).isInstanceOf(MaterialDemandDTO.class);
        assertThat(materialDemand).hasFieldOrPropertyWithValue("count", count);
        assertThat(materialDemand).hasFieldOrPropertyWithValue("satisfied", satisfied);
        assertThat(materialDemand).hasFieldOrPropertyWithValue("unsatisfied", unsatisfied);
        assertThat(materialDemand).hasFieldOrPropertyWithValue("state", mds);
        assertThat(materialDemand).hasFieldOrProperty("cuttingSize");
        MaterialDemandDTO md = (MaterialDemandDTO) materialDemand;
        assertThat(md.getCuttingSize()).hasFieldOrPropertyWithValue("origSize", String.valueOf(length));
        assertThat(md.getCuttingSize()).hasFieldOrPropertyWithValue("length", length);
        assertThat(md.getCuttingSize()).hasFieldOrPropertyWithValue("cuttingDimension", Dimension.dł);
        assertThat(md.getCuttingSize()).hasFieldOrPropertyWithValue("isOneDimensial", true);
        assertThat(md.getCuttingSize()).hasFieldOrPropertyWithValue("area", null);
    }

    @Test
    public void testSatisfyDemands1() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 6.0, 0.0, 6.0), md(1, 2.0));
        // then
        Map res = verifyResultObject(result);

        assertThat(res.get("retM_FU")).isNull();
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 2.0, 4.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 1, 2.0, 1, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands2() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 6.0, 0.0, 6.0), md(2, 2.0));
        // then
        Map res = verifyResultObject(result);

        assertThat(res.get("retM_FU")).isNull();
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 4.0, 2.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 2, 2.0, 2, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands3() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 6.0, 0.0, 6.0), md(3, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 6.0, 6.0, 0.0);
        assertThat(res.get("retM_PU")).isNull();
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 3, 2.0, 3, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands4() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 6.0, 1.0, 5.0), md(1, 2.0));
        // then
        Map res = verifyResultObject(result);

        assertThat(res.get("retM_FU")).isNull();
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 3.0, 3.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 1, 2.0, 1, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands5() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 6.0, 1.0, 5.0), md(2, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 6.0, 5.0, 1.0);
        assertThat(res.get("retM_PU")).isNull();
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 2, 2.0, 2, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands6() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 6.0, 0.0, 6.0), md(1, 2.0));
        // then
        Map res = verifyResultObject(result);

        assertThat(res.get("retM_FU")).isNull();
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 2.0, 4.0);
        verifyMaterials(res.get("retM_NU"), 1, 6.0, 0.0, 6.0);
        verifyMaterialDemand(res.get("retMd"), 1, 2.0, 1, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands7() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 6.0, 0.0, 6.0), md(2, 2.0));
        // then
        Map res = verifyResultObject(result);

        assertThat(res.get("retM_FU")).isNull();
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 4.0, 2.0);
        verifyMaterials(res.get("retM_NU"), 1, 6.0, 0.0, 6.0);
        verifyMaterialDemand(res.get("retMd"), 2, 2.0, 2, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands8() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 6.0, 0.0, 6.0), md(3, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 6.0, 6.0, 0.0);
        assertThat(res.get("retM_PU")).isNull();
        verifyMaterials(res.get("retM_NU"), 1, 6.0, 0.0, 6.0);
        verifyMaterialDemand(res.get("retMd"), 3, 2.0, 3, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands9() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 6.0, 0.0, 6.0), md(4, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 6.0, 6.0, 0.0);
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 2.0, 4.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 4, 2.0, 4, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands10() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 6.0, 0.0, 6.0), md(5, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 6.0, 6.0, 0.0);
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 4.0, 2.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 5, 2.0, 5, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands11() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 6.0, 0.0, 6.0), md(6, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 2, 6.0, 6.0, 0.0);
        assertThat(res.get("retM_PU")).isNull();
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 6, 2.0, 6, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands12() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(3, 6.0, 0.0, 6.0), md(7, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 2, 6.0, 6.0, 0.0);
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 2.0, 4.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 7, 2.0, 7, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands13() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(5, 6.0, 0.0, 6.0), md(7, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 2, 6.0, 6.0, 0.0);
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 2.0, 4.0);
        verifyMaterials(res.get("retM_NU"), 2, 6.0, 0.0, 6.0);
        verifyMaterialDemand(res.get("retMd"), 7, 2.0, 7, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands14() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 6.0, 2.0, 4.0), md(2, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 6.0, 6.0, 0.0);
        assertThat(res.get("retM_PU")).isNull();
        verifyMaterials(res.get("retM_NU"), 1, 6.0, 2.0, 4.0);
        verifyMaterialDemand(res.get("retMd"), 2, 2.0, 2, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands15() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(3, 6.0, 2.0, 4.0), md(4, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 2, 6.0, 6.0, 0.0);
        assertThat(res.get("retM_PU")).isNull();
        verifyMaterials(res.get("retM_NU"), 1, 6.0, 2.0, 4.0);
        verifyMaterialDemand(res.get("retMd"), 4, 2.0, 4, 0, MaterialDemandState.SATISFIED);
    }

    //------------------ 2X tests----------------//
    @Test
    public void testSatisfyDemands21() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 5.0, 0.0, 5.0), md(1, 2.0));
        // then
        Map res = verifyResultObject(result);

        assertThat(res.get("retM_FU")).isNull();
        verifyMaterials(res.get("retM_PU"), 1, 5.0, 2.0, 3.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 1, 2.0, 1, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands22() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 5.0, 0.0, 5.0), md(2, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 5.0, 4.0, 1.0);
        assertThat(res.get("retM_PU")).isNull();
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 2, 2.0, 2, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands23() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 5.0, 0.0, 5.0), md(1, 2.0));
        // then
        Map res = verifyResultObject(result);

        assertThat(res.get("retM_FU")).isNull();
        verifyMaterials(res.get("retM_PU"), 1, 5.0, 2.0, 3.0);
        verifyMaterials(res.get("retM_NU"), 1, 5.0, 0.0, 5.0);
        verifyMaterialDemand(res.get("retMd"), 1, 2.0, 1, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands24() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 5.0, 0.0, 5.0), md(2, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 5.0, 4.0, 1.0);
        assertThat(res.get("retM_PU")).isNull();
        verifyMaterials(res.get("retM_NU"), 1, 5.0, 0.0, 5.0);
        verifyMaterialDemand(res.get("retMd"), 2, 2.0, 2, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands25() {
        // given
        // when

        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 5.0, 0.0, 5.0), md(3, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 5.0, 4.0, 1.0);
        verifyMaterials(res.get("retM_PU"), 1, 5.0, 2.0, 3.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 3, 2.0, 3, 0, MaterialDemandState.SATISFIED);
    }

    //-------------------------------------------------------------
    //------------------------ 3X ---------------------------------
    //------------------------------------------------------------
    @Test
    public void testSatisfyDemands31() {
        // given
        // when

        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 4.0, 0.0, 4.0), md(2, 4.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 4.0, 4.0, 0.0);
        assertThat(res.get("retM_PU")).isNull();
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 2, 4.0, 1, 1, MaterialDemandState.PARTLY_SATISFIED);
    }

    @Test
    public void testSatisfyDemands32() {
        // given
        // when

        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 4.0, 0.0, 4.0), md(2, 4.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 2, 4.0, 4.0, 0.0);
        assertThat(res.get("retM_PU")).isNull();
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 2, 4.0, 2, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands33() {
        // given
        // when        
        Throwable thrown = catchThrowable(() -> {
            oneDimSatisfier.safisfyOneDinemsial(m(1, 3.0, 0.0, 3.0), md(1, 4.0));
        });
        assertThat(thrown).hasMessageContaining("Material is too short fo demand. Material.leftLength'3.0' MaterialDemand.length'4.0'");
        // then

    }

    @Test
    public void testSatisfyDemands34() {
        // given
        // when

        Object result = oneDimSatisfier.safisfyOneDinemsial(m(2, 4.0, 0.0, 4.0), md(3, 2.0));
        // then
        Map res = verifyResultObject(result);

        verifyMaterials(res.get("retM_FU"), 1, 4.0, 4.0, 0.0);
        verifyMaterials(res.get("retM_PU"), 1, 4.0, 2.0, 2.0);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 3, 2.0, 3, 0, MaterialDemandState.SATISFIED);
    }

    @Test
    public void testSatisfyDemands35() {
        // given
        // when        
        Throwable thrown = catchThrowable(() -> {
            oneDimSatisfier.safisfyOneDinemsial(m(1, 5.0, 2.0, 3.0), md(2, 4.0));
        });
        assertThat(thrown).hasMessageContaining("Material is too short fo demand. Material.leftLength'3.0' MaterialDemand.length'4.0'");
        // then
    }

    @Test
    public void testSatisfyDemands36() {
        // given
        // when        
        Throwable thrown = catchThrowable(() -> {
            oneDimSatisfier.safisfyOneDinemsial(m(2, 5.0, 2.0, 3.0), md(2, 4.0));
        });
        assertThat(thrown).hasMessageContaining("Material is too short fo demand. Material.leftLength'3.0' MaterialDemand.length'4.0'");
        // then
    }

    @Test
    public void testSatisfyDemands37() {
        // given
        // when        
        Throwable thrown = catchThrowable(() -> {
            oneDimSatisfier.safisfyOneDinemsial(m(2, 5.0, 2.0, 3.0), md(1, 4.0));
        });
        assertThat(thrown).hasMessageContaining("Material is too short fo demand. Material.leftLength'3.0' MaterialDemand.length'4.0'");
        // then
    }

    //-------------------------------------------------------------
    //------------------------ 4X ---------------------------------
    //------------------------------------------------------------
    @Test
    public void testSatisfyDemands41() {
        // given
        // when
        Object result = oneDimSatisfier.safisfyOneDinemsial(m(1, 6.0, 0.0, 6.0), md(1, 1.5));
        // then
        Map res = verifyResultObject(result);

        assertThat(res.get("retM_FU")).isNull();
        verifyMaterials(res.get("retM_PU"), 1, 6.0, 1.5, 4.5);
        assertThat(res.get("retM_NU")).isNull();
        verifyMaterialDemand(res.get("retMd"), 1, 1.5, 1, 0, MaterialDemandState.SATISFIED);
    }

}
