/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.utils.jung.layouts;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import java.util.Deque;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class LongestPathLayoutTest {
    
    public LongestPathLayoutTest() {
    }
    
    @Before
    public void setUp() {
    }

    @Test
    public void testSomeMethod() {
        int edgeNo =0;
        Graph<Integer, Integer> g = new SparseGraph();
        g.addEdge(edgeNo++, 1, 2, EdgeType.DIRECTED);
        g.addEdge(edgeNo++, 2, 3, EdgeType.DIRECTED);
        g.addEdge(edgeNo++, 3, 4, EdgeType.DIRECTED);
        g.addEdge(edgeNo++, 4, 2, EdgeType.DIRECTED);
        g.addEdge(edgeNo++, 4, 5, EdgeType.DIRECTED);
        g.addEdge(edgeNo++, 5, 6, EdgeType.DIRECTED);
        
        LongestPathLayout<Integer, Integer> layout = new LongestPathLayout(g, 1);
        Deque<Integer> longestPathLength = layout.getLongestPathLength(1);
        System.out.println("lon: " + longestPathLength);        
    }
    
}
