/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.utils;

import com.google.inject.AbstractModule;
import java.lang.reflect.Field;
import java.util.List;
import java.util.function.Predicate;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.business.apitests.GuiceInjector;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.util.TestBinderBuilder;
import regiec.knast.erp.api.util.TestGuiceBuilder;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ReflectionUtilTest extends GuiceInjector {

    @Before
    public void setUp() throws ObjectCreateError {
        AbstractModule module = new TestBinderBuilder(false)
                .build();
        TestGuiceBuilder objBuilder = new TestGuiceBuilder(module);
        ObjectBuilderFactory.configure(objBuilder);
//        super.initServices(); 
    }

    @Test
    public void test_recursiveWithPredicate() {
        Class clazz = Foo.class;
        Predicate<? super Field> namePredicate = (f -> f.getName().contains("Id"));
        List<ReflectionUtil.FieldInfo> result = ReflectionUtil.getRecursiveFieldsByPredicate(clazz, namePredicate);
        printFieldInfoList(result, clazz);
    }

    @Test
    public void test_recursiveWithPredicate_MaterialEntity() {
        Class clazz = MaterialEntity.class;
        Predicate<? super Field> namePredicate = (f -> f.getName().contains("Id"));
        List<ReflectionUtil.FieldInfo> result = ReflectionUtil.getRecursiveFieldsByPredicate(clazz, namePredicate);
        printFieldInfoList(result, clazz);
    }

    private void printFieldInfoList(List<ReflectionUtil.FieldInfo> result, Class clazz) {
        if (!result.isEmpty()) {
            System.out.println("\n---- for: " + clazz.getSimpleName());
            for (ReflectionUtil.FieldInfo fieldInfo : result) {
                System.out.println("field: " + fieldInfo.fieldPath());
            }
        }
    }

}
