/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.inject.AbstractModule;
import org.assertj.core.api.Assertions;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.business.MaterialTypeService;
import regiec.knast.erp.api.dao.interfaces.MaterialTypeDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.materialtypes.AddMaterialTypeRequest;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypeDTO;
import regiec.knast.erp.api.entities.MaterialTypeEntity;
import regiec.knast.erp.api.util.TestBinderBuilder;
import regiec.knast.erp.api.util.TestGuiceBuilder;

/**
 *
 * @author fbrzuzka
 */
public class FongoTest {

    private TestGuiceBuilder objBuilder;

    @Before
    public void setUp() {
        AbstractModule module = new TestBinderBuilder(true)
                .build();
        objBuilder = new TestGuiceBuilder(module);
        ObjectBuilderFactory.configure(objBuilder);
    }

    @Test
    public void shall_createMaterialTemplate() throws ObjectCreateError {
        // given
        String givenName = "testMaterialType";
        MaterialTypeService materialTypeService = objBuilder.create(MaterialTypeService.class);
        AddMaterialTypeRequest request = new AddMaterialTypeRequest();
        request.setName(givenName);

        // when
        MaterialTypeDTO materialTypeDTO = materialTypeService.addMaterialType(request);
        MaterialTypeDTO returned = materialTypeService.getMaterialType(new BasicGetRequest(materialTypeDTO.getId()));

        // then
        Assertions.assertThat(materialTypeDTO).isNotNull();
        Assertions.assertThat(returned).isNotNull();
        Assertions.assertThat(materialTypeDTO.getName()).isEqualTo(givenName);
        Assertions.assertThat(materialTypeDTO).isEqualToComparingFieldByFieldRecursively(returned);
    }
    

    @Test
    public void shall_updateOneField() throws ObjectCreateError {
        // given
        String mtId = "111122223333444455556666";
        String givenName = "testMaterialType";
        String newName = "nowe";
        MaterialTypeDAOInterface materialTypeDao = objBuilder.create(MaterialTypeDAOInterface.class);
        MaterialTypeEntity mt = new MaterialTypeEntity();
        mt.setName(givenName);
        
        
        // when
        materialTypeDao.add(mt, mtId);
        materialTypeDao.updateOneField(mtId, "name", newName);
        
        MaterialTypeEntity returned = materialTypeDao.get(mtId);

        // then
        Assertions.assertThat(returned.getName()).isEqualTo(newName);
    }

}
