package regiec.knast.erp.api.util;

import com.google.inject.AbstractModule;
import java.util.HashMap;
import java.util.Map;
import regiec.knast.erp.api.MyBinder;
import regiec.knast.erp.api.dao.driver.MongoInterface;

/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
/**
 *
 * @author fbrzuzka
 */
public class TestBinderBuilder {

    Map<Class, Class> map = new HashMap();
    Map<Class, Object> mapMock = new HashMap();

    public TestBinderBuilder(boolean useFongo) {
        map.putAll(MyBinder.BIND_MAP);
        if (useFongo) {
//            map.remove(MongoInterface.class);
            map.put(MongoInterface.class, FongoDriver.class);
//            Fongo fongo = new Fongo("fongo_test_server");
        }
    }

    public TestBinderBuilder override(Class clazz, Object mock) {
        mapMock.put(clazz, mock);
        if (map.containsKey(clazz)) {
            map.remove(clazz);
        }
        return this;
    }

    public AbstractModule build() {

        TestBinder binder = new TestBinder();
        return binder;
    }

    class TestBinder extends AbstractModule {

        @Override
        protected void configure() {
            for (Map.Entry<Class, Class> e : map.entrySet()) {
                bind(e.getKey()).to(e.getValue());
            }
            for (Map.Entry<Class, Object> e : mapMock.entrySet()) {
                bind(e.getKey()).toInstance(e.getValue());
            }
        }
    }
}
