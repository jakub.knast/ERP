/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.util;

import com.github.fakemongo.Fongo;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import regiec.knast.erp.api.dao.driver.MongoDriver;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.model.MoneyERP;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
class FongoDriver implements MongoInterface {

    private final Datastore datastore;
    private final MongoClient mongoClient;
    private Morphia morphia;
    private GridFS gridFS;

    @Inject
    private FongoDriver() {
        Fongo fongo = new Fongo("fongo_test_server");
        mongoClient = fongo.getMongo();
//            mongoClient = new MongoClient("localhost", 27017);
        morphia = new Morphia();
        morphia.mapPackage("regiec.knast.erp.api.entities", true);
        datastore = morphia.createDatastore(mongoClient, "erp");
        morphia.getMapper().getOptions().setStoreEmpties(true);
        morphia.getMapper().getConverters().addConverter(new MongoDriver.MoneyConverter(MoneyERP.class));
        gridFS = new GridFS(datastore.getDB());
    }

    @Override
    public Datastore getDatastore() {
        return datastore;
    }

    @Override
    public void closeConnections() {
        mongoClient.close();
    }

    @Override
    public GridFS getGridFS() {
        return gridFS;
    }
}
