/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.entities.containers;

import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Test;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.utils.ErpPrinter;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductEntitiesListTest {
    
    @Test
    public void test_mergeProducts() {
    
        ProductEntity e11 = p("1111", "ord1", "prodNo1", "010", 1);  
        ProductEntity e12 = p("2222", "ord1", "prodNo1", "010", 2);  
        ProductEntity e13 = p("3333", "ord1", "prodNo1", "010", 3);  
        
        ProductEntity e21 = p("4444", "ord1", "prodNo1", "020", 1);  
        ProductEntity e22 = p("5555", "ord1", "prodNo1", "020", 2);  
        
        ProductEntity e31 = p("6666", "ord1", "prodNo2", "010", 1);  
        ProductEntity e41 = p("7777", "ord2", "prodNo2", "010", 10);                 
        ProductEntitiesList given = new ProductEntitiesList(Lists.newArrayList(e11, e12, e13, e21, e22, e31, e41));
        
        ProductEntity r1 = p("1111-2222-3333", "ord1", "prodNo1", "010", 6);  
        ProductEntity r2 = p("4444-5555", "ord1", "prodNo1", "020", 3);  
        ProductEntity r3 = p("6666", "ord1", "prodNo2", "010", 1);  
        ProductEntity r4 = p("7777", "ord2", "prodNo2", "010", 10);          
        ProductEntitiesList expected = new ProductEntitiesList(Lists.newArrayList(r1, r2, r3 ,r4));
        
        ProductEntitiesList result = given.mergeProducts();
        
        ErpPrinter.printObject(result, "result");
        ErpPrinter.printObject(expected, "expected");
        
        Assertions.assertThat(result).hasSize(4);
        Assertions.assertThat(result).usingFieldByFieldElementComparator().containsOnlyElementsOf(expected);
        
        
    }
    
    private ProductEntity p(String id, String ordNo, String prodNo, String pos, int count) {
        ProductEntity product = new ProductEntity();
        product.setId(id);
        product.setOrderNo(ordNo);
        product.setProductionOrderNo(prodNo);
        product.setPositionOnOrder(pos);
        product.setCount(count);      
        return product;
    }
    
}
