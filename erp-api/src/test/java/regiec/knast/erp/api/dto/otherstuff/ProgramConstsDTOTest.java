/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.dto.otherstuff;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.dto.EntityStatesDto;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProgramConstsDTOTest {
    
    public ProgramConstsDTOTest() {
    }
    
    @Before
    public void setUp() {
    }

    @Test
    public void shall_createEntitiesStateMap() {
//        ContextInitializer.getInstance().contextInitialized();
        ProgramConstsDTO dto = new ProgramConstsDTO();
        Map<String, EntityStatesDto> createEntitiesStateMap = dto.createEntitiesStateMap();
        Gson g = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(g.toJson(createEntitiesStateMap));
//        ErpPrinter.printObject(createEntitiesStateMap, "map");
    }
    
}
