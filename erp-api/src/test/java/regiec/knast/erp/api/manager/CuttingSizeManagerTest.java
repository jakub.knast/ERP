/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.Dimension;

/**
 *
 * @author fbrzuzka
 */
public class CuttingSizeManagerTest {
    
    @Before
    public void setUp() {
        
    }    

    @Test
    public void testCreateCuttingSize() {
        // given
        Dimension d = Dimension.dł;
        String size = "1,23";
        CuttingSize expected = new CuttingSize();
        expected.setCuttingDimension(d);
        expected.setOrigSize(size);
        expected.setIsOneDimensial(Boolean.TRUE);
        expected.setLength(1.23);
        
        // when
        CuttingSize result = CuttingSizeManager.createCuttingSize(d, size);
        
        // then
        Assertions.assertThat(result).isEqualToComparingFieldByFieldRecursively(expected);
    }

    
    @Test
    public void testParseLength() {
        
        Assertions.assertThat(CuttingSizeManager.parseLength("1,23")).isEqualTo(1.23);
        Assertions.assertThat(CuttingSizeManager.parseLength("1,33")).isEqualTo(1.33);
        Assertions.assertThat(CuttingSizeManager.parseLength("121,23")).isEqualTo(121.23);
        Assertions.assertThat(CuttingSizeManager.parseLength("0,0")).isEqualTo(0.0);
        Assertions.assertThat(CuttingSizeManager.parseLength("0")).isEqualTo(0.0);
        Assertions.assertThat(CuttingSizeManager.parseLength("12312")).isEqualTo(12312);
        Assertions.assertThat(CuttingSizeManager.parseLength("0,1231")).isEqualTo(0.1231);
        
        Assertions.assertThat(CuttingSizeManager.parseLength("1.23")).isEqualTo(1.23);
        Assertions.assertThat(CuttingSizeManager.parseLength("1.33")).isEqualTo(1.33);
        Assertions.assertThat(CuttingSizeManager.parseLength("121.23")).isEqualTo(121.23);
        Assertions.assertThat(CuttingSizeManager.parseLength("0.0")).isEqualTo(0.0);
        Assertions.assertThat(CuttingSizeManager.parseLength("0")).isEqualTo(0.0);
        Assertions.assertThat(CuttingSizeManager.parseLength("12312")).isEqualTo(12312);
        Assertions.assertThat(CuttingSizeManager.parseLength("0.1231")).isEqualTo(0.1231);        
    }
    
    @Test
    public void testParseLengthExceptions() {
            
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength("1,,23");})).hasMessageContaining("Length '1,,23' not match pattern");
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength(",23");})).hasMessageContaining("Length ',23' not match pattern");
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength("1;23");})).hasMessageContaining("Length '1;23' not match pattern");
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength("1,23,2");})).hasMessageContaining("Length '1,23,2' not match pattern");
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength("1x23");})).hasMessageContaining("Length '1x23' not match pattern");
        
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength("1..23");})).hasMessageContaining("Length '1..23' not match pattern");
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength(".23");})).hasMessageContaining("Length '.23' not match pattern");
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength("1;23");})).hasMessageContaining("Length '1;23' not match pattern");
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength("1.23.2");})).hasMessageContaining("Length '1.23.2' not match pattern");
        assertThat(catchThrowable(() -> { CuttingSizeManager.parseLength("1x23");})).hasMessageContaining("Length '1x23' not match pattern");
    }
    
    @Test
    public void testValidatorOneDim() {        
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "1")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "1.1")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "1,4")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "12,4")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "01.40123")).isTrue();
        
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "0")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "0.0")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "0,0")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "01.40.123")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "01.s23")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "01x23")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.A, "")).isFalse();   
    }
    
    @Test
    public void testValidatorTwoDim() {        
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "1x1")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "1.1x1")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "1,4x132")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "12,4x0,012")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "01.40123x43")).isTrue();
        
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "0x0")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "0x1")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "0.0x1")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "0,0x1")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "01.40.1x23")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "01.x23")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "01x2x3")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxB, "")).isFalse();
    }
    
    @Test
    public void testValidatorTwoIdenticalDim() {        
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "1x1")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "1.1x1.1")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "1,4x1.4")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "2.2x2,2")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "0,2x0.2")).isTrue();
        
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "0x0")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "0x1")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "0.0x1")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "0,0x1")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "1.1x11")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "01.x23")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "01x2x3")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxA, "")).isFalse();
    }
    
    @Test
    public void testValidatorThreeDim() {        
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "1x1x2")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "1.1x1x0,1")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "1,4x132x1.02")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "12,4x0,012x12")).isTrue();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "01.40123x43x12,0")).isTrue();
        
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "0")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "0x0")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "0x0x0")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "0x1x2")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "0.0x1x2.2")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "0,0x1x2")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "01.40.1x23x2")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "01.x23x2")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "01x2x3x2x2")).isFalse();
        Assertions.assertThat(CuttingSizeManager.isValid(Dimension.AxBxC, "")).isFalse();
    }
}
