/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.SellerEntity;
import regiec.knast.erp.api.entities.containers.SellerEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface SellerDAOInterface extends DAOBaseInterface<SellerEntity, SellerEntitiesList> {

    SellerEntity get(String id);

    SellerEntitiesList list(PaginableFilterWithCriteriaRequest paginableFilterWithCriteria);
}
