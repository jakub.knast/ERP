/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.business.apitests;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.assertj.core.api.Assertions;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.materialDemand.*;
import regiec.knast.erp.api.dto.order.*;
import regiec.knast.erp.api.dto.orderedproducts.UpdateOrderPosition;
import regiec.knast.erp.api.dto.productionorder.ProductionOrdersListDTO;
import regiec.knast.erp.api.dto.producttemplates.*;
import regiec.knast.erp.api.dto.professionGroup.*;
import regiec.knast.erp.api.model.*;
import regiec.knast.erp.api.model.states.*;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class PrepareMyDemand extends AllServicesInjector {
    private static MoneyERP money = MoneyERP.create(new BigDecimal(1.0), "PLN");

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(PrepareMyDemand.class);
        PrepareMyDemand mongoTest = (PrepareMyDemand) create;
        try {
            mongoTest.initServices();
            mongoTest.test_addOrderWithPositions();
        } catch (Exception ex) {
            Logger.getLogger(ListTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    public void test_addOrderWithPositions() {

        String operationNameName = "ciecie";
        String professionGroupName = "prof1";
        String professionGroupCode = "01";
        String ptName = "profilek produkt";
        String materialTypeName = "Profile";
        String materialTemplateCode = "material1";
        String recipientName = "recipient1";
        String orderNo = "order-" + (int)((System.currentTimeMillis() - 1535034803822l)/1000);

        String productTemplateId = null;
        String materialTypetId = null;
        String materialTemplateId = null;
        String materialId = null;
        String recipientId = null;
        String orderId = null;
        String productionOrderId = null;
        String materialDemandId = null;
        String operationNameId = null;
        String professionGroupId = null;
        String profilekProductTemplateId = "5b75c6176f3098228c0ea92d";

        // add operation name
//        OperationNameDTO operation = addOperationName(operationNameName);
//        operationNameId = operationNameService.listOperationNames(new PaginableFilterWithCriteriaRequest()).getElements().get(0).getId();
        // add profession group
        ProfessionGroupDTO professionGroup = professionGroupService.listProfessionGroups(new PaginableFilterWithCriteriaRequest()).getElements().get(0);
        professionGroupId = professionGroup.getId();
        operationNameId = professionGroup.getAllowOperations().get(0).getId();

        // add product template
        productTemplateId = profilekProductTemplateId;
        ProductTemplateDTO ptDTO = productTemplateService.getProductTemplate(new BasicGetRequest(profilekProductTemplateId));

        recipientId = "5975c9984b13ec08e472ddc3"; // promag

        // add empty order
        OrderDTO addOrder = orderService.addOrder(new AddOrderRequest(recipientId, orderNo, "comments", new Date(), false));
        orderId = addOrder.getId();
        //validate order exists
        OrderDTO order = orderService.getOrder(new BasicGetRequest(orderId));
        Assertions.assertThat(order).isNotNull();
        Assertions.assertThat(order.getState()).isEqualTo(OrderState.NEW);
        Assertions.assertThat(order.getNr()).isEqualTo(orderNo);

        // add product1 as position to order1
        UpdateOrderRequest updateOrderRequest = BeanUtilSilent.translateBeanAndReturn(order, new UpdateOrderRequest());
        updateOrderRequest.getOrderPositions().add(
                new UpdateOrderPosition(productTemplateId, null, "010", 1, money, money, 0.0, "comments", new Date(), null, 0, 0, new ArrayList()));
        
        OrderDTO updateOrder = orderService.updateOrder(updateOrderRequest);
        //validate order has position        
        OrderDTO order2 = orderService.getOrder(new BasicGetRequest(orderId));
        Assertions.assertThat(order2).isNotNull();
        Assertions.assertThat(order2.getState()).isEqualTo(OrderState.NEW);
        Assertions.assertThat(order2.getNr()).isEqualTo(orderNo);
        Assertions.assertThat(order2.getOrderPositions()).hasSize(1);
        Assertions.assertThat(order2.getOrderPositions().get(0).getProductTemplate().getId()).isEqualTo(productTemplateId);

        // generate production orders
        GenerateProductionOrderResponse generateProductionOrder = productionOrderService.generateProductionOrder(new GenerateProductionOrderRequest(orderId));
        //validate
        ProductionOrdersListDTO listProductionOrders = productionOrderService.listProductionOrders(
                PaginableFilterWithCriteriaRequest.builder()
                        .addCriteriaFilterBuilder().addConstraint("orderId", orderId, ConstraintType.EQUALS).buildCriteriaFilter()
                        .build());
        Assertions.assertThat(listProductionOrders).isNotNull();
        Assertions.assertThat(listProductionOrders.getElements()).isNotNull();
        Assertions.assertThat(listProductionOrders.getElements()).hasSize(1);
        Assertions.assertThat(listProductionOrders.getElements().get(0).getOrderId()).isEqualTo(orderId);
        productionOrderId = listProductionOrders.getElements().get(0).getId();

        Object generateMaterialDemands = materialDemandService.generateMaterialDemands(new GenerateMaterialDemandRequest(productionOrderId));
        // validate generated demand
        MaterialDemandsListDTO listMaterialDemands = materialDemandService.listMaterialDemands(
                PaginableFilterWithCriteriaRequest.builder()
                        .addCriteriaFilterBuilder().addConstraint("order.id", orderId, ConstraintType.EQUALS).buildCriteriaFilter().build());
        Assertions.assertThat(listMaterialDemands).isNotNull();
        Assertions.assertThat(listMaterialDemands.getElements()).isNotNull();
//        Assertions.assertThat(listMaterialDemands.getElements()).hasSize(1);
//        Assertions.assertThat(listMaterialDemands.getElements().get(0).getProductionOrderId()).isEqualTo(productionOrderId);
//        Assertions.assertThat(listMaterialDemands.getElements().get(0).getMaterialTemplate().getId()).isEqualTo(materialTemplateId);
//        Assertions.assertThat(listMaterialDemands.getElements().get(0).getOrder().get("id")).isEqualTo(orderId);
//        Assertions.assertThat(listMaterialDemands.getElements().get(0).getRecipient().get("id")).isEqualTo(recipientId);
//        materialDemandId = listMaterialDemands.getElements().get(0).getId();

        // add material to stock
//        CuttingSize length = CuttingSizeManager.createOneDimCuttingSize(6.0);
//        MaterialDTO addMaterial = materialService.addMaterial(
//                new AddMaterialRequest(materialTemplateId, length, 3, null, null, null, null, null, null, null, null));
//        materialId = addMaterial.getId();
//        // validate material template exists
//        MaterialDTO material = materialService.getMaterial(new GetMaterialRequest(addMaterial.getId()));
//        Assertions.assertThat(material).isNotNull();
//        Assertions.assertThat(material.getMaterialTemplate().getId()).isEqualTo(materialTemplateId);
//        Assertions.assertThat(material.getMaterialTemplate().getMaterialCode()).isEqualTo(materialTemplateCode);
//        Assertions.assertThat(material.getMaterialTemplate().getMaterialType().getId()).isEqualTo(materialTypetId);
//        Assertions.assertThat(material.getMaterialTemplate().getMaterialType().getName()).isEqualTo(materialTypeName);

//        // satisfy material demand
//        MaterialDemandDTO satisfyDemands = materialDemandService.satisfyDemands(new SatisfyDemandsRequest(materialDemandId, materialId));
//        MaterialDemandDTO materialDemand2 = materialDemandService.getMaterialDemand(new GetMaterialDemandRequest(materialDemandId));
//        Assertions.assertThat(satisfyDemands).isNotNull();
//        Assertions.assertThat(materialDemand2).isNotNull();
//        Assertions.assertThat(satisfyDemands).isEqualToComparingFieldByFieldRecursively(materialDemand2);
//        //validate demand are satisfied
//        Assertions.assertThat(materialDemand2.getState()).isEqualTo(MaterialDemandState.SATISFIED);
//        Assertions.assertThat(materialDemand2.getSatisfied()).isEqualTo(2);
//        Assertions.assertThat(materialDemand2.getUnsatisfied()).isEqualTo(0);
//        Assertions.assertThat(materialDemand2.getCount()).isEqualTo(2);
//
//        // validate there is two materials
//        MaterialListDTO listMaterials = materialService.listMaterials(PaginableFilterWithCriteriaRequest.builder().build());
//        Assertions.assertThat(listMaterials).isNotNull();
//        Assertions.assertThat(listMaterials.getSize()).isEqualTo(2);
//        Assertions.assertThat(listMaterials.getElements().size()).isEqualTo(2);
//        Assertions.assertThat(listMaterials.getElements().get(0)).isNotNull();
//        Assertions.assertThat(listMaterials.getElements().get(1)).isNotNull();
//        MaterialDTO e1 = listMaterials.getElements().get(0);
//        MaterialDTO e2 = listMaterials.getElements().get(1);
//        Assertions.assertThat(e1.getMaterialTemplateId()).isEqualTo(e2.getMaterialTemplateId());
//        printObject("e1", e1);
//        printObject("e2", e2);

//        materialService.getMaterial(new GetMaterialRequest(material))
    }
}
