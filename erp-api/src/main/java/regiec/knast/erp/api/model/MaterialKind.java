/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.EqualsAndHashCode
@lombok.NoArgsConstructor
public class MaterialKind implements Comparable<MaterialKind> {

    private String materialKind;

    public MaterialKind(String materialKind) {
        this.materialKind = materialKind;
    }

    @Override
    public String toString() {
        return materialKind;
    }

    @Override
    public int compareTo(MaterialKind o) {
        return this.getMaterialKind().compareTo(o.getMaterialKind());
    }
}
