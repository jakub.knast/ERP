/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.MaterialTypeEntity;
import regiec.knast.erp.api.entities.containers.MaterialTypeEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface MaterialTypeDAOInterface extends DAOBaseInterface<MaterialTypeEntity, MaterialTypeEntitiesList> {

    MaterialTypeEntity getMaterialTypeByName(String name);

}
