/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import java.util.Objects;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTypeDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.RestoreRequest;
import regiec.knast.erp.api.dto.ValidateUniquenessRequest;
import regiec.knast.erp.api.dto.material.RemoveMaterialRequest;
import regiec.knast.erp.api.dto.materialtemplates.*;
import regiec.knast.erp.api.dto.producttemplates.RefreshSthTemplateUsageResponse;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.MaterialTypeEntity;
import regiec.knast.erp.api.entities.containers.MaterialEntitiesList;
import regiec.knast.erp.api.entities.containers.MaterialTemplateEntitiesList;
import regiec.knast.erp.api.exceptions.RemoveEntityException;
import regiec.knast.erp.api.manager.MaterialTemplateManager;
import regiec.knast.erp.api.manager.MaterialTemplateUtil;
import regiec.knast.erp.api.manager.PartCardManager;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.ERPSafetyCheck;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MaterialTemplateService extends AbstractService<MaterialTemplateEntity, MaterialTemplateDTO> {

    private static final EntityType MY_ENTITY_TYPE = EntityType.MATERIAL_TEMPLATE;

    @Inject
    private Validator validator;
    @Inject
    private MaterialService materialService;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private MaterialTemplateManager materialTemplateManager;
    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private MaterialTypeDAOInterface materialTypeDAO;
    @Inject
    private MaterialTemplateUtil materialTemplateUtil;
    @Inject
    private PartCardManager partCardConverters;

    @NetAPI
    public MaterialTemplateDTO addMaterialTemplate(AddMaterialTemplateRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("materialCode", request.getMaterialCode(), null, EntityType.MATERIAL_TEMPLATE).throww();
        MaterialTemplateEntity entity = new MaterialTemplateEntity();
        BeanUtilSilent.translateBean(request, entity);
        MaterialTypeEntity materialType = materialTypeDAO.get(request.getMaterialTypeId());
        validator.validateNotNull(materialType, ExceptionCodes.NULL_VALUE, "materialType").throww();
        validator.validateNotRemoved(materialType).throww();
        validator.validateCuttingSize(materialType.getBaseDimensionTemplate(), entity.getParticularDimension()).throww();
        entity.setMaterialType(materialType);
        MaterialTemplateEntity entity1 = materialTemplateDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public MaterialTemplateDTO getMaterialTemplate(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public MaterialTemplateDTO updateMaterialTemplate(UpdateMaterialTemplateRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("materialCode", request.getMaterialCode(), id, EntityType.MATERIAL_TEMPLATE).throww();
        MaterialTemplateEntity entity = materialTemplateDAO.get(id);
        validator.validateNotNull(entity).throww();
        MaterialTemplateEntity updatedEntity = new MaterialTemplateEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);
        validator.validateCuttingSize(updatedEntity.getMaterialType().getBaseDimensionTemplate(), updatedEntity.getParticularDimension()).throww();
        materialTemplateDAO.update(updatedEntity);
        updateMaterials(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    private void updateMaterials(MaterialTemplateEntity updatedEntity) {
        MaterialEntitiesList materialEntitiesList = materialTemplateUtil.getMaterialsWithMaterialTemplate(updatedEntity.getId());
        for (MaterialEntity materialEntity : materialEntitiesList) {
            materialEntity.setMaterialTemplate(updatedEntity);
            materialDAO.update(materialEntity);
        }
    }

    @NetAPI
    public RemoveResponse removeMaterialTemplate(RemoveMaterialTemplateRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        MaterialTemplateEntity materialTemplateEntity = materialTemplateDAO.get(id);
        if (request.getConfirmed()) {
            ERPSafetyCheck safetyCheck = new ERPSafetyCheck();
            if (safetyCheck.checkIfEntityIsReferenced(materialTemplateEntity.getClass(), id)) {
                throw new RemoveEntityException("Cannot remove Material, its used somewhere");
            }
            if (!materialTemplateEntity.getRemoved()) {
                materialTemplateEntity.setRemoved(true);
                materialTemplateDAO.update(materialTemplateEntity);
            } else {
                int remove = materialTemplateDAO.remove(id);
            }
//            partCardConverters.removeEntityTemplateFromPartCardCompatibileEntity(MY_ENTITY_TYPE, id);
//            removeMaterials(id);
//        } else {
            //build consequencesMap
//            Map<String, Object> consequencesMap = materialTemplateUtil.buildRemoveConsequencesMap(materialTemplateEntity);
//            return new RemoveResponse(false, consequencesMap);
        }
        return new RemoveResponse(true, null);
    }

    private void removeMaterials(String materialTemplateEntityId) {
        final MaterialEntitiesList materialEntitiesList = materialTemplateUtil.getMaterialsWithMaterialTemplate(materialTemplateEntityId);
        RemoveMaterialRequest removeMaterialRequest = new RemoveMaterialRequest("", true, true);
        for (MaterialEntity materialEntity : materialEntitiesList) {
            removeMaterialRequest.setId(materialEntity.getId());
            materialService.removeMaterial(removeMaterialRequest);
        }
    }

    @NetAPI
    public MaterialTemplateDTO restoreMaterialTemplate(RestoreRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        MaterialTemplateEntity entity = materialTemplateDAO.get(id);
        if (entity.getRemoved() == false) {
            throw new ConversionErrorWithCode("Restore is avaliable only in when sth is removed", ExceptionCodes.CANNOT_RESTORE_SELLER_IN_THIS_STATE);
        }
        materialTemplateDAO.update(entity);
        return entity.convertToDTO();
    }

    @NetAPI
    public MaterialTemplatesListDTO listMaterialTemplates(PaginableFilterWithCriteriaRequest request) {
        return (MaterialTemplatesListDTO) super.list(request);
    }

    @NetAPI
    public Boolean validateUniqueMaterialCode(ValidateUniquenessRequest request) {
        return validator.validateUniqueness("materialCode", request.getValue(), request.getMyId(), EntityType.MATERIAL_TEMPLATE).status();
    }

    @NetAPI
    public RefreshSthTemplateUsageResponse refreshUsageInProductTemplate() {
        RefreshSthTemplateUsageResponse ret = new RefreshSthTemplateUsageResponse();
        MaterialTemplateEntitiesList allMaterials = materialTemplateDAO.list();
        int noDif = 0;
        int withDif = 0;

        for (MaterialTemplateEntity material : allMaterials) {
            List<String> renew = materialTemplateManager.findMaterialTemplateUsage(material.getId());
            if (!Objects.deepEquals(renew, material.getMaterialUsages())) {
                withDif++;
                RefreshSthTemplateUsageResponse.Obj obj = new RefreshSthTemplateUsageResponse.Obj();
                obj.setId(material.getId());
                obj.setName(material.getMaterialType().getName());
                obj.setNo(material.getMaterialCode());
                obj.setOld(material.getMaterialUsages());
                obj.setNeww(renew);

                ret.getDifferences().add(obj);
                System.out.println("remew.size: " + renew.size());
                materialTemplateDAO.updateOneField(material.getId(), "materialUsages", renew);
            } else {
                noDif++;
            }
        }
        ret.setWithDifference(withDif);
        ret.setWithoutDifference(noDif);
        return ret;
    }
}
