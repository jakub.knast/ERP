/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.satisfier;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Map;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDemandDAOInterface;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandDTO;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.manager.ProductionOrderManager;
import regiec.knast.erp.api.model.states.MaterialDemandState;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class MaterialDemandSatisfier {

    @Inject
    private Validator validator;
    @Inject
    private MaterialDemandDAOInterface materialDemandDAO;
    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private ProductionOrderManager productionOrderManager;
    @Inject
    private OneDimSatisfier oneDimSatisfier;
    @Inject
    private SheetDemandSatisfier sheetDemandSatisfier;

    public MaterialDemandDTO satisfyDemands(String id, String materialId) {
        MaterialEntity m = materialDAO.get(materialId);
        validator.validateNotNull(m).throww();
        MaterialDemandEntity md = materialDemandDAO.get(id);
        validator.validateNotNull(md).throww();
        return satisfyDemands(m, md);
    }

    public MaterialDemandDTO satisfyDemands(MaterialEntity m, MaterialDemandEntity md) {
        MaterialDemandDTO ret = null;
        System.out.println("materialDemand c: " + md.getCount());
        System.out.println("materialDemand l: " + md.getCuttingSize().toString());

        System.out.println("material       c: " + m.getCount());
        System.out.println("material       l: " + m.getLength());

        if (md.getState() == MaterialDemandState.SATISFIED) {
            throw new ConversionErrorWithCode("MaterialDemand is already satisfied", ExceptionCodes.MATERIAL_DEMAND_IS_ALREADY_SATISFIED, null);
        }
        if (md.getCuttingSize().getIsOneDimensial()) {
            Map<String, Object> retObj = (Map<String, Object>)oneDimSatisfier.safisfyOneDinemsial(m, md);
            ret = (MaterialDemandDTO)retObj.get("retMd");
        } else {
             ret = sheetDemandSatisfier.safisfySheetMaterial(m, md);
        }
        String productionOrderId = md.getProductionOrderId();
        productionOrderManager.updateMdStr(productionOrderId);
        productionOrderManager.updateStateToSatisfied(productionOrderId);
        return ret;
    }


}
