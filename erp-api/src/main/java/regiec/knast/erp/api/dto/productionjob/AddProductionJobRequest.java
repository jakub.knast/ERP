/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.productionjob;

import regiec.knast.erp.api.model.bases.ProductionJobBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AddProductionJobRequest extends ProductionJobBase {

}
