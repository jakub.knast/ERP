/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.order;

import java.util.Date;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class GenerateProductionOrderDispositionRequest {
    private String orderId;
    private int count;
    private Date deliveryDate;
}
