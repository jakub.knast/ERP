/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.product;

import java.util.List;
import org.jaxygen.dto.collections.PaginableListResponseBaseDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class ProductListDTO extends PaginableListResponseBaseDTO<ProductDTO> {

    public ProductListDTO(List<ProductDTO> products) {
        this.setElements(products);
    }

}
