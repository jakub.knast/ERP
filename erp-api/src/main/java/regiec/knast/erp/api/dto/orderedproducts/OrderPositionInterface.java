/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.dto.orderedproducts;

import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.model.states.ProductionOrderState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface OrderPositionInterface {

    ProductEntity getProduct();

    void setProduct(ProductEntity product);

    String getOrderPositionId();

    void setOrderPositionId(String orderPositionId);

    String getComments();

    void setComments(String comments);

    int getCount();

    void setCount(int count);

    Double getDiscount();

    void setDiscount(Double discount);

    MoneyERP getPiecePrice();

    void setPiecePrice(MoneyERP piecePrice);

    MoneyERP getPiecePriceWithDiscount();

    void setPiecePriceWithDiscount(MoneyERP piecePriceWithDiscount);

    String getPositionOnOrder();

    void setPositionOnOrder(String positionOnOrder);

    public default ProductionOrderEntity createBaseOfProductionOrder(OrderEntity orderEntity, DispositionOrderPosition.ProductionOrderNoGenerator generator) {
        ProductionOrderEntity productionOrderEntity = new ProductionOrderEntity();
        productionOrderEntity.setState(ProductionOrderState.NEW);
        productionOrderEntity.setNo(generator.generateNo(orderEntity.getNr()));
        productionOrderEntity.setOrderNo(orderEntity.getNr());
        productionOrderEntity.setOrderId(orderEntity.getId());
        productionOrderEntity.setProduct(this.getProduct());
        return productionOrderEntity;
    }

}
