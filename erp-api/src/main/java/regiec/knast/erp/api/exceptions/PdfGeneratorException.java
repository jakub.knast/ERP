/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.exceptions;

import org.jaxygen.typeconverter.exceptions.ConversionError;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class PdfGeneratorException extends ConversionError {

    public PdfGeneratorException(String message, Throwable cause) {
        super(message, cause);
    }

}
