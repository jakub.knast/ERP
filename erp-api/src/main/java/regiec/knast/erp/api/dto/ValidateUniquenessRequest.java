/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto;

import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class ValidateUniquenessRequest {

    //  private ValidationType validationType;
    private EntityType entityType;
    private String field;
    private String value;
    private String myId;
}
