/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto;

import regiec.knast.erp.api.dto.producttemplates.partcard.MaterialPart;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class IncorrectMaterialPartInfo {

    private String detalNo;
    private String detalType;
    private String detalId;
    private String pdfResourceId;
    private int materialPartIndex;
    private MaterialPart materialPart;
}
