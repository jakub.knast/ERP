/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.waretemplates;

import regiec.knast.erp.api.dto.waretypes.WareTypeDTO;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.bases.WareTemplateBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WareTemplateDTO extends WareTemplateBase {

    private String id;
    private String wareTypeId;
    private WareTypeDTO wareType;

    public WareTemplateDTO(String id, String name, Dimension dimension) {

        this.id = id;
    }

}
