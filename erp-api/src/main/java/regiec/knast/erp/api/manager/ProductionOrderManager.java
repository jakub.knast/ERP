/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionJobDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.order.GenerateSTHResponse;
import regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsRequest;
import regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsResponse;
import regiec.knast.erp.api.dto.productionorder.ProductionOrderDTO;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.entities.containers.ProductionJobEntitiesList;
import regiec.knast.erp.api.entities.containers.ProductionOrderEntitiesList;
import regiec.knast.erp.api.exceptions.NotImplementedException;
import regiec.knast.erp.api.manager.orders.GenerateMaterialDemandsTransformer;
import regiec.knast.erp.api.manager.orders.GenerateProductionJobsTransformer;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.pguidegenerator.ProductionGuideGenerator;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.model.states.ProductionJobState;
import regiec.knast.erp.api.model.states.ProductionOrderState;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
public class ProductionOrderManager {

    @Inject
    private Validator validator;
    @Inject
    private ProductionOrderDAOInterface productionOrderDAO;
    @Inject
    private ProductionJobDAOInterface productionJobDAO;
    @Inject
    private GenerateProductionJobsTransformer productionJobManager;
    @Inject
    private ProductDAOInterface productDAO;
    @Inject
    private ProductionGuideGenerator productionGuideGenerator;

    public GenerateSTHResponse removeForSureProductionOrder(ProductionOrderEntity productionOrder) {
        validator.validateNotNull(productionOrder).throww();

        Object defMessage = ImmutableMap.builder().put("message", "removed productionOrder " + productionOrder.getNo()).build();
        if (null == productionOrder.getState()) {
            return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", "Not implemented yet!").build());
        } else {
            switch (productionOrder.getState()) {
                case NEW:
                    productionOrderDAO.remove(productionOrder.getId());
                    return new GenerateSTHResponse(Boolean.TRUE, defMessage);
                case PLANNED:
                    ProductionJobEntitiesList productionJobs = productionJobDAO.list(PaginableFilterWithCriteriaRequest.builder().
                            addCriteriaFilterBuilder().addConstraint("productionOrderId", productionOrder.getId(), ConstraintType.EQUALS).buildCriteriaFilter()
                            .build());
                    List l = new ArrayList();
                    l.add(defMessage);
                    for (ProductionJobEntity productionJob : productionJobs) {
                        GenerateSTHResponse resp = productionJobManager.removeForSureProductionJob(productionJob);
                        if (resp.getMessage() instanceof Map) {
                            l.add(((Map) resp.getMessage()).get("message"));
                        } else {
                            l.add(resp.getMessage());
                        }
                    }
                    productionOrderDAO.remove(productionOrder.getId());
                    ImmutableMap.builder().put("message", l).build();
                    return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", l).build());
                default:
                    return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", "Not implemented yet!").build());
            }
        }
    }

    public GenerateSTHResponse cancelProductionOrder(ProductionOrderEntity productionOrder) {
        validator.validateNotNull(productionOrder).throww();

        if (productionOrder.getState() == ProductionOrderState.NEW) {
            return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", "nothing to do").build());

        } else if (productionOrder.getState() == ProductionOrderState.PLANNED) {

            ProductionJobEntitiesList productionJobs = productionJobDAO.list(PaginableFilterWithCriteriaRequest.builder().
                    addCriteriaFilterBuilder().addConstraint("productionOrderId", productionOrder.getId(), ConstraintType.EQUALS).buildCriteriaFilter()
                    .build());
            for (ProductionJobEntity productionJob : productionJobs) {
                productionJobManager.cancelProductionJob(productionJob);
            }
        }
        return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", "Not implemented yet!").build());
    }

    public GenerateSTHResponse generateAllNewProductionOrders() {

        GenerateSTHResponse ret = new GenerateSTHResponse();
        ret.setStatus(Boolean.TRUE);
        List mesList = new ArrayList();
        ret.setMessage(mesList);

        ProductionOrderEntitiesList li = productionOrderDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("state", "NEW", ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());
        for (ProductionOrderEntity poe : li) {
            GenerateProductionJobsResponse m = productionJobManager.generateProductionJobs(new GenerateProductionJobsRequest(poe.getId()));
            mesList.add(m.getMessage());
        }
        return ret;
    }

    public void updateMdStr(String productionOrderId) {
        ProductionOrderEntity productionOrder = productionOrderDAO.get(productionOrderId);
        validator.validateNotNull(productionOrder, "productionOrder in updateMdStr");
        List<String> mdStr = GenerateMaterialDemandsTransformer.stringifyMaterialDemands(productionOrder.getMaterialDemands());
        productionOrder.setMaterialDemandsStr(mdStr);
        productionOrderDAO.update(productionOrder);
    }

    public void updateStateToSatisfied(String productionOrderId) {
        ProductionOrderEntity productionOrder = productionOrderDAO.get(productionOrderId);
        validator.validateNotNull(productionOrder, "produc  tionOrder in updateMdStr");
        boolean areSomeUnsatifsied = productionOrder.getMaterialDemands().stream()
                .filter(md -> md.getUnsatisfied() > 0)
                .findAny()
                .isPresent();
        if (!areSomeUnsatifsied) {
            productionOrder.setState(ProductionOrderState.WITH_MATERIAL_DEMANDS);
            productionOrderDAO.update(productionOrder);
        }
    }

    /**
     * Set productionOrder as FINISHED. If closeJobs is set to true, it sets
     * also all its jobs. If false, it throw exception if any of its jobs is not
     * FINISHED
     *
     * @param productionOrderId id of production order
     * @param closeProductionJobs If closeJobs is set to true, it sets also all
     * its jobs. If false, it throw exception if any of its jobs is not FINISHED
     * @return ProductionOrderDTO
     */
    @Deprecated
    public ProductionOrderDTO finishProductionOrder(String productionOrderId, boolean closeProductionJobs) {
        ProductionOrderEntity productionOrder = productionOrderDAO.get(productionOrderId);
        validator.validateNotNull(productionOrder).throww();
        productionOrder.validateStateToFinish();
        List<ProductionJobEntity> productionJobs = productionOrder.getProductionJobs();
        if (closeProductionJobs) {
            for (ProductionJobEntity job : productionJobs) {
                job.setState(ProductionJobState.FINISHED);
                productionJobDAO.update(job);
            }
        } else {
            productionJobs.stream()
                    .filter(job -> !job.getState().equals(ProductionJobState.FINISHED))
                    .findAny()
                    .ifPresent((ProductionJobEntity job) -> {
                        throw new NotImplementedException("there is not finished production jobs: '" + job.getNo() + "'. First finish them;");
                    });
        }
        productionOrder.setState(ProductionOrderState.FINISHED);
        productionOrder = productionOrderDAO.update(productionOrder);
        ProductEntity productEntity = productDAO.get(productionOrder.getProduct().getId());
        productEntity.setState(ProductState.FINISHED);
        ProductEntity update = productDAO.update(productEntity);
        // just for DTO conversion purpose
        productionOrder.setProduct(update);
        return productionOrder.convertToDTO();
    }

}
