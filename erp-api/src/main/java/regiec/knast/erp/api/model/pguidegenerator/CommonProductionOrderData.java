/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.model.pguidegenerator;

import java.text.SimpleDateFormat;
import regiec.knast.erp.api.entities.ProductionOrderEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class CommonProductionOrderData {

    private String date = "Data złożenia zamówienia";
    private String endDate = "Termin realizacji";
    private String orderNo = "numer zamówienia";
    private String orderedDetalNo = "numer detalu";
    private String orderedDetalName = "nazwa detalu";
    private int pcsCount = 1;    
    private boolean generatingFromOrder = false;

    public CommonProductionOrderData(ProductionOrderEntity productionOrder) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String creationDate = productionOrder.getCreationDate() != null ? df.format(productionOrder.getCreationDate()) : "nie uzupełnione!";
        String endDate_ = productionOrder.getDeliveryDate() != null ? df.format(productionOrder.getDeliveryDate()) : "nie uzupełnione!";

        this.pcsCount = productionOrder.getCount();
        this.setDate(creationDate);
        this.setEndDate(endDate_);
        this.setOrderNo(productionOrder.getOrderNo());
        this.setOrderedDetalNo(productionOrder.getProduct().getProductTemplate().getProductNo());
        this.setOrderedDetalName(productionOrder.getProduct().getProductTemplate().getName());
    }

}
