/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.otherstuff;

import java.util.Set;
import java.util.TreeSet;
import regiec.knast.erp.api.model.Norm;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class NormSetDTO extends TreeSet<String> {

    public static NormSetDTO fromNormSet(Set<Norm> norms) {
        NormSetDTO ret = new NormSetDTO();
        for (Norm norm : norms) {
            ret.add(norm.toString());
        }
        return ret;
    }

}
