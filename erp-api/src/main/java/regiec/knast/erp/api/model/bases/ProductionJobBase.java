/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.model.TreeNodeType;
import regiec.knast.erp.api.model.states.ProductionJobState;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductionJobBase extends ObjectWithDate {

    private String no;
    @ErpIdEntity(entityClass = ProductionOrderEntity.class)
    private String productionOrderId;
    private String productionOrderNo;
    private TreeNodeType treeNodeType;
    @ErpIdEntity(entityClass = ProductionJobEntity.class)
    private String parentId;
    private String parentNo;
    private ProductionJobState state;
    private String comments;
    private List<Integer> testCasesNumbers = new ArrayList();
}
