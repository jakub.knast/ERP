/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils.entityfieldscache;

import com.google.inject.Singleton;
import com.mongodb.DBRef;
import java.util.Map;
import org.apache.commons.beanutils.ConvertUtils;
import org.jaxygen.converters.properties.EnumConverter;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.mongodb.morphia.annotations.Entity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.utils.EntityFieldsCache;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class ValueCreator<ENTITY extends EntityBase> {

    public Object createValue(String fieldName, String stringvalue, Class<ENTITY> clazz) {

        Class<?> propertyType = null;
        Map<String, Class> fieldsTypesFromClass = EntityFieldsCache.getFieldsTypesFromClass(clazz);
        if (fieldsTypesFromClass.containsKey(fieldName)) {
            propertyType = fieldsTypesFromClass.get(fieldName);
        } else {
            int i = fieldName.lastIndexOf(".");
            if (i > 0) {
                String refField = fieldName.substring(0, i);
                if (fieldsTypesFromClass.containsKey(refField)) {
                    propertyType = fieldsTypesFromClass.get(refField);
                }
                throw new ConversionError("there is no class for field '" + fieldName + "' in class '" + clazz.getName() + "'.");
            }
            throw new ConversionError("there is no class for field '" + fieldName + "' in class '" + clazz.getName() + "'.");
        }
        if (propertyType == InterfaceField.class) {
            InterfaceField interfaceField = EntityFieldsCache.getInterfaceFieldsTypesFromClass(clazz, fieldName);
            Class interfaceFieldPropertyType = interfaceField.getFieldType();
            Object interfaceFieldValue = createSimpleValue(interfaceFieldPropertyType, stringvalue);
            interfaceField.setValue(interfaceFieldValue);
            return interfaceField;
        } else {
            return createSimpleValue(propertyType, stringvalue);
        }

    }

    private Object createSimpleValue(Class<?> propertyType, String stringvalue) {
        Object value = null;
        if (propertyType.isEnum()) {
            ConvertUtils.register(new EnumConverter(), propertyType);
        }
        if (EntityBase.class.isAssignableFrom(propertyType)) {
            if (propertyType.isAnnotationPresent(Entity.class)) {
                Entity annotation = propertyType.getAnnotation(Entity.class);
                value = new DBRef(annotation.value(), stringvalue);
            } else {
                throw new ConversionError("You try to query on entity reference but your class: " + propertyType + "is not annotated with Entity.class");
            }
            // TODO do this with arrays
//        } else if (List.class.isAssignableFrom(propertyType)) {
//            if (propertyType.isAnnotationPresent(Entity.class)) {
//                Entity annotation = propertyType.getAnnotation(Entity.class);
//                value = new DBRef(annotation.value(), stringvalue);
//            } else {
//                throw new ConversionError("You try to query on entity reference but your class: " + propertyType + "is not annotated with Entity.class");
//            }
        }else if (stringvalue != null && !propertyType.equals(String.class)) {
            value = ConvertUtils.convert((String) stringvalue, propertyType);
        } else {
            value = stringvalue;
        }
        return value;
    }
}
