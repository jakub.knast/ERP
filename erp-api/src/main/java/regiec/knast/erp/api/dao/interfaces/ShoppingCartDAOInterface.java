/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.ShoppingCartEntity;
import regiec.knast.erp.api.entities.containers.ShoppingCartEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ShoppingCartDAOInterface extends DAOBaseInterface<ShoppingCartEntity, ShoppingCartEntitiesList> {

    ShoppingCartEntity getExistingOne();
    
    ShoppingCartEntity resetShoppingCart();
}
