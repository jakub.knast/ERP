/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import regiec.knast.erp.api.dto.material.MaterialDTO;
import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTO;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.interfaces.RemovableEntity;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.bases.MaterialBase;
import regiec.knast.erp.api.model.states.MaterialState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;
import regiec.knast.erp.api.validation.state.StateMachineAgregat;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("materials")
public class MaterialEntity extends MaterialBase implements EntityBase, ConvertableEntity, RemovableEntity {

    public static final List<MaterialState> statesForSatisfyingDemands = Lists.newArrayList(
            MaterialState.ADDED_MANUALY,
            MaterialState.ADDED_MANUALY_RESERVED,
            MaterialState.ON_STOCK_AVALIABLE,
            MaterialState.ON_STOCK_RESERVED
    );

    @Id
    private String id;
    private MaterialTemplateEntity materialTemplate;

    private CuttingSize length;
    @ErpIdEntity(entityClass = MaterialDemandEntity.class)
    private List<String> materialDemandsIdsWhereUsed = new ArrayList();
    private MaterialState state = MaterialState.INITIAL;
    private boolean removed;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public MaterialEntity cloneMe() {
        return BeanUtilSilent.deepClone(this);
    }

    public CuttingSize calculateLeftLength() {
        CuttingSize leftLength = this.getLength().substract(this.getReservedLength());
        this.setLeftLength(leftLength);
        this.validateLength();
        return leftLength;
    }

    public String debugDescribe() {
        return new StringBuilder()
                .append(materialTemplate.getMaterialType().getName())
                .append(" code: ").append(this.materialTemplate.getMaterialCode())
                .append(" state: ").append(this.getState())
                .append(" id: ").append(this.id)
                .toString();
    }

    public void goToOrderedState() {
        MaterialState toSet = isNotUsed() ? MaterialState.ORDERED : MaterialState.ORDERED_RESERVED;
        this.setState(toSet);
    }

    public void refreshSatisfiedState() {
        MaterialState toSet = MaterialState.ERROR;
        if (this.getState() == MaterialState.IN_SHOPPING_CART || this.getState() == MaterialState.IN_SHOPPING_CART_RESERVED) {
            toSet = isNotUsed() ? MaterialState.IN_SHOPPING_CART : MaterialState.IN_SHOPPING_CART_RESERVED;
        } else if (this.getState() == MaterialState.ON_STOCK_AVALIABLE || this.getState() == MaterialState.ON_STOCK_RESERVED) {
            toSet = isNotUsed() ? MaterialState.ON_STOCK_AVALIABLE : MaterialState.ON_STOCK_RESERVED;
        } else if (this.getState() == MaterialState.ADDED_MANUALY || this.getState() == MaterialState.ADDED_MANUALY_RESERVED) {
            toSet = isNotUsed() ? MaterialState.ADDED_MANUALY : MaterialState.ADDED_MANUALY_RESERVED;
        } else if (this.getState() == MaterialState.ORDERED || this.getState() == MaterialState.ORDERED_RESERVED) {
            toSet = isNotUsed() ? MaterialState.ORDERED : MaterialState.ORDERED_RESERVED;
        } else {
            Logger.getLogger(MaterialEntity.class.getCanonicalName()).log(Level.SEVERE, "Invalid state to set in material entity: " + this.debugDescribe());
            toSet = MaterialState.ERROR;
        }
        StateMachineAgregat.MATERIAL.validateChangeState(this.getState(), toSet);
        this.setState(toSet);
    }

    public void setState(MaterialState toGo) {
        StateMachineAgregat.MATERIAL.validateChangeState(this.getState(), toGo);
        this.state = toGo;
    }

    public void validateIsInStateForSatisfying() {
        if (!statesForSatisfyingDemands.contains(this.getState())) {
            throw new ERPInternalError("Material has wrong state for satisfying: " + this.getState());
        }
    }

    public boolean isPartiallyOrFullUsed() {
        return this.getLeftLength().smallerThan(length);
    }

    public boolean isNotUsed() {
        return !isPartiallyOrFullUsed();
    }

    public void validateLength() {
        if (this.getLength() == null) {
            throw new ConversionError("Length in material is null");
        }
        if (this.getLeftLength() == null) {
            throw new ConversionError("Left length in material is null");
        }
        if (this.getReservedLength() == null) {
            throw new ConversionError("Reserved length in material is null");
        }
        if (this.getLength().getIsOneDimensial()) {
            if (this.getLength().getLength() != this.getReservedLength().getLength() + this.getLeftLength().getLength()) {
                throw new ConversionError("Left length and reserved lenght not equals to length");
            }
        } else {
            double area = this.getLength().getArea().getA() * this.getLength().getArea().getB();
            double reservedArea = this.getReservedLength().getArea().getA() * this.getReservedLength().getArea().getB();
            double leftArea = this.getLeftLength().getArea().getA() * this.getLeftLength().getArea().getB();
            if (area < reservedArea + leftArea) {
                throw new ConversionError("whole part area is less than reserved and left area.");
            }
        }
    }

    @Override
    public MaterialDTO convertToDTO() {
        MaterialDTO dto = BeanUtilSilent.translateBeanAndReturn(this, new MaterialDTO());
        if (this.getMaterialTemplate() != null) {
            dto.setMaterialTemplate((MaterialTemplateDTO) this.getMaterialTemplate().convertToDTO());
            dto.setMaterialTemplateId(this.getMaterialTemplate().getId());
        }else{
            throw new ERPInternalError("There is no materialTemplate in material: " + this.getId());
        }
        return dto;
    }

}
