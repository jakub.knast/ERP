/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import java.util.ArrayList;
import java.util.List;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialPart extends MaterialPartBase {

    private String positionId;

    @Reference
    private MaterialTemplateEntity materialTemplate;

    public MaterialPartDTO materialPartToDTO( ) {
        final MaterialPartDTO materialPartDTO = new MaterialPartDTO();
        BeanUtilSilent.translateBean(this, materialPartDTO);
        materialPartDTO.setMaterialTemplate(this.getMaterialTemplate().convertToDTO());
        materialPartDTO.setMaterialTemplateId(materialPartDTO.getMaterialTemplate().getId());
        return materialPartDTO;
    }

    public static List<MaterialPartDTO> materialPartsListToDto(List<MaterialPart> materialParts) {
        List<MaterialPartDTO> ret = new ArrayList();
        if (materialParts != null) {
            for (final MaterialPart materialPart : materialParts) {
                ret.add(materialPart.materialPartToDTO());
            }
        }
        return ret;
    }
}
