/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.satisfier;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dao.interfaces.DeliveryOrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDemandDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandDTO;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.exceptions.NotImplementedException;
import regiec.knast.erp.api.model.Area;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.states.MaterialDemandState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.sheet.nesting.core.Bin;
import regiec.knast.sheet.nesting.core.BinPacking;
import regiec.knast.sheet.nesting.primitives.MArea;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class SheetDemandSatisfier {

    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private MaterialDemandDAOInterface materialDemandDAO;
    @Inject
    private DeliveryOrderDAOInterface deliveryOrderDAO;
    @Inject
    private ProductionOrderDAOInterface productionOrderDAO;

    public MaterialDemandDTO safisfySheetMaterial(MaterialEntity m, MaterialDemandEntity md) {
        /*
        Jak to zrobić:
        Scenariusz 1 - zaspokajanie po kolei
        1. weź wszystkie pasujace materiały ze stocka (koszyk + dostępne an stoku)
        2. odrzuc materiały użyte w 90%
        3. zbierz wszystkie przypisania zapotrzebowań i je anuluj
        4. ponownie przypisz wszystkie zapotrzebowania
        
        Scenariusz 2 - zapokoajanie po kolei ale łatwiejsze
        1. weź materiał z bazy
        2. weź wszystkie zapotrzebowania spełnione na tym materiale
        3. próbuj dodać zamówienie do materiału.
        4. rzuć exception jak się nie da, zwróć ok jak się da
        
        Scenariusz 3 - wsyzskti blachy się zamowią ;/
         */
        return scenario2(m, md);
    }

    private MaterialDemandDTO scenario2(MaterialEntity m, MaterialDemandEntity md) {
        MaterialDemandDTO ret;
        Bin sheetBin = ensureBinExistInMaterial(m);

        List<MArea> piecesList = demandToMAreaList(md);

        int materialCount = m.getCount();
        int notSatisfied = BinPacking.fillBinToFull(sheetBin, piecesList);
        int satisfiedInThisOperation = piecesList.size() - notSatisfied;

        m.setSheetBin(sheetBin);
        double percentUsageD = sheetBin.getUsageInDouble() * 100;
        int percentUsage = (int) Math.round(percentUsageD);
        m.setPercentUsage(percentUsageD);
        m.getReservedLength().setOrigSize(String.valueOf(percentUsage) + "%");
        m.getLeftLength().setOrigSize(String.valueOf(100 - percentUsage) + "%");
        m.getMaterialDemandsIdsWhereUsed().add(md.getId());

        if (notSatisfied == 0) {
            satisfyDemandFully(md);
        } else {
            satisfyDemandPartially(md, satisfiedInThisOperation);
            m.setCount(1);

            if (materialCount > 1) {
                MaterialEntity newm = BeanUtilSilent.translateBeanAndReturn(m, new MaterialEntity());
                // newm.setId(IDUtil.nextId());
                newm.setReservedLength(m.getReservedLength().cloneMe());
                newm.setLeftLength(m.getLeftLength().cloneMe());
                newm.setMaterialDemandsIdsWhereUsed(new ArrayList());
                newm.setSheetBin(new Bin(sheetBin.getDimension()));

                newm.setCount(materialCount - 1);
                newm.setPercentUsage(0.0);
                newm.getReservedLength().setOrigSize(String.valueOf(0) + "%");
                newm.getLeftLength().setOrigSize(String.valueOf(100) + "%");
                materialDAO.add(newm);
            }
        }

        materialDAO.update(m);
        MaterialDemandEntity update = materialDemandDAO.update(md);
        return update.convertToDTO();
    }

    private List<MArea> demandToMAreaList(MaterialDemandEntity md) {
        List<MArea> ret = new ArrayList();
        Integer unsatisfiedCount = md.getUnsatisfied();
        CuttingSize orderedCuttingSize = md.getCuttingSize();
        Double orderedA = orderedCuttingSize.getArea().getA();
        Double orderedB = orderedCuttingSize.getArea().getB();
        int orderedAmm = (int) Math.round(1000 * orderedA);
        int orderedBmm = (int) Math.round(1000 * orderedB);

        for (int i = 0; i < unsatisfiedCount; i++) {
            MArea mArea = new MArea(new Rectangle(orderedAmm, orderedBmm), i);
            ret.add(mArea);
        }
        return ret;
    }

    private Bin ensureBinExistInMaterial(MaterialEntity m) {
        if (m.getSheetBin() == null) {
            Area area = m.getLength().getArea();
            Double a = area.getA();
            Double b = area.getB();
            int amm = (int) Math.round(1000 * a);
            int bmm = (int) Math.round(1000 * b);

            Dimension binDimension = new Dimension(amm, bmm);
            Bin sheetBin = new Bin(binDimension);
            m.setSheetBin(sheetBin);
        }
        validateBinArea(m.getSheetBin());
        return m.getSheetBin();
    }

    private void satisfyDemandFully(MaterialDemandEntity md) {
        md.setSatisfied(md.getCount());
        md.setUnsatisfied(0);
        md.setState(MaterialDemandState.SATISFIED);
    }

    private void satisfyDemandPartially(MaterialDemandEntity md, int satisfiedInThisOperation) {

        int alreadySat = md.getSatisfied();
        int alreadyUnsat = md.getUnsatisfied();
        md.setUnsatisfied(alreadyUnsat - satisfiedInThisOperation);
        md.setSatisfied(alreadySat + satisfiedInThisOperation);
        md.setState(MaterialDemandState.PARTLY_SATISFIED);
    }

    private void validateBinArea(Bin sheetBin) {
        Dimension dimension = sheetBin.getDimension();
        if (dimension.width < 20 || dimension.getHeight() < 20) {
            throw new NotImplementedException("Bin has to small size. It is only " + dimension + ". It should me in milimeters not in meters");
        }
    }
}
