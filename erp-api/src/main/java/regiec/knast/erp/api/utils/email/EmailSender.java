/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils.email;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.InitialContext;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.lang3.StringUtils;
import regiec.knast.erp.api.dto.system.SendEmailWithExcepitonInfoRequest;

/**
 *
 * @author fbrzuzka
 */
public class EmailSender {

    private static String from = "erp.exceptions@gmail.com";
    private static String to = "erp.exceptions@gmail.com";
    private static String image2 = "R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";
    private static final String pre = "data:image/png;base64,";

    private static final String newLineCode = "&#10&#13";
    private static final String tabulatorCode = "&#9";

    private byte[] imageEmcoded;
    private String request;
    private String response;

    public static void main(String[] args) {
        SendEmailWithExcepitonInfoRequest re = new SendEmailWithExcepitonInfoRequest("request babababaab", "response babababab", image2);
        new EmailSender(re).sendEmail();
    }

    public EmailSender(SendEmailWithExcepitonInfoRequest request) {
        this.request = request.getRequest();
        this.response = request.getResponse();
        if (request.getScreenshot() != null) {
            String encoded = request.getScreenshot().startsWith(pre) ? request.getScreenshot().substring(pre.length()) : request.getScreenshot();
            imageEmcoded = decodeBase64(encoded);
        } else {
            imageEmcoded = new byte[0];
        }
    }

    public boolean sendFromGlassfish() {
        try {
            InitialContext ctx = new InitialContext();
            Session mailSession = (Session) ctx.lookup("mail/session");

            Message msg = createMessage(mailSession);

//            Message msg = new MimeMessage(mailSession);
//            msg.setSubject("Hello World!");
            msg.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(to));
            msg.setFrom(new InternetAddress(from));

            Transport.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean sendEmail() {
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        // Get a Properties object
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.store.protocol", "pop3");
        props.put("mail.transport.protocol", "smtp");
        final String username = "erp.exceptions@gmail.com";//
        final String password = "TomekIKuba";
        try {
            Session session = Session.getDefaultInstance(props,
                    new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            Message message = createMessage(session);

            Transport.send(message);
            System.out.println("Message sent.");
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private Message createMessage(Session session) throws MessagingException, IOException {
        // -- Create a new message --
        Message message = new MimeMessage(session);

        Multipart multipart = new MimeMultipart();

        MimeBodyPart htmlpart = new MimeBodyPart();
        String html = createHTMLContent(request, response);
        htmlpart.setContent(html, "text/html; charset=utf-8");
        multipart.addBodyPart(htmlpart);

        MimeBodyPart attachement = new MimeBodyPart();
        String fileName = "screenshot.jpg";
//            DataSource source = new FileDataSource(file);
        DataSource source = new ByteArrayDataSource(imageEmcoded, "image/jpg");
        attachement.setDataHandler(new DataHandler(source));
        attachement.setFileName(fileName);
        multipart.addBodyPart(attachement);

        message.setContent(multipart);

        // -- Set the FROM and TO fields --
        message.setFrom(new InternetAddress(from));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to, false));
        message.setSubject("Hello, there is new exception :)");
        message.setSentDate(new Date());
        return message;
    }

    private String createHTMLContent(String request, String response) {

        ObjectMapper mapper = new ObjectMapper();
        String requestPreetified = "";

        try {
            Object json = mapper.readValue(request, Object.class);
            requestPreetified = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
        } catch (IOException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String responsePreetified = "";

        try {
            Object json = mapper.readValue(response, Object.class);
            responsePreetified = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
        } catch (IOException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
            responsePreetified = StringUtils.replace(responsePreetified, "\\n", "\n");
            responsePreetified = StringUtils.replace(responsePreetified, "\\r", "\r");
            responsePreetified = StringUtils.replace(responsePreetified, "\\t", "\t");
        System.out.println("------------------------------------------------");
        System.out.println("------------------------------------------------");
        System.out.println("------------------------------------------------");
        System.out.println(responsePreetified);
        System.out.println("------------------------------------------------");
        System.out.println("------------------------------------------------");
        System.out.println("------------------------------------------------");

//        String[] split = responsePreetified.split("\\\\n");
//        List<String> re = new ArrayList();
//        for (String s : split) {
//            String ss = StringUtils.replace(s, "\\t", "   ");
//            String sss = StringUtils.replace(ss, "\\r", "");
//            re.add("<p>" + sss + "</p>");
//        }
//        String join = String.join("\n", re);
        String html = new StringBuilder()
                .append("\n<html>")
                .append("\n   <body>")
                .append("\n       <h2>Request</h2>")
                .append("\n       <pre>").append(requestPreetified).append("</pre>")
                .append("\n       <br>")
                .append("\n       <h2>Response</h2>")
                .append("\n       <pre>").append(responsePreetified).append("</pre>")
                .append("\n       <br>")
                .append("\n   </body>")
                .append("\n</html>")
                .toString();
        System.out.println(html);
        return html;
    }

    private static byte[] decodeBase64(String encoded) {
        byte[] parseBase64Binary = DatatypeConverter.parseBase64Binary(encoded);
        return parseBase64Binary;
    }

}
