/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.orderedproducts;

import java.util.Date;
import java.util.List;
import regiec.knast.erp.api.model.MoneyERP;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddOrderPosition extends OrderPositionBase {

    private String productTemplateId;

    public AddOrderPosition(String productTemplateId, String orderPositionId, String positionOnOrder, int count, MoneyERP piecePrice, MoneyERP piecePriceWithDiscount, 
            Double discount, String comments, Date deliveryDate, String productionOrderId, int disposedCount, int toDisposeCount, List<String> dispositionsId) {
        
        super(orderPositionId, positionOnOrder, count, piecePrice, piecePriceWithDiscount, discount, comments, deliveryDate, productionOrderId, disposedCount, toDisposeCount, dispositionsId);
        this.productTemplateId = productTemplateId; 
    }

}
