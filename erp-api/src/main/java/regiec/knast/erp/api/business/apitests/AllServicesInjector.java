/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilder;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.business.*;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AllServicesInjector extends GuiceInjector {

    public MaterialTemplateService materialTemplateService;
    public ProductionOrderService productionOrderService;
    public SystemService systemService;
    public DeliveryOrderService deliveryOrderService;
    public MaterialTypeService materialTypeService;
    public ProfessionGroupService professionGroupService;
    public TodoIssuesService todoIssuesService;
    public DevelopmentService developmentService;
    public OperationNameService operationNameService;
    public ProviderService providerService;
    public WareService wareService;
    public OrderService orderService;
    public RecipientService recipientService;
    public WareTemplateService wareTemplateService;
    public MachineService machineService;
    public ProductService productService;
    public ResourceInfoService resourceInfoService;
    public WareTypeService wareTypeService;
    public MaterialDemandService materialDemandService;
    public ProductTemplateService productTemplateService;
    public SellerService sellerService;
    public WorkerService workerService;
    public MaterialService materialService;
    public ProductionJobService productionJobService;
    public ShoppingCartService shoppingCartService;

    public void initServices() {
        try {
            ObjectBuilder builder = ObjectBuilderFactory.instance();
            this.materialTemplateService = builder.create(MaterialTemplateService.class);
            this.productionOrderService = builder.create(ProductionOrderService.class);
            this.systemService = builder.create(SystemService.class);
            this.deliveryOrderService = builder.create(DeliveryOrderService.class);
            this.materialTypeService = builder.create(MaterialTypeService.class);
            this.professionGroupService = builder.create(ProfessionGroupService.class);
            this.todoIssuesService = builder.create(TodoIssuesService.class);
            this.developmentService = builder.create(DevelopmentService.class);
            this.operationNameService = builder.create(OperationNameService.class);
            this.providerService = builder.create(ProviderService.class);
            this.wareService = builder.create(WareService.class);
            this.orderService = builder.create(OrderService.class);
            this.recipientService = builder.create(RecipientService.class);
            this.wareTemplateService = builder.create(WareTemplateService.class);
            this.machineService = builder.create(MachineService.class);
            this.productService = builder.create(ProductService.class);
            this.resourceInfoService = builder.create(ResourceInfoService.class);
            this.wareTypeService = builder.create(WareTypeService.class);
            this.materialDemandService = builder.create(MaterialDemandService.class);
            this.productTemplateService = builder.create(ProductTemplateService.class);
            this.sellerService = builder.create(SellerService.class);
            this.workerService = builder.create(WorkerService.class);
            this.materialService = builder.create(MaterialService.class);
            this.productionJobService = builder.create(ProductionJobService.class);
            this.shoppingCartService = builder.create(ShoppingCartService.class);
        } catch (ObjectCreateError ex) {
            Logger.getLogger(AllServicesInjector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
