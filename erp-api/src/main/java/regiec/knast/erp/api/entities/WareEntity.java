/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import com.google.common.collect.ImmutableMap;
import java.util.Date;
import java.util.Map;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import regiec.knast.erp.api.dto.ware.WareDTO;
import regiec.knast.erp.api.dto.waretemplates.WareTemplateDTO;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.WareBase;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("wares")
public class WareEntity extends WareBase implements EntityBase, ConvertableEntity {

    @Id
    private String id;
    private WareTemplateEntity wareTemplate;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public WareDTO convertToDTO() {
        WareDTO dTO =  BeanUtilSilent.translateBeanAndReturn(this, new WareDTO());
        if (this.getWareTemplate() != null) {
            dTO.setWareTemplate((WareTemplateDTO)this.getWareTemplate().convertToDTO());
            dTO.setWareTemplateId(this.getWareTemplate().getId());
        }
        return dTO;
    }
    
    public Map<String, Object> buildRemoveConsequencesMap( ) {
        return ImmutableMap.<String, Object>builder()
                .put("entityType", WareEntity.class.getSimpleName())
                .put("id", this.getId())
                .put("wareCode", this.getWareTemplate().getWareCode())
                .put("count", this.getCount())
                .put("name", this.getWareTemplate().getWareType().getName())
                .build();
    }

}
