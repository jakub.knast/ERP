/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.seenmodit.core.PositionManager;
import org.seenmodit.core.exceptions.model.Pair;
import org.seenmodit.core.exceptions.model.PositionsChangesRet;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.MaterialAssortmentItem;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.model.ProviderAssortment;
import regiec.knast.erp.api.model.SellUnit;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class ProviderAssortmentManager {

    @Inject
    private Validator validator;
    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private ProviderAssortmentUtil providerAssortmentUtil;
    private PositionManager positionManager = PositionManager.create(new IDUtil());

    public void validateMaterialAssortimentCorrectness(ProviderAssortment assortment, boolean initId) {
        //validate if all is ok
        if (assortment != null && assortment.getMaterialAssortment() != null) {
            for (MaterialAssortmentItem mai : assortment.getMaterialAssortment()) {
                validator.validateNotNullAndNotEmpty(mai.getMaterialTemplateId()).throww();
                MaterialTemplateEntity mt = materialTemplateDAO.get(mai.getMaterialTemplateId());
                validator.validateNotNull(mt, ExceptionCodes.ENTITY_NOT_EXIST, "material template");

                mai.setMaterialCode(mt.getMaterialCode());
                mai.setMaterialTypeName(mt.getMaterialType().getName());
                mai.setMaterialBaseSize(mt.getParticularDimension());

                for (MaterialAssortmentPosition position : mai.getMaterialAssortmentPositions()) {
                    validator.validateNotNull(position.getSalesLength(), ExceptionCodes.NULL_VALUE).throww();
                    validator.validateNotNull(position.getSalesPrice(), ExceptionCodes.NULL_VALUE).throww();
                    validator.validateCuttingSize(position.getSalesLength()).throww();

                    validatorDetal.validateProductTemplatePrice(position.getSalesPrice()).throww();
                }
            }
        }
    }

    public void handleUpdatedMaterialAssortment(List<Pair<MaterialAssortmentItem, MaterialAssortmentItem>> other, ProviderEntity provider)  {
        for (Pair<MaterialAssortmentItem, MaterialAssortmentItem> pair : other) {
            MaterialAssortmentItem actual = pair.getFirst();
            MaterialAssortmentItem updated = pair.getSecond();

            //TODO: handle if updated is materialCode from item.
            //prepare item from providerEntity, where update positions
            // TODO: check if actual is ok to get material code
            final String materialCode = actual.getMaterialCode();
            MaterialAssortmentItem itemWhereToAdd = providerAssortmentUtil.findExistingMaterialAssortmentInProvider(provider, materialCode);
            MaterialTemplateEntity materialTemplate = providerAssortmentUtil.getMateriaLTemplateByMaterialCode(materialCode);
            Dimension cuttingDimension = materialTemplate.getMaterialType().getCuttingDimensionTemplate();

            // check if positions in this level are different
            PositionsChangesRet<MaterialAssortmentPosition, MaterialAssortmentPosition> findAddedRemovedUpdatedPositions
                    = positionManager.findAddedRemovedUpdated(actual.getMaterialAssortmentPositions(), updated.getMaterialAssortmentPositions());

            // handle added positions
            addManyMaterialAssortmentsPositionToAssortmentItem(itemWhereToAdd, findAddedRemovedUpdatedPositions.getAdded(), cuttingDimension);

            // handle removed
            handleRemovedMaterialAssortmentPosition(itemWhereToAdd, findAddedRemovedUpdatedPositions.getRemoved());

            // handle updated or not changed
            for (Pair<MaterialAssortmentPosition, MaterialAssortmentPosition> pair1 : findAddedRemovedUpdatedPositions.getOther()) {
                MaterialAssortmentPosition actualAssPos = pair1.getFirst();
                MaterialAssortmentPosition updatedAssPos = pair1.getSecond();
                // find if any value changes                   
                if (!actualAssPos.getSalesLength().getOrigSize().equals(updatedAssPos.getSalesLength().getOrigSize())) {
                    System.out.println("Differece found in orig salesLength: " + actualAssPos.getSalesLength() + " old: " + updatedAssPos.getSalesLength());
                    CuttingSize salesLength = CuttingSizeManager.createCuttingSize(cuttingDimension, updatedAssPos.getSalesLength().getOrigSize());
                    actualAssPos.setSalesLength(salesLength);
                }
                if (!actualAssPos.getSalesPrice().equals(updatedAssPos.getSalesPrice())) {
                    System.out.println("Differece found in salesPrice: " + actualAssPos.getSalesPrice() + " old: " + updatedAssPos.getSalesPrice());
                    actualAssPos.setSalesPrice(updatedAssPos.getSalesPrice());
                }
                if (actualAssPos.getSellUnit() != updatedAssPos.getSellUnit()) {
                    System.out.println("Differece found in sellUnit: " + actualAssPos.getSellUnit() + " old: " + updatedAssPos.getSellUnit());
                    actualAssPos.setSellUnit(updatedAssPos.getSellUnit());
                }
            }
        }
    }

    public void handleAddedMaterialAssortment(List<MaterialAssortmentItem> added, ProviderEntity provider) {
        for (MaterialAssortmentItem addedAssortment : added) {

            final String materialCode = addedAssortment.getMaterialCode();
            MaterialAssortmentItem itemWhereToAdd = providerAssortmentUtil.findExistingMaterialAssortmentInProvider(provider, materialCode);

            MaterialTemplateEntity materialTemplate = providerAssortmentUtil.getMateriaLTemplateByMaterialCode(materialCode);
            Dimension cuttingDimension = materialTemplate.getMaterialType().getCuttingDimensionTemplate();

            providerAssortmentUtil.ensureAssortmentIsNotNull(provider);

            if (itemWhereToAdd == null) {
                itemWhereToAdd = providerAssortmentUtil.createNewEmptyMaterialAssortment(materialTemplate, materialCode);
                provider.getAssortment().getMaterialAssortment().add(itemWhereToAdd);
            }
            addManyMaterialAssortmentsPositionToAssortmentItem(itemWhereToAdd, addedAssortment.getMaterialAssortmentPositions(), cuttingDimension);
        }
    }

    private void addManyMaterialAssortmentsPositionToAssortmentItem(MaterialAssortmentItem itemWhereToAdd,
            List<MaterialAssortmentPosition> materialAssortmentPositions, Dimension cuttingDimension) {

        for (MaterialAssortmentPosition materialAssortmentPosition : materialAssortmentPositions) {
            // create cutting size for given string size
            String origSize = materialAssortmentPosition.getSalesLength().getOrigSize();
            MoneyERP salesPrice = materialAssortmentPosition.getSalesPrice();
            SellUnit sellUnit = materialAssortmentPosition.getSellUnit();

            CuttingSize salesLength = CuttingSizeManager.createCuttingSize(cuttingDimension, origSize);
            providerAssortmentUtil.addMaterialAssortmentPositionToAssortmentItem(itemWhereToAdd, salesLength, salesPrice, sellUnit);
        }
    }

    public void handleRemovedMaterialAssortment(List<MaterialAssortmentItem> removed, ProviderEntity provider) {
        for (MaterialAssortmentItem removedAssortment : removed) {
            String positionId = removedAssortment.getPositionId();
            provider.getAssortment().getMaterialAssortment()
                    .removeIf(ma -> ma.getPositionId().equals(positionId));
        }
    }

    private void handleRemovedMaterialAssortmentPosition(MaterialAssortmentItem itemFromToRemove, List<MaterialAssortmentPosition> removedPositions) {
        for (MaterialAssortmentPosition removedPosition : removedPositions) {
            String removedPositionId = removedPosition.getPositionId();
            itemFromToRemove.getMaterialAssortmentPositions().removeIf(pos -> pos.getPositionId().equals(removedPositionId));
        }
    }

}
