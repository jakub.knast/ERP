/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProviderAssortment {
    
    private List<WareAssortment> wareAssortment = new ArrayList();
    private List<MaterialAssortmentItem> materialAssortment = new ArrayList();

    public void validateMe() {
        for (WareAssortment w : wareAssortment) {
            w.validateMe();
        }
        for (MaterialAssortmentItem m : materialAssortment) {
            m.validateMe();
        }
    }
}
