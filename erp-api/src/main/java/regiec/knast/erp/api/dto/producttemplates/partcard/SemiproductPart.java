/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import java.util.ArrayList;
import java.util.List;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.producttemplates.ProductTemplateDTO;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class SemiproductPart extends SemiproductPartBase {

    private String positionId;
    @Reference
    private ProductTemplateEntity productTemplate;

    public static List<SemiproductPartDTO> semiproductPartsListToDto(List<SemiproductPart> semiproductsParts) {
        List<SemiproductPartDTO> ret = new ArrayList();
        if (semiproductsParts != null) {
            for (final SemiproductPart sptPart : semiproductsParts) {
                ret.add(sptPart.semiproductPartToDTO());
            }
        }
        return ret;
    }

    public SemiproductPartDTO semiproductPartToDTO() {
        final SemiproductPartDTO semiproductPartDTO = new SemiproductPartDTO();
        BeanUtilSilent.translateBean(this, semiproductPartDTO);
        semiproductPartDTO.setProductTemplate((ProductTemplateDTO)this.getProductTemplate().convertToDTO());
        semiproductPartDTO.setProductTemplateId(semiproductPartDTO.getProductTemplate().getId());
        return semiproductPartDTO;
    }
}
