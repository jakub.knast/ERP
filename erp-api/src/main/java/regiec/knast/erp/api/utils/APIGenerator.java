/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class APIGenerator {

    private final String sep = File.separator;

    public String newFieldName = "DOCUMENT";
    
    public String serviceName = upperCamelCase(newFieldName);
    public String serviceName2 = lowerCamelCase(newFieldName);
    public String service_ENUM_NAME = newFieldName;
    public String fooServiceName = "Seller";
    public String fooServiceName2 = "seller";
    public String foo_ENUM_NAME = "SELLER";
    public String projectLocation = "C:/git/ERP/erp-api/src/main/java";
    public String projectPackage = "regiec.knast.erp.api";
    public String projectPackagePath = "regiec/knast/erp/api";

    private String upperCamelCase(String name){
        List<String> splited = Arrays.asList(name.split("_"));
        return splited.stream()
                .map(s -> StringUtils.capitalize(s.toLowerCase()))
                .collect(Collectors.joining());
    }
   
    private String lowerCamelCase(String name){        
        List<String> splited = Arrays.asList(name.split("_"));
        
        if(splited.size() == 1){
           return splited.get(0).toLowerCase();
        }else{
           return splited.get(0).toLowerCase() + splited.stream()
                .skip(1)
                .map(s -> StringUtils.capitalize(s.toLowerCase()))
                .collect(Collectors.joining());
        }
         
    }
    public static void main(String[] args) throws Exception {
        APIGenerator aPIGenerator = new APIGenerator();
        aPIGenerator.copyRequests();
        aPIGenerator.copyBases();
        aPIGenerator.copyEntities();
        aPIGenerator.copyEntitiesContainer();
        aPIGenerator.copyConverter();
        aPIGenerator.copyService();
        aPIGenerator.copyDAO();
        aPIGenerator.copyDAOInterface();
        aPIGenerator.copyState();
        aPIGenerator.bindService();
        aPIGenerator.bindDAO();
        aPIGenerator.bindInDAOHandler();
        aPIGenerator.addEntityTypeEnum();
        aPIGenerator.addEntityTypeToClassMapping();
        aPIGenerator.addEntityTypeToEntityStateClassMapping();

    }

    public void copyRequests() throws Exception {
        String fromPackage = "dto" + sep + fooServiceName2;
        String toPackage = "dto" + sep + serviceName2;
        copyFile("AddSellerRequest.java", fromPackage, toPackage);
        copyFile("GetSellerRequest.java", fromPackage, toPackage);
        copyFile("RemoveSellerRequest.java", fromPackage, toPackage);
        copyFile("RestoreSellerRequest.java", fromPackage, toPackage);
        copyFile("SellerDTO.java", fromPackage, toPackage);
        copyFile("SellersListDTO.java", fromPackage, toPackage);
        copyFile("UpdateSellerRequest.java", fromPackage, toPackage);
    }

    public void copyBases() throws Exception {
        String fromPackage = "model/bases";
        String toPackage = "model/bases";
        copyFile("SellerBase.java", fromPackage, toPackage);
    }

    public void copyEntities() throws Exception {
        String fromPackage = "entities";
        String toPackage = fromPackage;
        copyFile("SellerEntity.java", fromPackage, toPackage);
    }

    public void copyEntitiesContainer() throws Exception {
        String fromPackage = "entities/containers";
        String toPackage = fromPackage;
        copyFile("SellerEntitiesList.java", fromPackage, toPackage);
    }

    public void copyConverter() throws Exception {
        String fromPackage = "converters";
        String toPackage = fromPackage;
        copyFile("SellerConverters.java", fromPackage, toPackage);
    }

    public void copyService() throws Exception {
        String fromPackage = "business";
        String toPackage = fromPackage;
        copyFile("SellerService.java", fromPackage, toPackage);
    }

    public void copyDAO() throws Exception {
        String fromPackage = "dao";
        String toPackage = fromPackage;
        copyFile("SellerDAO.java", fromPackage, toPackage);
    }

    public void copyDAOInterface() throws Exception {
        String fromPackage = "dao/interfaces";
        String toPackage = fromPackage;
        copyFile("SellerDAOInterface.java", fromPackage, toPackage);
    }

    public void copyState() throws Exception {
        String fromPackage = "model/states";
        String toPackage = fromPackage;
        copyFile("SellerState.java", fromPackage, toPackage);
    }

    public void bindService() throws Exception {
        String bindServiceBaseString = "//Here_will_be_insert_binding_service";
        //  String importServiceBaseString = "//Here_will_be_insert_import_service";
        String bindServiceReplacementString = "clases.add(" + serviceName + "Service.class);\n" + bindServiceBaseString;
        //  String importServiceReplacementString = "import regiec.knast.erp.api.business." + serviceName + "Service;\n" + importServiceBaseString;
        File file = new File(projectLocation + sep + projectPackagePath + sep + "ERPClassRegistry.java");
        replaceInFile(file, bindServiceBaseString, bindServiceReplacementString);
        //    replaceInFile(file, importServiceBaseString, importServiceReplacementString);
    }

    public void bindDAO() throws Exception {
        String bindDAOBaseString = "//Here_will_be_insert_binding_dao";
        //   String importDAOInterfaceBaseString = "//Here_will_be_insert_import_daoInterface";
        //   String importDAOBaseString = "//Here_will_be_insert_import_dao";
        String bindDAOReplacementString = "BIND_MAP.put(" + serviceName + "DAOInterface.class, " + serviceName + "DAO.class);\n" + bindDAOBaseString;
        //  String importDAOInterfaceReplacementString = "import regiec.knast.erp.api.dao.interfaces." + serviceName + "DAOInterface;\n" + importDAOInterfaceBaseString;
        //  String importDAOReplacementString = "import regiec.knast.erp.api.dao." + serviceName + "DAO;\n" + importDAOBaseString;
        File file = new File(projectLocation + sep + projectPackagePath + sep + "MyBinder.java");
        replaceInFile(file, bindDAOBaseString, bindDAOReplacementString);
        //  replaceInFile(file, importDAOInterfaceBaseString, importDAOInterfaceReplacementString);
        //  replaceInFile(file, importDAOBaseString, importDAOReplacementString);
    }

    public void bindInDAOHandler() throws Exception {
        String injectBaseString = "//Here_will_be_insert_dao_inject";
        String bindBaseString = "//Here_will_be_insert_dao_binding";
        String injectReplacementString = "    @Inject\n    private " + serviceName + "DAOInterface " + serviceName2 + "DAO;\n" + injectBaseString;
        String bindReplacementString = "            case " + service_ENUM_NAME + ":\n" + "                return " + serviceName2 + "DAO;\n" + bindBaseString;
        File file = new File(projectLocation + sep + projectPackagePath + sep + "utils/DaoHandler.java");
        replaceInFile(file, injectBaseString, injectReplacementString);
        replaceInFile(file, bindBaseString, bindReplacementString);
    }

    public void addEntityTypeEnum() throws Exception {
        String addEntityTypeEnumBaseString = "//Here_will_be_insert_entityType";
        String addEntityTypeEnumReplacementString = service_ENUM_NAME + ",\n" + addEntityTypeEnumBaseString;
        File file = new File(projectLocation + sep + projectPackagePath + sep + "model/EntityType.java");
        replaceInFile(file, addEntityTypeEnumBaseString, addEntityTypeEnumReplacementString);
    }

    public void addEntityTypeToClassMapping() throws Exception {
        String addEntityTypeToClassMappingBaseString = "//Here_will_be_insert_entityType_to_entityClass_mapping";
        String addEntityTypeToClassMappingReplacementString = "CLASS_MAP.put(EntityType." + service_ENUM_NAME + ", " + serviceName + "Entity.class);" + "\n" + addEntityTypeToClassMappingBaseString;
        File file = new File(projectLocation + sep + projectPackagePath + sep + "utils/EntityClassToEntityTypeCache.java");
        replaceInFile(file, addEntityTypeToClassMappingBaseString, addEntityTypeToClassMappingReplacementString);
    }

    public void addEntityTypeToEntityStateClassMapping() throws Exception {
        String addEntityTypeToClassMappingBaseString = "//Here_will_be_insert_entityType_to_EntityStateClass_mapping";
        String addEntityTypeToClassMappingReplacementString = "CLASS_MAP.put(EntityType." + service_ENUM_NAME + ", " + serviceName + "State.class);" + "\n" + addEntityTypeToClassMappingBaseString;
        File file = new File(projectLocation + sep + projectPackagePath + sep + "utils/EntityTypeToStates_Map.java");
        replaceInFile(file, addEntityTypeToClassMappingBaseString, addEntityTypeToClassMappingReplacementString);
    }

    public void copyFile(String fileName, String fromPackage, String toPackage) throws Exception {
        String resourcePath = "/basemodel/" + fromPackage + sep + fileName;
        System.out.println("copy resource:");
        System.out.println("from : " + resourcePath);
        final InputStream addSellerRequestStream = APIGenerator.class.getResourceAsStream(resourcePath);
        String addSellerRequestString = IOUtils.toString(addSellerRequestStream);
        addSellerRequestString = StringUtils.replace(addSellerRequestString, fooServiceName, serviceName);
        addSellerRequestString = StringUtils.replace(addSellerRequestString, fooServiceName2, serviceName2);
        addSellerRequestString = StringUtils.replace(addSellerRequestString, foo_ENUM_NAME, service_ENUM_NAME);

        String newFileName = StringUtils.replace(fileName, fooServiceName, serviceName);
        File file = new File(projectLocation + sep + projectPackagePath + sep + toPackage + sep + newFileName);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        System.out.println("to   : " + file.getAbsolutePath());
        FileOutputStream fos = new FileOutputStream(file);
        IOUtils.write(addSellerRequestString, fos);
    }

    public void replaceInFile(File file, String from, String to) throws Exception {

        InputStream in = new FileInputStream(file);
        String fileContent = IOUtils.toString(in);

        fileContent = StringUtils.replace(fileContent, from, to);

        System.out.println("to   : " + file.getAbsolutePath());
        FileOutputStream fos = new FileOutputStream(file);
        IOUtils.write(fileContent, fos);
    }
}
