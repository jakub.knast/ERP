/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities.containers;

import org.jaxygen.collections.PartialArrayList;
import regiec.knast.erp.api.entities.WareTemplateEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class WareTemplateEntitiesList extends PartialArrayList<WareTemplateEntity> {

}
