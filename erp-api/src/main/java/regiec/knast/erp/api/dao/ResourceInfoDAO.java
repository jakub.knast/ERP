/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ResourceInfoDAOInterface;
import regiec.knast.erp.api.entities.ResourceInfoEntity;
import regiec.knast.erp.api.entities.containers.ResourceInfoEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ResourceInfoDAO extends DAOBase<ResourceInfoEntity, ResourceInfoEntitiesList> implements ResourceInfoDAOInterface {

    private final Datastore datastore;

    @Inject
    public ResourceInfoDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }
}
