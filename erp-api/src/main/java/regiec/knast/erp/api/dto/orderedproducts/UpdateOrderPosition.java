/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.orderedproducts;

import java.util.Date;
import java.util.List;
import org.seenmodit.core.interfaces.UpdatePosition;
import regiec.knast.erp.api.model.MoneyERP;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
//@lombok.Getter
//@lombok.Setter
@lombok.NoArgsConstructor
//@lombok.AllArgsConstructor
public class UpdateOrderPosition extends AddOrderPosition implements UpdatePosition {

    public UpdateOrderPosition(String productTemplateId, String orderPositionId, String positionOnOrder, int count, MoneyERP piecePrice, MoneyERP piecePriceWithDiscount, 
            Double discount, String comments, Date deliveryDate, String productionOrderId, int disposedCount, int toDisposeCount, List<String> dispositionsId) {
        
        super(productTemplateId, orderPositionId, positionOnOrder, count, piecePrice, piecePriceWithDiscount, discount, comments, deliveryDate, productionOrderId, disposedCount, toDisposeCount, dispositionsId);
    }

    @Override
    public String getPositionId() {
        return getOrderPositionId();
    }

    @Override
    public void setPositionId(String positionId) {
        setOrderPositionId(positionId);
    }
}
