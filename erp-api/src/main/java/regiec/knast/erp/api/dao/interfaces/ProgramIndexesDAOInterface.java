/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ProgramIndexesDAOInterface {

    Long nextIndex(EntityType indexType);
    
    Long nextIndexOrReserInNewYear(EntityType indexType);
    
    Long nextIndexOrReserInNewDay(EntityType indexType);

}
