/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.productionplan;

import java.util.ArrayList;
import regiec.knast.erp.api.interfaces.EntityBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ProductionPlanCompatibileEntity extends EntityBase {

    public ArrayList<ProductionPlan> getProductionPlan();

    public void setProductionPlan(ArrayList<ProductionPlan> productionPlan);

    public String getId();
}
