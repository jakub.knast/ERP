/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import regiec.knast.erp.api.entities.ResourceInfoEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ResourceCompatibile {

    public ResourceInfoEntity getResourceInfo();

    public void setResourceInfo(ResourceInfoEntity resourceInfoEntity);
}
