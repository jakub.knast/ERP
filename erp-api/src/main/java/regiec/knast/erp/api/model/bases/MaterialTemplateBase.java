/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.List;
import regiec.knast.erp.api.model.MaterialKind;
import regiec.knast.erp.api.model.MaterialUnit;
import regiec.knast.erp.api.model.Norm;
import regiec.knast.erp.api.model.SteelGrade;
import regiec.knast.erp.api.model.Tolerance;
import regiec.knast.erp.api.model.states.MaterialTemplateState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialTemplateBase extends ObjectWithDate {

    private String materialCode;
    private Double meterToKgRatio;
    //  private String materialSpecies;
    private MaterialKind materialKind;
    private SteelGrade steelGrade;
    private String particularDimension;
    private MaterialUnit unit;
    private Tolerance tolerance;
    private Norm norm;
    private List<String> materialUsages;    
    private String comments;
    private MaterialTemplateState state = MaterialTemplateState.NORMAL;
    private boolean removed;
}
