/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.List;
import regiec.knast.erp.api.model.Norm;
import regiec.knast.erp.api.model.states.WareTemplateState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WareTemplateBase extends ObjectWithDate {

    private String wareCode;
    private String name;
    private Norm norm;
    private Double kgToPieceRatio;
    private List<String> wareUsages;
    private String comments;
    private WareTemplateState state = WareTemplateState.NORMAL;
    private boolean removed;
}
