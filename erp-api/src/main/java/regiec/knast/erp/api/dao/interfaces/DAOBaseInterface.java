/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import com.mongodb.gridfs.GridFSDBFile;
import java.io.InputStream;
import org.jaxygen.collections.PartialArrayList;
import regiec.knast.erp.api.dao.util.OneByFilterResponse;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.interfaces.ConvertableEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 * @param <ENTITY>
 * @param <LIST>
 */
public interface DAOBaseInterface<ENTITY extends ConvertableEntity, LIST extends PartialArrayList<ENTITY>> {

    ENTITY add(ENTITY entity);

    ENTITY add(ENTITY entity, String id);

    ENTITY get(String id);

    ENTITY update(ENTITY entity);

    int updateOneField(String entityId, String fieldName, Object value);

    int remove(String id);

    Boolean isEntityExist(Class<ENTITY> clazz, String id);
    
    Boolean isEntityExist(Class<ENTITY> clazz, String idFieldName, String id);

    LIST list();

    LIST list(PaginableFilterWithCriteriaRequest paginableFilterWithCriteria);

    OneByFilterResponse<ENTITY> getOneByFilter(PaginableFilterWithCriteriaRequest paginableFilterWithCriteria);

    String addFile(InputStream inputStream, String fileName);

    GridFSDBFile getFileById(String fileId);

    void removeFile(String fileId);
}
