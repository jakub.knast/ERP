/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.material;

import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTO;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.bases.MaterialBase;
import regiec.knast.erp.api.model.states.MaterialState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialDTO extends MaterialBase {

    private String id;
    private CuttingSize length;
    private MaterialTemplateDTO materialTemplate;
    private String materialTemplateId;    
    private MaterialState state;
    private boolean removed;

}
