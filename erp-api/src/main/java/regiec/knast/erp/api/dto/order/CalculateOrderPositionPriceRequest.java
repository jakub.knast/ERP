/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.order;

import regiec.knast.erp.api.model.MoneyERP;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class CalculateOrderPositionPriceRequest {
    
    private MoneyERP piecePrice;
    private Integer count;
    private Double discount;
}