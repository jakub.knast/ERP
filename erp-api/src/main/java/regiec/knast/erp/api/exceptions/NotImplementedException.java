/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.exceptions;

import com.google.common.collect.ImmutableMap;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author fbrzuzka
 */
public class NotImplementedException extends ConversionErrorWithCode{
    
    public NotImplementedException() {
        super("Unsupported", ExceptionCodes.NOT_IMPLEMENTED, null);
    }
    
    public NotImplementedException(String functionName) {
        super("Unsupported", ExceptionCodes.NOT_IMPLEMENTED, ImmutableMap.of("functionName", functionName));
    }
    
}
