/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.DevelopmentDAOInterface;
import regiec.knast.erp.api.entities.DevelopmentEntity;
import regiec.knast.erp.api.entities.containers.DevelopmentEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class DevelopmentDAO extends DAOBase<DevelopmentEntity, DevelopmentEntitiesList> implements DevelopmentDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = DevelopmentEntity.class;

    @Inject
    public DevelopmentDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

}
