/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.orderedproducts;

import org.mongodb.morphia.annotations.Reference;
import org.seenmodit.core.interfaces.Position;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.interfaces.IndexableEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class OrderPosition extends OrderPositionBase implements Position, OrdinaryOrderPosition, DispositionOrderPosition , IndexableEntity{

    @Reference
    private ProductEntity product;

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }
    

    @Override
    public String getPositionId() {
        return getOrderPositionId();
    }

    @Override
    public void setPositionId(String positionId) {
        setOrderPositionId(positionId);
    }

}
