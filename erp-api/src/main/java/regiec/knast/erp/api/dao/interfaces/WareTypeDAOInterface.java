/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.WareTypeEntity;
import regiec.knast.erp.api.entities.containers.WareTypeEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface WareTypeDAOInterface extends DAOBaseInterface<WareTypeEntity, WareTypeEntitiesList> {

}
