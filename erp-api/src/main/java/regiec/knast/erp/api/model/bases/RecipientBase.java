/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.model.states.RecipientState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class RecipientBase extends ObjectWithDate {

    private String name;
    private String nip;
    private String street;
    private String postalCode;
    private String city;
    private String email;
    private String phone;
    private int daysForPayment;
    private RecipientState state;
    private String comments;
    private boolean removed;
}
