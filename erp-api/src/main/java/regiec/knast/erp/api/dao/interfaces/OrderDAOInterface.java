/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.containers.OrderEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface OrderDAOInterface extends DAOBaseInterface<OrderEntity, OrderEntitiesList> {

}
