/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.containers.MaterialTemplateEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MaterialTemplateDAO extends DAOBase<MaterialTemplateEntity, MaterialTemplateEntitiesList> implements MaterialTemplateDAOInterface {

    private final Datastore datastore;

    @Inject
    public MaterialTemplateDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

    @Override
    public MaterialTemplateEntity getByName(String name) {
        MaterialTemplateEntity entity = datastore.createQuery(MaterialTemplateEntity.class).disableValidation().field("name").equal(name).get();
        return entity;
    }
}
