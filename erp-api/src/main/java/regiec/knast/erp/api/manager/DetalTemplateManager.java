/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.producttemplates.partcard.PartCard;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductEntitiesList;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;
import regiec.knast.erp.api.model.Constraint;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CriteriaFilter;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class DetalTemplateManager {

    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private ProductDAOInterface productDAO;
    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;

    public void ensurePartCartListExist(ProductTemplateEntity dte) {
        if (dte.getPartCard() == null) {
            dte.setPartCard(new PartCard());
        }
        if (dte.getPartCard().getMaterialParts() == null) {
            dte.getPartCard().setMaterialParts(new ArrayList());
        }
        if (dte.getPartCard().getWareParts() == null) {
            dte.getPartCard().setWareParts(new ArrayList());
        }
        if (dte.getPartCard().getSemiproductParts() == null) {
            dte.getPartCard().setSemiproductParts(new ArrayList());
        }
    }

    public void updateProducts(ProductTemplateEntity updatedEntity) {
        ProductEntitiesList productEntitiesList = getProductsWithProductTemplate(updatedEntity);
        for (ProductEntity productEntity : productEntitiesList) {
            productEntity.setProductTemplate(updatedEntity);
            productDAO.update(productEntity);
        }
    }

    public ProductEntitiesList getProductsWithProductTemplate(ProductTemplateEntity updatedEntity) {
        return getProductsWithProductTemplate(updatedEntity.getId());
    }

    public ProductEntitiesList getProductsWithProductTemplate(String productTemplateEntityId) {
        PaginableFilterWithCriteriaRequest req = new PaginableFilterWithCriteriaRequest();
        CriteriaFilter criteriaFilters = new CriteriaFilter();
        req.setCriteriaFilters(criteriaFilters);
        criteriaFilters.setConstraints(Lists.newArrayList(new Constraint("productTemplate.id", productTemplateEntityId, ConstraintType.EQUALS)));
        return productDAO.list(req);
    }

    private void updateTimesInParentProducts(ProductTemplateEntity productTemplate) {
        ProductTemplateEntitiesList li = productTemplateDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder().addConstraint("partCard.semiproductParts.productTemplate", productTemplate.getId(), ConstraintType.EQUALS).buildCriteriaFilter()
                .build());
        for (ProductTemplateEntity pt : li) {
            System.out.println("  pt: " + pt.getName());
            BuildingTimeCalculator.updateBuildingTimes(pt);
            productTemplateDAO.update(pt);
        }
    }

    public void updateTimesInParentDetals(ProductTemplateEntity productTemplate) {
        BuildingTimeCalculator.updateBuildingTimes(productTemplate);
        productTemplateDAO.update(productTemplate);
        updateTimesInParentProducts(productTemplate);
    }

    public void handleValidationStatus(ProductTemplateEntity dte) {
        List<String> validationList = validatorDetal.validateProductTemplate(dte);
        boolean valid = validationList.isEmpty();
        dte.setValidationStatus(valid ? "tak" : "nie");
        dte.setValidationMessage(validationList);
    }

    public List<String> findProductTemplateUsage(String productTemplateId) {
        List<String> renew = new ArrayList();
        ProductTemplateEntitiesList usages = productTemplateDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("partCard.semiproductParts.productTemplate", productTemplateId, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());
        for (ProductTemplateEntity usage : usages) {
            renew.add("numer: " + usage.getProductNo() + ", nazwa: " + usage.getName());
        }
        return renew;
    }
    public void refreshUsage(String productTemplateId){
        List<String> renew = findProductTemplateUsage(productTemplateId);
        productTemplateDAO.updateOneField(productTemplateId, "ptUsages", renew);
    }
    
}
