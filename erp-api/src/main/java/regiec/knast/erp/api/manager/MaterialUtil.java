/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Map;
import regiec.knast.erp.api.entities.MaterialEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MaterialUtil {

    @Inject
    public MaterialUtil() {

    }

    public Map<String, Object> buildRemoveConsequencesMap(MaterialEntity entity) {
        return ImmutableMap.<String, Object>builder()
                .put("entityType", MaterialEntity.class.getSimpleName())
                .put("id", entity.getId())
                .put("materialCode", entity.getMaterialTemplate().getMaterialCode())
                .put("count", entity.getCount())
                .put("name", entity.getMaterialTemplate().getMaterialType().getName())
                .put("particularDimension", entity.getMaterialTemplate().getParticularDimension())
                .build();
    }
    
}
