/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.utils;

import com.google.gson.Gson;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import org.jaxygen.dto.Downloadable;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.ContextInitializer;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;
import regiec.knast.erp.api.model.DocumentInterface;
import regiec.knast.erp.api.model.wzgenerator.DownloadableStream;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public abstract class PdfGenerator<DATA extends DocumentInterface> {

    public final Gson g = ContextInitializer.getInstance().GSON();

    public abstract void generateToStream(OutputStream pdfOutputStream) throws PdfGeneratorException;

    public abstract DATA getData();

    public <T extends Object> T buildObj(Class<T> type) {
        try {
            return ObjectBuilderFactory.instance().create(type);
        } catch (ObjectCreateError ex) {
            String className = type != null ? type.getCanonicalName() : "";
            throw new ERPInternalError("Cannot create instance of class '" + className + "'", ex);
        }
    }
    
    public Downloadable toDownloadable(String fileName) {
        ByteArrayOutputStream pdfstream = new ByteArrayOutputStream();
        this.generateToStream(pdfstream);
        byte[] bs = pdfstream.toByteArray();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bs);
        DownloadableStream downloadable = new DownloadableStream(byteArrayInputStream, fileName);
        downloadable.setDisposition(Downloadable.ContentDisposition.attachment);
        return downloadable;
    }
}
