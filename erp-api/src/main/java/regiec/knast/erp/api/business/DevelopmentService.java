/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.DevelopmentDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.development.AddDevelopmentRequest;
import regiec.knast.erp.api.dto.development.DevelopmentDTO;
import regiec.knast.erp.api.dto.development.DevelopmentsListDTO;
import regiec.knast.erp.api.dto.development.RemoveDevelopmentRequest;
import regiec.knast.erp.api.dto.development.UpdateDevelopmentRequest;
import regiec.knast.erp.api.entities.DevelopmentEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.DevelopmentState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class DevelopmentService extends AbstractService<DevelopmentEntity, DevelopmentDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.DEVELOPMENT;

    @Inject
    private DevelopmentDAOInterface developmentDAO;
    @Inject
    private Validator validator;

    @NetAPI
    public DevelopmentDTO addDevelopment(AddDevelopmentRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("name", request.getName(), null, MY_ENTITY_TYPE).throww();
        validator.validateUniqueness("nip", request.getNip(), null, MY_ENTITY_TYPE).throww();

        DevelopmentEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new DevelopmentEntity());
        entity.setState(DevelopmentState.NORMAL);
        DevelopmentEntity entity1 = developmentDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public DevelopmentDTO getDevelopment(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public DevelopmentDTO updateDevelopment(UpdateDevelopmentRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("name", request.getName(), id, MY_ENTITY_TYPE).throww();
        validator.validateUniqueness("nip", request.getNip(), id, MY_ENTITY_TYPE).throww();
        DevelopmentEntity entity = developmentDAO.get(id);
        validator.validateNotNull(entity).throww();
        DevelopmentEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new DevelopmentEntity());
        BeanUtilSilent.translateBean(request, updatedEntity);
        developmentDAO.update(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeDevelopment(RemoveDevelopmentRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        DevelopmentEntity developmentEntity = developmentDAO.get(id);
        if (request.getConfirmed()) {
            developmentEntity.setRemoved(true);
            developmentDAO.update(developmentEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(developmentEntity));
            return new RemoveResponse(false, null);
        }
    }

    @NetAPI
    public DevelopmentsListDTO listDevelopments(PaginableFilterWithCriteriaRequest request) {
        return (DevelopmentsListDTO) super.list(request);
    }
}
