/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public enum WareDimension {

    brak,
    dł,
    fi,
    Gr,
    A,
    AxA,
    AxB,
    fixGr,
    AxBxGr,
    fixC,
    AxBxC
}
