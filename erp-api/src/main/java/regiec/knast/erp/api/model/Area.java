/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class Area {

    private Double a;
    private Double b;

    @Override
    public String toString() {
        return a + "x" + b;
    }

    public String toOrigSize() {
        return a + "x" + b;
    }

    boolean smallerOrEqualThan(Area area) {
        return this.a <= area.a && this.b <= area.b;
    }

    boolean smallerThan(Area area) {
        return this.a < area.a && this.b < area.b
                || this.a <= area.a && this.b < area.b
                || this.a < area.a && this.b <= area.b;
    }
}
