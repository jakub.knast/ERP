/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.otherstuff;

import java.util.Set;
import java.util.TreeSet;
import regiec.knast.erp.api.model.MaterialKind;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class MaterialKindDTO extends TreeSet<String> {

    public static MaterialKindDTO fromNormSet(Set<MaterialKind> materialKinds) {
        MaterialKindDTO ret = new MaterialKindDTO();
        for (MaterialKind materialKind : materialKinds) {
            ret.add(materialKind.toString());
        }
        return ret;
    }

}
