/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.shoppingCart;

import regiec.knast.erp.api.model.bases.ShoppingCartBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ShoppingCartDTO extends ShoppingCartBase {

    private String id;


}
