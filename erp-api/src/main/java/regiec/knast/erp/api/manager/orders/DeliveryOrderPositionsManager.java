/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.orders;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.seenmodit.core.exceptions.model.Pair;
import regiec.knast.erp.api.dao.interfaces.DeliveryOrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProviderDAOInterface;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderDTO;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderPosition;
import regiec.knast.erp.api.dto.deliveryorder.MoveDeliveryOrderPositionToOtherProviderRequest;
import regiec.knast.erp.api.entities.DeliveryOrderEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.exceptions.NotImplementedException;
import regiec.knast.erp.api.manager.ProviderAssortmentUtil;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;
import regiec.knast.erp.api.model.states.DeliveryOrderState;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class DeliveryOrderPositionsManager {

    @Inject
    private DeliveryOrderDAOInterface deliveryOrderDAO;
    @Inject
    private ProviderDAOInterface providerDAO;
    @Inject
    private ProviderAssortmentUtil providerAssortmentUtil;
    @Inject
    private DeliveryOrderManager deliveryOrderManager;
    @Inject
    private Validator validator;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;

    public void updateMaterialsOnPositionOnDeliveryOrder(MaterialEntity m, Set<String> idsOfAddedMaterials) {
        // we update only if there is newly added materials
        if (idsOfAddedMaterials.size() > 0) {
            DeliveryOrderEntity dor = deliveryOrderDAO.get(m.getDeliveryOrderId());
            for (DeliveryOrderPosition pos : dor.getPositions()) {
                if (pos.getMaterialsIds().contains(m.getId())) {
                    pos.getMaterialsIds().addAll(idsOfAddedMaterials);
                    deliveryOrderDAO.update(dor);
                    break;
                }
            }
        }
    }

    public void handleAdded(List<DeliveryOrderPosition> added, DeliveryOrderEntity deliveryOrder) {
        for (DeliveryOrderPosition pos : added) {
            updateAdditionalFieldsInDeliveryOrderPosition(pos);
            deliveryOrder.getPositions().add(pos);
        }
    }

    public void handleRemoved(List<DeliveryOrderPosition> removed, DeliveryOrderEntity deliveryOrder) {
        for (DeliveryOrderPosition toRemove : removed) {
            deliveryOrder.getPositions()
                    .removeIf(pos -> pos.getPositionId().equals(toRemove.getPositionId()));
        }
    }

    public void handleOther(List<Pair<DeliveryOrderPosition, DeliveryOrderPosition>> other, DeliveryOrderEntity deliveryOrder) {
        for (Pair<DeliveryOrderPosition, DeliveryOrderPosition> pair : other) {
            DeliveryOrderPosition origin = pair.getFirst();
            DeliveryOrderPosition updated = pair.getSecond();

            updateAdditionalFieldsInDeliveryOrderPosition(updated);

            for (int i = 0; i < deliveryOrder.getPositions().size(); i++) {
                if (deliveryOrder.getPositions().get(i).getPositionId().equals(origin.getPositionId())) {
                    deliveryOrder.getPositions().set(i, updated);
                    break;
                }
            }
        }
    }

    public void updateAdditionalFieldsInDeliveryOrderPosition(DeliveryOrderPosition position) {
        MaterialTemplateEntity materialTemplate = materialTemplateDAO.get(position.getMaterialTemplateId());
        position.setMaterialCode(materialTemplate.getMaterialCode());
        position.setMaterialName(materialTemplate.getMaterialType().getName());
    }

    public DeliveryOrderDTO confirmDeliveryOrderPosition(String deliveryOrderId, String positionId) {
        validator.validateNotNullAndNotEmpty(deliveryOrderId, ExceptionCodes.NULL_VALUE, "deliveryOrderId").throww();
        validator.validateNotNullAndNotEmpty(positionId, ExceptionCodes.NULL_VALUE, "positionId").throww();

        DeliveryOrderEntity entity = deliveryOrderDAO.get(deliveryOrderId);
        validator.validateNotNull(entity, ExceptionCodes.NULL_VALUE, "deliveryOrderEntity").throww();

        if (entity.getState() != DeliveryOrderState.WAITIN_FOR_APROVEMENT) {
            throw new NotImplementedException("Approvement of position is allow only in state 'waiting for approvement'");
        }

        DeliveryOrderPosition position = entity.getPositions().stream()
                .filter(pos -> pos.getPositionId().equals(positionId))
                .findAny()
                .orElseGet(() -> {
                    return null;
                });
        if (position == null) {
            throw new NotImplementedException("there is no position with that id in that delivery order");
        }
        position.setConfirmed(true);

        boolean allConfirmed = checkIfAllPositionsConfirmed(entity);
        if (allConfirmed) {
            entity.setState(DeliveryOrderState.APROVED);
        }
        DeliveryOrderEntity update = deliveryOrderDAO.update(entity);
        return update.convertToDTO(); 
    }

    private boolean checkIfAllPositionsConfirmed(DeliveryOrderEntity entity) {
        boolean thereIsSomeUnConfiormed = entity.getPositions().stream()
                .anyMatch(pos -> !pos.getConfirmed());
        return !thereIsSomeUnConfiormed;
    }

    public DeliveryOrderDTO moveDeliveryOrderPositionToOtherProvider(MoveDeliveryOrderPositionToOtherProviderRequest request) {

        validator.validateNotNull(request).throww();
        final String deliveryOrderId = request.getDeliveryOrderId();
        final String otherProviderId = request.getOtherProviderId();
        final String otherAssotrmentId = request.getOtherAssotrmentId();
        final String positionId = request.getPositionId();
        validator.validateNotNullAndNotEmpty(deliveryOrderId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "deliveryOrderId").throww();
        validator.validateNotNullAndNotEmpty(otherProviderId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "otherProviderId").throww();
        validator.validateNotNullAndNotEmpty(otherAssotrmentId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "otherAssotrmentId").throww();
        validator.validateNotNullAndNotEmpty(positionId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "positionId").throww();

        //1. remove this position from current deliveryOrder        
        DeliveryOrderEntity deliveryOrder = deliveryOrderDAO.get(deliveryOrderId);
        validator.validateNotNull(deliveryOrder, ExceptionCodes.NULL_VALUE, "deliveryOrderEntity").throww();
        Optional<DeliveryOrderPosition> positionOptional = deliveryOrder.getPositions().stream()
                .filter(pos -> pos.getPositionId().equals(positionId))
                .findAny();
        if (!positionOptional.isPresent()) {
            throw new NotImplementedException("there is no position with that id for given deliveryOrder");
        }
        DeliveryOrderPosition position = positionOptional.get();

        boolean removeStatus = deliveryOrder.getPositions().removeIf(pos -> pos.getPositionId().equals(positionId));
        if (!removeStatus) {
            throw new NotImplementedException("there is no position with that id for given deliveryOrder");
        }

        //2. Create new Delivery order specialy for this position        
        ProviderEntity providerEntity = providerDAO.get(otherProviderId);
        validator.validateNotNull(providerEntity, "providerEntity").throww();

        MaterialAssortmentPosition otherAssortment = providerAssortmentUtil.findAssortmentPositionInProvider(providerEntity, otherAssotrmentId);
        validator.validateNotNull(otherAssortment, "otherAssortment").throww();

        DeliveryOrderEntity otherDeliveryOrder = new DeliveryOrderEntity();
        otherDeliveryOrder.setState(DeliveryOrderState.WAITIN_FOR_APROVEMENT);
        otherDeliveryOrder.setNo(deliveryOrderManager.getNextDeliveryOrderNo());
        otherDeliveryOrder.setProviderId(otherProviderId);
        otherDeliveryOrder.setProviderName(providerEntity.getName());

        position.setAssortmentId(otherAssotrmentId);
        position.setConfirmed(false);
        position.setSalesPrice(otherAssortment.getSalesPrice());

        otherDeliveryOrder.setPositions(Lists.newArrayList(position));
        // this "setOrderMail" must be at the end of creating deliveryOrder, it uses added positions and other fields.
        otherDeliveryOrder.setOrderMail(deliveryOrderManager.generateDeliveryOrderMessage(otherDeliveryOrder));

        //4. save delivery orders
        DeliveryOrderEntity ret;
        // jesli stare jest puste, to niech nowe weźmie Id starego.
        if (deliveryOrder.getPositions().isEmpty()) {
            otherDeliveryOrder.setId(deliveryOrder.getId());
            ret = deliveryOrderDAO.update(otherDeliveryOrder);
        } else {
            ret = deliveryOrderDAO.update(deliveryOrder);
            deliveryOrderDAO.add(otherDeliveryOrder);
        }
        return ret.convertToDTO();
    }
}
