/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.dto.materialDemand.TopProductInfo;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.states.MaterialDemandState;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialDemandBase extends ObjectWithDate {

    private MaterialDemandState state;
    @ErpIdEntity(entityClass = ProductionOrderEntity.class)
    private String productionOrderId;
    private TopProductInfo topProductInfo;
    private Integer count; 
    private CuttingSize cuttingSize;
    private Integer satisfied;
    private Integer unsatisfied;
    private boolean removed;
}
