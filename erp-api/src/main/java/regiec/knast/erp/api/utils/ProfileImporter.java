/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import regiec.knast.erp.api.business.apitests.xmlimport.profile.Profile;
import regiec.knast.erp.api.business.apitests.xmlimport.profile.ProfileRoot;
import regiec.knast.erp.api.dto.materialtemplates.AddMaterialTemplateRequest;
import regiec.knast.erp.api.model.MaterialUnit;
import regiec.knast.erp.api.model.Norm;
import regiec.knast.erp.api.model.SteelGrade;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class ProfileImporter {

    private String profiletypeId = "foooooooo";
    private static Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    public ProfileImporter(String profiletypeId) {
        this.profiletypeId = profiletypeId;
    }

    public List<AddMaterialTemplateRequest> giveMeProfiles() throws JAXBException {
        ProfileRoot importProfiles = importProfiles();
        List<AddMaterialTemplateRequest> parsedProfiles = createProfilesAddRequests(importProfiles.getMaterial());
        return parsedProfiles;
    }

    public ProfileRoot importProfiles() throws JAXBException {

        JAXBContext jc = JAXBContext.newInstance(ProfileRoot.class);
        Unmarshaller u = jc.createUnmarshaller();
        //  XMLMaterialRoot manager = (XMLMaterialRoot) u.unmarshal(ClassLoader.getSystemResourceAsStream("m.xml"));
        InputStream is = ProfileImporter.class.getResourceAsStream("/profile.xml");
        ProfileRoot materials = (ProfileRoot) u.unmarshal(is);
        return materials;
    }

    public List<AddMaterialTemplateRequest> createProfilesAddRequests(List<Profile> profiles) {
        List<AddMaterialTemplateRequest> templates = new ArrayList();
        for (Profile p : profiles) {
            if (!Strings.isNullOrEmpty(p.getNazwa())) {
                final String name = p.getNazwa();
                String[] pp = name.split(" ");

                AddMaterialTemplateRequest m = new AddMaterialTemplateRequest();
                m.setMaterialTypeId(profiletypeId);
                String norma = pp[1].replaceAll("_", " ");
                m.setNorm(new Norm(norma.toUpperCase()));
                m.setSteelGrade(new SteelGrade(pp[2].toUpperCase()));
                m.setParticularDimension(pp[3].toUpperCase());
                m.setMaterialCode(p.getKod());
                m.setUnit(MaterialUnit.valueOf(p.getJednostka().toLowerCase()));
                templates.add(m);
            }

        }
        System.out.println(gson.toJson(templates));
        return templates;
    }

    private void objectToXML(File file, Object objectToMarshall) throws Exception {
        file.createNewFile();

        JAXBContext jc = JAXBContext.newInstance(ProfileRoot.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(objectToMarshall, file);

    }
}
