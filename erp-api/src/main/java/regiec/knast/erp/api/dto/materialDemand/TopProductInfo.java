/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.materialDemand;

import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class TopProductInfo {
    
    @ErpIdEntity(entityClass = ProductEntity.class)
    private String productId;
    private String productNo;
    private String productName;

}
