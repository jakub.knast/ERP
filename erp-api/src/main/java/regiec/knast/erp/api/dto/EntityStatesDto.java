/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class EntityStatesDto {
    
    private String entityName;
    private List<StateVerticleDto> verticles = new ArrayList();

    public EntityStatesDto(String entityName) {
        this.entityName = entityName;
    }
}
