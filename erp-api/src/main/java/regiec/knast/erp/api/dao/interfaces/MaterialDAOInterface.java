/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.containers.MaterialEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface MaterialDAOInterface extends DAOBaseInterface<MaterialEntity, MaterialEntitiesList> {

}
