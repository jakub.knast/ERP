/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.materialDemand;

import regiec.knast.erp.api.model.bases.MaterialDemandBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AddMaterialDemandRequest extends MaterialDemandBase {

}
