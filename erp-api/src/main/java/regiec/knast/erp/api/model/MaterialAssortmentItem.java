/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.List;
import org.seenmodit.core.interfaces.Position;
import org.seenmodit.core.interfaces.UpdatePosition;
import regiec.knast.erp.api.validation.ValidationException;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialAssortmentItem implements Position, UpdatePosition{
    
    private String positionId;
    private String materialCode;
    private String materialTypeName;
    private String materialBaseSize;
    private String materialTemplateId;
    private List<MaterialAssortmentPosition> materialAssortmentPositions = new ArrayList();    

    @Override
    public String getPositionId() {
        return positionId;
    }

    @Override
    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    void validateMe() {
        if(Strings.isNullOrEmpty(materialCode)){
            throw new ValidationException("material code is null or empty");
        }
        if(Strings.isNullOrEmpty(materialTypeName)){
            throw new ValidationException("materialtypeName is null or empty");
        }
        if(Strings.isNullOrEmpty(materialBaseSize)){
            throw new ValidationException("material size is null or empty");
        }
        if(Strings.isNullOrEmpty(materialTemplateId)){
            throw new ValidationException("materialTemplateId is null or empty");
        }
        for (MaterialAssortmentPosition ass : materialAssortmentPositions) {
            ass.validateMe();
        }
    }
}
