/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.base.Strings;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlan;
import regiec.knast.erp.api.entities.ProductTemplateEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class BuildingTimeCalculator {

    private static Touple calculateBuildingTime(ProductTemplateEntity spt) {
        Touple t = new Touple();
        if (spt.getProductionPlan() != null) {
            for (ProductionPlan productionPlan : spt.getProductionPlan()) {
                t.tpz += productionPlan.getPreparationTime();
                t.tj += productionPlan.getOperationTime();
            }
        }
        return t;
    }

    private static Touple calculateTotalBuildingTime(ProductTemplateEntity spt) {
        Touple tt = new Touple();
        if (spt.getPartCard() != null && spt.getPartCard().getSemiproductParts() != null) {
            for (SemiproductPart semiproductPart : spt.getPartCard().getSemiproductParts()) {
                ProductTemplateEntity pt = semiproductPart.getProductTemplate();
                tt.tpz += pt.getTotalBuildingTimeTPZ() != null ? pt.getTotalBuildingTimeTPZ() : 0;
                tt.tj += (pt.getTotalBuildingTimeTJ() != null ? pt.getTotalBuildingTimeTJ() : 0) * semiproductPart.getCount();
            }
        }
        Touple t = calculateBuildingTime(spt);
        tt.tpz += t.tpz;
        tt.tj += t.tj;
        return tt;
    }

    private static Double round(double d) {
        double ll = new Double(Math.round(d * 100));
        return ll / 100;
    }

    public static void updateBuildingTimes(ProductTemplateEntity dte) {
        Touple t = calculateBuildingTime(dte);
        dte.setBuildingTimeTPZ(t.tpz);
        dte.setBuildingTimeTJ(t.tj);
        dte.setBuildingTime(round(t.tpz + t.tj));

        Touple tt = calculateTotalBuildingTime(dte);
        dte.setTotalBuildingTimeTPZ(tt.tpz);
        dte.setTotalBuildingTimeTJ(tt.tj);
        dte.setTotalBuildingTime(round(tt.tpz + tt.tj));

        dte.setBuildingTimeStr(buildTimeToString(t));
        dte.setTotalBuildingTimeStr(buildTimeToString(tt));
    }

    private static String buildTimeToString(Touple t) {
        return s(t.tpz + t.tj) + " (" + s(t.tpz) + " + " + s(t.tj) + ")";
    }

    private static String s(double i) {
        double ll = new Double(Math.round(i * 100));
        return String.valueOf(ll / 100);
    }

    public static boolean isRefreshTimesInParentDetailsNeed(TimeLaps timeLaps, ProductTemplateEntity updatedEntity) {
        return !timeLaps.buildingTimeStr.equals(updatedEntity.getBuildingTimeStr())
                || !timeLaps.totalBuildingTimeStr.equals(updatedEntity.getTotalBuildingTimeStr());
    }

    public static TimeLaps buildingTimesTimelaps(ProductTemplateEntity dte) {
        String bt = Strings.nullToEmpty(dte.getBuildingTimeStr());
        String tbt = Strings.nullToEmpty(dte.getTotalBuildingTimeStr());
        return new TimeLaps(bt, tbt);
    }

    static class Touple {
        double tpz = 0.0;
        double tj = 0.0;
    }

    public static class TimeLaps {

        String buildingTimeStr;
        String totalBuildingTimeStr;

        public TimeLaps() {
        }

        public TimeLaps(String buildingTimeStr, String totalBuildingTimeStr) {
            this.buildingTimeStr = buildingTimeStr;
            this.totalBuildingTimeStr = totalBuildingTimeStr;
        }

    }
}
