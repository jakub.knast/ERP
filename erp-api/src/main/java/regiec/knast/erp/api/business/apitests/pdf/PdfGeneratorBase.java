/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests.pdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import regiec.knast.erp.api.business.apitests.GuiceInjector;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class PdfGeneratorBase extends GuiceInjector {

    String command = "start \"\" /max ";

    public void openPdfInViever(File file) {
        executeCommand(command + file.getAbsolutePath());
    }

    public File createFile(File outFile) {
        outFile.getParentFile().mkdirs();
        if (!outFile.exists()) {
            try {
                outFile.createNewFile();
            } catch (IOException ex) {
                throw new PdfGeneratorException("Cannot create output file", ex);
            }
        }
        return outFile;
    }

    private void executeCommand(String command) {
        try {
            ProcessBuilder builder = new ProcessBuilder(
                    //                    "cmd.exe", "/c", "cd \"C:\\Program Files\\Microsoft SQL Server\" && dir");
                    "cmd.exe", "/c", command);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while (true) {
                line = r.readLine();
                if (line == null) {
                    break;
                }
                System.out.println(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(PdfGeneratorBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String filePath(String fileName) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd.hh.mm.ss");
        String now = dt.format(new Date());
        return "C://Users/fbrzuzka/prodGuide/" + fileName + "-" + now + ".pdf";
    }
}
