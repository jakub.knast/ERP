/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.resourceinfo;

import org.jaxygen.dto.collections.PaginableListResponseBaseDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ResourceInfoListDTO extends PaginableListResponseBaseDTO<ResourceInfoDTO> {

}
