/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.WareDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WareTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WareTypeDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.producttemplates.RefreshSthTemplateUsageResponse;
import regiec.knast.erp.api.dto.ware.RemoveWareRequest;
import regiec.knast.erp.api.dto.waretemplates.AddWareTemplateRequest;
import regiec.knast.erp.api.dto.waretemplates.RemoveWareTemplateRequest;
import regiec.knast.erp.api.dto.waretemplates.UpdateWareTemplateRequest;
import regiec.knast.erp.api.dto.waretemplates.WareTemplateDTO;
import regiec.knast.erp.api.dto.waretemplates.WareTemplatesListDTO;
import regiec.knast.erp.api.entities.WareEntity;
import regiec.knast.erp.api.entities.WareTemplateEntity;
import regiec.knast.erp.api.entities.WareTypeEntity;
import regiec.knast.erp.api.entities.containers.WareEntitiesList;
import regiec.knast.erp.api.entities.containers.WareTemplateEntitiesList;
import regiec.knast.erp.api.manager.PartCardManager;
import regiec.knast.erp.api.manager.WareTemplateManager;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class WareTemplateService extends AbstractService<WareTemplateEntity, WareTemplateDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.WARE_TEMPLATE;

    @Inject
    private WareDAOInterface wareDAO;
    @Inject
    private WareTemplateDAOInterface wareTemplateDAO;
    @Inject
    private WareTypeDAOInterface wareTypeDAO;
    @Inject
    private PartCardManager partCardConverters;
    @Inject
    private Validator validator;
    @Inject
    private WareTemplateManager wareTemplateManager;
    @Inject
    private WareService wareService;

    @NetAPI
    public WareTemplateDTO addWareTemplate(AddWareTemplateRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("wareCode", request.getWareCode(), null, EntityType.WARE_TEMPLATE).throww();
        WareTemplateEntity entity = new WareTemplateEntity();
        BeanUtilSilent.translateBean(request, entity);
        WareTypeEntity wareType = wareTypeDAO.get(request.getWareTypeId());
        entity.setWareType(wareType);
        WareTemplateEntity entity1 = wareTemplateDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public WareTemplateDTO getWareTemplate(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public WareTemplateDTO updateWareTemplate(UpdateWareTemplateRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("wareCode", request.getWareCode(), id, EntityType.WARE_TEMPLATE).throww();
        WareTemplateEntity entity = wareTemplateDAO.get(id);
        validator.validateNotNull(entity).throww();
        WareTemplateEntity updatedEntity = new WareTemplateEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);
        wareTemplateDAO.update(updatedEntity);

        WareEntitiesList wareEntitiesList = wareTemplateManager.getWaresWithWareTemplate(id);
        for (WareEntity wareEntity : wareEntitiesList) {
            wareEntity.setWareTemplate(updatedEntity);
            wareDAO.update(wareEntity);
        }
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeWareTemplate(RemoveWareTemplateRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        WareTemplateEntity wareTemplateEntity = wareTemplateDAO.get(id);
        if (request.getConfirmed()) {
            wareTemplateEntity.setRemoved(true);
            if (request.getRemoveRequestFromUp()) {
                wareTemplateEntity.setWareType(wareTypeDAO.get(wareTemplateEntity.getWareType().getId()));
            }
            wareTemplateDAO.update(wareTemplateEntity);
            removeWares(id);
            partCardConverters.removeEntityTemplateFromPartCardCompatibileEntity(MY_ENTITY_TYPE, id);
            return new RemoveResponse(true, null);
        } else {
            //build consequencesMap
            Map<String, Object> consequencesMap = wareTemplateManager.buildRemoveConsequencesMap(wareTemplateEntity);
            return new RemoveResponse(false, consequencesMap);
        }
    }

    private void removeWares(String wareTemplateEntityId) {
        final WareEntitiesList wareEntitiesList = wareTemplateManager.getWaresWithWareTemplate(wareTemplateEntityId);
        RemoveWareRequest removeWareRequest = new RemoveWareRequest("", true, true);
        for (WareEntity wareEntity : wareEntitiesList) {
            removeWareRequest.setId(wareEntity.getId());
            wareService.removeWare(removeWareRequest);
        }
    }

    @NetAPI
    public WareTemplatesListDTO listWareTemplates(PaginableFilterWithCriteriaRequest request) {
        return (WareTemplatesListDTO) super.list(request);
    }

    @NetAPI
    public RefreshSthTemplateUsageResponse refreshUsageInProductTemplate() {
        RefreshSthTemplateUsageResponse ret = new RefreshSthTemplateUsageResponse();
        WareTemplateEntitiesList allWares = wareTemplateDAO.list();
        int noDif = 0;
        int withDif = 0;

        for (WareTemplateEntity ware : allWares) {
            List<String> renew = wareTemplateManager.findWareTemplateUsage(ware.getId());
            if (!Objects.deepEquals(renew, ware.getWareUsages())) {
                withDif++;
                RefreshSthTemplateUsageResponse.Obj obj = new RefreshSthTemplateUsageResponse.Obj();
                obj.setId(ware.getId());
                obj.setName(ware.getName());
                obj.setNo(ware.getWareCode());
                obj.setOld(ware.getWareUsages());
                obj.setNeww(renew);

                ret.getDifferences().add(obj);
                wareTemplateDAO.updateOneField(ware.getId(), "wareUsages", renew);
            } else {
                noDif++;
            }
        }
        ret.setWithDifference(withDif);
        ret.setWithoutDifference(noDif);
        return ret;
    }
}
