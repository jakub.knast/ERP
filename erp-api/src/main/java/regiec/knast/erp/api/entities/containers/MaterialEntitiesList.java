/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities.containers;

import java.util.Collection;
import org.jaxygen.collections.PartialArrayList;
import regiec.knast.erp.api.entities.*;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class MaterialEntitiesList extends PartialArrayList<MaterialEntity> {

    public MaterialEntitiesList(Collection<? extends MaterialEntity> c, int totalSize) {
        super(c, totalSize);
    }
}
