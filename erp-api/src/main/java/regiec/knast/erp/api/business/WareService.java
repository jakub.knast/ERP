/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.WareDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WareTemplateDAOInterface;
import regiec.knast.erp.api.dto.*;
import regiec.knast.erp.api.dto.ware.*;
import regiec.knast.erp.api.entities.WareEntity;
import regiec.knast.erp.api.entities.WareTemplateEntity;
import regiec.knast.erp.api.exceptions.RemoveEntityException;
import regiec.knast.erp.api.model.states.WareState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.ERPSafetyCheck;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class WareService extends AbstractService<WareEntity, WareDTO> {

    @Inject
    private WareDAOInterface wareDAO;
    @Inject
    private WareTemplateDAOInterface wareTemplateDAO;

    @NetAPI
    public WareDTO addWare(AddWareRequest request) {
        WareEntity entity = new WareEntity();
        BeanUtilSilent.translateBean(request, entity);
        WareTemplateEntity wareTemplate = wareTemplateDAO.get(request.getWareTemplateId());
        entity.setWareTemplate(wareTemplate);
        entity.setState(WareState.ADDED_MANUALY);
        WareEntity entity1 = wareDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public WareDTO getWare(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public WareDTO updateWare(UpdateWareRequest request) {
        String id = request.getId();
        WareEntity entity = wareDAO.get(id);
        WareEntity updatedEntity = new WareEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);
        wareDAO.update(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeWare(RemoveWareRequest request) {
        String id = request.getId();
        WareEntity wareEntity = wareDAO.get(id);
        if (request.getConfirmed()) {
//                if (request.getRemoveRequestFromUp()) {
//                    wareEntity.setWareTemplate(wareTemplateDAO.get(wareEntity.getWareTemplate().getId()));
//                }
            ERPSafetyCheck safetyCheck = new ERPSafetyCheck();
            if (safetyCheck.checkIfEntityIsReferenced(WareEntity.class, id)) {
                throw new RemoveEntityException("Cannot remove Ware, its used somewhere");
            }
            wareEntity.setRemoved(true);
            wareDAO.update(wareEntity);
            return new RemoveResponse(true, null);
        } else {
            return new RemoveResponse(false, wareEntity.buildRemoveConsequencesMap());
        }
    }

    @NetAPI
    public WareListDTO listWares(PaginableFilterWithCriteriaRequest request) {
        return (WareListDTO) super.list(request);
    }
}
