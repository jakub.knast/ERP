/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.productionjob;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dto.product.DetalDTO;
import regiec.knast.erp.api.model.bases.ProductionJobBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductionJobDTO extends ProductionJobBase {

    private String id;
    private ProductionJobDTO parent;
    private List<ProductionJobDTO> children = new ArrayList(); 
    private DetalDTO detal;
    private boolean removed;


}
