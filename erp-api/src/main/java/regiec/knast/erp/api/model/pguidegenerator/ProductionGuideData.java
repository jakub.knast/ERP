/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.pguidegenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import regiec.knast.erp.api.business.apitests.pdf.ProductionGuideGeneratorRunner;
import regiec.knast.erp.api.dto.producttemplates.partcard.MaterialPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.PartCard;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.WarePart;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlan;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;
import regiec.knast.erp.api.model.WareSizeType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class ProductionGuideData {

    private static final String EMPTY = "-";
//    private static int idgenerator = 1;

    private String id;
    private String date = "1 luty 2018";
    private String endDate = "14 maj 2018";
    private String orderNo = "123-123-123";
    private String orderedDetalNo = "numer detalu na zamówieniu";
    private String orderedDetalName = "nazwa detalu na zamówieniu";
    private int count = 0;
//    private int thatCount = 0;
    private int pcsCount = 0;
    private List<Integer> multipliers = new ArrayList();
    private String countStr;
    private boolean generatingFromOrder = false;

    private String detalNo = "numer detalu";
    private String detalName = "nazwa detalu";
    private String parentDetalNo = "numer detalu nadrzednego";
    private String parentDetalName = "nazwa detalu nadrzednego";
    private List<BillOfMaterial> billOfMaterials = new ArrayList();
    private List<Semipart> semiparts = new ArrayList();
    private List<ProductionPlanItem> operations = new ArrayList();

    public static void main(String[] args) throws PdfGeneratorException, IOException {
        ProductionGuideGeneratorRunner.main(args);
    }

    public final void calculateCounts() {
        this.count = 1;
        for (Integer m : multipliers) {
            this.count *= m;
        }
        if (multipliers.size() == 1) {
            this.countStr = String.valueOf(count);
        } else {
            String strs = multipliers.stream()
                    .map(i -> String.valueOf(i))
                    .collect(Collectors.joining("x"));
            this.countStr = strs + " = " + count;
        }
    }

    private ProductionGuideData(CommonProductionOrderData commonProductionOrderData, ProdGuideIdGen prodGuideIdGen) {
        this.id = String.valueOf(prodGuideIdGen.get());
        this.date = commonProductionOrderData.getDate();
        this.endDate = commonProductionOrderData.getEndDate();
        this.orderNo = commonProductionOrderData.getOrderNo();
        this.orderedDetalNo = commonProductionOrderData.getOrderedDetalNo();
        this.orderedDetalName = commonProductionOrderData.getOrderedDetalName();
        this.pcsCount = commonProductionOrderData.getPcsCount();
        this.generatingFromOrder = commonProductionOrderData.getGeneratingFromOrder();
    }

    public ProductionGuideData(ProductTemplateEntity pt, ProductTemplateEntity parentPT, Supplier<String> barcodeGenerator, ProdGuideIdGen idGen) {
        this(new CommonProductionOrderData(), pt, parentPT, barcodeGenerator, idGen, new ArrayList());
    }

    public ProductionGuideData(CommonProductionOrderData commonProductionOrderData, ProductTemplateEntity pt, ProductTemplateEntity parentPT, Supplier<String> barcodeGenerator, ProdGuideIdGen idGen, List<Integer> multipliers) {
        this(commonProductionOrderData, idGen);
        this.multipliers = multipliers;
        this.detalNo = pt.getProductNo();
        this.detalName = pt.getName();
        this.parentDetalNo = parentPT != null ? parentPT.getProductNo() : EMPTY;
        this.parentDetalName = parentPT != null ? parentPT.getName() : EMPTY;
        this.addProductionPlanItemList(pt.getProductionPlan(), barcodeGenerator);
        this.addBOM(pt.getPartCard());
        this.addSemi(pt.getPartCard());
        this.calculateCounts();
    }

    public final void addProductionPlanItemList(List<ProductionPlan> productionPlans, Supplier<String> barcodeGenerator) {
        for (ProductionPlan pp : productionPlans) {
            ProductionPlanItem item = new ProductionPlanItem();
            item.setNo(pp.getOperationNumber());
            item.setOperation(pp.getOperationName().getName());
            item.setOperationBarcode(barcodeGenerator.get());
            item.setProfessionGroup(pp.getProfessionGroup().getLabel());
            item.setComments(pp.getComments());
            this.getOperations().add(item);
        }
    }

    public final void addSemi(PartCard partCard) {
        int no = 0;
        if (partCard != null && partCard.getSemiproductParts() != null) {
            for (SemiproductPart sp : partCard.getSemiproductParts()) {
                Semipart semipart = new Semipart();
                semipart.setNo(++no);
                semipart.setDetalId(sp.getProductTemplate().getId());
                semipart.setDetalNo(sp.getProductTemplate().getProductNo());
                semipart.setDetalName(sp.getProductTemplate().getName());
                semipart.setThatCount(sp.getCount());
                semipart.setMultipliers(new ArrayList(this.multipliers));
                semipart.calculateCounts();
                this.semiparts.add(semipart);
            }
        }
    }

    public final void addBOM(PartCard partCard) {
        int no = 0;
        if (partCard != null && partCard.getMaterialParts() != null) {
            for (MaterialPart mp : partCard.getMaterialParts()) {
                BillOfMaterial bom = new BillOfMaterial();
                MaterialTemplateEntity mt = mp.getMaterialTemplate();
                bom.setNo(++no);
                bom.setMaterialNo(mt.getMaterialCode());
                String desription = mt.getMaterialType().getName() + " "
                        + mt.getParticularDimension();
//                + " ["
//                        + mp.getMaterialTemplate().getMaterialType().getBaseDimensionUnit() + "] ";
                bom.setDescription(desription);
                String cuttingSize = mp.getCuttedSectionSize() + "\n["
                        + mt.getMaterialType().getCuttingDimensionUnit() + "]";
                bom.setCuttedSectionSize(cuttingSize);
                bom.setNorm(mt.getNorm() != null ? mt.getNorm().getNorm() : EMPTY);
                bom.setTolerance(mt.getTolerance() != null ? mt.getTolerance().getTolerance() : EMPTY);
                bom.setMaterialKind(mt.getMaterialKind() != null ? mt.getMaterialKind().getMaterialKind() : EMPTY);
                bom.setSteelGrade(mt.getSteelGrade() != null ? mt.getSteelGrade().getSteelGrade() : EMPTY);
                bom.setThatCount(mp.getCount());
                bom.setMultipliers(new ArrayList(this.multipliers));
                bom.calculateCounts();
                this.billOfMaterials.add(bom);
            }
        }

        if (partCard != null && partCard.getWareParts() != null) {
            for (WarePart wp : partCard.getWareParts()) {
                BillOfMaterial bom = new BillOfMaterial();
                bom.setNo(++no);
                bom.setMaterialNo(wp.getWareTemplate().getWareCode());
                WareSizeType sizeType = wp.getWareTemplate().getWareType().getSizeType();
                String desription = wp.getWareTemplate().getWareType().getName() + " "
                        + wp.getWareTemplate().getName() + " ["
                        + (sizeType != null ? sizeType.name() : "") + "] ";
                bom.setDescription(desription);
                bom.setThatCount(wp.getCount());
                bom.setMultipliers(new ArrayList(this.multipliers));
                bom.calculateCounts();
                String countStr = bom.getCountStr();
                bom.setCountStr(countStr + " " + wp.getWareSizeTypeOrEmptyString());

//                bom.setCuttedSectionSize(cuttingSize);                
                this.billOfMaterials.add(bom);
            }
        }
    }

// Prepare proguide data
    public static Map<String, ProductionGuideData> prepareProdGuideData(ProductTemplateEntity pt, ProductTemplateEntity parentPT,
            CommonProductionOrderData commonData, List<String> orderedPagesIds, Supplier<String> barcodeGenerator, ProdGuideIdGen idGen) {

        List<Integer> multipliers = new ArrayList();
        Map<String, ProductionGuideData> ret = new LinkedHashMap();
        if (orderedPagesIds.contains(pt.getId())) {
            // add first count multiplier - how much pcs we have on order
            multipliers.add(commonData.getPcsCount());
            ProductionGuideData data = new ProductionGuideData(commonData, pt, parentPT, barcodeGenerator, idGen, multipliers);
            ret.put(pt.getId(), data);
        }
        if (isThereSomeSemis(pt)) {
            List<Integer> cloneMul = new ArrayList(multipliers);
            ret.putAll(prepareInnerProdGuideData(pt, commonData, orderedPagesIds, barcodeGenerator, idGen, cloneMul));
        }
        addChildGuideNumbers(ret);
        return ret;
    }

    private static Map<String, ProductionGuideData> prepareInnerProdGuideData(ProductTemplateEntity pt,
            CommonProductionOrderData commonData, List<String> orderedPagesIds, Supplier<String> barcodeGenerator, ProdGuideIdGen idGen, List<Integer> multipliers) {

        Map<String, ProductionGuideData> ret = new LinkedHashMap();
        for (SemiproductPart spp : pt.getPartCard().getSemiproductParts()) {
            ProductTemplateEntity innerPT = spp.getProductTemplate();
            List<Integer> cloneMul = new ArrayList(multipliers);
            cloneMul.add(spp.getCount());
            if (orderedPagesIds.contains(pt.getId())) {
                ProductionGuideData data = new ProductionGuideData(commonData, innerPT, pt, barcodeGenerator, idGen, cloneMul);
                ret.put(innerPT.getId(), data);
            }
            if (isThereSomeSemis(innerPT)) {
                ret.putAll(prepareInnerProdGuideData(innerPT, commonData, orderedPagesIds, barcodeGenerator, idGen, cloneMul));
            }
        }
        return ret;
    }

    private static boolean isThereSomeSemis(ProductTemplateEntity innerPT) {
        return innerPT.getPartCard() != null
                && innerPT.getPartCard().getSemiproductParts() != null
                && !innerPT.getPartCard().getSemiproductParts().isEmpty();
    }

    private static void addChildGuideNumbers(Map<String, ProductionGuideData> map) {
        for (ProductionGuideData pg : map.values()) {
            if (pg.getSemiparts() != null) {
                for (Semipart semipart : pg.getSemiparts()) {
                    semipart.setProdGuideNo(map.get(semipart.getDetalId()).getId());
                }
            }
        }
    }

}
