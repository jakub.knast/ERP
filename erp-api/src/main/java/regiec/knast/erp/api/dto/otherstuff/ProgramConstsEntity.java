/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.otherstuff;

import java.util.TreeSet;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.MaterialKind;
import regiec.knast.erp.api.model.Norm;
import regiec.knast.erp.api.model.SteelGrade;
import regiec.knast.erp.api.model.Tolerance;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("programConsts")
public class ProgramConstsEntity extends ProgramConstsBase implements EntityBase {

    @Id
    private String id;

    private TreeSet<Norm> norm = new TreeSet();
    private TreeSet<SteelGrade> steelGrade = new TreeSet();
    private TreeSet<Tolerance> tolerance = new TreeSet();
    private TreeSet<MaterialKind> materialKind = new TreeSet();

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
