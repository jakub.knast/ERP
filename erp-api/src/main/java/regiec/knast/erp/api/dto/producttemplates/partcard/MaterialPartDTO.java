/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialPartDTO extends MaterialPartBase {

    private String positionId;
    private MaterialTemplateDTO materialTemplate;
    private String materialTemplateId;
}
