/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities.containers;

import java.util.Collection;
import org.jaxygen.collections.PartialArrayList;
import regiec.knast.erp.api.entities.WorkerEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class WorkerEntitiesList extends PartialArrayList<WorkerEntity> {

    public WorkerEntitiesList() {
    }

    public WorkerEntitiesList(Collection<? extends WorkerEntity> c) {
        super(c);
    }

    
}
