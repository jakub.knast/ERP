/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProviderDAOInterface;
import regiec.knast.erp.api.dao.util.OneByFilterResponse;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.exceptions.DatabaseInconsistencyException;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.MaterialAssortmentItem;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.model.ProviderAssortment;
import regiec.knast.erp.api.model.SellUnit;
import regiec.knast.erp.api.model.bases.ProviderBase;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class ProviderAssortmentUtil {

    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private ProviderDAOInterface providerDAO;
    @Inject
    private Validator validator;

    public MaterialTemplateEntity getMateriaLTemplateByMaterialCode(String materialCode) {
        OneByFilterResponse<MaterialTemplateEntity> mt = materialTemplateDAO.getOneByFilter(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("materialCode", materialCode, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());
        if (mt.multipleEntityExist()) {
            throw new ConversionErrorWithCode("There are multiple materials with the same materialCode: " + materialCode, ExceptionCodes.NOT_IMPLEMENTED, null);
        }
        if (!mt.onlyOneEntityExist()) {
            throw new ConversionErrorWithCode("There are no material tempalte with material code like: " + materialCode, ExceptionCodes.NOT_IMPLEMENTED, null);
        }
        MaterialTemplateEntity materialTemplate = mt.getEntity();
        return materialTemplate;
    }

    public void addMaterialAssortmentPositionToAssortmentItem(MaterialAssortmentItem itemWhereToAdd, CuttingSize salesLength, MoneyERP salesPrice, SellUnit sellUnit) {
        List<MaterialAssortmentPosition> positions = itemWhereToAdd.getMaterialAssortmentPositions();
        Optional<MaterialAssortmentPosition> obj = positions.stream()
                .filter(p -> p.getSalesLength().equalsByOrigSizeAndDimension(salesLength))
                .findAny();
        if (obj.isPresent()) {
            //update existing position
            obj.get().setSalesPrice(salesPrice);
        } else {
            // add new position to material assortment item
            itemWhereToAdd.getMaterialAssortmentPositions().add(new MaterialAssortmentPosition(IDUtil.nextId_(), salesLength, salesPrice, sellUnit));
        }
    }

    public MaterialAssortmentItem createNewEmptyMaterialAssortment(MaterialTemplateEntity materialTemplate, String materialCode) {
        MaterialAssortmentItem itemWhereToAdd = new MaterialAssortmentItem(
                IDUtil.nextId_(),
                materialCode,
                materialTemplate.getMaterialType().getName(),
                materialTemplate.getParticularDimension(),
                materialTemplate.getId(),
                new ArrayList());
        return itemWhereToAdd;
    }
    
    public MaterialAssortmentItem createNewMaterialAssortment(CuttingSize salesLength, MoneyERP salesPrice,
            MaterialTemplateEntity materialTemplate, String materialCode, SellUnit sellUnit) {

        List<MaterialAssortmentPosition> positions = Lists.newArrayList(new MaterialAssortmentPosition(IDUtil.nextId_(), salesLength, salesPrice, sellUnit));
        MaterialAssortmentItem itemWhereToAdd = new MaterialAssortmentItem(
                IDUtil.nextId_(),
                materialCode,
                materialTemplate.getMaterialType().getName(),
                materialTemplate.getParticularDimension(),
                materialTemplate.getId(),
                positions);
        return itemWhereToAdd;
    }

    public MaterialAssortmentItem findExistingMaterialAssortmentInProvider(List<MaterialAssortmentItem> matAssList, String materialCode) {
        List<MaterialAssortmentItem> collect = matAssList.stream()
                .filter(matAss -> matAss.getMaterialCode().equals(materialCode))
                .collect(Collectors.toList());
        if (collect.size() > 1) {
            throw new DatabaseInconsistencyException("There are many material assortment in provider with code: " + materialCode);
        }
        if (!collect.isEmpty()) {
            return collect.get(0);
        }
        return null;
    }
    
    public MaterialAssortmentItem findExistingMaterialAssortmentInProvider(ProviderEntity provider, String materialCode) {
        if(provider != null && provider.getAssortment() != null && provider.getAssortment().getMaterialAssortment() != null){
            List<MaterialAssortmentItem> matAss = provider.getAssortment().getMaterialAssortment();
            return findExistingMaterialAssortmentInProvider(matAss, materialCode);
        }
        return null;        
    }
    
    public MaterialAssortmentItem findExistingMaterialAssortmentInProvider(String providerId, String materialCode) {
        validator.validateNotNullAndNotEmpty(providerId, ExceptionCodes.NULL_VALUE, "providerId").throww();
        validator.validateNotNullAndNotEmpty(materialCode, ExceptionCodes.NULL_VALUE, "materialCode").throww();
        ProviderEntity provider = providerDAO.get(providerId);
        validator.validateNotNull(provider, ExceptionCodes.NULL_VALUE, "providerEntity").throww();
        
        if(provider != null && provider.getAssortment() != null && provider.getAssortment().getMaterialAssortment() != null){
            List<MaterialAssortmentItem> matAss = provider.getAssortment().getMaterialAssortment();
            return findExistingMaterialAssortmentInProvider(matAss, materialCode);
        }
        return null;        
    }
    
    public MaterialAssortmentPosition findAssortmentPositionInProvider(String providerId, String assortmentId) {
        validator.validateNotNullAndNotEmpty(providerId, ExceptionCodes.NULL_VALUE, "providerId").throww();
        validator.validateNotNullAndNotEmpty(assortmentId, ExceptionCodes.NULL_VALUE, "assortmentId").throww();
        ProviderEntity provider = providerDAO.get(providerId);
        validator.validateNotNull(provider, ExceptionCodes.NULL_VALUE, "providerEntity").throww();
        
        return findAssortmentPositionInProvider(provider, assortmentId);        
    }
    
    public MaterialAssortmentPosition findAssortmentPositionInProvider(ProviderEntity provider, String assortmentId) {
        MaterialAssortmentPosition ret = null;
        validator.validateNotNullAndNotEmpty(assortmentId, ExceptionCodes.NULL_VALUE, "assortmentId").throww();
        validator.validateNotNull(provider, ExceptionCodes.NULL_VALUE, "providerEntity").throww();
        
        if(provider != null && provider.getAssortment() != null && provider.getAssortment().getMaterialAssortment() != null){
            List<MaterialAssortmentPosition> collect = provider.getAssortment().getMaterialAssortment().stream()
                    .flatMap(assItem -> assItem.getMaterialAssortmentPositions().stream())
                    .collect(Collectors.toList());
            for (MaterialAssortmentPosition asPos : collect) {
                if(asPos.getPositionId().equals(assortmentId)){
                    ret = asPos;
                    break;
                }
            }            
        }
        return ret;        
    }

    public void ensureAssortmentIsNotNull(ProviderBase provider) {
        if (provider.getAssortment() == null) {
            provider.setAssortment(new ProviderAssortment());
        }
        if (provider.getAssortment().getMaterialAssortment() == null) {
            provider.getAssortment().setMaterialAssortment(new ArrayList());
        }
        if (provider.getAssortment().getWareAssortment() == null) {
            provider.getAssortment().setWareAssortment(new ArrayList());
        }
    }

}
