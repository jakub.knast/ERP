/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.validation.state;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.model.states.StateInterface;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public abstract class StateMachine<STATE extends Enum<STATE> & StateInterface> {

//    public final HashMap<T, Set<T>> POSSIBLE_CHANGES = new HashMap();
    private final PossibleChangesMap<STATE> FROM_STATE;
//    private final PossibleChangesMap<STATE> TO_STATE;

    public StateMachine(PossibleChangesMap<STATE> possibleChanges) {
        this.FROM_STATE = possibleChanges;
//        this.TO_STATE = FROM_STATE.invertMe();
        validateThereAllStates();
    }

    public PossibleChangesMap<STATE> listaNaztępnikow() {
        return FROM_STATE.cloneMe();
    }

    public void validateChangeState(STATE actual, STATE toGo, RuntimeException e) {
        if (!FROM_STATE.containsKey(actual)) {
            throw new ERPInternalError("There is no defined state chain for state: " + actual);
        }
        Set<Adjacent<STATE>> toGet = FROM_STATE.get(actual);
        Adjacent<STATE> adjacentToGo = new Adjacent<>(toGo, "", "");
        if (!toGet.contains(adjacentToGo)) {
            throw e;
        }
    }

    public void validateChangeState(STATE actual, STATE toGo) {
        validateChangeState(actual, toGo, new ERPInternalError("program does not allow to get from state: " + actual + " to state: " + toGo));
    }

    public Set<STATE> getStatesAfter(STATE actual) {
        if (!FROM_STATE.containsKey(actual)) {
            throw new ERPInternalError("There is no defined state chain for state: " + actual);
        }
        Set<Adjacent<STATE>> get = FROM_STATE.get(actual);
        Set<STATE> ret = get.stream()
                .map(adj -> adj.stateToGo)
                .collect(Collectors.toSet());
        return ret;
    }

//    public Set<STATE> getStatesBefor(STATE actual) {
//        if (!TO_STATE.containsKey(actual)) {
//            throw new ERPInternalError("There is no defined state chain for state: " + actual);
//        }
//        return TO_STATE.get(actual);
//    }

    private void validateThereAllStates() {
        STATE someEnum = (STATE) new ArrayList(FROM_STATE.keySet()).get(0);
        STATE[] enums = (STATE[]) someEnum.getClass().getEnumConstants();
        for (STATE enumConstant : enums) {
            if (!FROM_STATE.containsKey(enumConstant)) {
                throw new ConversionErrorWithCode("You forgot to register enum value: " + enumConstant.name() + " in state machine for enum class: " + enums.getClass().getSimpleName(), ExceptionCodes.INTERNAL_ERROR);
            }
        }
    }

    public static class PossibleChangesMap<T> extends HashMap<T, Set<Adjacent<T>>> {

        public PossibleChangesMap<T> put(T state, Adjacent<T>... adjacents) {
            if (this.containsKey(state)) {
                throw new ConversionErrorWithCode("there is duplicated key: " + state, ExceptionCodes.INTERNAL_ERROR);
            }
            List<Adjacent<T>> adjacentsList = Lists.newArrayList(adjacents);
            Set<Adjacent<T>> toAdd = new HashSet(adjacentsList);
            if (toAdd.size() != adjacentsList.size()) {
                throw new ConversionErrorWithCode("there is duplicated in states to go for state: " + state, ExceptionCodes.INTERNAL_ERROR);
            }
            this.put(state, toAdd);
            return this;
        }

        public PossibleChangesMap<T> putOne(T state, Adjacent<T> stateToGo) {
            if (!this.containsKey(state)) {
                this.put(state, new HashSet());
            }
             Set<Adjacent<T>>  actualValues = this.get(state);
            boolean isUnique = actualValues.add(stateToGo);
            if (!isUnique) {
                throw new ConversionErrorWithCode("there is duplicated in states to go for state: " + state, ExceptionCodes.INTERNAL_ERROR);
            }
            return this;
        }

//        public PossibleChangesMap<T> invertMe() {
//            PossibleChangesMap inverted = new PossibleChangesMap();
//            for (Map.Entry<T, Set<T>> e : this.entrySet()) {
//                T key = e.getKey();
//                Set<T> values = e.getValue();
//                for (T value : values) {
//                    inverted.putOne(value, key);
//                }
//            }
//            return inverted;
//        }
        
        public PossibleChangesMap<T> cloneMe() {
            return BeanUtilSilent.deepClone(this);
        }
    }

    @lombok.NoArgsConstructor
    @lombok.AllArgsConstructor
    public static class Adjacent<STATE> {

        public STATE stateToGo;
        public String method;
        public String methodVisibleName;

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 29 * hash + Objects.hashCode(this.stateToGo);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Adjacent<?> other = (Adjacent<?>) obj;
            if (!Objects.equals(this.stateToGo, other.stateToGo)) {
                return false;
            }
            return true;
        }
        
    }

}
