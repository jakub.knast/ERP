/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.states;

import regiec.knast.erp.api.model.colors.StatesColors;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public enum DeliveryOrderState implements StateInterface {

    NORMAL("Nowy", StatesColors.NEW_COLOR),
    WAITIN_FOR_APROVEMENT("Czeka na potwierdzenie", StatesColors.WAITIN_FOR_APROVEMENT_COLOR),
    APROVED("Potwierdzone", StatesColors.APROVED_COLOR);
   
    private DeliveryOrderState(String translation, StatesColors stateColor) {
        this(translation, stateColor, false);
    }

    private DeliveryOrderState(String translation, StatesColors stateColor, boolean removable) {
        this.translation = translation;
        this.backgroundColor = stateColor.getBackgroundColor();
        this.fontColor = stateColor.getFontColor();
        this.removable = removable;
    }

    private String translation;
    private String backgroundColor;
    private String fontColor;
    private boolean removable;

    @Override
    public boolean isRemovable() {
        return removable;
    }

    @Override
    public String getTranslation() {
        return translation;
    }

    @Override
    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public String getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public String getFontColor() {
        return fontColor;
    }

    @Override
    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }
}
