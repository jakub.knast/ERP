/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.jaxygen.objectsbuilder.ObjectBuilder;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class GuiceBuilder implements ObjectBuilder {

    private static ObjectBuilder instance = null;
    private Injector injector = null;

    private GuiceBuilder() {
        init(new MyBinder());
    }

    private void init(AbstractModule module) {
        injector = Guice.createInjector(module);
    }

    @Override
    public final Object create(final Class clazz) throws ObjectCreateError {
        return injector.getInstance(clazz);
    }

    public static ObjectBuilder getInstance() {
        if (instance == null) {
            instance = new GuiceBuilder();
        }
        return instance;
    }
}
