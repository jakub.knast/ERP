/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class UpdateWarePart extends AddWarePart {

    private String positionId;

    public UpdateWarePart(String positionId, String wareTemplateId, Integer count, String positionOnTechDraw, String comments) {
        super(wareTemplateId, count, positionOnTechDraw, comments);
        this.positionId = positionId;
    }

    public UpdateWarePart(String positionId, String wareTemplateId) {
        super(wareTemplateId);
        this.positionId = positionId;
    }
     
}
