/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.manager;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WareDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WareTemplateDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.WareEntity;
import regiec.knast.erp.api.entities.WareTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;
import regiec.knast.erp.api.entities.containers.WareEntitiesList;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class WareTemplateManager {

    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;
    @Inject
    private WareTemplateDAOInterface wareTemplateDAO;
    @Inject
    private PartCardManager partCardManager;
    @Inject
    private WareDAOInterface wareDAO;

    public Map<String, Object> buildRemoveConsequencesMap(WareTemplateEntity entity) {
        Map<String, Object> consequencesMap = ImmutableMap.<String, Object>builder()
                .put("entityType", entity.getClass().getSimpleName())
                .put("id", entity.getId())
                .put("wareCode", entity.getWareCode())
                .put("name", entity.getWareType().getName())
                .put("leafs", new ArrayList<>())
                .put("semiproductPartCard", new ArrayList<>())
                .put("productPartCard", new ArrayList<>())
                .build();
        final WareEntitiesList wareEntitiesList = getWaresWithWareTemplate(entity.getId());
        for (WareEntity wareEntity : wareEntitiesList) {
            Map<String, Object> wareConsequencesMap = wareEntity.buildRemoveConsequencesMap();
            ((ArrayList) consequencesMap.get("leafs")).add(wareConsequencesMap);
        }

        final ProductTemplateEntitiesList productTempaltesWithPartCards
                = (ProductTemplateEntitiesList) partCardManager.getPartCardsCompatibileEntitiesWithElementFromCollection(
                        EntityType.WARE_TEMPLATE,
                        entity.getId());
        for (ProductTemplateEntity productTempalteWithPartCards : productTempaltesWithPartCards) {
            ImmutableMap<String, Object> build = ImmutableMap.<String, Object>builder()
                    .put("entityType", "Lista Materiałowa" + ProductEntity.class.getSimpleName())
                    .put("id", productTempalteWithPartCards.getId())
                    .put("productNo", productTempalteWithPartCards.getProductNo())
                    .put("name", productTempalteWithPartCards.getName())
                    .build();
            ((ArrayList) consequencesMap.get("productPartCard")).add(build);
        }
        return consequencesMap;
    }

    public WareEntitiesList getWaresWithWareTemplate(String wareTemplateEntityId) {
        PaginableFilterWithCriteriaRequest req = PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder().addConstraint("wareTemplate.id", wareTemplateEntityId, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build();
        return wareDAO.list(req);

    }

    public List<String> findWareTemplateUsage(String wareTemplateId) {
        List<String> renew = new ArrayList();
        ProductTemplateEntitiesList usages = productTemplateDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("partCard.wareParts.wareTemplate", wareTemplateId, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());
        for (ProductTemplateEntity usage : usages) {
            renew.add("numer: " + usage.getProductNo() + ", nazwa: " + usage.getName());
        }
        return renew;
    }

    public void refreshUsage(String wareTemplateId) {
        List<String> renew = findWareTemplateUsage(wareTemplateId);
        wareTemplateDAO.updateOneField(wareTemplateId, "wareUsages", renew);
    }
}
