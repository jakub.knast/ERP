/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.business.apitests;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.dao.interfaces.DocumentDAOInterface;
import regiec.knast.erp.api.entities.DocumentEntity;
import regiec.knast.erp.api.model.DocumentInterface;
import regiec.knast.erp.api.model.invoicegenerator.InvoiceData;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class DocumentTest extends AllServicesInjector {

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(DocumentTest.class);
        DocumentTest mongoTest = (DocumentTest) create;
        try {
            mongoTest.initServices();
            mongoTest.test_readDocument();
        } catch (Exception ex) {
            Logger.getLogger(ListTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    private void test_saveDocumentToMongo() {
                DocumentInterface invoice = new InvoiceData("hj", "sd", null, null, null);

        DocumentEntity doc = new DocumentEntity();
        doc.setName("test1");
        doc.setContent(invoice);
        
        DocumentDAOInterface dao = super.buildObj(DocumentDAOInterface.class);
        DocumentEntity result = dao.add(doc);
        
        printResult(result);
        
    }

    private void test_readDocument() {
        String docId = "5b97537ab39c521bb8f37f88";

        DocumentDAOInterface dao = super.buildObj(DocumentDAOInterface.class);
        DocumentEntity result = dao.get(docId);
        
        printResult(result);
        
    }

    
}
