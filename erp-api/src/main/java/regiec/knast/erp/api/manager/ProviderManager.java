/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProviderDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.provider.AssortmentForMaterialResponse;
import regiec.knast.erp.api.dto.provider.FindMaterialsFromProviderRequest;
import regiec.knast.erp.api.dto.provider.FindMaterialsFromProviderResponse;
import regiec.knast.erp.api.dto.provider.FindOtherProviderForMaterialRequest;
import regiec.knast.erp.api.dto.provider.FindOtherProviderForMaterialResponse;
import regiec.knast.erp.api.dto.provider.FindProviderForMaterialRequest;
import regiec.knast.erp.api.dto.provider.FindProviderForMaterialResponse;
import regiec.knast.erp.api.dto.provider.OtherAssortmentForMaterialResponse;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.entities.containers.ProviderEntitiesList;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.MaterialAssortmentItem;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;
import regiec.knast.erp.api.model.ProviderAssortment;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class ProviderManager {

    @Inject
    private ProviderDAOInterface providerDAO;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private Validator validator;

    public FindProviderForMaterialResponse findProviderForMaterial(FindProviderForMaterialRequest request) {
        validator.validateNotNull(request).throww();
        final String materialTemplateId = request.getMaterialTemplateId();
        validator.validateNotNullAndNotEmpty(materialTemplateId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "materialTemplateId").throww();

        MaterialTemplateEntity materialTemplate = materialTemplateDAO.get(materialTemplateId);
        validator.validateNotNull(materialTemplate, "materialTemplate").throww();
        final String materialCode = materialTemplate.getMaterialCode();

        List<AssortmentForMaterialResponse> ret = new ArrayList();

        ProviderEntitiesList providers = providerDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("assortment.materialAssortment.materialCode", materialCode, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());

        for (ProviderEntity p : providers) {
            List<MaterialAssortmentItem> maList = p.getAssortment().getMaterialAssortment();
            for (MaterialAssortmentItem ma : maList) {
                if (ma.getMaterialCode().equals(materialCode)) {
                    AssortmentForMaterialResponse resp = new AssortmentForMaterialResponse(p.getName(), p.getId(), new ArrayList());
                    for (MaterialAssortmentPosition pos : ma.getMaterialAssortmentPositions()) {
                        resp.getMaterialAssortment().add(pos);
                    }
                    if (!resp.getMaterialAssortment().isEmpty()) {
                        ret.add(resp);
                    }
                }
            }
        }
        return new FindProviderForMaterialResponse(ret, request.getMaterialTemplateId());
    }

    public FindMaterialsFromProviderResponse findMaterialsFromProvider(FindMaterialsFromProviderRequest request) {
        FindMaterialsFromProviderResponse ret = new FindMaterialsFromProviderResponse();

        validator.validateNotNull(request).throww();

        final String materialTemplateId = request.getMaterialTemplateId();
        validator.validateNotNullAndNotEmpty(materialTemplateId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "materialTemplateId").throww();

        final String prividerId = request.getProviderId();
        validator.validateNotNullAndNotEmpty(prividerId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "prividerId").throww();
        ProviderEntity providerEntity = providerDAO.get(prividerId);
        validator.validateNotNull(providerEntity, ExceptionCodes.NULL_VALUE, "providerEmtity");

        ProviderAssortment assortment = providerEntity.getAssortment();
        if (assortment == null || assortment.getMaterialAssortment() == null) {
            return ret;
        }

        for (MaterialAssortmentItem item : assortment.getMaterialAssortment()) {
            if (item.getMaterialTemplateId().equals(materialTemplateId)) {
                ret.getMaterialAssortment().addAll(item.getMaterialAssortmentPositions());
            }
            break;
        }
        return ret;
    }

    public FindOtherProviderForMaterialResponse findOtherProviderForMaterial(FindOtherProviderForMaterialRequest request) {
        validator.validateNotNull(request).throww();
        final String materialTemplateId = request.getMaterialTemplateId();
        final String assortmentToOmitId = request.getAssortmentId();
        final String salesLengthOrig = request.getSalesLengthOrig();
        validator.validateNotNullAndNotEmpty(materialTemplateId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "materialTemplateId").throww();
        validator.validateNotNullAndNotEmpty(assortmentToOmitId, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "assortmentId").throww();
        validator.validateNotNullAndNotEmpty(salesLengthOrig, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "salesLengthOrig").throww();

        MaterialTemplateEntity materialTemplate = materialTemplateDAO.get(materialTemplateId);
        validator.validateNotNull(materialTemplate, "materialTemplate").throww();
        final String materialCode = materialTemplate.getMaterialCode();

        List<OtherAssortmentForMaterialResponse> ret = new ArrayList();

        ProviderEntitiesList providers = providerDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("assortment.materialAssortment.materialCode", materialCode, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());

        for (ProviderEntity p : providers) {
            List<MaterialAssortmentItem> maList = p.getAssortment().getMaterialAssortment();
            for (MaterialAssortmentItem ma : maList) {
                if (ma.getMaterialCode().equals(materialCode)) {
                    OtherAssortmentForMaterialResponse resp = null;
                    for (MaterialAssortmentPosition pos : ma.getMaterialAssortmentPositions()) {
                        // we dont want the same position as we have before
                        // we want only this length as we have
                        if (!pos.getPositionId().equals(assortmentToOmitId) && pos.getSalesLength().getOrigSize().equals(salesLengthOrig)) {
                            resp = new OtherAssortmentForMaterialResponse(p.getName(), p.getId(), pos);
                            break;
                        }
                    }
                    if (resp != null) {
                        ret.add(resp);
                    }
                }
            }
        }
        return new FindOtherProviderForMaterialResponse(ret, request.getMaterialTemplateId());
    }

}
