/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.OperationNameDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.operationName.AddOperationNameRequest;
import regiec.knast.erp.api.dto.operationName.OperationNameDTO;
import regiec.knast.erp.api.dto.operationName.OperationNamesListDTO;
import regiec.knast.erp.api.dto.operationName.RemoveOperationNameRequest;
import regiec.knast.erp.api.dto.operationName.UpdateOperationNameRequest;
import regiec.knast.erp.api.entities.OperationNameEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.OperationNameState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class OperationNameService extends AbstractService<OperationNameEntity, OperationNameDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.OPERATION_NAME;

    @Inject
    private OperationNameDAOInterface operationNameDAO;
    @Inject
    private Validator validator;

    @NetAPI
    public OperationNameDTO addOperationName(AddOperationNameRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("name", request.getName(), null, MY_ENTITY_TYPE).throww();

        OperationNameEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new OperationNameEntity());
        entity.setState(OperationNameState.NORMAL);
        OperationNameEntity entity1 = operationNameDAO.add(entity);
        return entity1.convertToDTO(); 
    }

    @NetAPI
    public OperationNameDTO getOperationName(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public OperationNameDTO updateOperationName(UpdateOperationNameRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("name", request.getName(), id, MY_ENTITY_TYPE).throww();
        OperationNameEntity entity = operationNameDAO.get(id);
        validator.validateNotNull(entity).throww();
        OperationNameEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new OperationNameEntity());
        BeanUtilSilent.translateBean(request, updatedEntity);
        operationNameDAO.update(updatedEntity);
        return updatedEntity.convertToDTO(); 
    }

    @NetAPI
    public RemoveResponse removeOperationName(RemoveOperationNameRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        OperationNameEntity operationNameEntity = operationNameDAO.get(id);
        if (request.getConfirmed()) {
            operationNameEntity.setRemoved(true);
            operationNameDAO.update(operationNameEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(operationNameEntity));
            return new RemoveResponse(false, null);
        }
    }

    @NetAPI
    public OperationNamesListDTO listOperationNames(PaginableFilterWithCriteriaRequest request) {
        return (OperationNamesListDTO) super.list(request);
    }
}
