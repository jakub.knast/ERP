/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ResourceInfoBase extends ObjectWithDate {

    private String resourceName;
    @ErpIdEntity(entityClass = ErpIdEntity.Dummy.class)
    private String resourceId;
    @ErpIdEntity(entityClass = ErpIdEntity.Dummy.class)
    private String loobackId;

}
