/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.utils;

import java.util.HashMap;
import java.util.Map;
import org.jaxygen.dto.collections.PaginableListResponseBaseDTO;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderDTO;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrdersListDTO;
import regiec.knast.erp.api.dto.development.DevelopmentDTO;
import regiec.knast.erp.api.dto.development.DevelopmentsListDTO;
import regiec.knast.erp.api.dto.document.DocumentDTO;
import regiec.knast.erp.api.dto.document.DocumentsListDTO;
import regiec.knast.erp.api.dto.machines.MachineDTO;
import regiec.knast.erp.api.dto.machines.MachinesListDTO;
import regiec.knast.erp.api.dto.material.MaterialDTO;
import regiec.knast.erp.api.dto.material.MaterialListDTO;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandDTO;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandsListDTO;
import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTO;
import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplatesListDTO;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypeDTO;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypesListDTO;
import regiec.knast.erp.api.dto.operationName.OperationNameDTO;
import regiec.knast.erp.api.dto.operationName.OperationNamesListDTO;
import regiec.knast.erp.api.dto.order.OrderDTO;
import regiec.knast.erp.api.dto.order.OrdersListDTO;
import regiec.knast.erp.api.dto.product.ProductDTO;
import regiec.knast.erp.api.dto.product.ProductListDTO;
import regiec.knast.erp.api.dto.productionjob.ProductionJobDTO;
import regiec.knast.erp.api.dto.productionjob.ProductionJobsListDTO;
import regiec.knast.erp.api.dto.productionorder.ProductionOrderDTO;
import regiec.knast.erp.api.dto.productionorder.ProductionOrdersListDTO;
import regiec.knast.erp.api.dto.producttemplates.ProductTemplateDTO;
import regiec.knast.erp.api.dto.producttemplates.ProductTemplatesListDTO;
import regiec.knast.erp.api.dto.professionGroup.ProfessionGroupDTO;
import regiec.knast.erp.api.dto.professionGroup.ProfessionGroupsListDTO;
import regiec.knast.erp.api.dto.provider.ProviderDTO;
import regiec.knast.erp.api.dto.provider.ProvidersListDTO;
import regiec.knast.erp.api.dto.recipient.RecipientDTO;
import regiec.knast.erp.api.dto.recipient.RecipientsListDTO;
import regiec.knast.erp.api.dto.resourceinfo.ResourceInfoDTO;
import regiec.knast.erp.api.dto.resourceinfo.ResourceInfoListDTO;
import regiec.knast.erp.api.dto.seller.SellerDTO;
import regiec.knast.erp.api.dto.seller.SellersListDTO;
import regiec.knast.erp.api.dto.shoppingCart.ShoppingCartDTO;
import regiec.knast.erp.api.dto.todoIssues.TodoIssuesDTO;
import regiec.knast.erp.api.dto.todoIssues.TodoIssuessListDTO;
import regiec.knast.erp.api.dto.ware.WareDTO;
import regiec.knast.erp.api.dto.ware.WareListDTO;
import regiec.knast.erp.api.dto.waretemplates.WareTemplateDTO;
import regiec.knast.erp.api.dto.waretemplates.WareTemplatesListDTO;
import regiec.knast.erp.api.dto.waretypes.WareTypeDTO;
import regiec.knast.erp.api.dto.waretypes.WareTypesListDTO;
import regiec.knast.erp.api.dto.workers.WorkerDTO;
import regiec.knast.erp.api.dto.workers.WorkersListDTO;
import regiec.knast.erp.api.entities.*;
import regiec.knast.erp.api.entities.containers.*;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.EntityBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class EntityHeadquarters {

    private static final Map<Class, Class> CLASS_TO_DTO = new HashMap();
    private static final Map<Class, Class> CLASS_TO_ENTITIES_LIST = new HashMap();
    private static final Map<Class, Class<? extends PaginableListResponseBaseDTO>> CLASS_TO_LIST_DTO = new HashMap();

    static {
        CLASS_TO_DTO.put(MaterialEntity.class, MaterialDTO.class);
        CLASS_TO_DTO.put(MaterialTemplateEntity.class, MaterialTemplateDTO.class);
        CLASS_TO_DTO.put(MaterialTypeEntity.class, MaterialTypeDTO.class);
        CLASS_TO_DTO.put(WareEntity.class, WareDTO.class);
        CLASS_TO_DTO.put(WareTemplateEntity.class, WareTemplateDTO.class);
        CLASS_TO_DTO.put(WareTypeEntity.class, WareTypeDTO.class);
        CLASS_TO_DTO.put(ProductEntity.class, ProductDTO.class);
        CLASS_TO_DTO.put(ProductTemplateEntity.class, ProductTemplateDTO.class);
        CLASS_TO_DTO.put(ProviderEntity.class, ProviderDTO.class);
        CLASS_TO_DTO.put(SellerEntity.class, SellerDTO.class);
        CLASS_TO_DTO.put(RecipientEntity.class, RecipientDTO.class);
        CLASS_TO_DTO.put(ProductionOrderEntity.class, ProductionOrderDTO.class);
        CLASS_TO_DTO.put(OrderEntity.class, OrderDTO.class);
        CLASS_TO_DTO.put(DeliveryOrderEntity.class, DeliveryOrderDTO.class);
        CLASS_TO_DTO.put(WorkerEntity.class, WorkerDTO.class);
        CLASS_TO_DTO.put(MachineEntity.class, MachineDTO.class);
        CLASS_TO_DTO.put(ProductionJobEntity.class, ProductionJobDTO.class);
        CLASS_TO_DTO.put(MaterialDemandEntity.class, MaterialDemandDTO.class);
        CLASS_TO_DTO.put(DevelopmentEntity.class, DevelopmentDTO.class);
        CLASS_TO_DTO.put(ShoppingCartEntity.class, ShoppingCartDTO.class);
        CLASS_TO_DTO.put(TodoIssuesEntity.class, TodoIssuesDTO.class);
        CLASS_TO_DTO.put(OperationNameEntity.class, OperationNameDTO.class);
        CLASS_TO_DTO.put(ProfessionGroupEntity.class, ProfessionGroupDTO.class);
        CLASS_TO_DTO.put(DocumentEntity.class, DocumentDTO.class);
        CLASS_TO_DTO.put(ResourceInfoEntity.class, ResourceInfoDTO.class);
//Here_will_be_insert_entityType_to_entityClass_mapping

        CLASS_TO_ENTITIES_LIST.put(MaterialEntity.class, MaterialEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(MaterialTemplateEntity.class, MaterialTemplateEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(MaterialTypeEntity.class, MaterialTypeEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(WareEntity.class, WareEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(WareTemplateEntity.class, WareTemplateEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(WareTypeEntity.class, WareTypeEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(ProductEntity.class, ProductEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(ProductTemplateEntity.class, ProductTemplateEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(ProviderEntity.class, ProviderEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(SellerEntity.class, SellerEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(RecipientEntity.class, RecipientEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(ProductionOrderEntity.class, ProductionOrderEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(OrderEntity.class, OrderEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(DeliveryOrderEntity.class, DeliveryOrderEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(WorkerEntity.class, WorkerEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(MachineEntity.class, MachineEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(ProductionJobEntity.class, ProductionJobEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(MaterialDemandEntity.class, MaterialDemandEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(DevelopmentEntity.class, DevelopmentEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(ShoppingCartEntity.class, ShoppingCartEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(TodoIssuesEntity.class, TodoIssuesEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(OperationNameEntity.class, OperationNameEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(ProfessionGroupEntity.class, ProfessionGroupEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(DocumentEntity.class, DocumentEntitiesList.class);
        CLASS_TO_ENTITIES_LIST.put(ResourceInfoEntity.class, ResourceInfoEntitiesList.class);

        CLASS_TO_LIST_DTO.put(MaterialEntity.class, MaterialListDTO.class);
        CLASS_TO_LIST_DTO.put(MaterialTemplateEntity.class, MaterialTemplatesListDTO.class);
        CLASS_TO_LIST_DTO.put(MaterialTypeEntity.class, MaterialTypesListDTO.class);
        CLASS_TO_LIST_DTO.put(WareEntity.class, WareListDTO.class);
        CLASS_TO_LIST_DTO.put(WareTemplateEntity.class, WareTemplatesListDTO.class);
        CLASS_TO_LIST_DTO.put(WareTypeEntity.class, WareTypesListDTO.class);
        CLASS_TO_LIST_DTO.put(ProductEntity.class, ProductListDTO.class);
        CLASS_TO_LIST_DTO.put(ProductTemplateEntity.class, ProductTemplatesListDTO.class);
        CLASS_TO_LIST_DTO.put(ProviderEntity.class, ProvidersListDTO.class);
        CLASS_TO_LIST_DTO.put(SellerEntity.class, SellersListDTO.class);
        CLASS_TO_LIST_DTO.put(RecipientEntity.class, RecipientsListDTO.class);
        CLASS_TO_LIST_DTO.put(ProductionOrderEntity.class, ProductionOrdersListDTO.class);
        CLASS_TO_LIST_DTO.put(OrderEntity.class, OrdersListDTO.class);
        CLASS_TO_LIST_DTO.put(DeliveryOrderEntity.class, DeliveryOrdersListDTO.class);
        CLASS_TO_LIST_DTO.put(WorkerEntity.class, WorkersListDTO.class);
        CLASS_TO_LIST_DTO.put(MachineEntity.class, MachinesListDTO.class);
        CLASS_TO_LIST_DTO.put(ProductionJobEntity.class, ProductionJobsListDTO.class);
        CLASS_TO_LIST_DTO.put(MaterialDemandEntity.class, MaterialDemandsListDTO.class);
        CLASS_TO_LIST_DTO.put(DevelopmentEntity.class, DevelopmentsListDTO.class);
//        CLASS_TO_LIST_DTO.put(ShoppingCartEntity.class, ShoppingCartListDTO.class); 
        CLASS_TO_LIST_DTO.put(TodoIssuesEntity.class, TodoIssuessListDTO.class);
        CLASS_TO_LIST_DTO.put(OperationNameEntity.class, OperationNamesListDTO.class);
        CLASS_TO_LIST_DTO.put(ProfessionGroupEntity.class, ProfessionGroupsListDTO.class);
        CLASS_TO_LIST_DTO.put(DocumentEntity.class, DocumentsListDTO.class);
        CLASS_TO_LIST_DTO.put(ResourceInfoEntity.class, ResourceInfoListDTO.class);
//Here_will_be_insert_entityType_to_entityClass_mapping
    }

    public static Class getDto(Class<? extends EntityBase> entityClass) {
        if (!CLASS_TO_DTO.containsKey(entityClass)) {
            throw new ERPInternalError("There is no DTo in chace for entity class: " + entityClass.getCanonicalName());
        }
        return CLASS_TO_DTO.get(entityClass);
    }

    public static Class<? extends PaginableListResponseBaseDTO> getListDto(Class<? extends EntityBase> entityClass) {
        if (!CLASS_TO_LIST_DTO.containsKey(entityClass)) {
            throw new ERPInternalError("There is no DTo in chace for entity class: " + entityClass.getCanonicalName());
        }
        return CLASS_TO_LIST_DTO.get(entityClass); 
    }
}
