/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class RefreshSthTemplateUsageResponse {

    private int withDifference=0;
    private int withoutDifference=0;
    
    private List<Obj> differences = new ArrayList();

    @lombok.Getter
    @lombok.Setter
    @lombok.NoArgsConstructor
    @lombok.AllArgsConstructor
    public static class Obj {

        private String name;
        private String no;
        private String id;

        private List<String> old = new ArrayList();
        private List<String> neww = new ArrayList();

    }
}
