/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.materialtypes;

import regiec.knast.erp.api.model.bases.MaterialTypeBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AddMaterialTypeRequest extends MaterialTypeBase{

}
