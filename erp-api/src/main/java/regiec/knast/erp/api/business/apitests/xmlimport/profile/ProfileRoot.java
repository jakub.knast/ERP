/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests.xmlimport.profile;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Data
@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProfileRoot {

    private List<Profile> material;

}
