/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.order;

import java.util.ArrayList;
import java.util.Date;
import regiec.knast.erp.api.dto.orderedproducts.AddOrderPosition;
import regiec.knast.erp.api.model.bases.OrderBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddOrderRequest extends OrderBase {

    private ArrayList<AddOrderPosition> orderPositions = new ArrayList();
    private String recipientId;
    private boolean frameworkContractOrder;

    public AddOrderRequest(String recipientId, String nr, String comments, Date orderCreationDate, boolean frameworkContractOrder) {
        super(nr, comments, orderCreationDate, false);
        this.recipientId = recipientId;
        this.frameworkContractOrder = frameworkContractOrder;
    }
}
