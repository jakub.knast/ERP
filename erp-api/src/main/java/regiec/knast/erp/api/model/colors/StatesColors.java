/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.colors;

import java.awt.Color;

/**
 *
 * @author fbrzuzka
 */
public enum StatesColors {
    //
    //Commons
    ERROR_COLOR(StatesColors.lightRed, StatesColors.black),
    REMOVED_COLOR(StatesColors.lightRed, StatesColors.black),
    NEW_COLOR(StatesColors.lazur, StatesColors.black),
    NORMAL_COLOR(StatesColors.lightGreen, StatesColors.black),
    //
    //DeliveryOrderState
    WAITIN_FOR_APROVEMENT_COLOR(StatesColors.orange, StatesColors.black),
    APROVED_COLOR(StatesColors.lazur, StatesColors.black),
    //
    //DevelopmentState
    //
    //MachineState    
    WORKING_COLOR(StatesColors.lazur, StatesColors.black),
    BROKEN_COLOR(StatesColors.lightRed, StatesColors.black),
    //
    //MaterialDemandState
    NOT_SATISFIED_COLOR(StatesColors.orange, StatesColors.black),
    PARTLY_SATISFIED_COLOR(StatesColors.orange, StatesColors.black),
    SATISFIED_COLOR(StatesColors.lightGreen, StatesColors.black),
    //
    //MaterialState
    ADDED_MANUALY_COLOR(StatesColors.lazur, StatesColors.black),
    ORDERED_COLOR(StatesColors.orange, StatesColors.black),
    IN_SHOPPING_CART_COLOR(StatesColors.orange, StatesColors.black),
    IN_SHOPPING_CART_RESERVED_COLOR(StatesColors.darkOrange, StatesColors.black),
    ON_STOCK_AVALIABLE_COLOR(StatesColors.lazur, StatesColors.black),
    ON_STOCK_RESERVED_COLOR(StatesColors.lazur, StatesColors.black),
    RESERVED_COLOR(StatesColors.lazur, StatesColors.black),
    IN_PRODUCTION_COLOR(StatesColors.lazur, StatesColors.black),
    DONE_COLOR(StatesColors.lightGreen, StatesColors.black),
    CANCELED_FROM_ORDER_COLOR(StatesColors.lightRed, StatesColors.black),
    CANCELED_FROM_PRODUCTION_COLOR(StatesColors.lightRed, StatesColors.black),
    BROKEN_ON_STOCK_COLOR(StatesColors.lightRed, StatesColors.black),
    BROKEN_ON_PRODUCTION_COLOR(StatesColors.lightRed, StatesColors.black),
    ///MaterialTemplateState
    //
    //MaterialTypeState
    //
    //OrderState    
    PLANNED_COLOR(StatesColors.lazur, StatesColors.black),
    FINISHED_COLOR(StatesColors.lightGreen, StatesColors.black),
    PARTLY_DISPOSED(StatesColors.lazur, StatesColors.black),
    DISPOSED(StatesColors.lightGreen, StatesColors.black),
    //
    //ProductState
    //ADDED_MANUALY
    //ORDERED
    //PLANNED
    //IN_PRODUCTION
    //REMOVED_FROM_ORDER
    //
    //ProductTemplateState
    //
    //ProductTypeState
    //
    //ProductionJobState
    //
    //ProductionOrderState
    MATERIAL_DEMANDS_UNCOMPLETE_COLOR(StatesColors.orange, StatesColors.black),
    //PLANNED
    WITH_MATERIAL_DEMANDS_COLOR(StatesColors.lightGreen, StatesColors.black),
    ON_PRODUCTION_COLOR(StatesColors.lazur, StatesColors.black),
    CANCELED_COLOR(StatesColors.lightRed, StatesColors.black),
    //
    //ProviderState
    //
    //RecipientState
    //
    //SellerState
    //
    //
    //WareState
    //ADDED_MANUALY
    //
    //WareTemplateState
    //
    //WareTypeState
    //
    //WorkerState
    RESTORED_COLOR(StatesColors.lazur, StatesColors.black),
    FIRED_COLOR(StatesColors.lightRed, StatesColors.black);

    private String backgroundColor;
    private String fontColor;

    private static final String black = "#000000";
    private static final String white = "#ffffff";
    private static final String lazur = "#26cdcd";
    private static final String lightRed = "#ff7c4c";
    private static final String lightGreen = "#95cb9d ";
    private static final String orange = "#ffb732  ";
    private static final String darkOrange = "#D98020  ";

    private StatesColors(String backgroundColor, String fontColor) {
        this.backgroundColor = backgroundColor;
        this.fontColor = fontColor;
    }

    private StatesColors(Color backgroundColor, Color fontColor) {
        this.backgroundColor = colorToHex(backgroundColor);
        this.fontColor = colorToHex(fontColor);
    }

    private String colorToHex(Color color) {
        return "#" + Integer.toHexString(color.getRGB()).substring(2).toUpperCase();
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }
}
