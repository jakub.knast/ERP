/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.utils;

import com.google.common.collect.Lists;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import static org.exolab.castor.mapping.loader.Types.isSimpleType;
import org.reflections.Reflections;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.EntityBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ReflectionUtil {
//
//    public static List<Field> getFieldsWithAnnotation(Class c, Class<? extends Annotation> a) {
//        return Arrays.stream(c.getDeclaredFields())
//                .filter(f -> f.isAnnotationPresent(a))
//                .collect(Collectors.toList());
//    }
//
//    public static List<Field> getRecursiveFieldsWithAnnotation(Class c, Class<? extends Annotation> a) {
//        return Arrays.stream(c.getDeclaredFields())
//                .filter(f -> f.isAnnotationPresent(a))
//                .collect(Collectors.toList());
//        
//    }

    public static List<Field> getAllFields(Class<?> type) {
        List<Field> fields = new ArrayList();
        fields.addAll(Arrays.asList(type.getDeclaredFields()));
        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }

    private static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));
        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }

    public static List<Field> getFieldsWithAnnotation(Class c, Class<? extends Annotation> a) {
        return getAllFields(c).stream()
                .filter(f -> f.isAnnotationPresent(a))
                .collect(Collectors.toList());
    }

    public static List<Field> getRecursiveFieldsWithAnnotation(Class c, Class<? extends Annotation> a) {
        List<Field> annotated = new ArrayList();
        getRecursiveFieldsWithAnnotation(c, a, annotated);
        return annotated;
    }

    private static void getRecursiveFieldsWithAnnotation(Class c, Class<? extends Annotation> a, List<Field> annotated) {
        List<Field> allFields = getAllFields(c);
        allFields.stream()
                .filter(f -> f.isAnnotationPresent(a))
                .forEach(f -> annotated.add(f));

        for (Field fi : allFields) {
            Class type = fi.getType();
            if (!isSimpleType(type) && !type.isEnum()) {
                // is ojbect
                getRecursiveFieldsWithAnnotation(type, a, annotated);
            }
        }
    }

    public static List<FieldInfo> getFieldsByPredicate(Class c, Predicate<? super Field> predicate) {
        List<FieldInfo> collected = new ArrayList();
        List<Field> allFields = getAllFields(c);
        allFields.stream()
                .filter(predicate)
                .forEach(f -> collected.add(new FieldInfo(f, c, new ArrayList())));
        return collected;
    }

    public static List<FieldInfo> getRecursiveFieldsByPredicate(Class c, Predicate<? super Field> predicate) {
        return getRecursiveFieldsByPredicate(c, predicate, (Field t) -> false);
    }

    public static List<FieldInfo> getRecursiveFieldsByPredicate(Class c, Predicate<? super Field> predicate, Predicate<? super Field> stopPredicate) {
        List<FieldInfo> collected = new ArrayList();
        getRecursiveFieldsByPredicate(c, predicate, stopPredicate, collected, new ArrayList(), new HashSet(), 0);
        return collected;
    }

    private static void getRecursiveFieldsByPredicate(Class c, Predicate<? super Field> predicate, Predicate<? super Field> stopPredicate, List<FieldInfo> collected, List<String> path, Set<Class> visitedClasses, int loopCounter) {
        if (loopCounter > 100) {
            throw new ERPInternalError("Probably loop on class: " + c.getSimpleName());
        }
        // this class is already in this set, we return to prevent infinite loops
        if (visitedClasses.add(c) == false) {
            return;
        }
        List<Field> allFields = getAllFields(c);
        allFields.stream()
                .filter(predicate)
                .forEach(f -> collected.add(new FieldInfo(f, c, path)));

        for (Field fi : allFields) {
            Class type = fi.getType();
            if (!isSimpleType(type) && !type.isEnum() && !stopPredicate.test(fi)) {
                // is object
                loopCounter++;
                List<String> clonedPath = new ArrayList(path);
                clonedPath.add(fi.getName());
                getRecursiveFieldsByPredicate(type, predicate, stopPredicate, collected, clonedPath, visitedClasses, loopCounter);
            }
        }
    }

    public static Set<Class<? extends EntityBase>> getAllEntitiesClasses() {
        return new Reflections("regiec.knast").getSubTypesOf(EntityBase.class);
    }

    @lombok.AllArgsConstructor
    public static class FieldInfo {

        public final Field field;
        public final Class clazz;
        public final List<String> path;

        public String fieldPath() {
            ArrayList<String> li = Lists.newArrayList(path);
            li.add(field.getName());
            return String.join(".", li);
        }
    }
}
