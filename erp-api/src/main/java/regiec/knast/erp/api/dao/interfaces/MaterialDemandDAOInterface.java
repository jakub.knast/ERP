/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.containers.MaterialDemandEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface MaterialDemandDAOInterface extends DAOBaseInterface<MaterialDemandEntity, MaterialDemandEntitiesList> {

}
