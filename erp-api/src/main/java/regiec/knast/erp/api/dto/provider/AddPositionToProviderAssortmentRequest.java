/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.provider;

import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.model.SellUnit;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddPositionToProviderAssortmentRequest {

    private String id;
    private String salesLength;
    private MoneyERP salesPrice;
    private String materialCode;
    private SellUnit sellUnit;
}
