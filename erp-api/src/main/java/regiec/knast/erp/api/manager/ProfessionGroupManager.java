/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.manager;

import regiec.knast.erp.api.entities.ProfessionGroupEntity;

/**
 *
 * @author fbrzuzka
 */
public class ProfessionGroupManager {

    private final static ProfessionGroupEntity transportGroup;
    static{
        transportGroup = new ProfessionGroupEntity();
        transportGroup.setCode("15");
        transportGroup.setName("Transport");
        transportGroup.setLabel("15-Transport");
    }
    public static ProfessionGroupEntity getTransportGroup() {        
        return transportGroup;
    }
    
}
