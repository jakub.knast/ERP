/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class RemoveWithConfirmationRequest {

    private String id;
    private Boolean confirmed = false;
    private Boolean removeRequestFromUp = false;

    public Boolean getRemoveRequestFromUp() {
        return removeRequestFromUp;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }
}
