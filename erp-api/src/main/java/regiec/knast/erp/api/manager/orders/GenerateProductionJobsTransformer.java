/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.orders;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionJobDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dto.order.GenerateSTHResponse;
import regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsRequest;
import regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsResponse;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.dto.semiproduct.FromCreatingProductionJobContext;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.exceptions.ErpStateValidateionException;
import regiec.knast.erp.api.manager.ProductManager;
import regiec.knast.erp.api.model.TreeNodeType;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.model.states.ProductionJobState;
import regiec.knast.erp.api.model.states.ProductionOrderState;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author fbrzuzka
 */
public class GenerateProductionJobsTransformer {

    private static int i = 0;

    @Inject
    private Validator validator;
    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private ProductionOrderDAOInterface productionOrderDAO;
    @Inject
    private ProductionJobDAOInterface productionJobDAO;
    @Inject
    private ProductManager productManager;
    @Inject
    private ProductDAOInterface productDAO;

    private List<ProductionJobEntity> generated = new ArrayList();

    public GenerateProductionJobsResponse generateProductionJobs(GenerateProductionJobsRequest from) {
        generated = new ArrayList();
        validator.validateNotNull(from).throww();
        validator.validateNotNull(from.getProductionOrderId()).throww();
        ProductionOrderEntity productionOrderEntity = productionOrderDAO.get(from.getProductionOrderId());
        validator.validateNotNull(productionOrderEntity).throww();
        validateState(productionOrderEntity.getState());
        validate(productionOrderEntity);
        String productId = productionOrderEntity.getProduct().getId();
        ProductEntity productEntity = productDAO.get(productId);
        validator.validateNotNull(productEntity).throww();

        ProductionJobEntity productionJobEntity = new ProductionJobEntity();
        productionJobEntity.setTestCasesNumbers(productionOrderEntity.getTestCasesNumbers());
        productionJobEntity.setTreeNodeType(TreeNodeType.ROOT);
        productionJobEntity.setProductEntity(productionOrderEntity.getProduct());
        productionJobEntity.setParentId(null);
        productionJobEntity.setParentNo(null);
        productionJobEntity.setParent(null);
        productionJobEntity.setState(ProductionJobState.NEW);
        productionJobEntity.setProductionOrderId(productionOrderEntity.getId());
        productionJobEntity.setProductionOrderNo(productionOrderEntity.getNo());
        productionJobEntity.setNo(generateNo(productionOrderEntity.getNo()));
        String id = IDUtil.nextId_();
        productionJobEntity.setId(id);
        productionJobEntity.setChildren(createChildrenTasks(productionOrderEntity, productionJobEntity));
        productionJobDAO.add(productionJobEntity, id);
        generated.add(productionJobEntity);
        productionOrderEntity.setProductionJobs(generated);
        productionOrderEntity.setState(ProductionOrderState .PLANNED);
        productionOrderDAO.update(productionOrderEntity);
        productEntity.setState(ProductState.IN_PRODUCTION);
        productDAO.update(productEntity);
        ImmutableList<ProductionJobEntity> copyOf = ImmutableList.copyOf(generated);
        for (ProductionJobEntity copy : copyOf) {
            copy.setParent(null);
            copy.getProductEntity().getProductTemplate().setPartCard(null);
            if (copy.getChildren() != null) {
                for (ProductionJobEntity child : copy.getChildren()) {
                    child.getProductEntity().getProductTemplate().setPartCard(null);
                }
            }
        }
        i = 0;
        return new GenerateProductionJobsResponse(Boolean.TRUE, "", copyOf);//create " + orderEntity.getOrderPositions().size() + " production orders");
    }

    private List<ProductionJobEntity> createChildrenTasks(ProductionOrderEntity productionOrderEntity, ProductionJobEntity parent) {
        if (productionOrderEntity != null
                && productionOrderEntity.getProduct() != null
                && productionOrderEntity.getProduct().getProductTemplate() != null) {
            return createChildrenTasks(productionOrderEntity.getProduct().getProductTemplate(), parent, productionOrderEntity);
        }
        return new ArrayList();
    }

    private List<ProductionJobEntity> createChildrenTasks(
            ProductTemplateEntity productTemplateEntity, ProductionJobEntity parent, ProductionOrderEntity productionOrderEntity) {
        List<ProductionJobEntity> ret = new ArrayList();
        if (productTemplateEntity != null
                && productTemplateEntity.getPartCard() != null
                && productTemplateEntity.getPartCard().getSemiproductParts() != null) {
            for (SemiproductPart semiproductPart1 : productTemplateEntity.getPartCard().getSemiproductParts()) {
                ret.add(createTaskFromDetal(semiproductPart1, parent, productionOrderEntity));
            }
        }
        return ret;
    }

    private ProductionJobEntity createTaskFromDetal(SemiproductPart semiproductPart, ProductionJobEntity parent, ProductionOrderEntity productionOrderEntity) {

        ProductionJobEntity productionJobEntity = new ProductionJobEntity();
        productionJobEntity.setTreeNodeType(TreeNodeType.LEAF);
        parent.setChildren(null);
        productionJobEntity.setParentId(parent.getId());
        productionJobEntity.setParentNo(parent.getNo());
        productionJobEntity.setParent(parent);
        productionJobEntity.setState(ProductionJobState.NEW);
        productionJobEntity.setTestCasesNumbers(productionOrderEntity.getTestCasesNumbers());
        productionJobEntity.setProductionOrderId(productionOrderEntity.getId());
        productionJobEntity.setProductionOrderNo(productionOrderEntity.getNo());
        productionJobEntity.setNo(generateNo(productionOrderEntity.getNo()));
        String id = IDUtil.nextId_();
        productionJobEntity.setId(id);
        final ProductEntity productEntity = generateProductEntity(semiproductPart, productionOrderEntity, parent.getProductEntity().getCount());
        productionJobEntity.setProductEntity(productEntity);
        if (semiproductPart != null) {
            productionJobEntity.setChildren(createChildrenTasks(semiproductPart.getProductTemplate(), productionJobEntity, productionOrderEntity));
        }

        productionJobDAO.add(productionJobEntity, id);
        generated.add(productionJobEntity);
        return productionJobEntity;
    }

    private String generateNo(String no) {
        return no + "-" + String.valueOf(i++);
    }

    private ProductEntity generateProductEntity(SemiproductPart semiproductPart, ProductionOrderEntity productionOrderEntity, Integer parentDetalsCount) {
        FromCreatingProductionJobContext context = FromCreatingProductionJobContext.builder()
                .productTemplateId(semiproductPart.getProductTemplate().getId())
                .count(semiproductPart.getCount() * parentDetalsCount)
                .productionOrderNo(productionOrderEntity.getNo())
                .productionOrderId(productionOrderEntity.getId())
                .orderNo(productionOrderEntity.getOrderNo())
                .orderId(productionOrderEntity.getOrderId())
                .build();
        return productManager.createToProductionJob(context);
    }

    private void validate(ProductionOrderEntity poe) {
        List<String> validationList = new ArrayList();
        if (poe == null
                || poe.getProduct() == null
                || poe.getProduct().getProductTemplate() == null) {
            String poeNo = poe != null ? poe.getNo() : "zamwienie jest null";
            validationList.add("w zamowieniu produkcyjnym " + poeNo + " szablon jest pusty");

        } else {
            validationList.addAll(validatorDetal.validateProductTemplate(poe.getProduct().getProductTemplate()));
        }
        if (!validationList.isEmpty()) {
            throw new ConversionErrorWithCode("detal is not valid", ExceptionCodes.DETAL_TEMPLATE__NOT_VALID,
                    ImmutableMap.builder().put("message", String.join("\n", validationList)).build());
        }
    }

    private void validateState(ProductionOrderState state) {
        switch (state) {
            case WITH_MATERIAL_DEMANDS: {
                break;
            }
            default: {
                throw new ErpStateValidateionException(ExceptionCodes.GENERATE_PRODUCTION_JOBS_ONLY_IN__WITH_MATERIAL_DEMANDS__STATE,
                        "cannot do generateProductionJobs on order in state: " + state, null);
            }
        }
    }

    public void cancelProductionJob(ProductionJobEntity productionJob) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public GenerateSTHResponse removeForSureProductionJob(ProductionJobEntity productionJob) {
        validator.validateNotNull(productionJob).throww();

        productionJobDAO.remove(productionJob.getId());
        return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", "removed productionJob " + productionJob.getNo()).build());
    }

}
