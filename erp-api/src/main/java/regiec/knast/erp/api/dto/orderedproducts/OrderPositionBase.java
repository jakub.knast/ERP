/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.orderedproducts;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import regiec.knast.erp.api.model.MoneyERP;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class OrderPositionBase  {

    private String orderPositionId;
    private String positionOnOrder;
    private int count;
    private MoneyERP piecePrice;
    private MoneyERP piecePriceWithDiscount;
    private Double discount = 0.0;
    private String comments;
    private Date deliveryDate;
    // reference to production order generated from this order posiiton
    private String productionOrderId;
    
    //framework order fields:
    private int disposedCount;
    private int toDisposeCount;
    // reference to produstion orders(dispositions) generated from this order posiiton
    private List<String> dispositionsId = new ArrayList();

    
    
}
