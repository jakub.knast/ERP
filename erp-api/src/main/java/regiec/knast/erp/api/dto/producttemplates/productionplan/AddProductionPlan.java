/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.productionplan;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class AddProductionPlan extends ProductionPlanBase {

    private String operationNameId;
    private String professionGroupId;

    public AddProductionPlan(String operationNameId, String professionGroupId, Double transportTime, Double preparationTime, Double operationTime, Integer operationNumber, String comments, String comments2) {
        super(transportTime, preparationTime, operationTime, operationNumber, comments, comments2);
        this.operationNameId = operationNameId;
        this.professionGroupId = professionGroupId;
    }
    
}
