
package regiec.knast.erp.api.adaptersgson;

/**
 *
 * @author https://github.com/fatroom/gson-money-typeadapter
 */

import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.math.BigDecimal;
import javax.money.*;
import regiec.knast.erp.api.model.MoneyERP;

/**
 * Allows parsing of any class that implements {@link MonetaryAmount}.
 */
class MoneyErpAdapter extends TypeAdapter<MoneyERP> {

//    private MonetaryAmountFactory<? extends MoneyERP> monetaryFactory;
//
//    MoneyErpAdapter(final MonetaryAmountFactory<? extends MoneyERP> monetaryFactory) {
//        this.monetaryFactory = monetaryFactory;
//    }

    @Override 
    public void write(final JsonWriter writer, final MoneyERP moneyErp) throws IOException {

        if (moneyErp == null) {
            writer.nullValue();
            return;
        }

        writer.beginObject()                                                 //
              .name("amount")                                                //
              .value(moneyErp.getNumber().numberValueExact(BigDecimal.class))   //
              .name("currency").value(moneyErp.getCurrency().getCurrencyCode()) //
              .endObject();
    }

    @Override
    public MoneyERP read(final JsonReader reader) throws IOException {

        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull();
            return null;
        }

        BigDecimal amount = null;
        String currency = null;
//        CurrencyUnit currency = null;

        try {

            reader.beginObject();
            while (reader.hasNext()) {

                switch (reader.nextName()) {

                    case "amount" :
                        amount = new BigDecimal(reader.nextString());
                        break;

                    case "currency" :
                        currency = reader.nextString();
//                        currency = Monetary.getCurrency(reader.nextString());
                        break;

                    default :
                        reader.skipValue();
                }
            }

            reader.endObject();

        } catch (NumberFormatException e) {
            throw new JsonSyntaxException("Non numeric String contained in the [amount] field.", e);
        } catch (UnknownCurrencyException e) {
            throw new JsonSyntaxException(e);
        }

        if (amount == null || currency == null) {
            String errorMessage = buildMissingFieldsErrorMessage(amount, currency);
            throw new JsonSyntaxException(errorMessage);
        }

        return MoneyERP.create(amount, currency);
    }

//    private String buildMissingFieldsErrorMessage(final BigDecimal amount, final CurrencyUnit currency) {
    private String buildMissingFieldsErrorMessage(final BigDecimal amount, final String currency) {
        StringBuilder builder = new StringBuilder();
        builder.append("Missing required fields from Monetary Amount: [");

        if (amount == null) {
            builder.append("amount");
        }

        if (currency == null) {
            builder.append("currency");
        }

        builder.append("].");
        return builder.toString();
    }
}