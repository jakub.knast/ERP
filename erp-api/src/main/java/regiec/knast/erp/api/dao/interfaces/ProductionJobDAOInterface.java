/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.entities.containers.ProductionJobEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ProductionJobDAOInterface extends DAOBaseInterface<ProductionJobEntity, ProductionJobEntitiesList> {

}
