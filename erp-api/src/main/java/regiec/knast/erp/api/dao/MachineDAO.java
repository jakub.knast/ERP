/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.MachineDAOInterface;
import regiec.knast.erp.api.entities.MachineEntity;
import regiec.knast.erp.api.entities.containers.MachineEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MachineDAO extends DAOBase<MachineEntity, MachineEntitiesList> implements MachineDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = MachineEntity.class;

    @Inject
    public MachineDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }
}
