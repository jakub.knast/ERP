/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.orders;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dao.interfaces.MaterialDemandDAOInterface;
import regiec.knast.erp.api.dao.interfaces.OrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dto.order.GenerateSTHResponse;
import regiec.knast.erp.api.dto.producttemplates.partcard.MaterialPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.entities.containers.MaterialDemandEntitiesList;
import regiec.knast.erp.api.exceptions.ErpStateValidateionException;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.model.states.MaterialDemandState;
import regiec.knast.erp.api.model.states.ProductionOrderState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
public class GenerateMaterialDemandsTransformer {

    @Inject
    private OrderDAOInterface orderDAO;
    @Inject
    private ProductionOrderDAOInterface productionOrderDAO;
    @Inject
    private Validator validator;
    @Inject
    private MaterialDemandDAOInterface materialDemandDAO;

    public Object generateMaterialDemands(String productionOrderId) {
        ProductionOrderEntity productionOrder = productionOrderDAO.get(productionOrderId);
        validator.validateNotNull(productionOrder).throww();
        validateState(productionOrder.getState());

        OrderEntity order = orderDAO.get(productionOrder.getOrderId());
        ProductEntity product = productionOrder.getProduct();
        ProductTemplateEntity productTemplateForDemand = productionOrder.getProduct().getProductTemplate();
        System.out.println("asd : " + productTemplateForDemand.getPartCard() != null);
        List<MaterialDemandEntity> demands = createDemandsRecursively(productTemplateForDemand, order, product, productionOrder.getId());
        MaterialDemandEntitiesList ens = new MaterialDemandEntitiesList();
        ens.addAll(demands);
        saveAll(ens);
        productionOrder.setState(ProductionOrderState.MATERIAL_DEMANDS_UNCOMPLETE);
        productionOrder.setMaterialDemands(demands);
        productionOrder.setMaterialDemandsStr(stringifyMaterialDemands(demands));
        productionOrderDAO.update(productionOrder);
        return new GenerateSTHResponse(Boolean.TRUE, ens.listToDto());
    }

    private void validateState(ProductionOrderState state) {
        switch (state) {
            case NEW: {
                break;
            }
            default: {
                throw new ErpStateValidateionException(ExceptionCodes.GENERATE_MATERIAL_DEMAND_ONLY_IN__NEW__STATE, "cannot do generateMateriaDemand on order in state", null);
            }
        }
    }

    private List<MaterialDemandEntity> createDemandsRecursively(ProductTemplateEntity template, OrderEntity order, ProductEntity product, String productionOrderId) {
        List<MaterialDemandEntity> demands = createForTemplate(template, order, product, productionOrderId);
        if (template.getPartCard().getSemiproductParts() != null) {
            for (SemiproductPart semiproductPart : template.getPartCard().getSemiproductParts()) {
                demands.addAll(createDemandsRecursively(semiproductPart.getProductTemplate(), order, product, productionOrderId));
            }
        }
        return demands;
    }

    private List<MaterialDemandEntity> createForTemplate(ProductTemplateEntity template, OrderEntity order, ProductEntity product, String productionOrderId) {
        List<MaterialDemandEntity> ret = new ArrayList();
        if (template.getPartCard().getMaterialParts() != null) {
            for (MaterialPart materialPart : template.getPartCard().getMaterialParts()) {
                MaterialDemandEntity mde = new MaterialDemandEntity();
                mde.setDetalTemplate(template);
                mde.setOrder(order);
                mde.setTopProductInfo(product.toTopProductInfo());
                mde.setProductionOrderId(productionOrderId);
                mde.setState(MaterialDemandState.NEW);
                mde.setMaterialTemplate(materialPart.getMaterialTemplate());
                if (product.getCount() == null || product.getCount() == 0 || materialPart.getCount() == null || materialPart.getCount() == 0) {
                    throw new ConversionError("Count is null");
                } else {
                    mde.setCount(product.getCount() * materialPart.getCount());
                    mde.setUnsatisfied(mde.getCount());
                    mde.setSatisfied(0);
                }
                mde.setCuttingSize(CuttingSizeManager.createCuttingSize(
                        materialPart.getMaterialTemplate().getMaterialType().getCuttingDimensionTemplate(),
                        materialPart.getCuttedSectionSizeWithBorders())
                );
                ret.add(mde);
            }
        }
        return ret;
    }

    private void saveAll(MaterialDemandEntitiesList ens) {
        for (MaterialDemandEntity e : ens) {
            materialDemandDAO.add(e);
        }
    }

    @Deprecated
    private ProductEntity trimProduct(ProductEntity product) {
        ProductEntity ret = BeanUtilSilent.translateBeanAndReturn(product, new ProductEntity());
        ret.setComments(null);
        ret.setCreationDate(null);
        ret.setDeadline(null);
        ret.setLastModificationDate(null);
        ret.setLocalization(null);
        ret.setRecipient(null);
        ret.getProductTemplate().setBuildingTime(null);
        ret.getProductTemplate().setBuildingTimeStr(null);
        ret.getProductTemplate().setBuildingTimeTJ(null);
        ret.getProductTemplate().setBuildingTimeTPZ(null);
        ret.getProductTemplate().setComments(null);
        ret.getProductTemplate().setCreationDate(null);
        ret.getProductTemplate().setLastModificationDate(null);
        ret.getProductTemplate().setPartCard(null);
        ret.getProductTemplate().setSalesOffers(null);
        ret.getProductTemplate().setProductionPlan(null);
        ret.getProductTemplate().setReportTemplate(null);
        ret.getProductTemplate().setTechDrawingDXF(null);
        ret.getProductTemplate().setTechDrawingPDF(null);
        ret.getProductTemplate().setTotalBuildingTime(null);
        ret.getProductTemplate().setTotalBuildingTimeStr(null);
        ret.getProductTemplate().setTotalBuildingTimeTJ(null);
        ret.getProductTemplate().setTotalBuildingTimeTPZ(null);
        ret.getProductTemplate().setValidationMessage(null);
        ret.getProductTemplate().setValidationStatus(null);
        return ret;
    }

    public static List<String> stringifyMaterialDemands(List<MaterialDemandEntity> demands) {
        List<String> strDem = new ArrayList();
        for (MaterialDemandEntity d : demands) {
            StringBuilder str = new StringBuilder();
//            str.append("Detal: ");
//            str.append(d.getDetalTemplate().getName());
//            str.append(", No: ");
//            str.append(d.getDetalTemplate().getNo());
            str.append("Spełnione? ");
            str.append(d.getCount().equals(d.getSatisfied()) ? "Tak" : "Nie");
            str.append(", Materiał: ");
            str.append(d.getMaterialTemplate().getMaterialType().getName());
            str.append(", Wymiar: ");
            str.append(d.getMaterialTemplate().getParticularDimension());
            str.append(", Ciętka: ");
            str.append(d.getCuttingSize().getIsOneDimensial() ? d.getCuttingSize().getLength().toString() : d.getCuttingSize().getArea().toString());
            str.append(", Ilość: ");
            str.append(d.getCount());
            strDem.add(str.toString());
        }
        return strDem;
    }
}
