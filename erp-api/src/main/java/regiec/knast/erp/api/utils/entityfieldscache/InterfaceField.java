/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils.entityfieldscache;

import java.util.List;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class InterfaceField {

    public InterfaceField(String searchName, List<String> overrideNames, Class fieldType) {
        this.searchName = searchName;
        this.overrideNames = overrideNames;
        this.fieldType = fieldType;
    }
    
    private String searchName;
    private List<String> overrideNames;
    private Class fieldType;
    private Object value;
    
}
