/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.containers.ProductEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ProductDAOInterface extends DAOBaseInterface<ProductEntity, ProductEntitiesList> {

    ProductEntity getProduct(String id);

    ProductEntity updateProduct(ProductEntity entity);

    int removeProduct(String id);
   
}
