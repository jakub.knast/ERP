/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDemandDAOInterface;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.containers.MaterialDemandEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MaterialDemandDAO extends DAOBase<MaterialDemandEntity, MaterialDemandEntitiesList> implements MaterialDemandDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = MaterialDemandEntity.class;

    @Inject
    public MaterialDemandDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }
}
