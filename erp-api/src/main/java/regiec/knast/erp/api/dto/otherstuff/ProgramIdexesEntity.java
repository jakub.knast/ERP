/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.otherstuff;

import java.util.Date;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("programIndexes")
public class ProgramIdexesEntity implements EntityBase {

    @Id
    private String id;
    private Long indexValue;
    private EntityType indexType;
    private Date generationDate;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
