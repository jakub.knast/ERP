/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class RemoveProductTemplateSemiproductPartRequest{
    
    private String productTemplateId;
    private String semiproductPartId;
}
