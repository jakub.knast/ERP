/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.driver;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import java.math.BigDecimal;
import org.bson.types.Decimal128;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.converters.TypeConverter;
import org.mongodb.morphia.mapping.MappedField;
import regiec.knast.erp.api.model.MoneyERP;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MongoDriver implements MongoInterface {

    private final Datastore datastore;
    private final MongoClient mongoClient;
    private Morphia morphia;
    private GridFS gridFS;

    @Inject
    private MongoDriver() {
        mongoClient = new MongoClient("localhost", 27017);
        morphia = new Morphia();
        morphia.mapPackage("regiec.knast.erp.api.entities", true);
        datastore = morphia.createDatastore(mongoClient, "erp");
        morphia.getMapper().getOptions().setStoreEmpties(true);
        morphia.getMapper().getConverters().addConverter(new MoneyConverter(MoneyERP.class));
        gridFS = new GridFS(datastore.getDB());
    }

    public static class MoneyConverter extends TypeConverter {

        public MoneyConverter() {
        }

        public MoneyConverter(Class... types) {
            super(types);
        }

        @Override
        public Object decode(Class<?> targetClass, Object fromDBObject, MappedField optionalExtraInfo) {
            if (!MoneyERP.class.isAssignableFrom(targetClass)) {
                throw new UnsupportedOperationException("This is not a class for this typeConverter");
            }
            DBObject dbObj = (DBObject) fromDBObject;
            String currency = (String) dbObj.get("currency");
           Decimal128 decimal128 = ( Decimal128)dbObj.get("amount");
//            BigDecimal amount  = decimal128.bigDecimalValue();

//            Object decode = decode( org.bson.types.Decimal128.class,  dbObj.get("amount"), optionalExtraInfo);
//            System.out.println("decode: " + decode.getClass().getCanonicalName());
            MoneyERP moneyERP = MoneyERP.create(decimal128.bigDecimalValue(), currency);
            return moneyERP;
        }

        @Override
        public Object encode(Object value, MappedField optionalExtraInfo) {
            if (!MoneyERP.class.isAssignableFrom(value.getClass())) {
                throw new UnsupportedOperationException("This is not a class for this typeConverter");
            }
            MoneyERP money = (MoneyERP) value;
              DBObject obj = new BasicDBObject();
            obj.put("currency", money.getCurrency().getCurrencyCode());
            obj.put("amount", new Decimal128(money.getNumber().numberValueExact(BigDecimal.class)));
            return obj;
        }

    }

    @Override
    public Datastore getDatastore() {
        return datastore;
    }

    @Override
    public void closeConnections() {
        mongoClient.close();
    }

    @Override
    public GridFS getGridFS() {
        return gridFS;
    }
}
