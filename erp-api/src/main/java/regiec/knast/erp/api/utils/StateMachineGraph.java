/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import java.awt.*;
import java.util.Map;
import java.util.Set;
import javax.swing.JFrame;
import regiec.knast.erp.api.model.states.OrderState;
import regiec.knast.erp.api.utils.jung.layouts.LongestPathLayout;
import regiec.knast.erp.api.validation.state.StateMachine;
import regiec.knast.erp.api.validation.state.StateMachine.Adjacent;
import regiec.knast.erp.api.validation.state.StateMachineAgregat;

public class StateMachineGraph extends JFrame {

    private Integer edgeNo = 0;

    public static void main(String[] args) {
//TreeLayoutDemo.main(null);
//VertexLabelAsShapeDemo.main(null);
        StateMachineGraph sg = new StateMachineGraph();
    }

    public StateMachineGraph() {
        super("Graf maszyny stanów dla zamówienia");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Graph g = getGraph();
//        DelegateTree<Integer, String> delegateTree = new DelegateTree();
//        delegateTree.addVertex(0);
//        delegateTree.addChild("0-1", 0, 1, EdgeType.DIRECTED);

//        Forest<String, Integer> forest = new DelegateForest();
//
//        forest.addVertex("0");
//        forest.addEdge(0, "0", "1", EdgeType.DIRECTED);
//        forest.addEdge(1, "0", "2", EdgeType.DIRECTED);
//        forest.addEdge(2, "1", "3", EdgeType.DIRECTED);
        VisualizationViewer<String, String> vv = new VisualizationViewer(new LongestPathLayout(g, "order-INITIAL"));//TreeLayout(g, 100, 100), new Dimension(500, 500));
//        VisualizationViewer<Integer, String> vv = new VisualizationViewer(new KKLayout  (g));//TreeLayout(g, 100, 100), new Dimension(500, 500));
        vv.setBackground(Color.white);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        getContentPane().add(vv);

        pack();
        setVisible(true);
    }

    public Graph getGraph() {
        Graph<String, String> g = new SparseGraph();

        StateMachine.PossibleChangesMap<OrderState> listaNaztępnikow = StateMachineAgregat.ORDER.listaNaztępnikow();

        for (Map.Entry<OrderState, Set<Adjacent<OrderState>>> e : listaNaztępnikow.entrySet()) {
            OrderState key = e.getKey();
            String keyVert = "order-" + key.name();
            g.addVertex(keyVert);
            Set<Adjacent<OrderState>> value = e.getValue();
            for (Adjacent<OrderState> v : value) {
                String vVert = "order-" + v.stateToGo.name();
                g.addVertex(vVert);
                g.addEdge(keyVert + " -> " + vVert, keyVert, vVert, EdgeType.DIRECTED);

            }
        }
        return g;
    }
}
/*
    public SimpleGraph(java.util.List<ProductionTaskEntity> generated) {
        super("Mój drugi graf");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        DelegateTree<String, Integer> delegateTree = new DelegateTree();
        ProductionTaskEntity root = findRoot(generated);
        delegateTree.addVertex(root.getNo());
        addChildren(root, delegateTree);

        VisualizationViewer<String, Integer> vv = new VisualizationViewer(new TreeLayout(delegateTree, 100, 100), new Dimension(500, 500));
        vv.setBackground(Color.white);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        getContentPane().add(vv);

        pack();
        setVisible(true);
    }

    private Integer calculateCountFromParent(ProductionTaskEntity me) {
        if(me.getParent() == null){
            return me.getDetalEntity().getCount();
        }
        java.util.List<ProductionTaskEntity> children = me.getParent().getChildren();
        return 0;
    }

    class A {
        public String no;
        public String templateNo;
        public Integer count;
        public Integer totalCount;
        public String name;

        public A(String no, String templateNo, Integer count, Integer totalCount, String name) {
            this.no = no;
            this.templateNo = templateNo;
            this.count = count;
            this.totalCount = totalCount;
            this.name = name.length() <= 17 ? name : name.substring(0, 17);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof A)) {
                return false;
            }
            return ((A) obj).no.equals(this.no);
        }

        @Override
        public int hashCode() {
            return no.hashCode();
        }

    }

    public SimpleGraph(ProductionTaskEntity root) {
        super("Mój drugi graf");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        DelegateTree<A, Integer> delegateTree = new DelegateTree();
//        delegateTree.addVertex(root.getDetalEntity().getDetalTemplateEntity().getNo());
        delegateTree.addVertex(new A(
                root.getNo(), 
                root.getDetalEntity().getDetalTemplateEntity().getNo(), 
                calculateCountFromParent(root), 
                root.getDetalEntity().getCount(), 
                root.getDetalEntity().getDetalTemplateEntity().getName()));
        System.out.println("r: " + root.getDetalEntity().getDetalTemplateEntity().getId());
        addChildren(root, delegateTree);

        VisualizationViewer<A, Integer> vv = new VisualizationViewer(new TreeLayout(delegateTree, 130, 100), new Dimension(1800, 700));
        // this class will provide both label drawing and vertex shapes
        VertexLabelAsShapeRenderer<A, Integer> vlasr = new VertexLabelAsShapeRenderer(vv.getRenderContext());

        // customize the render context
        vv.getRenderContext().setVertexLabelTransformer(
                // this chains together Transformers so that the html tags
                // are prepended to the toString method output
                new ChainedTransformer<A, String>(new Transformer[]{
            //new ToStringLabeller<A>(),
            new Transformer<A, String>() {
                public String transform(A input) {
                    return "<html><center>" + input.no
                            + "<p>" + input.templateNo
                            + "<p>" + input.name;
                }
            }}));
        vv.getRenderContext().setVertexShapeTransformer(vlasr);
        // customize the renderer
        vv.getRenderer().setVertexRenderer(new GradientVertexRenderer(Color.gray, Color.white, true));
        vv.getRenderer().setVertexLabelRenderer(vlasr);
        // vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.setBackground(Color.white);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        getContentPane().add(vv);

        pack();
        setVisible(true);
    }

//    private void createTree() {
//        Graph graph = new DirectedSparseGraph();
//    	graph.addVertex("V0");
//    	graph.addEdge(edgeFactory.create(), "V0", "V1");
//    	graph.addEdge(edgeFactory.create(), "V0", "V2");
//    	graph.addEdge(edgeFactory.create(), "V1", "V4");
//    	graph.addEdge(edgeFactory.create(), "V2", "V3");
//    	graph.addEdge(edgeFactory.create(), "V2", "V5");
//    	graph.addEdge(edgeFactory.create(), "V4", "V6");
//    	graph.addEdge(edgeFactory.create(), "V4", "V7");
//    	graph.addEdge(edgeFactory.create(), "V3", "V8");
//    	graph.addEdge(edgeFactory.create(), "V6", "V9");
//    	graph.addEdge(edgeFactory.create(), "V4", "V10");
//    	
//       	graph.addVertex("A0");
//       	graph.addEdge(edgeFactory.create(), "A0", "A1");
//       	graph.addEdge(edgeFactory.create(), "A0", "A2");
//       	graph.addEdge(edgeFactory.create(), "A0", "A3");
//       	
//       	graph.addVertex("B0");
//    	graph.addEdge(edgeFactory.create(), "B0", "B1");
//    	graph.addEdge(edgeFactory.create(), "B0", "B2");
//    	graph.addEdge(edgeFactory.create(), "B1", "B4");
//    	graph.addEdge(edgeFactory.create(), "B2", "B3");
//    	graph.addEdge(edgeFactory.create(), "B2", "B5");
//    	graph.addEdge(edgeFactory.create(), "B4", "B6");
//    	graph.addEdge(edgeFactory.create(), "B4", "B7");
//    	graph.addEdge(edgeFactory.create(), "B3", "B8");
//    	graph.addEdge(edgeFactory.create(), "B6", "B9");
//       	
//    }
 */
