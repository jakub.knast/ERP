/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.business.apitests;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.material.AddMaterialRequest;
import regiec.knast.erp.api.dto.material.MaterialDTO;
import regiec.knast.erp.api.dto.material.RemoveMaterialRequest;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.model.*;
import regiec.knast.erp.api.utils.ErpPrinter;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class RemoveEntitiesTest extends AllServicesInjector {
    private static MoneyERP money = MoneyERP.create(new BigDecimal(1.0), "PLN");

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(RemoveEntitiesTest.class);
        RemoveEntitiesTest mongoTest = (RemoveEntitiesTest) create;
        try {
            mongoTest.initServices();
            mongoTest.test_removeMaterial();
        } catch (Exception ex) {
            Logger.getLogger(ListTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    public void test_removeMaterial() {
        AddMaterialRequest addRequest = new AddMaterialRequest("590c39eb71a24a08c8e3fd2c", CuttingSizeManager.createOneDimCuttingSize(3.0), 2, AddMaterialRequest.CreationContext.MANUAL);        
        MaterialDTO materialDto =   materialService.addMaterial(addRequest);
        
        RemoveResponse removeResult = materialService.removeMaterial(new RemoveMaterialRequest(materialDto.getId(), true, false));
        ErpPrinter.printResult(removeResult);      
    }
}
