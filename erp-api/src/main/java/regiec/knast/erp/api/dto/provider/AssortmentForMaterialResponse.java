/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.provider;

import java.util.List;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AssortmentForMaterialResponse {

    private String providerName;
    private String providerId;
    private List<MaterialAssortmentPosition> materialAssortment;

}
