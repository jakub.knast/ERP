/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class UpdateMaterialPart extends AddMaterialPart {

    private String positionId;

    public UpdateMaterialPart(String positionId, String materialTemplateId, String cuttedSectionSize, String cuttedSectionSizeWithBorders, Integer count, String comments, String positionOnTechDraw) {
        super(materialTemplateId, cuttedSectionSize, cuttedSectionSizeWithBorders, count, comments, positionOnTechDraw);
        this.positionId = positionId;
    }

    public UpdateMaterialPart(String positionId, String materialTemplateId) {
        super(materialTemplateId);
        this.positionId = positionId;
    }
    
}
