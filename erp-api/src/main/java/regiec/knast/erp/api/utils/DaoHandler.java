/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import regiec.knast.erp.api.dao.interfaces.*;
import regiec.knast.erp.api.entities.*;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.ValidationException;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class DaoHandler {

    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private MaterialTypeDAOInterface materialTypeDAO;
    @Inject
    private WareDAOInterface wareDAO;
    @Inject
    private WareTemplateDAOInterface wareTemplateDAO;
    @Inject
    private WareTypeDAOInterface wareTypeDAO;
    @Inject
    private ProductDAOInterface productDAO;
    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;
    @Inject
    private ProviderDAOInterface providerDAO;
    @Inject
    private SellerDAOInterface sellerDAO;
    @Inject
    private RecipientDAOInterface recipientDAO;
    @Inject
    private ProductionOrderDAOInterface productionOrderDAO;
    @Inject
    private OrderDAOInterface OrderDAO;
    @Inject
    private DeliveryOrderDAOInterface deliveryOrderDAO;
    @Inject
    private WorkerDAOInterface workerDAO;
    @Inject
    private MachineDAOInterface machinesDAO;
    @Inject
    private ProductionJobDAOInterface productionJobDAO;
    @Inject
    private MaterialDemandDAOInterface materialDemandDAO;
    @Inject
    private DevelopmentDAOInterface developmentDAO;
    @Inject
    private ShoppingCartDAOInterface shoppingCartDAO;
    @Inject
    private TodoIssuesDAOInterface todoIssuesDAO;
    @Inject
    private OperationNameDAOInterface operationNameDAO;
    @Inject
    private ProfessionGroupDAOInterface professionGroupDAO;
    @Inject
    private DocumentDAOInterface documentDAO;
//Here_will_be_insert_dao_inject

    public DAOBaseInterface getDaoByEntityType(EntityType entityType) {
        switch (entityType) {
            case MATERIAL:
                return materialDAO;
            case MATERIAL_TEMPLATE:
                return materialTemplateDAO;
            case MATERIAL_TYPE:
                return materialTypeDAO;
            case WARE:
                return wareDAO;
            case WARE_TEMPLATE:
                return wareTemplateDAO;
            case WARE_TYPE:
                return wareTypeDAO;
            case PRODUCT:
                return productDAO;
            case PRODUCT_TEMPLATE:
                return productTemplateDAO;
            case PROVIDER:
                return providerDAO;
            case SELLER:
                return sellerDAO;
            case RECIPIENT:
                return recipientDAO;
            case PRODUCTION_ORDER:
                return productionOrderDAO;
            case ORDER:
                return OrderDAO;
            case DELIVERY_ORDER:
                return deliveryOrderDAO;
            case WORKER:
                return workerDAO;
            case MACHINE:
                return machinesDAO;
            case PRODUCTION_JOB:
                return productionJobDAO;
            case MATERIAL_DEMAND:
                return materialDemandDAO;
            case DEVELOPMENT:
                return developmentDAO;
            case SHOPPING_CART:
                return shoppingCartDAO;
            case TODO_ISSUES:
                return todoIssuesDAO;
            case OPERATION_NAME:
                return operationNameDAO;
            case PROFESSION_GROUP:
                return professionGroupDAO;
            case DOCUMENT:
                return documentDAO;
//Here_will_be_insert_dao_binding
            default:
                throw new ValidationException("There is no DAO mapping for: " + entityType, ExceptionCodes.INTERNAL_ERROR, null);
        }
    }

    public DAOBaseInterface getDaoByEntityClass(Class<? extends EntityBase> entityCLass) {
        EntityType entityType = null;
        if (entityCLass.equals(MaterialEntity.class)) {
            entityType = EntityType.MATERIAL;
            //
        } else if (entityCLass.equals(MaterialTemplateEntity.class)) {
            entityType = EntityType.MATERIAL_TEMPLATE;
            //
        } else if (entityCLass.equals(MaterialTypeEntity.class)) {
            entityType = EntityType.MATERIAL_TYPE;
            //
            //
        } else if (entityCLass.equals(WareEntity.class)) {
            entityType = EntityType.WARE;
            //
        } else if (entityCLass.equals(WareTemplateEntity.class)) {
            entityType = EntityType.WARE_TEMPLATE;
            //
        } else if (entityCLass.equals(WareTypeEntity.class)) {
            entityType = EntityType.WARE_TYPE;
            //
            //
        } else if (entityCLass.equals(ProductEntity.class)) {
            entityType = EntityType.PRODUCT;
            //
        } else if (entityCLass.equals(ProductTemplateEntity.class)) {
            entityType = EntityType.PRODUCT_TEMPLATE;
            //
            //
        } else if (entityCLass.equals(ProviderEntity.class)) {
            entityType = EntityType.PROVIDER;
            //
        } else if (entityCLass.equals(SellerEntity.class)) {
            entityType = EntityType.SELLER;
            //
        } else if (entityCLass.equals(RecipientEntity.class)) {
            entityType = EntityType.RECIPIENT;
            //
            //
        } else if (entityCLass.equals(ProductionOrderEntity.class)) {
            entityType = EntityType.PRODUCTION_ORDER;
            //
        } else if (entityCLass.equals(OrderEntity.class)) {
            entityType = EntityType.ORDER;
            //
        } else if (entityCLass.equals(DeliveryOrderEntity.class)) {
            entityType = EntityType.DELIVERY_ORDER;
            //
            //
        } else if (entityCLass.equals(WorkerEntity.class)) {
            entityType = EntityType.WORKER;
            //
        } else if (entityCLass.equals(MachineEntity.class)) {
            entityType = EntityType.MACHINE;
            //
        } else if (entityCLass.equals(OperationNameEntity.class)) {
            entityType = EntityType.OPERATION_NAME;
            //
            //
        } else if (entityCLass.equals(ProductionJobEntity.class)) {
            entityType = EntityType.PRODUCTION_JOB;
            //
        } else if (entityCLass.equals(MaterialDemandEntity.class)) {
            entityType = EntityType.MATERIAL_DEMAND;
            //
        } else if (entityCLass.equals(DevelopmentEntity.class)) {
            entityType = EntityType.DEVELOPMENT;
            //
        } else if (entityCLass.equals(ShoppingCartEntity.class)) {
            entityType = EntityType.SHOPPING_CART;
            //
        } else if (entityCLass.equals(TodoIssuesEntity.class)) {
            entityType = EntityType.TODO_ISSUES;
            //
        } else if (entityCLass.equals(ProfessionGroupEntity.class)) {
            entityType = EntityType.PROFESSION_GROUP;
            //
        } else if (entityCLass.equals(DocumentEntity.class)) {
            entityType = EntityType.DOCUMENT;
            //
        } else {
            throw new ValidationException("There is no DAO mapping for: " + entityType, ExceptionCodes.INTERNAL_ERROR, null);

//Here_will_be_insert_dao_binding
        }
        System.out.println("" + entityType);
        return getDaoByEntityType(entityType);
    }
}
