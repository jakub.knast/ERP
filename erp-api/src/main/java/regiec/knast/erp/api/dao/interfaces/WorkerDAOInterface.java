/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.WorkerEntity;
import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface WorkerDAOInterface extends DAOBaseInterface<WorkerEntity, WorkerEntitiesList> {

    WorkerEntity get(String id);

    WorkerEntitiesList list(PaginableFilterWithCriteriaRequest paginableFilterWithCriteria);
}
