/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.orders;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import regiec.knast.erp.api.dao.interfaces.OrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProgramIndexesDAOInterface;
import regiec.knast.erp.api.dto.order.GenerateProductionOrderDispositionRequest;
import regiec.knast.erp.api.dto.order.GenerateProductionOrderRequest;
import regiec.knast.erp.api.dto.order.GenerateProductionOrderResponse;
import regiec.knast.erp.api.dto.orderedproducts.DispositionOrderPosition;
import regiec.knast.erp.api.dto.orderedproducts.OrderPosition;
import regiec.knast.erp.api.dto.orderedproducts.OrdinaryOrderPosition;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.exceptions.ErpStateValidateionException;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.OrderState;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class GenerateProductionOrderTransformer {

    private static int i = 0;
    @Inject
    private ProgramIndexesDAOInterface programIndexesDAO;
    @Inject
    private Validator validator;
    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private OrderDAOInterface orderDAO;
    @Inject
    private ProductionOrderDAOInterface productionOrderDAO;
    @Inject
    private ProductDAOInterface productDAO;

    private final DispositionOrderPosition.ProductionOrderNoGenerator PRODUCTION_ORDER_NO_GENERATOR = new DispositionOrderPosition.ProductionOrderNoGenerator() {
        @Override
        public String generateNo(String orderEntityNo) {

            Long lastIndex = programIndexesDAO.nextIndex(EntityType.PRODUCTION_ORDER);
            Long newIndex = lastIndex + 1;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH);
            int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

            return orderEntityNo + "-" + String.valueOf(String.valueOf(year) + "." + String.valueOf(month) + "." + String.valueOf(day) + "-" + String.valueOf(newIndex));
        }
    };

    public GenerateProductionOrderResponse generateProductionOrder(GenerateProductionOrderRequest from) {
        validator.validateNotNull(from).throww();
        validator.validateNotNull(from.getOrderId()).throww();

        OrderEntity orderEntity = orderDAO.get(from.getOrderId());
        validator.validateNotNull(orderEntity).throww();
        validateIfOrderHasPositions(orderEntity);
        validate(orderEntity);
        orderEntity.validateStateForGenetatingProductionOrder(OrderState.PLANNED);
        orderEntity.setState(OrderState.PLANNED);
        Map messagee = new HashMap();
        messagee.put("message", "create " + orderEntity.getOrderPositions().size() + " production orders");
        for (OrderPosition orderPosition : orderEntity.getOrderPositions()) {
            ProductionOrderEntity add = generateSingleProductionOrder(orderPosition, orderEntity);
            String productId = orderPosition.getProduct().getId();
            ProductEntity productEntity = productDAO.get(productId);
            productEntity.updateProductionOrderData(add.getId(), add.getNo());
            productEntity.setState(ProductState.PLANNED);
            productDAO.update(productEntity);
            messagee.put("id", add.getId());
        }
        orderDAO.update(orderEntity);
        return new GenerateProductionOrderResponse(Boolean.TRUE, messagee);
    }

    public GenerateProductionOrderResponse generateProductionOrderDisposition(GenerateProductionOrderDispositionRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNull(request.getOrderId()).throww();
        validator.validateNotNull(request.getDeliveryDate()).throww();
        validator.validateCount(request.getCount());

        Date deliveryDate = request.getDeliveryDate();
        int countOnDisposition = request.getCount();

        OrderEntity orderEntity = orderDAO.get(request.getOrderId());
        validator.validateNotNull(orderEntity).throww();
        orderEntity.validateStateForGenetatingProductionOrder(OrderState.PARTLY_DISPOSED_F);
        validate(orderEntity);
        Map messagee = new HashMap();
        messagee.put("message", "create " + orderEntity.getOrderPositions().size() + " production orders dispositions");

        OrderPosition orderPosition = orderEntity.getOrderPositions().get(0);
        validator.validateNotNull(orderPosition, "order position").throww();
        DispositionOrderPosition dispositionOrderPosition = (DispositionOrderPosition) orderPosition;
        dispositionOrderPosition.substractDisposition(countOnDisposition);
        String dispositionPOid = IDUtil.nextId_();
        ProductionOrderEntity addedDispositionPO = dispositionOrderPosition.createDispositionProductionOrder(orderEntity, PRODUCTION_ORDER_NO_GENERATOR, deliveryDate, countOnDisposition);
        addedDispositionPO.setId(dispositionPOid);
        addedDispositionPO.setOrderPositionId(orderPosition.getPositionId());

        ProductEntity productToSet = handleDisposedProduct(orderPosition.getProduct().getId(), countOnDisposition, addedDispositionPO, request.getDeliveryDate());
        addedDispositionPO.setProduct(productToSet);

        addedDispositionPO = productionOrderDAO.add(addedDispositionPO, dispositionPOid);
        orderPosition.setProductionOrderId(addedDispositionPO.getId());

        messagee.put("id", addedDispositionPO.getId());
        if (orderPosition.getToDisposeCount() > 0) {
            orderEntity.setState(OrderState.PARTLY_DISPOSED_F);
        } else {
            orderEntity.setState(OrderState.DISPOSED_F);
        }
        orderDAO.update(orderEntity);
        return new GenerateProductionOrderResponse(Boolean.TRUE, messagee);
    }

    public ProductEntity handleDisposedProduct(String productId, int countOnDisposition, ProductionOrderEntity productionOrderEntity, Date deliveryDate) {
        ProductEntity productEntity = productDAO.get(productId);
//        productDAO.update(productEntity);

        validator.validateNotNull(productEntity, "product").throww();
        List<ProductState> statesToPlan = Lists.newArrayList(ProductState.ORDERED);
        if (!statesToPlan.contains(productEntity.getState())) {
            throw new ErpStateValidateionException(ExceptionCodes.PRODUCT__CANNOT_FINISH_PRODUCT_IN_THIS_STATE, "this is not a state for finish product: " + productEntity.getState());
        }
        ProductEntity ret = null;
        if (countOnDisposition < productEntity.getCount()) {
            int origCount = productEntity.getCount();
            ProductEntity productCopy = productDAO.get(productId);
            productCopy.disposeMe(productionOrderEntity.getId(), productionOrderEntity.getNo(), deliveryDate, countOnDisposition);
            productCopy = productDAO.add(productCopy, IDUtil.nextId_());
            ret = productCopy;

            productEntity.setCount(origCount - countOnDisposition);
            productEntity = productDAO.update(productEntity);
        } else if (productEntity.getCount() == countOnDisposition) {
            productEntity.disposeMe(productionOrderEntity.getId(), productionOrderEntity.getNo(), deliveryDate, countOnDisposition);
            productEntity = productDAO.update(productEntity);
            ret = productEntity;
        } else {
            throw new ERPInternalError("Wow, how can you go there??");
        }
        return ret;
    }

    public GenerateProductionOrderResponse generateOneProductionOrder(OrderPosition orderPosition, OrderEntity orderEntity) {
        validator.validateNotNull(orderPosition, "order position").throww();
        Map messagee = new HashMap();
        ProductionOrderEntity add = generateSingleProductionOrder(orderPosition, orderEntity);
        messagee.put("id", add.getId());
        return new GenerateProductionOrderResponse(Boolean.TRUE, messagee);
    }

    //----------------------------------
    private ProductionOrderEntity generateSingleProductionOrder(OrderPosition orderPosition, OrderEntity orderEntity) {
        OrdinaryOrderPosition ordinaryOrderPosition = orderPosition;
        ProductionOrderEntity productionOrderEntity = ordinaryOrderPosition.createOrdinaryProductionOrder(orderEntity, PRODUCTION_ORDER_NO_GENERATOR);
        productionOrderEntity.setOrderPositionId(orderPosition.getPositionId());
        productionOrderEntity = productionOrderDAO.add(productionOrderEntity);
        orderPosition.setProductionOrderId(productionOrderEntity.getId());
        return productionOrderEntity;
    }

    private void validate(OrderEntity orderEntity) {
        for (OrderPosition orderPosition : orderEntity.getOrderPositions()) {
            List<String> validationList = new ArrayList();
            if (orderPosition.getProduct().getProductTemplate() == null) {
                validationList.add("w zamowieniu " + orderEntity.getNr() + " szablon jest pusty");
            } else {
                validationList.addAll(validatorDetal.validateProductTemplate(orderPosition.getProduct().getProductTemplate()));
            }
            if (!validationList.isEmpty()) {
                throw new ConversionErrorWithCode("detal is not valid", ExceptionCodes.DETAL_TEMPLATE__NOT_VALID,
                        ImmutableMap.builder().put("message", String.join("\n", validationList)).build());
            }
        }
    }

    private void validateIfOrderHasPositions(OrderEntity orderEntity) {
        if (!orderEntity.getFrameworkContractOrder() && orderEntity.getOrderPositions().isEmpty()) {
            throw new ConversionErrorWithCode("There is no positions in order to generate", ExceptionCodes.ORDER__NO_POSITION_TO_GENERATE);
        }
    }
}
