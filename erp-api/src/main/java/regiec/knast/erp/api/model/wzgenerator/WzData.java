/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.wzgenerator;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.model.DocumentInterface;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WzData implements DocumentInterface{

    private String documentNo = "006600";
    private String date = " 1 stycznia 2017";
    private SellerInfo providerInfo = new SellerInfo();
    private RecipientInfo recipientInfo = new RecipientInfo();
    private List<WzItem> products = new ArrayList();

    @Override
    public DocumentType getType() {
        return DocumentType.WZ;
    }
}
