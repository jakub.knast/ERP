/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.productionjob;

import java.util.List;
import regiec.knast.erp.api.entities.ProductionJobEntity;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class GenerateProductionJobsResponse {
    private Boolean status;
    private String message;
    private List<ProductionJobEntity> generated;
}
