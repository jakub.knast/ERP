/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.materialDemand;

import java.util.Map;
import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTOBasisc;
import regiec.knast.erp.api.model.bases.MaterialDemandBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialDemandDTO extends MaterialDemandBase {

    private String id;
    private MaterialTemplateDTOBasisc materialTemplate;
    private Map<String, Object> detalTemplate;
    private Map<String, Object> order;
    private Map<String, Object> recipient;
}
