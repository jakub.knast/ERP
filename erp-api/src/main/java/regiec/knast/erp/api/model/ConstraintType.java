/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public enum ConstraintType {
    EXISTS,
    EQUALS,
    NOT_EQUALS, 
    EQUALS_IGNORE_CASE,
    LIKE, 
    LIKE_IGNORE_CASE,
    GRATHER_THAN, 
    GRATHER_THAN_OR_EQUAL, 
    LESS_THAN,
    LESS_THAN_OR_EQUAL
}
