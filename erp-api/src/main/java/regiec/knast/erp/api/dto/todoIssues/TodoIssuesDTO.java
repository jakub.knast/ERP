/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.todoIssues;

import regiec.knast.erp.api.model.bases.TodoIssuesBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class TodoIssuesDTO extends TodoIssuesBase {

    private String id;


}
