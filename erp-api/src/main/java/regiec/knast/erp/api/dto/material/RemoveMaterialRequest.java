/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.material;

import regiec.knast.erp.api.dto.RemoveWithConfirmationRequest;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class RemoveMaterialRequest extends RemoveWithConfirmationRequest {

    public RemoveMaterialRequest(String id, Boolean confirmed, Boolean removeRequestFromUp) {
        super(id, confirmed, removeRequestFromUp);
    }
}
