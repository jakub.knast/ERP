/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ERPMain implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        ObjectBuilderFactory.configure(GuiceBuilder.getInstance()); // moved to ContextInitializer
        ContextInitializer.getInstance().contextInitialized();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ContextInitializer.getInstance().contextDestroyed();
    }

}
