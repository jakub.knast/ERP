/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.productionplan;

import java.util.ArrayList;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.mongodb.morphia.annotations.Reference;
import org.seenmodit.core.interfaces.Position;
import org.seenmodit.core.interfaces.UpdatePosition;
import regiec.knast.erp.api.dto.operationName.OperationNameDTO;
import regiec.knast.erp.api.entities.OperationNameEntity;
import regiec.knast.erp.api.entities.ProfessionGroupEntity;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class ProductionPlan extends ProductionPlanBase implements Position, UpdatePosition, Comparable<ProductionPlan> {

    private String positionId;
    @Reference
    private OperationNameEntity operationName;
    @Reference
    private ProfessionGroupEntity professionGroup;

    @Override
    public String toString() {
        return getOperationName().toString();
    }

    public boolean hasSameProffesionGroup(ProductionPlan pp) {
        return this.getProfessionGroup().getCode().equals(pp.getProfessionGroup().getCode());
    }

    @Override
    public int compareTo(ProductionPlan o) {
        return this.getOperationNumber().compareTo(o.getOperationNumber());
    }

    public static ArrayList<ProductionPlanDTO> productionPlanToDTO(final ArrayList<ProductionPlan> entities) throws ConversionError {
        ArrayList<ProductionPlanDTO> productionPlanDTOs = new ArrayList();
        if (entities != null) {
            for (ProductionPlan entity : entities) {
                productionPlanDTOs.add(entity.convertToDTO());
            }
        }
        return productionPlanDTOs;
    }

    public ProductionPlanDTO convertToDTO() {
        ProductionPlanDTO ppdto = BeanUtilSilent.translateBeanAndReturn(this, new ProductionPlanDTO());
        ppdto.setOperationName((OperationNameDTO)this.getOperationName().convertToDTO());
        ppdto.setProfessionGroup(this.getProfessionGroup().convertToDTO());
        return ppdto;
    }
}
