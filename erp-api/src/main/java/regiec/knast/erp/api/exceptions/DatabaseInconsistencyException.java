/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.exceptions;

import java.util.Map;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author fbrzuzka
 */
public class DatabaseInconsistencyException extends ConversionErrorWithCode{
    


    public DatabaseInconsistencyException(String message) {
        super(message, ExceptionCodes.DATABASE_INCONSISTENCY);
    }
    
    public DatabaseInconsistencyException(String message,  Throwable cause) {
        super(message, ExceptionCodes.DATABASE_INCONSISTENCY, null, cause);
    }
    
    public DatabaseInconsistencyException(String message, Map params, Throwable cause) {
        super(message, ExceptionCodes.DATABASE_INCONSISTENCY, params, cause);
    }
    
    
}
