/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Inject;
import java.util.TreeSet;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ProgramConstsDAOInterface;
import regiec.knast.erp.api.dto.otherstuff.ProgramConstsEntity;
import regiec.knast.erp.api.model.ConstType;
import regiec.knast.erp.api.model.MaterialKind;
import regiec.knast.erp.api.model.Norm;
import regiec.knast.erp.api.model.SteelGrade;
import regiec.knast.erp.api.model.Tolerance;
import regiec.knast.erp.api.utils.IDUtil;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProgramConstsDAO implements ProgramConstsDAOInterface {

    private final Datastore datastore;
    private Gson gson = new Gson();

    @Inject
    public ProgramConstsDAO(MongoInterface driver) {
        this.datastore = driver.getDatastore();
    }

    @Override
    public ProgramConstsEntity addConst(ConstType type, String value) throws ConversionError {
        ProgramConstsEntity entity = getConsts();

        if (!Strings.isNullOrEmpty(value)) {
            switch (type) {
                case norm:
                    Norm n = new Norm(value);
                    if (entity.getNorm() == null) {
                        entity.setNorm(new TreeSet<Norm>());
                    }
                    if (!entity.getNorm().contains(n)) {
                        entity.getNorm().add(n);
                    }
                    break;
                case steelGrade:
                    SteelGrade sg = new SteelGrade(value);
                    if (entity.getSteelGrade() == null) {
                        entity.setSteelGrade(new TreeSet<SteelGrade>());
                    }
                    if (!entity.getSteelGrade().contains(sg)) {
                        entity.getSteelGrade().add(sg);
                    }
                    break;
                case tolerance:
                    Tolerance t = new Tolerance(value);
                    if (entity.getTolerance() == null) {
                        entity.setTolerance(new TreeSet<Tolerance>());
                    }
                    if (!entity.getTolerance().contains(t)) {
                        entity.getTolerance().add(t);
                    }
                    break;
                case materialKind:
                    MaterialKind mk = new MaterialKind(value);
                    if (entity.getMaterialKind() == null) {
                        entity.setMaterialKind(new TreeSet<MaterialKind>());
                    }
                    if (!entity.getMaterialKind().contains(mk)) {
                        entity.getMaterialKind().add(mk);
                    }
                    break;
                default:
                    throw new ConversionError("there is no const like: " + type);
            }

        } else {
            throw new ConversionError("type or value is null");
        }
        datastore.save(entity);
        return entity;
    }

    @Override
    public ProgramConstsEntity getConsts() {
        ProgramConstsEntity entity = datastore.createQuery(ProgramConstsEntity.class).disableValidation().get();
        if (entity == null) {
            entity = new ProgramConstsEntity();
            entity.setId(IDUtil.nextId_());
            datastore.save(entity);
        }
        return entity;
    }
}
