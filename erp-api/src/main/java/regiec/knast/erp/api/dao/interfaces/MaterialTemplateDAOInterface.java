/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.containers.MaterialTemplateEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface MaterialTemplateDAOInterface extends DAOBaseInterface<MaterialTemplateEntity, MaterialTemplateEntitiesList> {


    MaterialTemplateEntity getByName(String name);
}
