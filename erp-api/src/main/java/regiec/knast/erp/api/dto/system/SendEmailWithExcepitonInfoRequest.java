/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.system;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class SendEmailWithExcepitonInfoRequest {

    private String screenshot;
    private String request;
    private String response;
}
