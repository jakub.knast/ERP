/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class BulkFileReplacer {

    private final String sep = File.separator;
    
    public String projectLocation = "C:/git/ERP/erp-api/src/main/java";
    public String projectPackage = "regiec.knast.erp.api";
    public String projectPackagePath = "regiec/knast/erp/api";

    public static void main(String[] args) throws Exception {
        BulkFileReplacer thiss = new BulkFileReplacer();
        thiss.run();
    }

    public void run() throws Exception {
        String where = "import regiec.knast.erp.api.dto.document.GetDocumentRequest;";
        String from = "return ConvertersRegistry\\.convert\\(entity1.*;";
        String to = "return entity1.convertToDTO();";
        //           import regiec.knast.erp.api.dto.BasicGetRequest;
        boolean test = false;
//        test = true;
        if (test) {
            Pattern pa = Pattern.compile(from);
            String fileContent = where.replaceAll(pa.pattern(), to);
//       String fileContent = StringUtils.replace(where, pa.toString(), to);
            System.out.println("fil: " + fileContent);
        } else {
            String[] services = new String[]{
                "MaterialService.java",
                "ProductionOrderService.java",
                "ResourceInfoService.java",
                "WareTemplateService.java",
                "DeliveryOrderService.java",
                "MaterialTemplateService.java",
                "ProductService.java",
                "SellerService.java",
                "WareTypeService.java",
                "DevelopmentService.java",
                "MaterialTypeService.java",
                "ProductTemplateService.java",
                "WorkerService.java",
                "DocumentService.java",
                "OperationNameService.java",
                "ProfessionGroupService.java",
                "MachineService.java",
                "OrderService.java",
                "ProviderService.java",
                "TodoIssuesService.java",
                "MaterialDemandService.java",
                "ProductionJobService.java",
                "RecipientService.java",
                "WareService.java"};
            for (String s : services) {
                replaceinService(s, from, to);
            }
        }
    }

    public void replaceinService(String serviceName, String from, String to) throws Exception {
        File file = new File(projectLocation + sep + projectPackagePath + sep + "business" + sep + serviceName);
        replaceInFile(file, from, to);
    }

    public void replaceInFile(File file, String from, String to) throws Exception {

        InputStream in = new FileInputStream(file);
        String fileContent = IOUtils.toString(in);

        Pattern pa = Pattern.compile(from);
        fileContent = fileContent.replaceAll(pa.pattern(), to);
//        fileContent = StringUtils.replace(fileContent, from, to);

        System.out.println("to   : " + file.getAbsolutePath());
        FileOutputStream fos = new FileOutputStream(file);
        IOUtils.write(fileContent, fos);
    }
}
