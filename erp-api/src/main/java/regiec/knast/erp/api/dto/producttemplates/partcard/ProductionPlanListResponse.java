/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import java.util.List;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlanDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor 
public class ProductionPlanListResponse {

    private List<ProductionPlanDTO> productionPlan;
}
