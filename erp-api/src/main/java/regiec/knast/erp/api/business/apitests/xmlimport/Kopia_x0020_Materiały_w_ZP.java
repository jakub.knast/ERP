/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests.xmlimport;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class Kopia_x0020_Materiały_w_ZP {

    @XmlElement(name = "Nazwa")
    private String Nazwa;

    @XmlElement(name = "Data")
    private String Data;

    @XmlElement(name = "Materiał")
    private String Materiał;

    @XmlElement(name = "SumaOfIlość")
    private String SumaOfIlość;

    @XmlElement(name = "Nr_zlec")
    private String Nr_zlec;

    @XmlElement(name = "Ilość_zlec")
    private String Ilość_zlec;

    @XmlElement(name = "Wydruk")
    private String Wydruk;

    @XmlElement(name = "Nr_x0020_detalu")
    private String Nr_x0020_detalu;
    private String jm;

    // @XmlElement(name = "BENNENUNG")
    private String BENNENUNG;

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Kopia_x0020_Materiały_w_ZP other = (Kopia_x0020_Materiały_w_ZP) obj;
        if (!Objects.equals(this.Materiał, other.Materiał)) {
            return false;
        }
        return true;
    }

}
