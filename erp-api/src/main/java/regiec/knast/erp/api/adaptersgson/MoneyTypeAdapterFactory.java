/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.adaptersgson;

/**
 *
 * @author https://github.com/fatroom/gson-money-typeadapter
 */
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import javax.money.MonetaryAmountFactory;
import regiec.knast.erp.api.model.MoneyERP;

/**
 * Allows Gson to deal with {@code javax.money} API.
 */
public class MoneyTypeAdapterFactory implements TypeAdapterFactory {

//    private final MonetaryAmountFactory<? extends MonetaryAmount> monetaryFactory;

    /**
     * This is the Default implementation. Here, the implementation of the {@code javax.money} defined in the classpath
     * will be used.
     */
    public MoneyTypeAdapterFactory() {
//        this(Monetary.getDefaultAmountFactory());
    }

    public MoneyTypeAdapterFactory(final MonetaryAmountFactory<? extends MoneyERP> monetaryFactory) {
//        this.monetaryFactory = monetaryFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> typeToken) {

        final Class<T> clazz = (Class<T>) typeToken.getRawType();

        if (MoneyERP.class.isAssignableFrom(clazz)) {
            return (TypeAdapter<T>) new MoneyErpAdapter();
//            return (TypeAdapter<T>) new MoneyErpAdapter(monetaryFactory);
//        } else if (CurrencyUnit.class.isAssignableFrom(clazz)) {
//            return (TypeAdapter<T>) new CurrencyUnitAdapter();
        }

        return null;
    }
}