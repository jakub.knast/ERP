/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.validation;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ValidationException extends ConversionErrorWithCode {

    private String reason;

    public String getReason() {
        return reason;
    }

    public ValidationException(String message) {
        super(message, ExceptionCodes.INCORRECT_VALUE, new HashMap());
    }
    public ValidationException(String message, ExceptionCodes code ) {
        super(message, code, new HashMap());
    }
    
    public ValidationException(String message, ExceptionCodes code, Map params) {
        super(message, code, params);
    }

    public ValidationException(String message, ExceptionCodes code, Map params, String reason) {
        super(message, code, params);
        this.reason = reason;
    }

    public ValidationException(String message, ExceptionCodes code, Map params, Throwable cause) {
        super(message, code, params, cause);
    }

}
