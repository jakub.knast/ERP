/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

import java.util.Objects;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.ValidationException;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class CuttingSize {

    private Dimension cuttingDimension;
    private String origSize;
    private Double length;
    private Area area;
    private Boolean isOneDimensial;

    public CuttingSize cloneMe() {
        CuttingSize clone = new CuttingSize();
        clone.setCuttingDimension(this.cuttingDimension);
        clone.setOrigSize(this.origSize);
        clone.setLength(this.length);
        clone.setArea(this.area);
        clone.setIsOneDimensial(this.isOneDimensial);
        return clone;
    }
    
    public void validateMe(){
        if (this.getIsOneDimensial()) {
            if (this.getLength() == null || this.getLength() <= 0) {
                throw new ValidationException("length must be grather than 0", ExceptionCodes.INTERNAL_ERROR, null);
            }
        } else {
            if (this.getArea() == null || this.getArea().getA() <= 0 || this.getArea().getB() <= 0) {
                throw new ValidationException("A and B in area must be grather than 0", ExceptionCodes.INTERNAL_ERROR, null);
            }
        }
    }
    
    public boolean valid(){
        if (this.getIsOneDimensial()) {
            if (this.getLength() == null || this.getLength() <= 0) {
                return false;
            }
        } else {
            if (this.getArea() == null || this.getArea().getA() <= 0 || this.getArea().getB() <= 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        if (isOneDimensial) {
            return "cuttingDimension=" + cuttingDimension + ", origSize=" + origSize + ", length=" + length;
        } else {
            return "cuttingDimension=" + cuttingDimension + ", origSize=" + origSize + ", area=" + area;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.cuttingDimension);
        hash = 83 * hash + Objects.hashCode(this.length);
        hash = 83 * hash + Objects.hashCode(this.area);
        hash = 83 * hash + Objects.hashCode(this.isOneDimensial);
        return hash;
    }

    public boolean equalsByOrigSizeAndDimension(CuttingSize other) {
        return this.cuttingDimension == other.cuttingDimension && this.origSize.equals(other.origSize);
    }

    public boolean smallerOrEqualThan(CuttingSize cs) {
        if (isOneDimensial) {
            return this.length <= cs.length;
        } else {
            return this.area.smallerOrEqualThan(cs.area);
        }
    }

    public boolean smallerThan(CuttingSize cs) {
        if (isOneDimensial) {
            return this.length < cs.length;
        } else {
            return this.area.smallerThan(cs.area);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CuttingSize other = (CuttingSize) obj;
        if (this.cuttingDimension != other.cuttingDimension) {
            return false;
        }
        if (!Objects.equals(this.length, other.length)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        return Objects.equals(this.isOneDimensial, other.isOneDimensial);
    }

    public void multiplyMe(int multiplier) {
        if (this.isOneDimensial) {
            this.length *= multiplier;
            this.origSize = String.valueOf(this.length);
        } else {
            this.area = new Area(this.area.getA(), this.area.getB() * multiplier);
            this.origSize = this.area.toOrigSize();
        }
    }

    public CuttingSize multiply(int count) {
        CuttingSize ret = this.cloneMe();
        ret.multiplyMe(count);
        return ret;
    }

    public void substractMe(CuttingSize cuttingSize) {
        if (this.isOneDimensial) {
            this.length -= cuttingSize.getLength();
            this.origSize = String.valueOf(this.length);
        } else {
            double p = cuttingSize.area.getA() * cuttingSize.area.getB();
            double bToSlice = p / this.area.getA();
            this.area = new Area(this.area.getA(), this.area.getB() - bToSlice);
            this.origSize = this.area.toOrigSize();
        }
    }

    public CuttingSize substract(CuttingSize cuttingSize) {
        CuttingSize ret = this.cloneMe();
        ret.substractMe(cuttingSize);
        return ret;
    }

}
