/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.List;
import regiec.knast.erp.api.model.ProductTemplateType;
import regiec.knast.erp.api.model.SalesOffer;
import regiec.knast.erp.api.model.states.ProductTemplateState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductTemplateCreateBase extends ObjectWithDate {

    private String productNo;
    private String name;
    private String comments;
    private ProductTemplateType type = ProductTemplateType.SALE;
    private List<SalesOffer> salesOffers;
        
    private ProductTemplateState state = ProductTemplateState.NORMAL;        
}
