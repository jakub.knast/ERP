/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import java.awt.*;
import javax.swing.JFrame;

public class SimpleGraph extends JFrame {

    private Integer edgeNo = 0;

    public static void main(String[] args) {
//TreeLayoutDemo.main(null);
//VertexLabelAsShapeDemo.main(null);
        SimpleGraph sg = new SimpleGraph();
    }

    public SimpleGraph() {
        super("Mój pierwszy graf");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Graph g = getGraph();
        DelegateTree<Integer, String> delegateTree = new DelegateTree();
        delegateTree.addVertex(0);
        delegateTree.addChild("0-1", 0, 1, EdgeType.DIRECTED);

        Forest<String, Integer> forest = new DelegateForest();

        forest.addVertex("0");
        forest.addEdge(0, "0", "1", EdgeType.DIRECTED);
        forest.addEdge(1, "0", "2", EdgeType.DIRECTED);
        forest.addEdge(2, "1", "3", EdgeType.DIRECTED);
        VisualizationViewer<Integer, String> vv = new VisualizationViewer(new TreeLayout(delegateTree, 100, 100), new Dimension(500, 500));
        vv.setBackground(Color.white);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        getContentPane().add(vv);

        pack();
        setVisible(true);
    }

    public Graph getGraph() {
        Graph<Integer, String> g = new SparseGraph();
        g.addVertex((Integer) 1);
        g.addVertex((Integer) 2);
        g.addVertex((Integer) 3);
        g.addEdge("Edge-A", 1, 2, EdgeType.DIRECTED);
        g.addEdge("Edge-B", 2, 3, EdgeType.DIRECTED);
        g.addEdge("Edge-C", 3, 1, EdgeType.DIRECTED);
        return g;
    }
}
/*
    public SimpleGraph(java.util.List<ProductionTaskEntity> generated) {
        super("Mój drugi graf");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        DelegateTree<String, Integer> delegateTree = new DelegateTree();
        ProductionTaskEntity root = findRoot(generated);
        delegateTree.addVertex(root.getNo());
        addChildren(root, delegateTree);

        VisualizationViewer<String, Integer> vv = new VisualizationViewer(new TreeLayout(delegateTree, 100, 100), new Dimension(500, 500));
        vv.setBackground(Color.white);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        getContentPane().add(vv);

        pack();
        setVisible(true);
    }

    private Integer calculateCountFromParent(ProductionTaskEntity me) {
        if(me.getParent() == null){
            return me.getDetalEntity().getCount();
        }
        java.util.List<ProductionTaskEntity> children = me.getParent().getChildren();
        return 0;
    }

    class A {
        public String no;
        public String templateNo;
        public Integer count;
        public Integer totalCount;
        public String name;

        public A(String no, String templateNo, Integer count, Integer totalCount, String name) {
            this.no = no;
            this.templateNo = templateNo;
            this.count = count;
            this.totalCount = totalCount;
            this.name = name.length() <= 17 ? name : name.substring(0, 17);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof A)) {
                return false;
            }
            return ((A) obj).no.equals(this.no);
        }

        @Override
        public int hashCode() {
            return no.hashCode();
        }

    }

    public SimpleGraph(ProductionTaskEntity root) {
        super("Mój drugi graf");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        DelegateTree<A, Integer> delegateTree = new DelegateTree();
//        delegateTree.addVertex(root.getDetalEntity().getDetalTemplateEntity().getNo());
        delegateTree.addVertex(new A(
                root.getNo(), 
                root.getDetalEntity().getDetalTemplateEntity().getNo(), 
                calculateCountFromParent(root), 
                root.getDetalEntity().getCount(), 
                root.getDetalEntity().getDetalTemplateEntity().getName()));
        System.out.println("r: " + root.getDetalEntity().getDetalTemplateEntity().getId());
        addChildren(root, delegateTree);

        VisualizationViewer<A, Integer> vv = new VisualizationViewer(new TreeLayout(delegateTree, 130, 100), new Dimension(1800, 700));
        // this class will provide both label drawing and vertex shapes
        VertexLabelAsShapeRenderer<A, Integer> vlasr = new VertexLabelAsShapeRenderer(vv.getRenderContext());

        // customize the render context
        vv.getRenderContext().setVertexLabelTransformer(
                // this chains together Transformers so that the html tags
                // are prepended to the toString method output
                new ChainedTransformer<A, String>(new Transformer[]{
            //new ToStringLabeller<A>(),
            new Transformer<A, String>() {
                public String transform(A input) {
                    return "<html><center>" + input.no
                            + "<p>" + input.templateNo
                            + "<p>" + input.name;
                }
            }}));
        vv.getRenderContext().setVertexShapeTransformer(vlasr);
        // customize the renderer
        vv.getRenderer().setVertexRenderer(new GradientVertexRenderer(Color.gray, Color.white, true));
        vv.getRenderer().setVertexLabelRenderer(vlasr);
        // vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.setBackground(Color.white);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        getContentPane().add(vv);

        pack();
        setVisible(true);
    }

//    private void createTree() {
//        Graph graph = new DirectedSparseGraph();
//    	graph.addVertex("V0");
//    	graph.addEdge(edgeFactory.create(), "V0", "V1");
//    	graph.addEdge(edgeFactory.create(), "V0", "V2");
//    	graph.addEdge(edgeFactory.create(), "V1", "V4");
//    	graph.addEdge(edgeFactory.create(), "V2", "V3");
//    	graph.addEdge(edgeFactory.create(), "V2", "V5");
//    	graph.addEdge(edgeFactory.create(), "V4", "V6");
//    	graph.addEdge(edgeFactory.create(), "V4", "V7");
//    	graph.addEdge(edgeFactory.create(), "V3", "V8");
//    	graph.addEdge(edgeFactory.create(), "V6", "V9");
//    	graph.addEdge(edgeFactory.create(), "V4", "V10");
//    	
//       	graph.addVertex("A0");
//       	graph.addEdge(edgeFactory.create(), "A0", "A1");
//       	graph.addEdge(edgeFactory.create(), "A0", "A2");
//       	graph.addEdge(edgeFactory.create(), "A0", "A3");
//       	
//       	graph.addVertex("B0");
//    	graph.addEdge(edgeFactory.create(), "B0", "B1");
//    	graph.addEdge(edgeFactory.create(), "B0", "B2");
//    	graph.addEdge(edgeFactory.create(), "B1", "B4");
//    	graph.addEdge(edgeFactory.create(), "B2", "B3");
//    	graph.addEdge(edgeFactory.create(), "B2", "B5");
//    	graph.addEdge(edgeFactory.create(), "B4", "B6");
//    	graph.addEdge(edgeFactory.create(), "B4", "B7");
//    	graph.addEdge(edgeFactory.create(), "B3", "B8");
//    	graph.addEdge(edgeFactory.create(), "B6", "B9");
//       	
//    }
*/