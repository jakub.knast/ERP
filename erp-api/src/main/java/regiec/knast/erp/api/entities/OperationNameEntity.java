/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import regiec.knast.erp.api.dto.operationName.OperationNameDTO;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.OperationNameBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("operationNames")
public class OperationNameEntity extends OperationNameBase implements EntityBase, ConvertableEntity<OperationNameDTO> {

    @Id
    private String id;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }
    
    public static ArrayList<OperationNameDTO> entitiesToDtos(List<OperationNameEntity> entities) {
        ArrayList<OperationNameDTO> dtos = new ArrayList();
        for (OperationNameEntity e : entities) {
            dtos.add(e.convertToDTO());
        }
        return dtos;
    }

    public static String getTransportOperationName() {
        return "Transport";
    }

}
