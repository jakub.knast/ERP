/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.model.DocumentInterface;
import regiec.knast.erp.api.model.states.DocumentState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class DocumentBase extends ObjectWithDate {

    private String name;
    private DocumentInterface content;
    private DocumentState state;
    private boolean removed;
}
