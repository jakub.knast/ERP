/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.dto.Downloadable;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.order.GenerateProductionOrderDispositionRequest;
import regiec.knast.erp.api.dto.order.GenerateProductionOrderRequest;
import regiec.knast.erp.api.dto.order.GenerateProductionOrderResponse;
import regiec.knast.erp.api.dto.productionorder.GetProductionGuideForProductionOrderRequest;
import regiec.knast.erp.api.dto.productionorder.ProductionOrderDTO;
import regiec.knast.erp.api.dto.productionorder.ProductionOrdersListDTO;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.manager.ProductionOrderManager;
import regiec.knast.erp.api.manager.orders.GenerateProductionOrderTransformer;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.pguidegenerator.ProductionGuideGenerator;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class ProductionOrderService  extends AbstractService<ProductionOrderEntity, ProductionOrderDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.PRODUCTION_ORDER;

//    @Inject
//    private ProductionOrderDAOInterface productionOrderDAO;
    @Inject
    private Validator validator;
    @Inject
    private ProductionOrderManager productionOrderManager;
    @Inject
    private GenerateProductionOrderTransformer generateProductionOrderTransformer;
    @Inject
    private ProductionGuideGenerator productionGuideGenerator;

//    @NetAPI
//    public ProductionOrderDTO addProductionOrder(AddProductionOrderRequest request) {
//            validator.validateNotNull(request).throww();
//            validator.validateUniqueness("no", request.getNo(), null, MY_ENTITY_TYPE).throww();
//            ProductionOrderEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new ProductionOrderEntity());
//            entity.setState(ProductionOrderState.NEW); 
//            ProductionOrderEntity entity1 = productionOrderDAO.add(entity);
//            return entity1.convertToDTO();
//    }

    @NetAPI
    public ProductionOrderDTO getProductionOrder(BasicGetRequest request) {
        return super.get(request);
    }

//    @NetAPI
//    public ProductionOrderDTO updateProductionOrder(UpdateProductionOrderRequest request) {
//            validator.validateNotNull(request).throww();
//            String id = request.getId();
//            validator.validateNotNull(id).throww();
//            validator.validateUniqueness("no", request.getNo(), id, MY_ENTITY_TYPE).throww();
//            ProductionOrderEntity entity = productionOrderDAO.get(id);
//            validator.validateNotNull(entity).throww();
//            ProductionOrderEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new ProductionOrderEntity(entity));
//            BeanUtilSilent.translateBean(request, updatedEntity);
//            productionOrderDAO.update(updatedEntity);
//            return updatedEntity.convertToDTO();
//    }

//    @NetAPI
//    public RemoveResponse removeProductionOrder(RemoveProductionOrderRequest request) {
//        RemoveResponse ret = ConvertersRegistry.convert(request, RemoveResponse.class);
//        return ret;
//    }

    @NetAPI
    public ProductionOrdersListDTO listProductionOrders(PaginableFilterWithCriteriaRequest request) {
        return (ProductionOrdersListDTO) super.list(request);
    }

    @NetAPI
    public GenerateProductionOrderResponse generateProductionOrder(GenerateProductionOrderRequest request) {
        return generateProductionOrderTransformer.generateProductionOrder(request);
    }

    @NetAPI
    public GenerateProductionOrderResponse generateProductionOrderDisposition(GenerateProductionOrderDispositionRequest request) {
        return generateProductionOrderTransformer.generateProductionOrderDisposition(request);
    }

//    @NetAPI
//    public GenerateSTHResponse generateAllNewProductionOrders() {
//        return productionOrderManager.generateAllNewProductionOrders();
//    }

    @NetAPI
    public Downloadable getProductionGuide(GetProductionGuideForProductionOrderRequest request) {
        validator.validateNotNullAndNotEmpty(request.getProductionOrderId()).throww();
        String productionOrderId = request.getProductionOrderId();
        return productionGuideGenerator.getProductionGuide(productionOrderId, null, request.getForceApplicationDataContentType());
    }

}
