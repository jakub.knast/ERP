/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.model.states.MaterialState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MaterialStateConverter {

    public MaterialEntity convert(MaterialEntity entity, MaterialState from, MaterialState to) {
        //validate with statemachine

        return orderedToOnStockAvaliable(entity);
    }

    private MaterialEntity orderedToOnStockAvaliable(MaterialEntity entity) {

        return null;
    }

}
