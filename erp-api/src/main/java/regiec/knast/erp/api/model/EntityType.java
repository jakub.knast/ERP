/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public enum EntityType {
    MATERIAL,
    MATERIAL_TEMPLATE,
    MATERIAL_TYPE,
    WARE,
    WARE_TEMPLATE,
    WARE_TYPE,
    PRODUCT,
    PRODUCT_TEMPLATE,
    SEMIPRODUCT_TEMPLATE,
    RECIPIENT,
    PRODUCTION_ORDER,
    ORDER,
    DELIVERY_ORDER,
    WORKER,
    MACHINE,
    PROVIDER,
    PRODUCTION_JOB,
    MATERIAL_DEMAND,
    DEVELOPMENT,
    SHOPPING_CART,
    TODO_ISSUES,
    SELLER,
    OPERATION_NAME,
    PROFESSION_GROUP,
    DOCUMENT,
//Here_will_be_insert_entityType
    PRODUCTION_GUIDE_BARCODE,
    PRODUCTION_GUIDE,
    INVOICE
}
