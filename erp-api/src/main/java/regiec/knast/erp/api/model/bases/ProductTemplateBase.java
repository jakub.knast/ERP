/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductTemplateBase extends ProductTemplateCreateBase {

    private String validationStatus;
    private List<String> validationMessage = new ArrayList();
    private List<String> ptUsages = new ArrayList();
    private Double totalBuildingTime; // całkowity czas produkcji
    private Double buildingTime; //czas budowy podzespołu
    private Double buildingTimeTJ;
    private Double buildingTimeTPZ;
    private Double totalBuildingTimeTJ;
    private Double totalBuildingTimeTPZ;
    private String buildingTimeStr;
    private String totalBuildingTimeStr;
    private boolean removed;
}
