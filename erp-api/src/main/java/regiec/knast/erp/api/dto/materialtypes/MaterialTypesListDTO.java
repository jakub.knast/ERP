/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.materialtypes;

import org.jaxygen.dto.collections.PaginableListResponseBaseDTO;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypeDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MaterialTypesListDTO extends PaginableListResponseBaseDTO<MaterialTypeDTO> {

}
