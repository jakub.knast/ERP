/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.orders;

import regiec.knast.erp.api.model.MaterialAssortmentPosition;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class PositionAssortmentTemp {

    private String materialCode;
    private String materialTemplateId;
    private MaterialAssortmentPosition assortmentPosition;

}
