/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dao.ProgramConstsDAO;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.Validate2UniquenessRequest;
import regiec.knast.erp.api.dto.ValidateUniquenessRequest;
import regiec.knast.erp.api.dto.otherstuff.ProgramConstsDTO;
import regiec.knast.erp.api.dto.otherstuff.ProgramConstsEntity;
import regiec.knast.erp.api.dto.system.AddConstRequestDTO;
import regiec.knast.erp.api.dto.system.SendEmailWithExcepitonInfoRequest;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;
import regiec.knast.erp.api.manager.BuildingTimeCalculator;
import regiec.knast.erp.api.utils.email.EmailSender;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class SystemService {

    @Inject
    private Validator validator;
    @Inject
    private ProgramConstsDAO programConstsDAO;
    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;

    @NetAPI
    public ProgramConstsDTO getProgramConts() throws ConversionError {
        ProgramConstsEntity consts = programConstsDAO.getConsts();
        return ProgramConstsDTO.fromEntity(consts);
    }

    @NetAPI
    public ProgramConstsDTO addConst(AddConstRequestDTO request) throws ConversionError {
        ProgramConstsEntity consts = programConstsDAO.addConst(request.getType(), request.getValue());
        return ProgramConstsDTO.fromEntity(consts);
    }

    @NetAPI
    public Boolean validate(ValidateUniquenessRequest request) throws ConversionError {
        return validator.validateUniqueness(request.getField(), request.getValue(), request.getMyId(), request.getEntityType()).status();
    }

    @NetAPI
    public Boolean validate2(Validate2UniquenessRequest request) throws ConversionError {
        return validator.validateUniqueness2(request.getField1(), request.getValue1(), request.getField2(), request.getValue2(), request.getMyId(), request.getEntityType()).status();
    }

    @NetAPI
    public String updateBuildingTimesIn_PT_and_SPT() throws ConversionError {
        String ret = "\n";

        ProductTemplateEntitiesList ptList = productTemplateDAO.list();
        for (ProductTemplateEntity productTemplateEntity : ptList) {
            BuildingTimeCalculator.updateBuildingTimes(productTemplateEntity);
            productTemplateDAO.update(productTemplateEntity);
        }
        ret += "updated " + ptList.size() + " productTemplates\n";
        return ret;
    }

    @NetAPI
    public PaginableFilterWithCriteriaRequest paginableFilterWithCriteriaRequestTest(PaginableFilterWithCriteriaRequest request) {
        return request;
    }
        
    @NetAPI
    public void sendEmailWithExcepitonInfo(SendEmailWithExcepitonInfoRequest request) throws ObjectCreateError {
//        EmailSender emailSender = new EmailSender();
//        emailSender.sendEmail(request.getRequest(), request.getResponse(), request.getScreenshot());
        new EmailSender(request).sendFromGlassfish();
    }
    
    
    
}
