/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JsonDataSource;
import org.codehaus.groovy.control.CompilationFailedException;
import regiec.knast.erp.api.dao.interfaces.OrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProgramIndexesDAOInterface;
import regiec.knast.erp.api.dao.interfaces.RecipientDAOInterface;
import regiec.knast.erp.api.dao.interfaces.SellerDAOInterface;
import regiec.knast.erp.api.dto.orderedproducts.OrderPosition;
import regiec.knast.erp.api.dto.product.GenerateInvoicePdfRequest;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.entities.SellerEntity;
import regiec.knast.erp.api.entities.containers.ProductEntitiesList;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.model.invoicegenerator.*;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class InvoiceGenerator extends PdfGenerator<InvoiceData> {

    private SellerDAOInterface sellerDao;
    private RecipientDAOInterface recipientDAO;
    private ProductDAOInterface productDAO;
    private ProductionOrderDAOInterface productionOrderDAO;
    private OrderDAOInterface orderDAO;
    private Validator validator;

    private final String jasperInvoiceTemplateFileName = "invoice-template.jasper";
    private final String outPath = "C://Users/fbrzuzka/sample_invoice.pdf";
    private final String jsonData;
    private final InvoiceData invoiceData;


    @Override
    public InvoiceData getData() {
        return invoiceData;
    }
    
    public InvoiceGenerator() {
        System.out.println("created invoice generator with foo data");
        this.jsonData = getFooData();
        this.invoiceData = new InvoiceData();
    }

    public InvoiceGenerator(String jsonData) {
        System.out.println("created invoice generator with json data");
        this.jsonData = jsonData;
        this.invoiceData = new InvoiceData();
    }

    public InvoiceGenerator(InvoiceData data) {
        System.out.println("created wz generator with object data");
        this.invoiceData = data;
        this.jsonData = super.g.toJson(invoiceData);
    }


    public final String nextInvoiceNo() {
        ProgramIndexesDAOInterface indexes = super.buildObj(ProgramIndexesDAOInterface.class);
        Long index = indexes.nextIndexOrReserInNewYear(EntityType.INVOICE);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int year = cal.get(Calendar.YEAR);
        String documentNo = index + "/" + year;
        return documentNo;
    }
    
    public final void initInjected(){
        sellerDao = super.buildObj(SellerDAOInterface.class);
        recipientDAO = super.buildObj(RecipientDAOInterface.class);
        productDAO = super.buildObj(ProductDAOInterface.class);
        productionOrderDAO = super.buildObj(ProductionOrderDAOInterface.class);
        orderDAO = super.buildObj(OrderDAOInterface.class);
        validator = super.buildObj(Validator.class);      
    }

    public InvoiceGenerator(GenerateInvoicePdfRequest request) {
        System.out.println("created invoice generator from request");
        initInjected();
        this.invoiceData = new InvoiceData();

        invoiceData.setDocumentNo(nextInvoiceNo());
        invoiceData.setDate(new SimpleDateFormat("yyyy.MM.dd").format(new Date()));

        SellerEntity seller = sellerDao.get(request.getSellerId());
        SellerInfoInvoice sellerInfo = new SellerInfoInvoice(seller.getNip(), seller.getBankNamePLN(), seller.getBankAccountPLN(), seller.getName(), seller.getStreet(), seller.getPostalCode(), seller.getCity());
        invoiceData.setProviderInfo(sellerInfo);

        RecipientEntity recipient = recipientDAO.get(request.getRecipientId());
        RecipientInfoInvoice recipientinfo = new RecipientInfoInvoice(recipient.getNip(), recipient.getName(), recipient.getStreet(), recipient.getPostalCode(), recipient.getCity());
        invoiceData.setRecipientInfo(recipientinfo);

        
        ProductEntitiesList list = new ProductEntitiesList();
        List<String> productsIdsSplited = request.splitIds();
        for (String productId : productsIdsSplited) {
            ProductEntity product = productDAO.get(productId);
            list.add(product);
        }
        
        ProductEntitiesList mergedProducts = list.mergeProducts();
        int i = 1;
        for (ProductEntity product : mergedProducts) {
            validator.validateNotNull(product, "product").throww();
            ProductionOrderEntity productionOrder = productionOrderDAO.get(product.getProductionOrderId());
            validator.validateNotNull(productionOrder, "productionOrder").throww();
            OrderEntity order = orderDAO.get(productionOrder.getOrderId());
            validator.validateNotNull(order, "order").throww();
            OrderPosition orderPosition = order.findOrderPosition(productionOrder.getOrderPositionId());
            validator.validateNotNull(orderPosition, "orderPosition").throww();

            MoneyERP piecePrice = orderPosition.getPiecePriceWithDiscount();
            validator.validateNotNull(piecePrice, "piecePriceWithDiscount").throww();
            
            InvoiceItem invoiceItem = new InvoiceItem();
            invoiceItem.setCount(product.getCount());
            invoiceItem.setName(product.getProductTemplate().getName());
            invoiceItem.setProductNo(product.getProductTemplate().getProductNo());
            invoiceItem.setNo(i++);
            invoiceItem.setOrderNo(product.getOrderNo());
            invoiceItem.setPosition(orderPosition.getPositionOnOrder());
            invoiceItem.setUnit("szt");
            invoiceItem.setUnitPrice(piecePrice.roundTwoDecimalPlaces() + " " + piecePrice.getCurrency());
            invoiceItem.setValueNetto(piecePrice.multiply(product.getCount()).roundTwoDecimalPlaces() + " " + piecePrice.getCurrency());
            invoiceItem.setVat("23%");
            invoiceItem.setValueBrutto(piecePrice.multiply(product.getCount()).multiply(1.23).roundTwoDecimalPlaces() + " " + piecePrice.getCurrency());
            invoiceData.getProducts().add(invoiceItem);
        }

         this.jsonData = super.g.toJson(invoiceData);
    } 

    public void generateToStream(OutputStream pdfOutputStream) throws PdfGeneratorException {
        File jaspperTemplateFile = getJasperTemplate();
        InputStream jsonInput = new ByteArrayInputStream(this.jsonData.getBytes(Charset.forName("utf-8")));
        JsonDataSource as;
        try {
            as = new JsonDataSource(jsonInput);
        } catch (JRException ex) {
            throw new PdfGeneratorException("Cannot create JsonDataSource. Problem with jsonInput? ", ex);
        }
        Map parameters = new HashMap();
        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(jaspperTemplateFile.getAbsolutePath(), parameters, as);
            if (jasperPrint != null) {
                /**
                 * 1- export to PDF
                 */
                JasperExportManager.exportReportToPdfStream(jasperPrint, pdfOutputStream);
            }
        } catch (JRException e) {
            throw new CompilationFailedException(0, null, e);
        }
    }

    private File getJasperTemplate() {
        ClassLoader classLoader = getClass().getClassLoader();
        File jaspperInvoiceFile = new File(classLoader.getResource(jasperInvoiceTemplateFileName).getFile());
        return jaspperInvoiceFile;
    }

    private String getFooData() {
        return "{\n"
                + "	\"documentNo\": \"123123123\",\n"
                + "	\"providerInfo\": {\n"
                + "		\"name\": \"P.P.H.U. :\\\"JANGRA-BIS\\\" Export-Import Machyński Łukasz\",\n"
                + "		\"street\": \"Czerniejewska 4\",\n"
                + "		\"postal\": \"62-230\",\n"
                + "		\"city\": \"Września\"\n"
                + "	},\n"
                + "	\"recipientInfo\": {\n"
                + "		\"name\": \"HOMAG Machinery Sp. z o.o.\",\n"
                + "		\"street\": \"Prądzyńskiego 24\",\n"
                + "		\"postal\": \"63-000\",\n"
                + "		\"city\": \"Środa Wlkp.\"\n"
                + "	},\n"
                + "	\"date\": \"22 marca 2017\",\n"
                + "	\"products\": [{\n"
                + "		\"no\": 1,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	},\n"
                + "	{\n"
                + "		\"no\": 2,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	},\n"
                + "	{\n"
                + "		\"no\": 3,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	},\n"
                + "	{\n"
                + "		\"no\": 4,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	},\n"
                + "	{\n"
                + "		\"no\": 5,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	}]\n"
                + "}";
    }
}
