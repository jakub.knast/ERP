/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.driver;

import com.mongodb.gridfs.GridFS;
import org.mongodb.morphia.Datastore;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface MongoInterface {

    public Datastore getDatastore();

    public GridFS getGridFS();

    public void closeConnections();
}
