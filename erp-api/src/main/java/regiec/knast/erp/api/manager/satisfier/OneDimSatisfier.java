/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.satisfier;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDemandDAOInterface;
import regiec.knast.erp.api.dto.material.MaterialDTO;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandDTO;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.manager.orders.DeliveryOrderPositionsManager;
import regiec.knast.erp.api.model.states.MaterialDemandState;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.manager.MaterialUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class OneDimSatisfier {

    @Inject
    private MaterialDemandDAOInterface materialDemandDAO;
    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private MaterialUtil materialUtil;
    @Inject
    private DeliveryOrderPositionsManager deliveryOrderPositionsManager;

    public Object safisfyOneDinemsial(MaterialEntity m, MaterialDemandEntity md) {
        Double mRL = m.getReservedLength().getLength();
        Double mLL = m.getLeftLength().getLength();
        Double mdLength = md.getCuttingSize().getLength();
        if (mLL < mdLength) {
            throw new ConversionErrorWithCode("Material is too short fo demand. Material.leftLength'" + m.getLeftLength().getLength() + "' MaterialDemand.length'" + mdLength + "'",
                    ExceptionCodes.MATERIAL_TOO_SHORT_FOR_DEMAND,
                    ImmutableMap.of("mLeftLength", m.getLeftLength().getLength(), "mdLength", mdLength)
            );
        }

        MaterialDemandDTO retMd = new MaterialDemandDTO();
        MaterialDTO retM_FU = null;
        MaterialDTO retM_PU = null;
        MaterialDTO retM_NU = null;

        // calculate how many demand can be satisfied by one material
        int mdPerM = calculateHowManyDemandsPerMaterial(m.getLeftLength().getLength(), mdLength);

        // calculate how many materials we need for whole demand
//        int howManyMaterialWeNeed = calculateHowManyNeedMaterialForWholeDemand(md.getCount(), mdPerM);
//        System.out.println("We have md with " + mdLength + "m and material with " + m.getLeftLength() + "m left length. "
//                + "\nSo we can safisfy " + mdPerM + " demand with one material");
        int fullUsed = howManyFullUsed(m.getCount(), md.getCount(), mdPerM);
        int partlyUsed = howManyPartlyUsed(m, md.getCount(), mdLength, fullUsed, mdPerM, m.getCount());
        int notUsed = howManyNotUsed(m, fullUsed, partlyUsed);

        boolean origMaterialEntityUsed = false;
        Set<String> idsOfAddedMaterials = new HashSet();

        MaterialEntity newm = m.cloneMe();
        MaterialEntity newm2 = m.cloneMe();
        if (partlyUsed > 0) {
            Double m_PU_R = mRL + mdLength * (md.getCount() - fullUsed * mdPerM);
            Double m_PU_LL = m.getLength().getLength() - m_PU_R;
            m.setCount(partlyUsed);
            m.setReservedLength(CuttingSizeManager.createOneDimCuttingSize(m_PU_R));
            m.setLeftLength(CuttingSizeManager.createOneDimCuttingSize(m_PU_LL));
            updateOriginMaterial(m, true);
            origMaterialEntityUsed = true;
            retM_PU = m.convertToDTO();
        }
        if (fullUsed > 0) {
            Double m_FU_R = mRL + mdPerM * mdLength;
            Double m_FU_LL = m.getLength().getLength() - m_FU_R;
            if (origMaterialEntityUsed) {
                newm.setCount(fullUsed);
                newm.setReservedLength(CuttingSizeManager.createOneDimCuttingSize(m_FU_R));
                newm.setLeftLength(CuttingSizeManager.createOneDimCuttingSize(m_FU_LL));
                addNewMaterial(newm, idsOfAddedMaterials, true);
                retM_FU = newm.convertToDTO();
            } else {
                m.setCount(fullUsed);
                m.setReservedLength(CuttingSizeManager.createOneDimCuttingSize(m_FU_R));
                m.setLeftLength(CuttingSizeManager.createOneDimCuttingSize(m_FU_LL));
                updateOriginMaterial(m, true);
                origMaterialEntityUsed = true;
                retM_FU = m.convertToDTO();
            }
        }
        if (notUsed > 0) {
            if (origMaterialEntityUsed) {
                newm2.setCount(notUsed);
                newm2.setReservedLength(CuttingSizeManager.createOneDimCuttingSize(mRL));
                newm2.setLeftLength(CuttingSizeManager.createOneDimCuttingSize(m.getLength().getLength() - mRL));
                addNewMaterial(newm2, idsOfAddedMaterials, false);
                retM_NU = newm2.convertToDTO();
            } else {
                m.setCount(notUsed);
                m.setReservedLength(CuttingSizeManager.createOneDimCuttingSize(mRL));
                m.setLeftLength(CuttingSizeManager.createOneDimCuttingSize(m.getLength().getLength() - mRL));
                updateOriginMaterial(m, false);
                origMaterialEntityUsed = true;
                retM_NU = m.convertToDTO();
            }
        }
        md.setSatisfied(calculateSatisfied(retM_FU, retM_PU, mdPerM, mRL, mdLength));
        md.setUnsatisfied(md.getCount() - md.getSatisfied());
        if (md.getSatisfied().equals(md.getCount())) {
            md.setState(MaterialDemandState.SATISFIED);
        } else if (md.getSatisfied().equals(0)) {
            md.setState(MaterialDemandState.NOT_SATISFIED);
        } else {
            md.setState(MaterialDemandState.PARTLY_SATISFIED);
        }
        //TODO
//        deliveryOrderPositionsManager.updateMaterialsOnPositionOnDeliveryOrder(m, idsOfAddedMaterials);
        MaterialDemandEntity updatedMd = materialDemandDAO.update(md);
        retMd = updatedMd.convertToDTO();

        Map<String, Object> retMap = new LinkedHashMap();
        retMap.put("message", "OK");
        retMap.put("retM_FU", retM_FU);
        retMap.put("retM_PU", retM_PU);
        retMap.put("retM_NU", retM_NU);
        retMap.put("retMd", retMd);
        return retMap;
    }

    private void updateOriginMaterial(MaterialEntity m, boolean changeState) {
        m.validateLength();
        if (changeState) {
            m.refreshSatisfiedState();
        }
        materialDAO.update(m);
    }

    private void addNewMaterial(MaterialEntity newm, Set<String> idsOfAddedMaterials, boolean changeState) {
        newm.validateLength();
        if (changeState) {
            newm.refreshSatisfiedState();
        }
        materialDAO.add(newm, IDUtil.nextId_());
        idsOfAddedMaterials.add(newm.getId());
    }

    public int calculateHowManyDemandsPerMaterial(Double leftLength, Double mdLength) {
        return (int) (leftLength / mdLength);
    }

    public int calculateHowManyNeedMaterialForWholeDemand(Integer mdCount, int mdPerM) {
        Double howManyWeNeed = new Double(mdCount) / mdPerM;
        return (int) Math.ceil(howManyWeNeed);
    }

    private int howManyFullUsed(int mCount, int mdCount, int mdPerM) {
        double fraction = mdCount / mdPerM;
        return Math.min(mCount, (int) Math.floor(fraction));
    }

    private int howManyPartlyUsed(MaterialEntity m, Integer mdCount, Double mdLength, int fullUsed, int mdPerM, int mCount) {
        double fraction = mdCount * mdLength / m.getLeftLength().getLength();
        //     double fraction =  mdCount / mdPerM;
        return (int) Math.min(mCount, (int) Math.ceil(fraction)) - fullUsed;
    }

    private int howManyNotUsed(MaterialEntity m, int fullUsed, int partlyUsed) {
        return m.getCount() - (fullUsed + partlyUsed);
    }

    private Integer calculateSatisfied(MaterialDTO full, MaterialDTO partly, int mdPerM, Double origMaterialReserved, Double mdLength) {
        int satisfiedCount = 0;
        if (full != null) {
            satisfiedCount += full.getCount() * mdPerM;
        }
        if (partly != null) {
            Double reserved = partly.getReservedLength().getLength() - origMaterialReserved;
            satisfiedCount += (int) (reserved / mdLength);
        }
        return satisfiedCount;
    }

}
