/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.validation;

import com.google.common.base.Strings;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ValidationOut {

    private boolean status;
    private ValidationException ex;

    public ValidationOut(Boolean status) {
        this.status = status;
    }

    public ValidationOut(ValidationException ex) {
        this.ex = ex;
    }

    public Boolean status() {
        return this.status;
    }

    public void throww() {
        if (ex != null) {
            throw ex;
        }
    }

    public String getExceptionReason() {
        if (ex != null) {
            return Strings.nullToEmpty(ex.getReason());
        } else {
            return "";
        }
    }
}
