/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.model.pguidegenerator;

import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class GenerateProductionGuideRequest {

    private String productionOrderId;
    private OrderedPages orderedPages = new OrderedPages();

    @Deprecated
    public GenerateProductionGuideRequest(String productionOrderId, ProductionOrderDAOInterface productionOrderDAO, boolean drafts) {
        ProductionOrderEntity productionOrder = productionOrderDAO.get(productionOrderId);

        this.setProductionOrderId(productionOrderId);
        OrderedPages orderedPages = new OrderedPages();
        this.setOrderedPages(orderedPages);

        ProductTemplateEntity productTemplate = productionOrder.getProduct().getProductTemplate();
        orderedPages.addAll(fromPropductTemplate(productTemplate, drafts));
    }

    public GenerateProductionGuideRequest(ProductionOrderEntity productionOrder, boolean drafts) {
        this.setProductionOrderId(productionOrder.getId());
        ProductTemplateEntity productTemplate = productionOrder.getProduct().getProductTemplate();
        this.setOrderedPages(fromPropductTemplate(productTemplate, drafts));
    }

    public static OrderedPages fromPropductTemplate(ProductTemplateEntity productTemplate, boolean drafts) {

        OrderedPages orderedPages = new OrderedPages();
        orderedPages.add(new OrderedPage(productTemplate.getId(), true, drafts));
        for (SemiproductPart semiproductPart : productTemplate.getPartCard().getSemiproductParts()) {
            orderedPages.addAll(fromPropductTemplate(semiproductPart.getProductTemplate(), drafts));
        }
        return orderedPages;
    }
}
