/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.states;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class StateDescriptor {

    private StateInterface value;
    private String translation;
    private String backgroundColor;
    private String fontColor;

    public StateDescriptor(StateInterface state) {
        this.value = state;
        this.translation = state.getTranslation();
        this.backgroundColor = state.getBackgroundColor();
        this.fontColor = state.getFontColor();
    }

    public Map<StateInterface, StateDescriptor> getTranslations(Class<? extends Enum> enumClass) {
        Map<StateInterface, StateDescriptor> maps = new HashMap();
        StateInterface[] values = (StateInterface[]) enumClass.getEnumConstants();
        for (StateInterface value : values) {
            maps.put(value, new StateDescriptor(value));
        }
        return maps;
    }

    public static Map<StateInterface, StateDescriptor> getTranslationsMap(Class<? extends StateInterface> enumClass) {
        Map<StateInterface, StateDescriptor> maps = new HashMap();
        StateInterface[] values = (StateInterface[]) enumClass.getEnumConstants();
        for (StateInterface value : values) {
            maps.put(value, new StateDescriptor(value));
        }
        return maps;
    }
}
