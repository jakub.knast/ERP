/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.development;

import regiec.knast.erp.api.model.bases.DevelopmentBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AddDevelopmentRequest extends DevelopmentBase {

}
