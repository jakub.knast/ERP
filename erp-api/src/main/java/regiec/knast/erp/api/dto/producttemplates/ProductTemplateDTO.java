/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import java.util.ArrayList;
import regiec.knast.erp.api.dto.producttemplates.partcard.PartCardDTO;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlanDTO;
import regiec.knast.erp.api.dto.resourceinfo.ResourceInfoDTO;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.bases.ProductTemplateBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductTemplateDTO extends ProductTemplateBase {

    private String id;
    private String productTypeId;
    private ResourceInfoDTO techDrawingPDFdto;
    private ResourceInfoDTO techDrawingDXFdto;
    private ResourceInfoDTO reportTemplatedto;
    private PartCardDTO partCard;
    private ArrayList<ProductionPlanDTO> productionPlan = new ArrayList();
    
    public ProductTemplateDTO(String id, String name, Dimension dimension) {
        this.id = id;
    }

}
