/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import com.google.common.collect.Lists;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import static org.exolab.castor.mapping.loader.Types.isSimpleType;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.utils.anotations.ImplementedFields;
import regiec.knast.erp.api.utils.entityfieldscache.InterfaceField;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class EntityFieldsCache {

    public static Map<String, Map<String, Class>> entityMap = new HashMap();
    public static Map<String, Map<String, InterfaceField>> interfaceFieldMap = new HashMap();

    
    public static InterfaceField getInterfaceFieldsTypesFromClass(Class clazz, String fieldPath) {
        Map<String, InterfaceField> fieldsMap = interfaceFieldMap.get(clazz.getSimpleName());
        if (fieldsMap != null) {
            InterfaceField interfaceFieldInfo = fieldsMap.get(fieldPath);
            return interfaceFieldInfo;
        }
        return null;        
    }
    
    public static Map<String, Class> getFieldsTypesFromClass(Class clazz) {
        if (!entityMap.containsKey(clazz.getSimpleName())) {
            registerAndPrepareIfNotExist(clazz);
        }
        return entityMap.get(clazz.getSimpleName());

    }

    private static void registerAndPrepareIfNotExist(Class clazz) {
        String rootEntityClassName = clazz.getSimpleName();
        Map<String, Class> classFields = prepareForClass(clazz, rootEntityClassName);
        Map<String, Class> treeMap = new TreeMap(classFields);
        entityMap.put(rootEntityClassName, treeMap);
    }

    private static Map<String, Class> prepareForInterface(Class c, String fieldName, String rootEntityClassName) {
        Map<String, Class> classFields = new HashMap();
        Method[] methods = c.getDeclaredMethods();
        for (Method m : methods) {
            if (m.isAnnotationPresent(ImplementedFields.class)) {
                ImplementedFields anot = m.getAnnotation(ImplementedFields.class);
                String[] names = anot.names();
                String searchName = anot.searchName();
                Class fieldType = anot.fieldType();
                List<String> namesPaths = Lists.newArrayList(names).stream()
                        .map(n -> fieldName + "." + n)
                        .collect(Collectors.toList());
                InterfaceField clazz = new InterfaceField(searchName, namesPaths, fieldType);
                String fieldPathName = fieldName + "." + searchName;
                putToInterfaceFieldMap(rootEntityClassName, fieldPathName, clazz);
                classFields.put(searchName, InterfaceField.class);
            }
        }
        return classFields;
    }

    private static Map<String, Class> prepareForClass(Class c, String rootEntityClassName) {
        Map<String, Class> classFields = new HashMap();
        LinkedList<Field> fieldlist = new LinkedList();
        for (Field field : getAllFields(fieldlist, c)) {
//            if (field.isAnnotationPresent(RecursionReference.class)) {
//                System.out.println("recursion reference :D");
//            } else {
            Class<?> fieldType = field.getType();
            classFields.putAll(prepareForFieldType(fieldType, field.getName(), field, rootEntityClassName));
//            }
        }
        return classFields;
    }

    private static Map<String, Class> prepareForFieldType(Class<?> fieldType, String fieldName, Field field, String rootEntityClassName) {
        //  String fieldName = field.getName();
        Map<String, Class> classFields = new HashMap();
        if (isSimpleType(fieldType) || fieldType.isEnum()) {
            classFields.put(fieldName, fieldType);
            //      System.out.println("Simple " + fieldName + "  " + fieldType);
        } else if (List.class.isAssignableFrom(fieldType)) {
            //     System.out.println("List : " + fieldName + "  " + fieldType);
            Class<?> listArgumentClass = getArgumentTypeFromListField(field);
            Map<String, Class> innerClassFields = prepareForFieldType(listArgumentClass, fieldName, field, rootEntityClassName);
            for (Map.Entry<String, Class> entry : innerClassFields.entrySet()) {
                String fieldName2 = entry.getKey();
                classFields.put(fieldName2, entry.getValue());
                //  System.out.println(fieldName2 + "  " + entry.getValue());
            }
        } else if (fieldType.isInterface()) {
            System.out.println("found interface: " + fieldType.getSimpleName());
            Map<String, Class> interfaceAnnotatedFields = prepareForInterface(fieldType, fieldName, rootEntityClassName);
            for (Map.Entry<String, Class> entry : interfaceAnnotatedFields.entrySet()) {
                String fieldName2 = fieldName + "." + entry.getKey();
                classFields.put(fieldName2, entry.getValue());
                System.out.println(fieldName2 + "  " + entry.getValue());
            }
        } else {
            if (field.isAnnotationPresent(Reference.class)) {
                //      System.out.println("reference :D");
                classFields.put(field.getName(), field.getType());
            } else {
                //     System.out.println("other Object class: " + fieldType + "  " + fieldType);
                Map<String, Class> innerClassFields = prepareForClass(fieldType, rootEntityClassName);
                for (Map.Entry<String, Class> entry : innerClassFields.entrySet()) {
                    String fieldName2 = fieldName + "." + entry.getKey();
                    classFields.put(fieldName2, entry.getValue());
                    //       System.out.println(fieldName2 + "  " + entry.getValue());
                }
            }
        }
        return classFields;
    }

    public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));
        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }

    private static Class<?> getArgumentTypeFromListField(Field listField) {
        final Type argumentType = listField.getGenericType();
        ParameterizedType propertyType = null;
        if ((argumentType instanceof ParameterizedType)) {
            propertyType = (ParameterizedType) argumentType;
        } else {
            Type genericSuperclass = ((Class<?>) argumentType).getGenericSuperclass();
            if ((genericSuperclass instanceof ParameterizedType)) {
                propertyType = (ParameterizedType) genericSuperclass;
                return (Class<?>) propertyType.getActualTypeArguments()[0];
            } else {
                throw new ConversionErrorWithCode("no argument for List<> ", ExceptionCodes.INTERNAL_ERROR, null);
            }
        }
        return (Class<?>) propertyType.getActualTypeArguments()[0];
    }

    private static void putToInterfaceFieldMap(String rootEntityClassName, String fieldPathName, InterfaceField clazz) {
        if(!interfaceFieldMap.containsKey(rootEntityClassName)){
            interfaceFieldMap.put(rootEntityClassName, new HashMap());
        }
        interfaceFieldMap.get(rootEntityClassName).put(fieldPathName, clazz);
    }

    public void getLinkedListFields() {
        System.out.println(getAllFields(new LinkedList<Field>(), LinkedList.class));
    }
}
