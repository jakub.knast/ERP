/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.materialstate;

import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.model.states.MaterialState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class MaterialStateOrdered extends StateBase {

    public MaterialStateOrdered() {
        super(MaterialState.ORDERED);
    }

    @Reference
    private OrderEntity order;

}
