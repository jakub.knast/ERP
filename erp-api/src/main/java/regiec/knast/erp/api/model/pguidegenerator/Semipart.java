/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.pguidegenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class Semipart {

    private Integer no = 0;
    private int count = 0;
    private int thatCount = 0;
    private List<Integer> multipliers = new ArrayList();
    private String mulStr = "ilość";
    private String countStr = "ilość";
    private String detalId = null;
    private String detalNo = "Numer materiału";
    private String detalName = "Nazwa materiału";
    private String prodGuideNo = "numer przewodnika";
    
    public void calculateCounts(){
        List<String> strs = multipliers.stream()
                .map(i -> String.valueOf(i))
                .collect(Collectors.toList());
        this.mulStr = String.join("x", strs) + "x" + thatCount;
        this.count = thatCount;
        for (Integer m : multipliers) {
            this.count *= m;
        }
        this.countStr = String.valueOf(this.count);        
    }

}
