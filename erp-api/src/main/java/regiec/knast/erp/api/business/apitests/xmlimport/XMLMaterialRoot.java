/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests.xmlimport;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Data
@XmlRootElement(name = "dataroot")
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLMaterialRoot {

    private List<Kopia_x0020_Materiały_w_ZP> Kopia_x0020_Materiały_w_ZP;

}
