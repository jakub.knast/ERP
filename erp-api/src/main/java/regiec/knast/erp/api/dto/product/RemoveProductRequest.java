/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.product;

import regiec.knast.erp.api.dto.RemoveWithConfirmationRequest;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class RemoveProductRequest extends RemoveWithConfirmationRequest {

    public RemoveProductRequest(String id, Boolean confirmed, Boolean removeRequestFromUp) {
        super(id, confirmed, removeRequestFromUp);
    }

}
