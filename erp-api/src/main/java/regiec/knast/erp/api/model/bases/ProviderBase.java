/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.model.ProviderAssortment;
import regiec.knast.erp.api.model.states.ProviderState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProviderBase extends ObjectWithDate {

    private String name;
    private String nip;
    private String postalCode;
    private String city;
    private String street;
    private int daysForPayment;
    private String jangraSeller;
    private String phone;
    private String email;
    private ProviderState state;
    private String comments;
    private ProviderAssortment assortment;
    private boolean removed;
    
}
