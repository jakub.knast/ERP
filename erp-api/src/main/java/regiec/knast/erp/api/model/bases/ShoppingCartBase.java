/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dto.shoppingCart.ShoppingCartPosition;


/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ShoppingCartBase extends ObjectWithDate {

    private List<ShoppingCartPosition> positions = new ArrayList();
}
