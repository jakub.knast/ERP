/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.model.pguidegenerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class OrderedPages extends ArrayList<OrderedPage> {

    public List<OrderedPage> getAllSelectedPages() {
        return this.stream()
                .filter(op -> op.getDraft())
                .collect(Collectors.toList());
    }

    public OrderedPages(int initialCapacity) {
        super(initialCapacity);
    }

    public OrderedPages(Collection<? extends OrderedPage> c) {
        super(c);
    }

    public Set<String> getOrderedPagesIds() {
        return this.stream()
                .filter(op -> op.getProductGuide())
                .map(op -> op.getProductTempalteId())
                .collect(Collectors.toSet());
    }

    public Set<String> getOrderedDraftsIds() {
        return this.stream()
                .filter(op -> op.getDraft())
                .map(op -> op.getProductTempalteId())
                .collect(Collectors.toSet());
    }

    public List<String> getOrderedPagesIdsList() {
        return this.stream()
                .filter(op -> op.getProductGuide())
                .map(op -> op.getProductTempalteId())
                .collect(Collectors.toList());
    }

    public List<String> getOrderedDraftsIdsList() {
        return this.stream()
                .filter(op -> op.getDraft())
                .map(op -> op.getProductTempalteId())
                .collect(Collectors.toList());
    }

}
