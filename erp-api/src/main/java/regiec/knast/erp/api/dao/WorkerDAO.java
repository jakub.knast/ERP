/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.WorkerDAOInterface;
import regiec.knast.erp.api.entities.WorkerEntity;
import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class WorkerDAO extends DAOBase<WorkerEntity, WorkerEntitiesList> implements WorkerDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = WorkerEntity.class;

    @Inject
    public WorkerDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

}
