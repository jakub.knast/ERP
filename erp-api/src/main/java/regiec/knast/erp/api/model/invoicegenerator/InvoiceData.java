/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.invoicegenerator;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.model.DocumentInterface;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class InvoiceData implements DocumentInterface{

    private String documentNo = "006600";
    private String date = " 1 stycznia 2017";
    private SellerInfoInvoice providerInfo = new SellerInfoInvoice();
    private RecipientInfoInvoice recipientInfo = new RecipientInfoInvoice();
    private List<InvoiceItem> products = new ArrayList();
    
    public DocumentType getType(){
        return DocumentType.INVOICE;
    }
}
