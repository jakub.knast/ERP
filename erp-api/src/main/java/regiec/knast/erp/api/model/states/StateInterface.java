/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.states;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface StateInterface {

    String name();
    
    boolean isRemovable();
    
    String getTranslation();

    void setTranslation(String translation);

    String getBackgroundColor();

    void setBackgroundColor(String backgroundColor);

    String getFontColor();

    void setFontColor(String fontColor);
    
//    boolean isRemovable();
}
