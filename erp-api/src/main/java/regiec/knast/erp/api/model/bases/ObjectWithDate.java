/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.Date;
import org.jaxygen.annotations.APIBrowserIgnoreSetter;
import org.jaxygen.annotations.HiddenField;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
public abstract class ObjectWithDate {

    @HiddenField
    private Date creationDate;
    @HiddenField
    private Date lastModificationDate;

    @APIBrowserIgnoreSetter
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @APIBrowserIgnoreSetter
    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }
    
    public static Date cloneDate(Date origin){
        return new Date(origin.getTime());
    }
        
    public Date cloneCreationDate(){
        return cloneDate(creationDate);
    }
    
    public Date cloneLastModificationDate(){
        return cloneDate(creationDate);
    }
    
}
