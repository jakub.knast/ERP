/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.productionorder;

import regiec.knast.erp.api.model.bases.ProductionOrderBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AddProductionOrderRequest extends ProductionOrderBase {

}
