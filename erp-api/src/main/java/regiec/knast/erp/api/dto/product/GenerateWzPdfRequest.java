/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.product;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class GenerateWzPdfRequest {

    private String sellerId;
    private String recipientId;
    private List<String> productsIds = new ArrayList();
    
    public List<String> splitIds(){
        List<String> ret = new ArrayList();
        for (String productsId : this.productsIds) {
            ArrayList<String> splited = Lists.newArrayList(productsId.split("-"));
            ret.addAll(splited);
        }
        return ret;
    }
}
