/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.deliveryorder;

import regiec.knast.erp.api.model.bases.DeliveryOrderBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AddDeliveryOrderRequest extends DeliveryOrderBase {

}
