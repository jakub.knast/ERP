/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.order;

import regiec.knast.erp.api.model.MoneyERP;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class OrderPositionPiecePriceWithDiscountRequest {
    
    private Double discount;
    private MoneyERP piecePrice;
}