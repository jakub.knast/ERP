/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import java.awt.Dimension;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.manager.satisfier.SheetDemandSatisfier;
import regiec.knast.sheet.nesting.core.Bin;
import regiec.knast.sheet.nesting.utils.ResultDrawer;

/**
 *
 * @author fbrzuzka
 */
public class SheetDemandSatisfierIntegreationTest extends GuiceInjector {

    @Inject
    private SheetDemandSatisfier sheetDemandSatisfier;

    private Gson g = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(SheetDemandSatisfierIntegreationTest.class);
        SheetDemandSatisfierIntegreationTest mongoTest = (SheetDemandSatisfierIntegreationTest) create;
        try {
            mongoTest.doTest();
        } catch (Exception ex) {
            Logger.getLogger(GenerateProductionJobsTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    public void doTest() {
        MaterialEntity m = new MaterialEntity();
        m.setLength(CuttingSizeManager.createTwoDimCuttingSize(1.0, 1.0));
        
        MaterialDemandEntity md = new MaterialDemandEntity();
        md.setCount(3);
        md.setCuttingSize(CuttingSizeManager.createTwoDimCuttingSize(0.1, 0.2));
        List ret = (List)sheetDemandSatisfier.safisfySheetMaterial(m, md);
        Bin[] bins = (Bin[])ret.get(0);
        Dimension viewPortDimension = (Dimension)ret.get(1);
        printResult(bins, viewPortDimension);
    }

    private void printResult(Bin[] bins, Dimension viewPortDimension) {
        ResultDrawer resultDrawer = new ResultDrawer("jakitam");

        try {
            System.out.println("Generating bin images.........................");
            resultDrawer.drawbinToFile(bins, viewPortDimension);
            System.out.println();
            System.out.println("Generating bin images.........................");

            System.out.println("Generating bin description files....................");
            resultDrawer.createOutputFiles(bins);
            System.out.println("DONE!!!");
        } catch (IOException ex) {
            Logger.getLogger(SheetDemandSatisfier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
