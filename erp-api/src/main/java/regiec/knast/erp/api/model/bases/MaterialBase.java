/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.entities.DeliveryOrderEntity;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;
import regiec.knast.sheet.nesting.core.Bin;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialBase extends ObjectWithDate {

    private int count;
    private CuttingSize reservedLength;
    private CuttingSize leftLength;
//    private CuttingSize totalLength;
    private Double percentUsage = 0.0;
    private Bin sheetBin;
    private String comments;
    private String localization;
    @ErpIdEntity(entityClass = DeliveryOrderEntity.class)
    private String deliveryOrderId;
}
