/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.operationName;

import regiec.knast.erp.api.model.bases.OperationNameBase;
import regiec.knast.erp.api.model.states.OperationNameState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class AddOperationNameRequest extends OperationNameBase {

    public AddOperationNameRequest(String name, OperationNameState state) {
        super(name, state, false);
    }

}
