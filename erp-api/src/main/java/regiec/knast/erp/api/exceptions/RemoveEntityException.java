/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.exceptions;

import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author fbrzuzka
 */
public class RemoveEntityException extends ConversionErrorWithCode{
    
    
    public RemoveEntityException(String message) {
        super(message, ExceptionCodes.REMOVE_ENTITY); 
    }
    
    public RemoveEntityException(String message, Exception ex) {
        super(message, ExceptionCodes.REMOVE_ENTITY, null, ex); 
    }
        
}
