/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api;

import java.util.ArrayList;
import java.util.List;
import org.jaxygen.invoker.ClassRegistry;
import regiec.knast.erp.api.business.*;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ERPClassRegistry implements ClassRegistry {

    @Override
    public List<Class> getRegisteredClasses() {
        List<Class> clases = new ArrayList();
        clases.add(MaterialService.class);
        clases.add(OrderService.class);
        clases.add(ProductService.class);
        clases.add(MaterialTypeService.class);
        clases.add(WareService.class);
        clases.add(ProviderService.class);
        clases.add(SellerService.class);
        clases.add(WareTypeService.class);
        clases.add(SystemService.class);
        clases.add(MaterialTemplateService.class);
        clases.add(ProductTemplateService.class);
        clases.add(WareTemplateService.class);
        clases.add(ResourceInfoService.class);
        clases.add(RecipientService.class);
        clases.add(ProductionOrderService.class);
        clases.add(DeliveryOrderService.class);
        clases.add(WorkerService.class);
        clases.add(MachineService.class);
        clases.add(ProductionJobService.class);
        clases.add(MaterialDemandService.class);
        clases.add(DevelopmentService.class);
        clases.add(ShoppingCartService.class);
        clases.add(TodoIssuesService.class);
        clases.add(OperationNameService.class);
        clases.add(ProfessionGroupService.class);
clases.add(DocumentService.class);
//Here_will_be_insert_binding_service
        return clases;
    }
}
