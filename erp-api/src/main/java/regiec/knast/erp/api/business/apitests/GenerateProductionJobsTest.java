/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dao.interfaces.ProductionJobDAOInterface;
import regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsRequest;
import regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsResponse;
import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.manager.orders.GenerateProductionJobsTransformer;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class GenerateProductionJobsTest extends GuiceInjector {

    @Inject
    private GenerateProductionJobsTransformer productionJobManager;

    @Inject
    private ProductionJobDAOInterface productionJobDAO;

    private Gson g = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(GenerateProductionJobsTest.class);
        GenerateProductionJobsTest mongoTest = (GenerateProductionJobsTest) create;
        try {
            mongoTest.generateProductionJobs();
        } catch (Exception ex) {
            Logger.getLogger(GenerateProductionJobsTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    public void generateProductionJobs() throws ConversionError {
        GenerateProductionJobsRequest from = new GenerateProductionJobsRequest("599c6ff7d8b0af2c8c6e2f92");
        GenerateProductionJobsResponse generateProductionJobs = productionJobManager.generateProductionJobs(from);

        for (ProductionJobEntity productionJobEntity : generateProductionJobs.getGenerated()) {
            //   gson. 
            productionJobEntity.setParent(null);
            String toJson = g.toJson(productionJobEntity);
            System.out.println(toJson);
        }
    }
}
