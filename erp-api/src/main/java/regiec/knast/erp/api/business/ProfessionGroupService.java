/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.OperationNameDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProfessionGroupDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WorkerDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.professionGroup.AddProfessionGroupRequest;
import regiec.knast.erp.api.dto.professionGroup.ListProfessionGroupsWithOperationIdRequest;
import regiec.knast.erp.api.dto.professionGroup.ProfessionGroupDTO;
import regiec.knast.erp.api.dto.professionGroup.ProfessionGroupsListDTO;
import regiec.knast.erp.api.dto.professionGroup.RemoveProfessionGroupRequest;
import regiec.knast.erp.api.dto.professionGroup.UpdateProfessionGroupRequest;
import regiec.knast.erp.api.entities.OperationNameEntity;
import regiec.knast.erp.api.entities.ProfessionGroupEntity;
import regiec.knast.erp.api.entities.containers.ProfessionGroupEntitiesList;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.ProfessionGroupState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class ProfessionGroupService extends AbstractService<ProfessionGroupEntity, ProfessionGroupDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.PROFESSION_GROUP;

    @Inject
    private ProfessionGroupDAOInterface professionGroupDAO;
    @Inject
    private Validator validator;
    @Inject
    private WorkerDAOInterface workerDAO;
    @Inject
    private OperationNameDAOInterface operationNameDAO;

    @NetAPI
    public ProfessionGroupDTO addProfessionGroup(AddProfessionGroupRequest request) {
            validator.validateNotNull(request).throww();
            validator.validateNotNullAndNotEmpty(request.getName(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "name").throww();
            validator.validateNotNullAndNotEmpty(request.getCode(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "code").throww();
            validator.validateUniqueness("name", request.getName(), null, MY_ENTITY_TYPE).throww();
            validator.validateUniqueness("code", request.getCode(), null, MY_ENTITY_TYPE).throww();

            ProfessionGroupEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new ProfessionGroupEntity());
            entity.setLabel(entity.createLabel());
            entity.setAllowOperations(getAllowOperations(request.getAllowOperationsIds()));
            entity.setState(ProfessionGroupState.NORMAL);
            ProfessionGroupEntity entity1 = professionGroupDAO.add(entity);
            return entity1.convertToDTO();
    }

    @NetAPI
    public ProfessionGroupDTO getProfessionGroup(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public ProfessionGroupDTO updateProfessionGroup(UpdateProfessionGroupRequest request) {
            validator.validateNotNull(request).throww();
            String id = request.getId();
            validator.validateNotNull(id).throww();
            validator.validateNotNull(request).throww();
            validator.validateNotNullAndNotEmpty(request.getName(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "name").throww();
            validator.validateNotNullAndNotEmpty(request.getCode(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "code").throww();
            validator.validateUniqueness("name", request.getName(), id, MY_ENTITY_TYPE, ExceptionCodes.PROFESSION_GROUP__NAME_NOT_UNIQUE).throww();
            validator.validateUniqueness("code", request.getCode(), id, MY_ENTITY_TYPE, ExceptionCodes.PROFESSION_GROUP__CODE_NOT_UNIQUE).throww();

            ProfessionGroupEntity entity = professionGroupDAO.get(id);
            validator.validateNotNull(entity).throww();
            ProfessionGroupEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new ProfessionGroupEntity());
            BeanUtilSilent.translateBean(request, updatedEntity);
            updatedEntity.setAllowOperations(getAllowOperations(request.getAllowOperationsIds()));
            updatedEntity.setLabel(updatedEntity.createLabel());
            professionGroupDAO.update(updatedEntity);
            return updatedEntity.convertToDTO();
    }
    
    public List<OperationNameEntity> getAllowOperations(List<String> operationsIds) {
        List<OperationNameEntity> ret = new ArrayList();
        if (operationsIds != null) {
            for (String oid : operationsIds) {
                final OperationNameEntity get1 = operationNameDAO.get(oid);
                ret.add(get1);
            }
        }
        return ret;
    }


    @NetAPI
    public RemoveResponse removeProfessionGroup(RemoveProfessionGroupRequest request) {
            validator.validateNotNull(request).throww();
            String id = request.getId();
            validator.validateNotNull(id).throww();
            ProfessionGroupEntity professionGroupEntity = professionGroupDAO.get(id);
            if (request.getConfirmed()) {
                professionGroupEntity.setRemoved(true);
                professionGroupDAO.update(professionGroupEntity);
                return new RemoveResponse(true, null);
            } else {
                //   return new RemoveResponse(false, buildRemoveConsequencesMap(professionGroupEntity));
                return new RemoveResponse(false, null);
            }
    }

    @NetAPI
    public ProfessionGroupsListDTO listProfessionGroups(PaginableFilterWithCriteriaRequest request) {
        return (ProfessionGroupsListDTO) super.list(request);
    }

    @NetAPI
    public ProfessionGroupsListDTO listProfessionGroupsWithOperationId(ListProfessionGroupsWithOperationIdRequest request) {
        ProfessionGroupEntitiesList list = professionGroupDAO
                .listProfessionGroupsWithOperationId(request.getOperationNameId());        
        return listToDto(list); 
    }
}
