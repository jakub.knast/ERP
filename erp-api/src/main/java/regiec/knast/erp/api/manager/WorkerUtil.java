/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.apache.commons.lang3.math.NumberUtils;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dao.interfaces.ProgramConstsDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WorkerDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.ProfessionGroupEntity;
import regiec.knast.erp.api.entities.WorkerEntity;
import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class WorkerUtil {

    @Inject
    private WorkerDAOInterface workerDAO;
    @Inject
    private ProgramConstsDAOInterface programConstsDAO;
    @Inject
    private Validator validator;

    public String calculateWorkerMinorNo(ProfessionGroupEntity myGroup) {
        String ret = "";

        PaginableFilterWithCriteriaRequest req = PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder().addConstraint("professionGroup", myGroup.getId(), ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build();
        WorkerEntitiesList list = workerDAO.list(req);
        int maxIndex = finxMaxIndex(list);
        int newIndex = maxIndex + 1;
        ret = String.valueOf(newIndex);
        if (newIndex < 10) {
            ret = "0" + ret;
        }
        return ret;
    }

    private int finxMaxIndex(WorkerEntitiesList list) {
        int max = 0;
        for (WorkerEntity workerEntity : list) {
            if (workerEntity.getWorkerMinorNo() != null && NumberUtils.isDigits(workerEntity.getWorkerMinorNo())) {
                int index = Integer.parseInt(workerEntity.getWorkerMinorNo());
                max = index > max ? index : max;
            }
        }
        return max;
    }

    public String calculateWorkerCode(ProfessionGroupEntity myGroup, String workerMinorNo) {
        if (myGroup != null && !Strings.isNullOrEmpty(myGroup.getCode())) {
            return myGroup.getCode() + "-" + workerMinorNo;
        } else {
            throw new ConversionError("professionalGroup for worker is empty or null");
        }
    }
}
