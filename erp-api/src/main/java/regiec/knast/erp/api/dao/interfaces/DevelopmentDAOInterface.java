/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.DevelopmentEntity;
import regiec.knast.erp.api.entities.containers.DevelopmentEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface DevelopmentDAOInterface extends DAOBaseInterface<DevelopmentEntity, DevelopmentEntitiesList> {

}
