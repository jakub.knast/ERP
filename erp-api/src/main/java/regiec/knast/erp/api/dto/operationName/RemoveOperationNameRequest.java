/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.operationName;

import regiec.knast.erp.api.dto.RemoveWithConfirmationRequest;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class RemoveOperationNameRequest extends RemoveWithConfirmationRequest {

}
