/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils.anotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import regiec.knast.erp.api.interfaces.IndexableEntity;

/**
 *
 * @author fbrzuzka
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ErpIdEntity {

    public Class<? extends IndexableEntity > entityClass();
    public static class Dummy implements IndexableEntity{
        
    }
}
