/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.materialtemplates;

import regiec.knast.erp.api.dto.materialtypes.MaterialTypeDTO;
import regiec.knast.erp.api.dto.resourceinfo.ResourceInfoDTO;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.bases.MaterialTemplateBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialTemplateDTO extends MaterialTemplateBase {

    private String id;
    private String materialTypeId;
    private MaterialTypeDTO materialType;
    private ResourceInfoDTO resourceInfoDTO;

    public MaterialTemplateDTO(String id, String name, Dimension dimension) {

        this.id = id;
    }

}
