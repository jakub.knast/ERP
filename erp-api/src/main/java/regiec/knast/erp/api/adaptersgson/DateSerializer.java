/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.adaptersgson;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.Date;
import lombok.extern.java.Log;
import regiec.knast.erp.api.ContextInitializer;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Log
public class DateSerializer implements JsonSerializer<Date> {

    @Override
    public JsonElement serialize(Date t, Type type, JsonSerializationContext jsc) {
        return t == null ? null : new JsonPrimitive(ContextInitializer.DATE_FORMAT.format(t));
    }
}
