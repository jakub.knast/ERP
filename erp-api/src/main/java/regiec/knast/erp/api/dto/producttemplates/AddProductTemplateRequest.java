/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import org.jaxygen.dto.Uploadable;
import regiec.knast.erp.api.model.bases.ProductTemplateCreateBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddProductTemplateRequest extends ProductTemplateCreateBase  {

    private Uploadable techDrawingPDF;
    private Uploadable techDrawingDXF;
    private Uploadable reportTemplate;
}
