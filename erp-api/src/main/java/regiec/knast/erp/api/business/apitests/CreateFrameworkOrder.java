/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.business.apitests;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.assertj.core.api.Assertions;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.order.*;
import regiec.knast.erp.api.dto.orderedproducts.UpdateOrderPosition;
import regiec.knast.erp.api.dto.producttemplates.*;
import regiec.knast.erp.api.model.*;
import regiec.knast.erp.api.model.states.*;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class CreateFrameworkOrder extends AllServicesInjector {
    private static MoneyERP money = MoneyERP.create(new BigDecimal(1.0), "PLN");

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(CreateFrameworkOrder.class);
        CreateFrameworkOrder mongoTest = (CreateFrameworkOrder) create;
        try {
            mongoTest.initServices();
            mongoTest.test_addOrderWithPositions();
        } catch (Exception ex) {
            Logger.getLogger(ListTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    public void test_addOrderWithPositions() {
        String orderNo = "oRamowe zamówienie-" + (int)((System.currentTimeMillis() - 1535034803822l)/1000);

        boolean isFramework = true;
        String productTemplateId = null;
        String recipientId = null;
        String orderId = null;
        String productionOrderId = null;
        String profilekProductTemplateId = "5b75c6176f3098228c0ea92d";

           // add product template
        productTemplateId = profilekProductTemplateId;
        ProductTemplateDTO ptDTO = productTemplateService.getProductTemplate(new BasicGetRequest(profilekProductTemplateId));

        recipientId = "5975c9984b13ec08e472ddc3"; // promag

        // add empty order
        OrderDTO addOrder = orderService.addOrder(new AddOrderRequest(recipientId, orderNo, "comments", new Date(), isFramework));
        orderId = addOrder.getId();
        //validate order exists
        OrderDTO order = orderService.getOrder(new BasicGetRequest(orderId));
        Assertions.assertThat(order).isNotNull();
        Assertions.assertThat(order.getState()).isEqualTo(OrderState.NEW);
        Assertions.assertThat(order.getFrameworkContractOrder()).isEqualTo(isFramework);
        Assertions.assertThat(order.getNr()).isEqualTo(orderNo);

        // add product1 as position to order1
        UpdateOrderRequest updateOrderRequest = BeanUtilSilent.translateBeanAndReturn(order, new UpdateOrderRequest());
        updateOrderRequest.getOrderPositions().add(
                new UpdateOrderPosition(productTemplateId, null, "010", 10, money, money, 0.0, "comments", new Date(), null, 0, 0, new ArrayList()));
        
        OrderDTO updateOrder = orderService.updateOrder(updateOrderRequest);
        //validate order has position        
        OrderDTO order2 = orderService.getOrder(new BasicGetRequest(orderId));
        Assertions.assertThat(order2).isNotNull();
        Assertions.assertThat(order2.getState()).isEqualTo(OrderState.NEW);
        Assertions.assertThat(order2.getNr()).isEqualTo(orderNo);
        Assertions.assertThat(order2.getOrderPositions()).hasSize(1);
        Assertions.assertThat(order2.getOrderPositions().get(0).getProductTemplate().getId()).isEqualTo(productTemplateId);

      
    }
}
