/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddWarePart extends WarePartBase {

    private String wareTemplateId;

    public AddWarePart(String wareTemplateId, Integer count, String positionOnTechDraw, String comments) {
        super(count, positionOnTechDraw, comments);
        this.wareTemplateId = wareTemplateId;
    }
    
    
}
