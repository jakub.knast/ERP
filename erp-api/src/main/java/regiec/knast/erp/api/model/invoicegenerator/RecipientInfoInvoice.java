/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.invoicegenerator;

import com.google.common.base.Strings;
import regiec.knast.erp.api.model.wzgenerator.*;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
//@lombok.AllArgsConstructor
public class RecipientInfoInvoice extends CompanyInfoBase {

    private String nip = "a";

    public RecipientInfoInvoice(String nip, String name, String street, String postal, String city) {
        super(Strings.nullToEmpty(name), Strings.nullToEmpty(street), Strings.nullToEmpty(postal), Strings.nullToEmpty(city));
        this.nip = Strings.nullToEmpty(nip);
    }

}
