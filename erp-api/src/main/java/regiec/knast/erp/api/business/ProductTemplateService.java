/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.dto.Downloadable;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.order.GenerateSTHResponse;
import regiec.knast.erp.api.dto.product.RemoveProductRequest;
import regiec.knast.erp.api.dto.producttemplates.*;
import regiec.knast.erp.api.dto.producttemplates.partcard.*;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlanDTO;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductEntitiesList;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;
import regiec.knast.erp.api.manager.DetalTemplateManager;
import regiec.knast.erp.api.manager.ResourceManager;
import regiec.knast.erp.api.manager.crud.PartCardCRUD;
import regiec.knast.erp.api.manager.crud.ProductionPlanCRUD;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.pguidegenerator.GenerateProductionGuideRequest;
import regiec.knast.erp.api.model.pguidegenerator.OrderedPages;
import regiec.knast.erp.api.model.pguidegenerator.ProductionGuideGenerator;
import regiec.knast.erp.api.model.wzgenerator.DownloadableStream;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class ProductTemplateService extends AbstractService<ProductTemplateEntity, ProductTemplateDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.PRODUCT_TEMPLATE;

    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private PartCardCRUD partCardCRUD;
    @Inject
    private ProductionPlanCRUD productionPlanManager;
    @Inject
    private ResourceManager resourceManager;
    @Inject
    private ProductionGuideGenerator productionGuideGenerator;
    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;
    @Inject
    private Validator validator;
    @Inject
    private ProductService productService;
    @Inject
    private DetalTemplateManager detalTemplateManager;

    @NetAPI
    public ProductTemplateDTO addProductTemplate(AddProductTemplateRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("productNo", request.getProductNo(), null, MY_ENTITY_TYPE).throww();
        validatorDetal.validateProductionSalesOffers(request).throww();
        validatorDetal.validateProductTemplatePrice(request.getSalesOffers()).throww();
        validator.validateNotNullAndNotEmpty(request.getName(), ExceptionCodes.PRODUCT_TEMPLATE__NAME_NULL_OR_EMPTY).throww();

        // There must be id in new entity. It's for update resource function.
        ProductTemplateEntity productTemplateEntity = new ProductTemplateEntity(IDUtil.nextId_());
        BeanUtilSilent.translateBean(request, productTemplateEntity);

        productTemplateEntity.setBuildingTime(0.0);
        productTemplateEntity.setBuildingTimeTJ(0.0);
        productTemplateEntity.setBuildingTimeTPZ(0.0);

        productTemplateEntity.setTotalBuildingTime(0.0);
        productTemplateEntity.setTotalBuildingTimeTJ(0.0);
        productTemplateEntity.setTotalBuildingTimeTPZ(0.0);
        resourceManager.updateResources(productTemplateEntity, request);
        productTemplateDAO.add(productTemplateEntity, productTemplateEntity.getId());
        return productTemplateEntity.convertToDTO();
    }

    @NetAPI
    public ProductTemplateDTO getProductTemplate(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public ProductTemplateDTO updateProductTemplateSummary(UpdateProductTemplateSummaryRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateNotNull(request.getName()).throww();
        validatorDetal.validateProductionSalesOffers(request).throww();
        validatorDetal.validateProductTemplatePrice(request.getSalesOffers()).throww();
        validator.validateUniqueness("productNo", request.getProductNo(), id, MY_ENTITY_TYPE).throww();

        ProductTemplateEntity entity = productTemplateDAO.get(id);
        validator.validateNotNull(entity).throww();

        ProductTemplateEntity updatedEntity = new ProductTemplateEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);

        resourceManager.updateResources(updatedEntity, request);

        productTemplateDAO.update(updatedEntity);
        detalTemplateManager.updateProducts(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeProductTemplate(RemoveProductTemplateRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        ProductTemplateEntity productTemplateEntity = productTemplateDAO.get(id);
        if (request.getConfirmed()) {
            productTemplateEntity.setRemoved(true);
            productTemplateDAO.update(productTemplateEntity);
            removeProducts(id);
            //    removeFromPartCards(id);
            return new RemoveResponse(true, null);
        } else {
            //build consequencesMap
            Map<String, Object> consequencesMap = buildRemoveConsequencesMap(productTemplateEntity);
            return new RemoveResponse(false, consequencesMap);
        }
    }

    private void removeProducts(String productTemplateEntityId) {
        final ProductEntitiesList productEntitiesList = detalTemplateManager.getProductsWithProductTemplate(productTemplateEntityId);
        RemoveProductRequest removeProductRequest = new RemoveProductRequest("", true, true);
        for (ProductEntity productEntity : productEntitiesList) {
            removeProductRequest.setId(productEntity.getId());
            productService.removeProduct(removeProductRequest);
        }
    }

    public Map<String, Object> buildRemoveConsequencesMap(ProductTemplateEntity entity) {
        Map<String, Object> consequencesMap = ImmutableMap.<String, Object>builder()
                .put("entityType", entity.getClass().getSimpleName())
                .put("id", entity.getId())
                .put("productNo", entity.getProductNo())
                .put("leafs", new ArrayList<>())
                .build();
        final ProductEntitiesList productEntitiesList = detalTemplateManager.getProductsWithProductTemplate(entity.getId());
        for (ProductEntity productEntity : productEntitiesList) {
            Map<String, Object> productConsequencesMap = productEntity.buildRemoveConsequencesMap();
            ((ArrayList) consequencesMap.get("leafs")).add(productConsequencesMap);
        }
        return consequencesMap;
    }

    @NetAPI
    public ProductTemplatesListDTO listProductTemplates(PaginableFilterWithCriteriaRequest request) {
        return (ProductTemplatesListDTO) super.list(request);
    }

    @NetAPI
    public MaterialPartListResponse addProductTemplateMaterialPart(AddProductTemplateMaterialPartRequest request) {
        validator.validateNotNull(request, "AddProductTemplateMaterialPartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getMaterialPart(), "materialPartItem").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<MaterialPartDTO> ret = partCardCRUD.addMaterialPartToDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getMaterialPart());

        detalTemplateManager.updateProducts(spt);
        return new MaterialPartListResponse(ret);
    }

    @NetAPI
    public WarePartListResponse addProductTemplateWarePart(AddProductTemplateWarePartRequest request) {
        validator.validateNotNull(request, "AddProductTemplateWarePartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getWarePart(), "warePart").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<WarePartDTO> ret = partCardCRUD.addWarePartToDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getWarePart());
        detalTemplateManager.updateProducts(spt);
        return new WarePartListResponse(ret);
    }

    @NetAPI
    public SemiproductPartListResponse addProductTemplateProductPart(AddProductTemplateProductPartRequest request) {
        validator.validateNotNull(request, "AddProductTemplateSemiproductPartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getSemiproductPart(), "semiproductPart").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<SemiproductPartDTO> ret = partCardCRUD.addSemiproductPartToDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getSemiproductPart());
        detalTemplateManager.updateProducts(spt);
        return new SemiproductPartListResponse(ret);
    }

    @NetAPI
    public MaterialPartListResponse updateProductTemplateMaterialPart(UpdateProductTemplateMaterialPartRequest request) {
        validator.validateNotNull(request, "UpdateProductTemplateMaterialPartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getMaterialPart(), "materialPart").throww();
        validator.validateNotNull(request.getMaterialPart().getPositionId(), "materialPart.positionId").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<MaterialPartDTO> ret = partCardCRUD.updateMaterialPartInDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getMaterialPart());
        detalTemplateManager.updateProducts(spt);
        return new MaterialPartListResponse(ret);
    }

    @NetAPI
    public WarePartListResponse updateProductTemplateWarePart(UpdateProductTemplateWarePartRequest request) {
        validator.validateNotNull(request, "UpdateProductTemplateWarePartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getWarePart(), "warePart").throww();
        validator.validateNotNull(request.getWarePart().getPositionId(), "warePart.positionId").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<WarePartDTO> ret = partCardCRUD.updateWarePartInDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getWarePart());
        detalTemplateManager.updateProducts(spt);
        return new WarePartListResponse(ret);
    }

    @NetAPI
    public SemiproductPartListResponse updateProductTemplateProductPart(UpdateProductTemplateProductPartRequest request) {
        validator.validateNotNull(request, "UpdateProductTemplateSemiproductPartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getSemiproductPart(), "semiproductPart").throww();
        validator.validateNotNull(request.getSemiproductPart().getPositionId(), "productPart.positionId").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<SemiproductPartDTO> ret = partCardCRUD.updateSemiproductPartInDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getSemiproductPart());
        detalTemplateManager.updateProducts(spt);
        return new SemiproductPartListResponse(ret);
    }

    @NetAPI
    public MaterialPartListResponse removeProductTemplateMaterialPart(RemoveProductTemplateMaterialPartRequest request) {
        validator.validateNotNull(request, "UpdateProductTemplateMaterialPartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getMaterialPartId(), "materialPartId").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<MaterialPartDTO> ret = partCardCRUD.removeMaterialPartInDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getMaterialPartId());
        detalTemplateManager.updateProducts(spt);
        return new MaterialPartListResponse(ret);
    }

    @NetAPI
    public WarePartListResponse removeProductTemplateWarePart(RemoveProductTemplateWarePartRequest request) {
        validator.validateNotNull(request, "UpdateProductTemplateWarePartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getWarePartId(), "warePartId").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<WarePartDTO> ret = partCardCRUD.removeWarePartInDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getWarePartId());
        detalTemplateManager.updateProducts(spt);
        return new WarePartListResponse(ret);
    }

    @NetAPI
    public SemiproductPartListResponse removeProductTemplateSemiproductPart(RemoveProductTemplateSemiproductPartRequest request) {
        validator.validateNotNull(request, "UpdateProductTemplateSemiproductPartRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getSemiproductPartId(), "semiproductPartId").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<SemiproductPartDTO> ret = partCardCRUD.removeSemiproductPartInDetalTempalte(spt, EntityType.PRODUCT_TEMPLATE, request.getSemiproductPartId());
        detalTemplateManager.updateProducts(spt);
        return new SemiproductPartListResponse(ret);
    }

    //----------------------------PRODUCTION PLAN-------------------------------
    @NetAPI
    public ProductionPlanListResponse addProductTemplateProdPlanItem(AddProductTemplateProdPlanItemRequest request) {
        validator.validateNotNull(request, "AddProductTemplateProdPlanItemRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getProdPlanItem(), "prodPlanItem").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<ProductionPlanDTO> ret = productionPlanManager.addProdPlanItemToDetalTempalte(spt, request.getProdPlanItem());
        detalTemplateManager.updateProducts(spt);
        return new ProductionPlanListResponse(ret);
    }

    @NetAPI
    public ProductionPlanListResponse updateProductTemplateProdPlanItem(UpdateProductTemplateProdPlanItemRequest request) {
        validator.validateNotNull(request, "UpdateProductTemplateProdPlanItemRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getProdPlanItem(), "prodPlanItem").throww();
        validator.validateNotNull(request.getProdPlanItem().getPositionId(), "prodPlanItem.positionId").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<ProductionPlanDTO> ret = productionPlanManager.updateProdPlanItemToDetalTempalte(spt, request.getProdPlanItem());
        detalTemplateManager.updateProducts(spt);
        return new ProductionPlanListResponse(ret);
    }

    @NetAPI
    public ProductionPlanListResponse removeProductTemplateProdPlanItem(RemoveProductTemplateProdPlanItemRequest request) {
        validator.validateNotNull(request, "RemoveProductTemplateProdPlanItemRequest").throww();
        validator.validateNotNull(request.getProductTemplateId(), "productTemplateId").throww();
        validator.validateNotNull(request.getProdPlanItemId(), "prodPlanItemId").throww();

        ProductTemplateEntity spt = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(spt, ExceptionCodes.ENTITY_NOT_EXIST, "ProductTemplateEntity").throww();
        List<ProductionPlanDTO> ret = productionPlanManager.removeProdPlanItemInDetalTempalte(spt, request.getProdPlanItemId());
        detalTemplateManager.updateProducts(spt);
        return new ProductionPlanListResponse(ret);
    }

    //----------------------------------------
    //----------------------
    @NetAPI
    public GenerateSTHResponse validateProductTemplate(ValidateProductTemplateRequest request) {
        ProductTemplateEntity pte = productTemplateDAO.get(request.getProductTemplateId());
        List<String> validationList = validatorDetal.validateProductTemplate(pte);
        boolean valid = validationList.isEmpty();
        pte.setValidationStatus(valid ? "tak" : "nie");
        pte.setValidationMessage(validationList);
        productTemplateDAO.update(pte);
        Object retMessage = "Ok";
        boolean status = true;
        if (!valid) {
            retMessage = String.join("\n", validationList);
            status = false;
        }
        return new GenerateSTHResponse(status, ImmutableMap.builder().put("message", retMessage).build());
    }

    @NetAPI
    public GenerateSTHResponse validateAllProductsTemplate(ValidateAllProductTemplatesRequest request) {
        List<String> invalid = new ArrayList();
        invalid.add("Niepoprawne:");
        ProductTemplateEntitiesList list = productTemplateDAO.list();
        for (ProductTemplateEntity pte : list) {
            List<String> validationList = validatorDetal.validateProductTemplate(pte);
            boolean valid = validationList.isEmpty();
            if (!valid) {
                invalid.add("produkt " + pte.getProductNo() + " - " + pte.getName());
            }
            if (request.getUpdateValidationErrorsField()) {
                pte.setValidationStatus(valid ? "tak" : "nie");
                pte.setValidationMessage(validationList);
                productTemplateDAO.update(pte);
            }
        }

        Object retMessage = "Ok";
        boolean status = true;
        if (invalid.size() > 1) {
            retMessage = String.join("\n", invalid);
            status = false;
        }
        return new GenerateSTHResponse(status, ImmutableMap.builder().put("message", retMessage).build());
    }

    @NetAPI
    public Downloadable getProductionGuide(GetProductionGuideForProductTemplateRequest request) throws IOException {
        validator.validateNotNullAndNotEmpty(request.getProductTemplateId()).throww();

        String productTemplateId = request.getProductTemplateId();
        ProductTemplateEntity productTemplate = productTemplateDAO.get(productTemplateId);
        String pdfName = productTemplate.getName() + "-szablon-przewodnika.pdf";
        validator.validateNotNull(productTemplate).throww();

        ByteArrayOutputStream arrayOutStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutStream = new BufferedOutputStream(arrayOutStream);

        OrderedPages orderedPages = GenerateProductionGuideRequest.fromPropductTemplate(productTemplate, true);
        productionGuideGenerator.generateFromProductTemplate(productTemplate, orderedPages, bufferedOutStream);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(arrayOutStream.toByteArray());
        DownloadableStream downloadableStream = new DownloadableStream(inputStream, pdfName);
        if (request.getForceApplicationDataContentType()) {
            downloadableStream.setContentType("application/data");
        }
        return downloadableStream;
    }

    @NetAPI
    public RefreshSthTemplateUsageResponse refreshUsageInProductTemplate() {
        RefreshSthTemplateUsageResponse ret = new RefreshSthTemplateUsageResponse();
        ProductTemplateEntitiesList allPT = productTemplateDAO.list();
        int noDif = 0;
        int withDif = 0;

        for (ProductTemplateEntity pt : allPT) {
            List<String> renew = detalTemplateManager.findProductTemplateUsage(pt.getId());
            if (!Objects.deepEquals(renew, pt.getPtUsages())) {
                withDif++;
                RefreshSthTemplateUsageResponse.Obj obj = new RefreshSthTemplateUsageResponse.Obj();
                obj.setId(pt.getId());
                obj.setName(pt.getName());
                obj.setNo(pt.getProductNo());
                obj.setOld(pt.getPtUsages());
                obj.setNeww(renew);

                ret.getDifferences().add(obj);
                productTemplateDAO.updateOneField(pt.getId(), "ptUsages", renew);
            } else {
                noDif++;
            }
        }
        ret.setWithDifference(withDif);
        ret.setWithoutDifference(noDif);
        return ret;
    }
}
