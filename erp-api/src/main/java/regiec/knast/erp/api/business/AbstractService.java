/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import org.jaxygen.collections.PartialArrayList;
import org.jaxygen.dto.collections.PaginableListResponseBaseDTO;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.jodah.typetools.TypeResolver;
import regiec.knast.erp.api.dao.interfaces.DAOBaseInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.utils.DaoHandler;
import regiec.knast.erp.api.utils.EntityHeadquarters;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class AbstractService<ENTITY extends ConvertableEntity, DTO> {

    @Inject
    private Validator validator;
    @Inject
    private DaoHandler daoHandler;

    protected Class<ENTITY> entityClazz;
    protected Class<DTO> dtoClazz;

    private void initGenericArgument() {
        if (entityClazz == null) {
            Class<?>[] types = TypeResolver.resolveRawArguments(AbstractService.class, this.getClass());
            entityClazz = (Class<ENTITY>) types[0];
            dtoClazz = (Class<DTO>) types[1];
        }
    }

    public DTO get(BasicGetRequest request) {
        initGenericArgument();
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        DAOBaseInterface dao = daoHandler.getDaoByEntityClass(entityClazz);
        ENTITY entity = (ENTITY) dao.get(id);
        return (DTO) entity.convertToDTO();
    }

    public <LIST_DTO extends PaginableListResponseBaseDTO<DTO>> LIST_DTO list(PaginableFilterWithCriteriaRequest request) {
        initGenericArgument();
        validator.validateNotNull(request).throww();
        DAOBaseInterface dao = daoHandler.getDaoByEntityClass(entityClazz);
        PartialArrayList<ConvertableEntity> entities = dao.list(request);
        return listToDto(entities);    
    }
    
    public <LIST_DTO extends PaginableListResponseBaseDTO> LIST_DTO listToDto(PartialArrayList<? extends ConvertableEntity> entities) {
        initGenericArgument();
        Class<? extends PaginableListResponseBaseDTO> listDTOClass = EntityHeadquarters.getListDto(entityClazz);
        LIST_DTO rc;
        try {
            rc = (LIST_DTO) listDTOClass.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ConversionError("Could not create an instace of retun class while converting paginable collections", ex);
        }

        List arrayList = new ArrayList(entities.size());
        for (ConvertableEntity entity : entities) {
            arrayList.add(entity.convertToDTO());
        }
        rc.setElements(arrayList);
        rc.setSize(entities.getTotalSize());
        return rc;
    }
    
    

}
