/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.product;

import java.util.Date;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.Builder
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class FromCreatingOrderContext {

    private boolean fromFrameworkOrder;
    private String productTemplateId;
    private Integer count;
    private String recipientId;
    private String orderNo;
    private String positionOnOrder;
    private Date deliveryDate;
}
