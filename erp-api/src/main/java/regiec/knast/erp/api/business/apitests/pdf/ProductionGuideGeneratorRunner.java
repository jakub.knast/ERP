/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests.pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.jaxygen.dto.Downloadable;
import regiec.knast.erp.api.business.ProductTemplateService;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dto.producttemplates.GetProductionGuideForProductTemplateRequest;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;
import regiec.knast.erp.api.model.pguidegenerator.GenerateProductionGuideRequest;
import regiec.knast.erp.api.model.pguidegenerator.ProductionGuideData;
import regiec.knast.erp.api.model.pguidegenerator.ProductionGuideGenerator;
import regiec.knast.erp.api.model.pguidegenerator.ProductionGuideGeneratorPDF;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductionGuideGeneratorRunner extends PdfGeneratorBase {

    String command = "start \"\" /max ";
    private final ProductionOrderDAOInterface productionOrderDAO;
    private final ProductionGuideGenerator productionGuideGenerator;

    public static void main(String[] args) throws PdfGeneratorException, IOException {
        ProductionGuideGeneratorRunner runner = new ProductionGuideGeneratorRunner();
//        runner.testPG100611();
        runner.testFromService();
//        runner.testFOoo();
    }

    public ProductionGuideGeneratorRunner() {
        productionOrderDAO = this.buildObj(ProductionOrderDAOInterface.class);
        productionGuideGenerator = this.buildObj(ProductionGuideGenerator.class);
    }

    public void testPG100611() throws FileNotFoundException, IOException {
        File finalGuide = createFile(new File(filePath("final-guide-merged")));
        System.out.println("zapisuję do pliku " + finalGuide.getAbsolutePath());
        try (FileOutputStream fileOutputStream = new FileOutputStream(finalGuide)) {
//            pg100611
            String productionOrderId = "5bbb8ed8e5b47f289ce8e1f4";

////plytka i profil
//            String productionOrderId = "5b2954c3bf33942a70e53b01";
//// profil
//            String productionOrderId = "5b39fb60679cde182075a484";

            GenerateProductionGuideRequest request = new GenerateProductionGuideRequest(productionOrderId, productionOrderDAO, true);
            super.printRequest(request);
            productionGuideGenerator.generateFromProductionOrder(request, fileOutputStream);
            fileOutputStream.flush();
        }

        openPdfInViever(finalGuide);
    }

    public void testFromService() throws FileNotFoundException, IOException {
        File finalGuide = createFile(new File(filePath("final-guide-merged-downloadable")));
        System.out.println("zapisuję do pliku " + finalGuide.getAbsolutePath());
        try (FileOutputStream fileOutputStream = new FileOutputStream(finalGuide)) {
//     
            ProductTemplateService productTemplateService = this.buildObj(ProductTemplateService.class);
            GetProductionGuideForProductTemplateRequest request = 
                    new GetProductionGuideForProductTemplateRequest("5b4e4eac3f61e1142060c232", true);
            Downloadable result = productTemplateService.getProductionGuide(request);
            IOUtils.copy(result.getStream(), fileOutputStream);
            fileOutputStream.flush();
        }
        openPdfInViever(finalGuide);

    }

    private void testFOoo() {
        File file = testGenereteTostream("sampleFoooooProductionGuide", ProductionGuideGeneratorPDF.getFooData());
        openPdfInViever(file);
    }

    private File testGenereteTostream(String fileName, ProductionGuideData data) throws PdfGeneratorException {
        ProductionGuideGeneratorPDF generator = new ProductionGuideGeneratorPDF(data);
        String fileNamePath = filePath(fileName);
        System.out.println("zapisuję do pliku " + fileNamePath);
        File outFile = createFile(new File(fileNamePath));
        try (FileOutputStream fileOutputStream = new FileOutputStream(outFile)) {
            generator.generateToStream(fileOutputStream);
        } catch (IOException ex) {
            throw new PdfGeneratorException("Cannot find output file", ex);
        }
        return outFile;
    }
}
