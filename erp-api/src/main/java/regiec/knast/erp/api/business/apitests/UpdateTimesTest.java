/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.inject.Inject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.manager.DetalTemplateManager;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class UpdateTimesTest extends GuiceInjector {

    @Inject
    private DetalTemplateManager detalTemplateUtil;

    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(UpdateTimesTest.class);
        UpdateTimesTest test = (UpdateTimesTest) create;
        try {
            test.test();
            //test.validateSemi("592ea2565c448d08cc60b095");
        } catch (Exception ex) {
            Logger.getLogger(UpdateTimesTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            test.cleanUp();
        }
    }

    public void test() {
        ProductTemplateEntity entity = productTemplateDAO.get("5935402fc9ac2308f8589317");
        detalTemplateUtil.updateTimesInParentDetals( entity);
    }
    

}
