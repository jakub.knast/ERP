/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.containers.ProductEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductDAO extends DAOBase<ProductEntity, ProductEntitiesList> implements ProductDAOInterface {

    private final Datastore datastore;
    private final Class entityClass = ProductEntity.class;

    @Inject
    public ProductDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

    @Override
    public ProductEntity getProduct(String id) {
        ProductEntity entity = datastore.createQuery(ProductEntity.class).disableValidation().field("id").equal(id).get();
        return entity;
    }

    @Override
    public ProductEntity updateProduct(ProductEntity entity) {
        datastore.save(entity);
        return entity;
    }

    @Override
    public int removeProduct(String id) {
        WriteResult writeResult = datastore.delete(ProductEntity.class, id);
        return writeResult.getN();
    }
}
