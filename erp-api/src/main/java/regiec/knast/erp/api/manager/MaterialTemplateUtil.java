/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Map;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.MaterialEntitiesList;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;
import regiec.knast.erp.api.model.Constraint; 
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CriteriaFilter;
import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MaterialTemplateUtil {

    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private PartCardManager partCardManager;
    @Inject
    private MaterialUtil materialUtil;

    public Map<String, Object> buildRemoveConsequencesMap(MaterialTemplateEntity entity) {
        Map<String, Object> consequencesMap = ImmutableMap.<String, Object>builder()
                .put("entityType", entity.getClass().getSimpleName())
                .put("id", entity.getId())
                .put("materialCode", entity.getMaterialCode())
                .put("name", entity.getMaterialType().getName())
                .put("particularDimension", entity.getParticularDimension())
                .put("leafs", new ArrayList<>())
                .put("semiproductPartCard", new ArrayList<>())
                .put("productPartCard", new ArrayList<>())
                .build();
        final MaterialEntitiesList materialEntitiesList = getMaterialsWithMaterialTemplate(entity.getId());
        for (MaterialEntity materialEntity : materialEntitiesList) {
            Map<String, Object> materialConsequencesMap = materialUtil.buildRemoveConsequencesMap(materialEntity);
            ((ArrayList) consequencesMap.get("leafs")).add(materialConsequencesMap);
        }
        final ProductTemplateEntitiesList productTempaltesWithPartCards
                = partCardManager.getPartCardsCompatibileEntitiesWithElementFromCollection(
                        EntityType.MATERIAL_TEMPLATE,
                        entity.getId());
        for (ProductTemplateEntity productTempalteWithPartCards : productTempaltesWithPartCards) {
            ImmutableMap<String, Object> build = ImmutableMap.<String, Object>builder()
                    .put("entityType", "Lista Materiałowa" + ProductEntity.class.getSimpleName())
                    .put("id", productTempalteWithPartCards.getId())
                    .put("productNo", productTempalteWithPartCards.getProductNo())
                    .put("name", productTempalteWithPartCards.getName())
                    .build();
            ((ArrayList) consequencesMap.get("semiproductPartCard")).add(build);
        }
        return consequencesMap;
    }

    public MaterialEntitiesList getMaterialsWithMaterialTemplate(String materialTemplateEntityId) {
        PaginableFilterWithCriteriaRequest req = new PaginableFilterWithCriteriaRequest();
        CriteriaFilter criteriaFilter = new CriteriaFilter();
        req.setCriteriaFilters(criteriaFilter);
        criteriaFilter.setConstraints(Lists.newArrayList(new Constraint("materialTemplate.id", materialTemplateEntityId, ConstraintType.EQUALS)));
        return materialDAO.list(req);
    }
}
