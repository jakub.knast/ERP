/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.interfaces;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface RemovableEntity {

    public boolean getRemoved();

}
