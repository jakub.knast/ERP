/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.professionGroup.ProfessionGroupDTO;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.ProfessionGroupBase;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("professionGroups")
public class ProfessionGroupEntity extends ProfessionGroupBase implements EntityBase, ConvertableEntity<ProfessionGroupDTO> {

    @Id
    private String id;
    @Reference
    private List<OperationNameEntity> allowOperations = new ArrayList();

    public String createLabel() {
        return this.getCode() + "-" + this.getName();
    }

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public ProfessionGroupDTO convertToDTO() {
        try {
            ProfessionGroupDTO dto = (ProfessionGroupDTO) BeanUtilSilent.translateBeanAndReturn(this, getDtoClass().newInstance());
            dto.setAllowOperations(OperationNameEntity.entitiesToDtos(this.getAllowOperations()));
            return dto;
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ERPInternalError("cannot convert " + this.getClass().getCanonicalName() + "to DTO object", ex);
        }
    }
}
