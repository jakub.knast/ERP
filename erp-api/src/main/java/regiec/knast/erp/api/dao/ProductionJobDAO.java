/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.entities.containers.ProductionJobEntitiesList;
import regiec.knast.erp.api.dao.interfaces.ProductionJobDAOInterface;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductionJobDAO extends DAOBase<ProductionJobEntity, ProductionJobEntitiesList> implements ProductionJobDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = ProductionJobEntity.class;

    @Inject
    public ProductionJobDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

}
