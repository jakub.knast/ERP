/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.product;

import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class GenerateInvoicePdfRequest extends GenerateWzPdfRequest{

    public GenerateInvoicePdfRequest(String sellerId, String recipientId, List<String> productsIds) {
        super(sellerId, recipientId, productsIds);
    }

}
