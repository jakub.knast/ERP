/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.seenmodit.core.PositionManager;
import org.seenmodit.core.exceptions.model.PositionsChangesRet;
import regiec.knast.erp.api.dao.interfaces.ProviderDAOInterface;
import regiec.knast.erp.api.dto.*;
import regiec.knast.erp.api.dto.provider.*;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.manager.ProviderAssortmentManager;
import regiec.knast.erp.api.manager.ProviderAssortmentUtil;
import regiec.knast.erp.api.manager.ProviderManager;
import regiec.knast.erp.api.model.*;
import regiec.knast.erp.api.model.states.ProviderState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class ProviderService extends AbstractService<ProviderEntity, ProviderDTO> {

    @Inject
    private ProviderDAOInterface providerDAO;
    @Inject
    private Validator validator;
    @Inject
    private ProviderAssortmentManager providerAssortmentManager;
    @Inject
    private ProviderAssortmentUtil providerAssortmentUtil;
    @Inject
    private ProviderManager providerManager;
    private PositionManager positionManager = PositionManager.create(new IDUtil());

    @NetAPI
    public ProviderDTO addProvider(AddProviderRequest request) {
        validator.validateNotNull(request, ExceptionCodes.NULL_VALUE).throww();
        validator.validateNotNullAndNotEmpty(request.getName(), ExceptionCodes.NULL_VALUE, "name").throww();
        validator.validateNotNullAndNotEmpty(request.getNip(), ExceptionCodes.NULL_VALUE, "nip").throww();
        validator.validateUniqueness("name", request.getName(), null, EntityType.PROVIDER).throww();
        validator.validateUniqueness("nip", request.getNip(), null, EntityType.PROVIDER).throww();

        if (request.getAssortment() != null && request.getAssortment().getMaterialAssortment() != null) {
            //validate if all is ok
            providerAssortmentManager.validateMaterialAssortimentCorrectness(request.getAssortment(), true);
        }
        ProviderEntity entity = new ProviderEntity();
        BeanUtilSilent.translateBean(request, entity);
        entity.setState(ProviderState.NORMAL);
        ProviderEntity entity1 = providerDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public ProviderDTO getProvider(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public ProviderDTO updateProvider(UpdateProviderRequest request) {
        try {
            validator.validateNotNull(request, ExceptionCodes.NULL_VALUE).throww();
            validator.validateNotNullAndNotEmpty(request.getId(), ExceptionCodes.NULL_VALUE, "id").throww();
            validator.validateNotNullAndNotEmpty(request.getName(), ExceptionCodes.NULL_VALUE, "name").throww();
            validator.validateNotNullAndNotEmpty(request.getNip(), ExceptionCodes.NULL_VALUE, "nip").throww();
            validator.validateUniqueness("name", request.getName(), request.getId(), EntityType.PROVIDER).throww();
            validator.validateUniqueness("nip", request.getNip(), request.getId(), EntityType.PROVIDER).throww();

            String id = request.getId();
            ProviderEntity entity = providerDAO.get(id);
            validator.validateNotNull(entity).throww();            
            ProviderEntity updatedEntity = new ProviderEntity();
            BeanUtilSilent.translateBean(entity, updatedEntity);
            BeanUtilSilent.translateBean(request, updatedEntity);

            providerAssortmentUtil.ensureAssortmentIsNotNull(entity);
            providerAssortmentUtil.ensureAssortmentIsNotNull(request);

            ProviderAssortment currentAssortment = entity.getAssortment();
            ProviderAssortment toUpdateAssortment = request.getAssortment();

            // we revert translated assortment due to special hnandling this below
            updatedEntity.setAssortment(entity.getAssortment());

            providerAssortmentManager.validateMaterialAssortimentCorrectness(toUpdateAssortment, false);

            PositionsChangesRet<MaterialAssortmentItem, MaterialAssortmentItem> findAddedRemovedUpdatedAssortment
                    = positionManager.findAddedRemovedUpdated(currentAssortment.getMaterialAssortment(), toUpdateAssortment.getMaterialAssortment());
            // handle added, removed and rest of assortment
            providerAssortmentManager.handleUpdatedMaterialAssortment(findAddedRemovedUpdatedAssortment.getOther(), updatedEntity);
            providerAssortmentManager.handleAddedMaterialAssortment(findAddedRemovedUpdatedAssortment.getAdded(), updatedEntity);
            providerAssortmentManager.handleRemovedMaterialAssortment(findAddedRemovedUpdatedAssortment.getRemoved(), updatedEntity);

            updatedEntity.getAssortment().validateMe();
            providerDAO.update(updatedEntity);
            return updatedEntity.convertToDTO();
        } catch (Exception ex) {
            throw new ConversionError("", ex);
        }
    }

    @NetAPI
    public RemoveResponse removeProvider(RemoveProviderRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        ProviderEntity providerEntity = providerDAO.get(id);
        if (request.getConfirmed()) {
            providerEntity.setRemoved(true);
            providerDAO.update(providerEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(providerEntity));
            return new RemoveResponse(false, null);
        }
    }

    @NetAPI
    public ProviderDTO restoreProvider(RestoreRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        ProviderEntity providerEntity = providerDAO.get(id);
        if (providerEntity.getRemoved() == false) {
            throw new ConversionErrorWithCode("Restore is avaliable only if provider is removed", ExceptionCodes.CANNOT_RESTORE_PROVIDER_IN_THIS_STATE);
        }
        providerEntity.setState(ProviderState.NORMAL);
        providerDAO.update(providerEntity);
        return providerEntity.convertToDTO();
    }

    @NetAPI
    public ProvidersListDTO listProviders(PaginableFilterWithCriteriaRequest request) {
        return (ProvidersListDTO) super.list(request);
    }

    @NetAPI
    public FindProviderForMaterialResponse findProviderForMaterial(FindProviderForMaterialRequest request) {
        FindProviderForMaterialResponse ret = providerManager.findProviderForMaterial(request);
        return ret;
    }

    @NetAPI
    public FindOtherProviderForMaterialResponse findOtherProviderForMaterial(FindOtherProviderForMaterialRequest request) {
        FindOtherProviderForMaterialResponse ret = providerManager.findOtherProviderForMaterial(request);
        return ret;
    }

    @NetAPI
    public FindMaterialsFromProviderResponse findMaterialsFromProvider(FindMaterialsFromProviderRequest request) {
        FindMaterialsFromProviderResponse ret = providerManager.findMaterialsFromProvider(request);
        return ret;
    }

    @NetAPI
    public ProviderDTO addPositionToAssortment(AddPositionToProviderAssortmentRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getId(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "id").throww();
        validator.validateNotNullAndNotEmpty(request.getMaterialCode(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "materialCode").throww();
        validator.validateNotNull(request.getSalesLength(), ExceptionCodes.NULL_VALUE, "salesLength").throww();
        validator.validateNotNull(request.getSalesPrice(), ExceptionCodes.NULL_VALUE, "salesPrice").throww();
        validator.validateNotNull(request.getSellUnit(), ExceptionCodes.NULL_VALUE, "sellunit").throww();
        if (request.getSalesPrice().isNegativeOrZero()) {
            throw new ConversionErrorWithCode("price and length must be grather than 0", ExceptionCodes.NOT_IMPLEMENTED, null);
        }

        final String origSalesLength = request.getSalesLength();
        final MoneyERP salesPrice = request.getSalesPrice();
        final String materialCode = request.getMaterialCode();
        final SellUnit sellUnit = request.getSellUnit();

        ProviderEntity provider = providerDAO.get(request.getId());
        validator.validateNotNull(provider, "provider object").throww();

        MaterialTemplateEntity materialTemplate = providerAssortmentUtil.getMateriaLTemplateByMaterialCode(materialCode);
        Dimension cuttingDimension = materialTemplate.getMaterialType().getCuttingDimensionTemplate();

        // create cutting size for given string size
        CuttingSize salesLength = CuttingSizeManager.createCuttingSize(cuttingDimension, origSalesLength);
        providerAssortmentUtil.ensureAssortmentIsNotNull(provider);

        List<MaterialAssortmentItem> matAss = provider.getAssortment().getMaterialAssortment();

        MaterialAssortmentItem itemWhereToAdd = providerAssortmentUtil.findExistingMaterialAssortmentInProvider(matAss, materialCode);
        if (itemWhereToAdd != null) {
            providerAssortmentUtil.addMaterialAssortmentPositionToAssortmentItem(itemWhereToAdd, salesLength, salesPrice, sellUnit);
        } else {
            //there is no item with that materialCode
            itemWhereToAdd = providerAssortmentUtil.createNewMaterialAssortment(salesLength, salesPrice, materialTemplate, materialCode, sellUnit);
            provider.getAssortment().getMaterialAssortment().add(itemWhereToAdd);
        }

        ProviderEntity updated = providerDAO.update(provider);
        return updated.convertToDTO();
    }
}
