/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.material.AddMaterialRequest;
import regiec.knast.erp.api.dto.material.MaterialDTO;
import regiec.knast.erp.api.dto.material.MaterialListDTO;
import regiec.knast.erp.api.dto.material.RemoveMaterialRequest;
import regiec.knast.erp.api.dto.material.UpdateMaterialRequest;
import regiec.knast.erp.api.dto.otherstuff.ProgramConstsDTO;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.exceptions.RemoveEntityException;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.manager.MaterialUtil;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.states.MaterialState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.ERPSafetyCheck;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MaterialService extends AbstractService<MaterialEntity, MaterialDTO> {

    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private MaterialUtil materialUtil;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private Validator validator;

    @NetAPI
    public MaterialDTO addMaterial(AddMaterialRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getMaterialTemplateId(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "materialTemplateId").throww();
        validator.validateNotNull(request.getLength(), ExceptionCodes.NULL_VALUE, "length").throww();
        validator.validateNotNullAndNotEmpty(request.getLength().getOrigSize(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "length.origSize").throww();
        validator.validateNotNull(request.getCount(), ExceptionCodes.NULL_VALUE, "count").throww();
        validator.validateNotNull(request.getCreationContext(), ExceptionCodes.NULL_VALUE, "creationContext").throww();

        MaterialEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new MaterialEntity());
        switch (request.getCreationContext()) {
            case MANUAL: {
                entity.setState(MaterialState.ADDED_MANUALY);
                break;
            }
            case SHOPPING_CART: {
                entity.setState(MaterialState.IN_SHOPPING_CART);
                break;
            }
        }
        MaterialTemplateEntity materialTemplate = materialTemplateDAO.get(request.getMaterialTemplateId());
        validator.validateNotNull(materialTemplate, ExceptionCodes.NULL_VALUE, "materialTemplate").throww();
        validator.validateNotRemoved(materialTemplate).throww(); 
        
        entity.setMaterialTemplate(materialTemplate); 
        Dimension cuttingDimension = materialTemplate.getMaterialType().getCuttingDimensionTemplate();

        CuttingSize length = CuttingSizeManager.createCuttingSize(cuttingDimension, request.getLength().getOrigSize());

        entity.setLength(length);
        entity.setLeftLength(length);
        entity.setReservedLength(CuttingSizeManager.createEmptyCuttingSize(cuttingDimension));
//            entity.setTotalLength(length.multiply(entity.getCount()));
        MaterialEntity entity1 = materialDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public MaterialDTO getMaterial(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public MaterialDTO updateMaterial(UpdateMaterialRequest request) {
        validator.validateNotNull(request, ExceptionCodes.NULL_VALUE, "UpdateMaterialRequest").throww();
        validator.validateNotNullAndNotEmpty(request.getMaterialTemplateId(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "materialTemplateId").throww();
        validator.validateNotNull(request.getLength(), ExceptionCodes.NULL_VALUE, "length").throww();
        validator.validateNotNullAndNotEmpty(request.getLength().getOrigSize(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "length.origSize").throww();
        validator.validateNotNull(request.getCount(), ExceptionCodes.NULL_VALUE, "count").throww();

        String id = request.getId();
        MaterialEntity entity = materialDAO.get(id);
        MaterialEntity updatedEntity = new MaterialEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);
        if (entity.getState() != MaterialState.ADDED_MANUALY) {
            throw new ConversionErrorWithCode("You can update materials if are in state 'Added_manualy'", ExceptionCodes.CANNOT_UPDATE_MATERIAL_IN_THIS_STATE);
        }

        Dimension cuttingDimension = updatedEntity.getMaterialTemplate().getMaterialType().getCuttingDimensionTemplate();
        CuttingSize length = CuttingSizeManager.createCuttingSize(cuttingDimension, request.getLength().getOrigSize());

        updatedEntity.setLength(length);
        updatedEntity.setLeftLength(length);
        updatedEntity.setReservedLength(CuttingSizeManager.createEmptyCuttingSize(cuttingDimension));
//            updatedEntity.setTotalLength(length.multiply(updatedEntity.getCount()));
        materialDAO.update(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeMaterial(RemoveMaterialRequest request) {
        String id = request.getId();
        MaterialEntity entity = materialDAO.get(id);
//        if (request.getConfirmed()) {
        ERPSafetyCheck safetyCheck = new ERPSafetyCheck();
        if (safetyCheck.checkIfEntityIsReferenced(MaterialEntity.class, id)) {
            throw new RemoveEntityException("Cannot remove Material, its used somewhere");
        }
        if (!entity.getRemoved()) {
            entity.setRemoved(true);
            materialDAO.update(entity);
        } else {
            int remove = materialDAO.remove(id);
        }
        return new RemoveResponse(true, null);

//        } else {
//            return new RemoveResponse(false, materialUtil.buildRemoveConsequencesMap(entity));
//        }
    }

    @NetAPI(description = "If you left id empty, you will get all materials")
    public MaterialListDTO listMaterials(PaginableFilterWithCriteriaRequest request) {
        return (MaterialListDTO) super.list(request);
    }

    @NetAPI
    public ProgramConstsDTO getProgramConts() {
        return new ProgramConstsDTO();
    }
}
