/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import regiec.knast.erp.api.dto.waretemplates.WareTemplateDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WarePartDTO extends WarePartBase {

    private String positionId;
    private WareTemplateDTO wareTemplate;
    private String wareTemplateId;
}
