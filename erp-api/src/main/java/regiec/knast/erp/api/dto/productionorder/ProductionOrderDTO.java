/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.productionorder;

import java.util.List;
import regiec.knast.erp.api.dto.product.ProductDTO;
import regiec.knast.erp.api.dto.productionjob.ProductionJobDTO;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.model.bases.ProductionOrderBase;
import regiec.knast.erp.api.model.states.ProductionOrderState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductionOrderDTO extends ProductionOrderBase {

    private String id;

    private ProductDTO product;
    private List<ProductionJobDTO> productionJobs;    
    private List<MaterialDemandEntity> materialDemands;
    private ProductionOrderState state;
    private boolean removed;

}
