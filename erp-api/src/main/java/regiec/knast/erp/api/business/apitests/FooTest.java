/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.inject.Inject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.dao.interfaces.OrderDAOInterface;
import regiec.knast.erp.api.entities.OrderEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class FooTest extends GuiceInjector {

    @Inject
    private OrderDAOInterface orderDAO;

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(FooTest.class);
        FooTest test = (FooTest) create;
        try {
            test.foo();
        } catch (Exception ex) {
            Logger.getLogger(FooTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            test.cleanUp();
        }
    }

    public void foo() {
        orderDAO = super.buildObj(OrderDAOInterface.class);
        OrderEntity get = orderDAO.get("5b8cc48546fbed08747e4eed");
        super.printResult(get);
    }
    
}
