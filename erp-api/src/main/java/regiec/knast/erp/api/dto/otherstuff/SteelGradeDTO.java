/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.otherstuff;

import java.util.Set;
import java.util.TreeSet;
import regiec.knast.erp.api.model.SteelGrade;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class SteelGradeDTO extends TreeSet<String> {

    public static SteelGradeDTO fromNormSet(Set<SteelGrade> steelGrades) {
        SteelGradeDTO ret = new SteelGradeDTO();
        for (SteelGrade steelGrade : steelGrades) {
            ret.add(steelGrade.toString());
        }
        return ret;
    }

}
