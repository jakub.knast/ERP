/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import com.mongodb.DBRef;
import java.util.List;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ProfessionGroupDAOInterface;
import regiec.knast.erp.api.entities.ProfessionGroupEntity;
import regiec.knast.erp.api.entities.containers.ProfessionGroupEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProfessionGroupDAO extends DAOBase<ProfessionGroupEntity, ProfessionGroupEntitiesList> implements ProfessionGroupDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = ProfessionGroupEntity.class;

    @Inject
    public ProfessionGroupDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }


    @Override
    public ProfessionGroupEntitiesList listProfessionGroupsWithOperationId(String operationNameId) {

        List<ProfessionGroupEntity> list = datastore.createQuery(ProfessionGroupEntity.class)
                .disableValidation()
                .field("allowOperations").hasThisOne(new DBRef("operationNames", operationNameId))
                .asList();
        ProfessionGroupEntitiesList ret = new ProfessionGroupEntitiesList();
        ret.addAll(list);
        ret.setTotalSize(list.size());
        return ret;
    }

}
