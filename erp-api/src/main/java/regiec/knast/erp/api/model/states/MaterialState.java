/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.states;

import regiec.knast.erp.api.model.colors.StatesColors;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public enum MaterialState implements StateInterface {
    ORDERED("Zamówiony", StatesColors.ORDERED_COLOR),
    ORDERED_RESERVED("Zarezerwowany, zamówiony", StatesColors.ORDERED_COLOR),
    //
    ADDED_MANUALY("Dodany ręcznie", StatesColors.ADDED_MANUALY_COLOR, true),
    ADDED_MANUALY_RESERVED("Zarezerwowany dodany ręcznie", StatesColors.RESERVED_COLOR),
    //
    IN_SHOPPING_CART("W koszyku", StatesColors.IN_SHOPPING_CART_COLOR),
    IN_SHOPPING_CART_RESERVED("Zarezerwowany w koszyku", StatesColors.RESERVED_COLOR),
    //
    ON_STOCK_AVALIABLE("Dostępny na magazynie", StatesColors.ON_STOCK_AVALIABLE_COLOR),
    ON_STOCK_RESERVED("Zarezerwowany na magazynie", StatesColors.RESERVED_COLOR),
    //
    IN_PRODUCTION("W produkcji", StatesColors.IN_PRODUCTION_COLOR),
    DONE("Wykorzystany", StatesColors.DONE_COLOR),
    CANCELED_FROM_ORDER("Skasowany z zamówienia", StatesColors.CANCELED_FROM_ORDER_COLOR),
    CANCELED_FROM_PRODUCTION("Skasowany z produkcji", StatesColors.CANCELED_FROM_PRODUCTION_COLOR),
    BROKEN_ON_STOCK("Zniszczony na magazynie", StatesColors.BROKEN_ON_STOCK_COLOR),
    BROKEN_ON_PRODUCTION("Zniszczony w produkcji", StatesColors.BROKEN_ON_PRODUCTION_COLOR),
    INITIAL("INITIAL", StatesColors.ERROR_COLOR),
    ERROR("ERROR", StatesColors.ERROR_COLOR);

    private MaterialState(String translation, StatesColors stateColor) {
        this(translation, stateColor, false);
    }

    private MaterialState(String translation, StatesColors stateColor, boolean removable) {
        this.translation = translation;
        this.backgroundColor = stateColor.getBackgroundColor();
        this.fontColor = stateColor.getFontColor();
        this.removable = removable;
    }

    private String translation;
    private String backgroundColor;
    private String fontColor;
    private boolean removable;

    @Override
    public boolean isRemovable() {
        return removable;
    }

    @Override
    public String getTranslation() {
        return translation;
    }

    @Override
    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public String getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public String getFontColor() {
        return fontColor;
    }

    @Override
    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }
}
