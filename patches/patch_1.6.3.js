
// --------------------------------------------------
// Drop collections
db.orders.drop()
db.productionOrders.drop()
db.productionJobs.drop()
db.products.drop()
db.materials.drop()
db.materialDemands.drop()
db.deliveryOrders.drop()
db.shoppingCarts.drop()


// --------------------------------------------------
// coś tam

var pgMap = {}
db.professionGroups.find({}).forEach(function(pg){
	pgMap[pg.code] = pg
})
//printjson(pgMap)

db.workers.find({}).forEach(function(w){
	var pgg = pgMap[w.professionGroup.code]
//	printjson(w.professionGroup)
	print(pgg._id)
	w.professionGroup = DBRef("professionGroups", pgg._id)

	db.workers.save(w)	
})



// update missing sale type
db.productTemplates.find({ "type": { $exists: false }, "salesOffers": { $exists: true } }).forEach(function(pt){
	pt.type = "SALE"
	db.productTemplates.save(pt)
})

db.productTemplates.find({ "type": { $exists: false }, "salesOffers": { $exists: false } }).forEach(function(pt){
	pt.type = "NO_SALE"
	db.productTemplates.save(pt)
})



// --------------------------------------------------
// Fix money in providers

function digMatPos(p){
 p.salesPrice = {"currency" : "PLN", "amount" : NumberDecimal(p.salesPrice)}
 printjson(p)
}

function digMat(p){
 if(p.materialAssortmentPositions){
    p.materialAssortmentPositions.forEach(digMatPos)
 }
}


function digProvider(p){
 if(p.assortment && p.assortment.materialAssortment){
    p.assortment.materialAssortment.forEach(digMat)
 	db.providers.save(p)
 }
}

db.providers.find({}).forEach(digProvider)

// --------------------------------------------------
// Fix money in productTemplates

function fixSaleOffer(p){
 if(p.price && typeof(p.price) !== "object"){
 	p.price = {"currency" : "PLN", "amount" : NumberDecimal(p.price)}
 	printjson(p)
 }
}

function digProd(p){
 if(p.salesOffers){
    p.salesOffers.forEach(fixSaleOffer)
 	db.productTemplates.save(p)
 }
}
db.productTemplates.find({}).forEach(digProd)

// --------------------------------------------------
// Fix removed REMOVED states

var arr = [];
function p(o){
 	arr.push(o) 
}
db.materials.find({state : "REMOVED"}).forEach(function(o){
	o.state = "NORMAL"
	o.removed = true;
	p("material: " + o._id)
	db.materials.deleteOne({_id : o._id})
})

function setRemoved(dbName, state){
 	db.getCollection(dbName).find({state : "REMOVED"}).forEach(function(o){
	printjson(o)
		o.state = state
		o.removed = true;
		p(dbName + " : " + o._id)
		db.getCollection(dbName).save(o);
 	})
 	p("")
}
setRemoved("materialTemplates", "NORMAL")
setRemoved("materialTypes", "NORMAL")
setRemoved("productTemplates", "NORMAL")
setRemoved("wareTemplates", "NORMAL")
setRemoved("wareTypes", "NORMAL")
setRemoved("operationNames", "NORMAL")
setRemoved("orders", "NORMAL")
setRemoved("professionGroups", "NORMAL")
setRemoved("providers", "NORMAL")
setRemoved("recipients", "NORMAL")
setRemoved("sellers", "NORMAL")
setRemoved("machiness", "NORMAL")
setRemoved("shoppingCarts", "NORMAL")
setRemoved("workers", "FIRED")


arr.forEach(function(o){
	print(o)
})

// --------------------------------------------------
// set not removed entities to "removed" : false

var cols = [
    "machiness", 
    "materialDemands", 
    "materialTemplates", 
    "materialTypes", 
    "materials", 
    "operationNames", 
    "orders", 
    "productTemplates", 
    "productionOrders", 
    "products", 
    "professionGroups", 
    "providers", 
    "recipients", 
    "resourceInfos", 
    "sellers", 
    "shoppingCarts", 
    "todoIssuess", 
    "wareTemplates", 
    "wareTypes", 
    "workers"
]

var addRemove = function(name){
	db.getCollection(name).update(
	{ "removed": { $ne: true } },
	{ $set: {
       removed: false
     }},
	{multi : true})
}
cols.forEach(addRemove)

