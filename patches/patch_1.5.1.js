
//odkomentowa� dodawanie operation names i professiongroups
//------------------===================------------------------
// add operationNames to program consts
var onIds = {
	"Ci�cie": ObjectId().str,
	"Wypalanie": ObjectId().str,
	"Wiercenie": ObjectId().str,
	"Toczenie": ObjectId().str,
	"Frezowanie": ObjectId().str,
	"Gratowanie": ObjectId().str,
	"Gwintowanie": ObjectId().str,
	"Montowanie": ObjectId().str,
	"Szlifowanie": ObjectId().str,
	"Spawanie": ObjectId().str,
	"Wykrawanie": ObjectId().str,
	"Gi�cie": ObjectId().str,
	"Gi�cie_r�czne": ObjectId().str,
	"Prostowanie": ObjectId().str,
	"Sczepianie": ObjectId().str,
	"Malowanie": ObjectId().str,
	"Oklejanie_detali": ObjectId().str,
	"Mycie": ObjectId().str,
	"Przegwintowanie": ObjectId().str,
	"Transport": ObjectId().str
}

var onsNames = {}
Object.keys(onIds).forEach(function(o){
	var on = {"_id" : onIds[o], "name" : o, "state" : "NORMAL"};
	onsNames[o] = on
	db.operationNames.save(on)	
})

function getONreference(id){
	return DBRef("operationNames", id)
}
function getPGreference(id){
	return DBRef("professionGroups", id)
}

//------------------===================------------------------
// add professionGroups
var pgsNames = {}
var pgsCodes = {}

var c = db.programConsts.findOne({})
var pgsConsts = c.professionGroup
pgsConsts.forEach(function(pg){
	var o = {"_id" : ObjectId().str, "name" : pg.name, "code" : pg.code, "label" : pg.label, "allowOperations" : []}
	pg.allowOperations = pg.allowOperations.map(function(ao){
	  	o.allowOperations.push(getONreference(onIds[ao]))
	})
	pgsNames[pg.name] = o
	pgsCodes[pg.code] = o
	//printjson(o.allowOperations)	
})

printjson(pgsNames)

Object.keys(pgsNames).forEach(function(o){	
	db.professionGroups.save(pgsNames[o])	
	//printjson(pgsNames[o])
})

//------------------===================------------------------

function doo(pp, id){
	if(!pp.professionGroup){
		print("!!!!!! --- nie ma profession group w productTemplate " + id)
	}else{
		var pgCode = pp.professionGroup.code
		pp.professionGroup = getPGreference(pgsCodes[pgCode]._id)
	}
	if(!pp.operationName){
		print("!!!!!! --- nie ma operationName w productTemplate " + id)
	}else{
		var ao = pp.operationName
		pp.operationName = getONreference(onIds[ao])
	}
}

//------------------===================------------------------
//update all productsTemplates and semiproductsTemplates productonplans

db.productTemplates.find({ "productionPlan": { $exists: true } }).forEach(function(pt){
	pt.productionPlan.forEach(function(pp){
		doo(pp, pt._id)	  
	})
	db.productTemplates.save(pt)
	//print(pt._id)
	//printjson(pt.productionPlan)
})
print("ok in productTemplates")

//------------------===================------------------------

db.semiproductTemplates.find({ "productionPlan": { $exists: true } }).forEach(function(pt){
	pt.productionPlan.forEach(function(pp){
		doo(pp, pt._id)
	})
	db.semiproductTemplates.save(pt)
	//print(pt._id)
	//printjson(pt.productionPlan)
})
print("ok in semiproductTemplates")

//------------------===================------------------------
//------------------===================------------------------
//------------------===================------------------------
//------------------===================------------------------

//update all products and semiproducts productonplans
db.products.find({}).forEach(function(p){
	if(p.productTemplate.productionPlan){
		p.productTemplate.productionPlan.forEach(function(pp){
			doo(pp, p._id)
		})
		db.products.save(p)
		//print(p._id)
		//printjson(p.productionPlan)
	}
})
print("ok in product")

db.semiproducts.find({}).forEach(function(p){
	if(p.semiproductTemplate.productionPlan){
		p.semiproductTemplate.productionPlan.forEach(function(pp){
			doo(pp, p._id)
		})
		db.semiproducts.save(p)
		//print(p._id)
		//printjson(p.productionPlan)
	}
})
print("ok in semiproduct")

//------------------===================------------------------
//------------------===================------------------------
//------------------===================------------------------

//------------------===================------------------------

//update all MaterialDemands productonplans
db.materialDemands.find({}).forEach(function(md){
	if(md.detalTemplate.productionPlan){
		md.detalTemplate.productionPlan.forEach(function(pp){
			doo(pp, md._id)
		})
		db.materialDemands.save(md)
		//print(md._id)
		//printjson(md.productionPlan)
	}
})
print("ok in materialDemands")

