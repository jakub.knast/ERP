
var dryRun = false
var totalSize = 0

function printUsageStats(){
	var size = 0
	var hm = 0
	var hmch = 0
	db.fs.files.find({}).forEach(function(f){
		size = size + f.length
		hm = hm +1
	})

	db.fs.chunks.find({}).forEach(function(ch){
		hmch = hmch +1  
	})

	print("total size: " + (size / 100000) + "mb" + "\n" + 
	 "there is " + hm + " files" +  "\n" + 
	"there is " + hmch + " chunks")
}
function getFile(r){
	return db.fs.files.findOne({_id : new ObjectId(r.resourceId)})
}

function find(pt, rid){
	equals = pt.techDrawingPDF.$id === rid
	//print(pt.techDrawingPDF.$id + " " + rid +  " equal? " + equals)
	if(!equals && pt.techDrawingDXF){		 
		equals = pt.techDrawingDXF.$id ===rid
	//	print(pt.techDrawingDXF.$id + " " + rid +  " equal? " + equals)
			if(!equals && pt.reportTemplate){		 
				equals = pt.reportTemplate.$id === rid
				//print(pt.reportTemplate.$id + " " + rid +  " equal? " + equals) 
			}
	}
	return equals
}

function findUnusedResources(){
	var unusedRes = []
	db.resourceInfos.find({}).forEach(function(r){
		var rid = r._id;
		if(!r.loobackId){
			print("loopback id nie istnieje")
			return;
		}
		var equals = false
		var pt = db.productTemplates.findOne({_id : r.loobackId})
		if(pt){
			equals = find(pt, rid)
			if(!equals){
				print("nie ma pokrycia w product "+r._id+ " name: " + r.resourceName + " in PT and SPT, resourceId: " + r.resourceId)
				totalSize = totalSize + getFile(r).length
				unusedRes.push(r)
			}
		}else {
			pt = db.semiproductTemplates.findOne({_id : r.loobackId})
			if(pt){
				equals = find(pt, rid)
				if(!equals){
					print("nie ma pokrycia w semiproduct " + r._id + " name: " + r.resourceName + " in PT and SPT, resourceId: " + r.resourceId)
					totalSize = totalSize + getFile(r).length
				unusedRes.push(r)
				}	
			}
		}
		if(!pt){
		//	print("there is no "+r._id+ " name: " + r.resourceName + " in PT and SPT, resourceId: " + r.resourceId)		
			totalSize = totalSize + getFile(r).length
			unusedRes.push(r)
		}			
	})
	return unusedRes
}
function main(){
	
	printUsageStats()
	var unusedRes = findUnusedResources()
	printjson(unusedRes)
	print("to clean: " + (totalSize /1000000)  + "mb")
	
	if(!dryRun){
		unusedRes.forEach(function(r) {
			print("removing resource... " + r.resourceId)
			var chunks = db.fs.chunks.find({files_id:ObjectId(r.resourceId)}).toArray()
			chunks.forEach(function(ch){
				//printjson(ch)
				db.fs.chunks.remove({_id : ch._id})
			})
			db.fs.files.remove({_id : ObjectId(r.resourceId)})
			db.resourceInfos.remove({_id : r._id})
		})	
	}	
	printUsageStats()	
	
	var orphanedChunks = []
	
	var chunks = db.fs.chunks.find({}).toArray()
		chunks.forEach(function(ch){
		 var o = ch.files_id
		var f = db.fs.files.find({_id : ch.files_id}).toArray()
		if(f.length === 0){
		  orphanedChunks.push(ch._id)
		}
	})
	print("Ilość osieroconych chunków: " + orphanedChunks.length)
	orphanedChunks.forEach(function(chid){
		db.fs.chunks.remove({_id : chid})
	})
	printUsageStats()	
}
main()

	  






