
var change = function(mt, field){  
	if(typeof(mt[field]) === "string"){		
		var tmp = mt[field];
		mt[field] = {}
		mt[field][field] = tmp
		print('changed ' + field)
	}
}
print("materialTemplates: ")
var materialTemplates = db.materialTemplates.find({})
materialTemplates.forEach(function(mt){
	change(mt, "materialKind")
	change(mt, "tolerance")
	change(mt, "steelGrade")
	change(mt, "norm")	
	  db.materialTemplates.save(mt)
})

print("wareTemplates: ")
var wareTemplates = db.wareTemplates.find({})
wareTemplates.forEach(function(mt){
	change(mt, "materialKind")
	change(mt, "tolerance")
	change(mt, "steelGrade")
	change(mt, "norm")	
	  db.wareTemplates.save(mt)
})

print("materials: ")
var materials = db.materials.find({})
materials.forEach(function(m){
  if(m.materialTemplate){
    var mt = m.materialTemplate;
	change(mt, "materialKind")
	change(mt, "tolerance")
	change(mt, "steelGrade")
	change(mt, "norm")    
  }
  db.materials.save(m)
})