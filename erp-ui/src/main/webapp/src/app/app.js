'use strict';

angular.module('JangraERP', [
    'ngAnimate',
    'ngMessages',
    'ngSanitize',
    'ui.bootstrap',
    'ui.router',
    'toastr',
    'ui.slimscroll',
    'cgBusy',
    'ui.mask', //now not usage
    'ui-select-infinity',
    'ui.sortable',
    'formly',
    'minicolors',
    'daterangepicker',
    'bcPhoneNumber',
    'pascalprecht.translate',
    'ui.router.stateHelper',
    'ui.bootstrap.contextMenu',
    'textAngular',

    'JangraERP.core',
    'JangraERP.theme',
    'JangraERP.pages'
]);
