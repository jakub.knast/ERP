/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec 2017
 */
(function () {
    angular
        .module('JangraERP.pages.other.shoppingCart')
        .service('ShoppingCartModel', ShoppingCartModel);

    /** @ngInject */
    function ShoppingCartModel(AlertService, ShoppingCartRequestService) {
        class ShoppingCartSClass {
            constructor() {
                this.id = 'shoppingCartSingleton';
                this.positions = [];
                this.rService = ShoppingCartRequestService
            }

            getShoppingCart() {
                return this.rService.getShoppingCart()
                    .then(responseData => {
                        this.positions.length = 0;
                        Array.prototype.push.apply(this.positions, responseData.positions);
                        return this;
                    });
            };

            addPosition(positionData) {
                return this.rService.addPositionToShoppingCart(positionData)
                    .then(responseData => {
                        AlertService.success(this.rService.name + '.alert.addPosition');
                        this.positions.length = 0;
                        Array.prototype.push.apply(this.positions, responseData.positions);
                        return this;
                    });
            }

            updatePosition(positionData) {
                return this.rService.updatePositionInShoppingCart(positionData)
                    .then(responseData => {
                        AlertService.success(this.rService.name + '.alert.update');
                        this.positions.length = 0;
                        Array.prototype.push.apply(this.positions, responseData.positions);
                        return this;
                    });
            };

            finish() {
                return this.rService.finishShoppingCart()
                    .then(responseData => {
                        AlertService.success(this.rService.name + '.alert.finish');
                        this.positions.length = 0;
                        Array.prototype.push.apply(this.positions, responseData.positions);
                        return this;
                    });
            };
        }

        return new ShoppingCartSClass();
    }
})();
