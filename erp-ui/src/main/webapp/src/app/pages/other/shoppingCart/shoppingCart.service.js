/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.other.shoppingCart')
        .service('ShoppingCartRequestService', ShoppingCartRequestService);

    /** @ngInject */
    function ShoppingCartRequestService(requestHandler) {
        class ShoppingCartRequestClass {
            constructor() {
                this.name = 'ShoppingCart';
                this.subName = 'shoppingCart';
                this.requestHandler = requestHandler
            }

            getShoppingCart() {
                return this.requestHandler.send({
                    method: 'getShoppingCart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.' + this.subName + '.Get' + this.name + 'Request'
                });
            };

            updateShoppingCart(data, isFormData) {
                return this.requestHandler.send({
                    method: 'updateShoppingCart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.' + this.subName + '.Update' + this.name + 'Request',
                    data: data
                }, isFormData);
            };

            addPositionToShoppingCart(data, isFormData) {
                return this.requestHandler.send({
                    method: 'addPositionToShoppingCart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.' + this.subName + '.AddPositionTo' + this.name + 'Request',
                    data: {shoppingCartPosition: data}
                }, isFormData);
            };

            updatePositionInShoppingCart(data, isFormData) {
                return this.requestHandler.send({
                    method: 'updatePositionInShoppingCart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.' + this.subName + '.UpdatePositionIn' + this.name + 'Request',
                    data: data
                }, isFormData);
            };

            finishShoppingCart() {
                return this.requestHandler.send({
                    method: 'finishShoppingCart',
                    service: this.name + 'Service'
                });
            };
        }

        return new ShoppingCartRequestClass();
    }
})();
