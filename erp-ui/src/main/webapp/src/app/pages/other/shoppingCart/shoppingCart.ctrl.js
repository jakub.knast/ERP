/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class ShoppingCartCtrl {
        constructor(shoppingCartModel) {
            this.title = 'ShoppingCartCtrl - Development'
        }
    }

    angular
        .module('JangraERP.pages.other.shoppingCart')
        .controller('ShoppingCartCtrl', ShoppingCartCtrl);
})();
