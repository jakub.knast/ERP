/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.production.deliveryOrder')
        .factory('DeliveryOrderModel', DeliveryOrderModel)
        .factory('DeliveryOrderListModel', DeliveryOrderListModel);

    /** @ngInject */
    function DeliveryOrderModel(BaseModelClass, DeliveryOrderRequestService) {
        return class DeliveryOrderClass extends BaseModelClass {
            constructor(deliveryOrderData) {
                super(DeliveryOrderRequestService, deliveryOrderData);
            }

            confirmPosition(positionId) {
                return this.runServiceFunc('confirmPosition', {deliveryOrderId: this.data.id, positionId : positionId}, {message: 'DeliveryOrder.alert.confirmPosition', update: true});
            };
            
            findOtherProviderForMaterial(materialTemplateId, assortmentId, salesLengthOrig) {
                return this.runServiceFunc('findOtherProviderForMaterial', {
                    materialTemplateId: materialTemplateId,
                    assortmentId: assortmentId,
                    salesLengthOrig: salesLengthOrig
                }, {update: false});
            };

            moveDeliveryOrderPositionToOtherProvider(positionId, otherAssotrmentId, otherProviderId) {
                return this.runServiceFunc('moveDeliveryOrderPositionToOtherProvider', {
                    deliveryOrderId: this.data.id,
                    positionId: positionId,
                    otherAssotrmentId: otherAssotrmentId,
                    otherProviderId: otherProviderId
                }, {message: 'DeliveryOrder.alert.moveDeliveryOrderPositionToOtherProvider', update:true});
            };

            static getById(deliveryOrderId) {
                return new DeliveryOrderClass().getById(deliveryOrderId);
            };
        };
    }

    /** @ngInject */
    function DeliveryOrderListModel(ListModelClass, DeliveryOrderRequestService, DeliveryOrderModel) {
        return class DeliveryOrdersListClass extends ListModelClass {
            constructor() {
                super(DeliveryOrderRequestService, DeliveryOrderModel);
            }

            static list() {
                return new DeliveryOrdersListClass().refresh();
            }
        };
    }
})();
