/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.production.deliveryOrder')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(nestDataListConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultControls('deliveryOrderControls', {
            leftControl: '',
            rightControl: '<button type=button class="btn-with-icon btn btn-default" ng-click="cleanSearchModel()"><i class=ion-code></i>Wyczyść filtry</button>',
            menu: [{
                html: '<a class="btn-with-icon" href ng-click="$parent.$ctrl.modalPreview(item)"><i class=ion-eye></i>Podgląd</a>',
                compile: true
            }, {
                html: '<a class=btn-with-icon href ui-sref="^.element.detail({id: item.data.id})"><i class=ion-edit></i>Edytuj</a>',
                compile: true
            },
                null,
                {
                    html: '<a class=btn-with-icon href><i class=ion-checkmark></i>Potwierdź</a>',
                    click: (item, scope) => scope.$ctrl.confirmPosition(item)
                }, {
                    html: '<a class=btn-with-icon href><i class=ion-clipboard></i>Pokaż treść</a>',
                    click: (item, scope) => scope.$ctrl.showTextModal(item)
                }]
        });

        ListConfigServiceProvider.addDefaultConfig('DeliveryOrder',
            {
                vm: '$ctrl',
                id: 'DeliveryOrder',
                options: {selectable: true},
                controls: {name: 'deliveryOrderControls'},
                defaultColumns: ['no', 'providerLink', 'deliveryOrderPositions', 'DELIVERY_ORDER_STATE', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'production.deliveryOrder'},
                    no: null,
                    providerLink: {
                        id: 'providerLink',
                        key: 'providerName',
                        templateOptions: {
                            label: 'Dostawca',
                            placeholder: 'Dostawca'
                        },
                        tbody: '<td><a ui-sref="contractors.provider.element.detail({ id: item.data.providerId })">{{::item.data.providerName}}</a></td>',
                        search: '<th><text-filter filter="model.filter" key="providerName" on-change="changeFilter()"></text-filter></th>'
                    },
                    deliveryOrderPositions: null,
                    DELIVERY_ORDER_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateDeliveryOrder', 'UpdateDeliveryOrder'],
            {
                id: 'DeliveryOrder',
                options: {},
                fields: [
                    {id: 'providerId', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['CreateDeliveryOrderPositions', 'UpdateDeliveryOrderPositions'],
            {
                id: 'DeliveryOrderPositions',
                options: {},
                fields: [
                    {id: 'materialTemplateId', config: {templateOptions: {required: true}}},
                    {id: 'count', config: {templateOptions: {required: true}}}
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('DeliveryOrder',
            {
                id: 'DeliveryOrder',
                options: {},
                availableFields: {
                    no: null,
                    positionsStr: null,
                    DELIVERY_ORDER_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function nestDataListConfig(NestDataListConfigServiceProvider) {
        NestDataListConfigServiceProvider.addDefaultConfig('DeliveryOrderPositions',
            {
                id: 'DeliveryOrderPositions',
                options: {
                    path: 'positions',
                    formlyConfig: 'DeliveryOrderPositions',
                    customTemplateUrl: 'app/pages/production/deliveryOrder/deliveryOrderPositions/deliveryOrderPositions.html'
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'production.deliveryOrder',
                abstract: true,
                url: '/zamowieniaZewnetrzne',
                title: 'Zamówienia zewnętrzne',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 100
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista zamówień zewnętrznych',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "DeliveryOrderListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (DeliveryOrderListModel) {
                                return DeliveryOrderListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'DeliveryOrder',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Zamówienie zewnętrzne',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function (params) {
                            return 'Zamówienie zawnętrzne';
                        },
                        resolve: {
                            elementModel: function ($stateParams, DeliveryOrderModel) {
                                return DeliveryOrderModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            },
                            {
                                name: 'positions',
                                url: '/pozycja',
                                title: 'Pozycje',
                                templateUrl: 'app/core/components/nestDataList/nestDataList.base.html',
                                controller: 'BaseNestDataListCtrl',
                                controllerAs: "$ctrl",
                                breadcrumb: function () {
                                    return 'Pozycje na zamówieniu zewnętrznym';
                                },
                                resolve: {
                                    configName: function () {
                                        return 'DeliveryOrderPositions';
                                    }
                                }
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/zakupy/zamowieniaZewnetrzne', '/zakupy/zamowieniaZewnetrzne/lista');
    }
})();
