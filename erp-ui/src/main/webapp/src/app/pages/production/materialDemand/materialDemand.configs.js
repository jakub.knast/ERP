/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.production.materialDemand')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {

        ListConfigServiceProvider.addDefaultControls('materialDemandControls', {
            leftControl: '',
            rightControl:
                `<div class=btn-group dropdown-append-to-body uib-dropdown>
                    <button type=button class="btn-with-icon btn btn-default" ng-click="cleanSearchModel()"><i class=ion-code></i>Wyczyść filtry</button>
                    <button class="btn btn-default" type=button uib-dropdown-toggle><span class=caret></span></button>
                    <ul uib-dropdown-menu class="dropdown-menu-right">
                        <li><a class=btn-with-icon href ng-click="$ctrl.print()"><i class=ion-printer></i>Drukuj</a>
                        <li><a class=btn-with-icon href ng-click="$ctrl.refresh()"><i class=ion-refresh></i>Odśwież</a>
                        <li><a class=btn-with-icon href ng-click="customize()"><i class=ion-wrench></i>Konfiguruj</a>
                    </ul>
                </div>`,
            menu: [{
                html: '<a class=btn-with-icon href><i class=ion-eye></i>Przypisz materiał</a>',
                click: (item, scope) => scope.$ctrl.modalDemandSatisfying(item),
                displayed: item => item.data.state !== 'SATISFIED'
            }]
        });

        ListConfigServiceProvider.addDefaultConfig('MaterialDemand',
            {
                vm: '$ctrl',
                id: 'MaterialDemand',
                options: {selectable: true},
                controls: {name: 'materialDemandControls'},
                defaultColumns: ['orderNo', 'recipient', 'productNo', 'productName', 'detalNo', 'detalName', 'materialCode', 'name', 'particularDimension', 'count', 'unsatisfied', 'length', 'MATERIAL_DEMAND_STATE'],
                availableColumns: {
                    materialCode: {key: 'materialTemplate.materialCode'},
                    name: {key: 'materialTemplate.materialType.name'},
                    particularDimension: {key: 'materialTemplate.particularDimension'},
                    detalNo: {key: 'detalTemplate.productNo'},
                    detalName: {key: 'detalTemplate.name'},
                    productNo: {
                        key: 'topProductInfo.productNo',
                        templateOptions: {label: 'Nr produktu', placeholder: 'Nr produktu'}
                    },
                    productName: {key: 'topProductInfo.productName'},
                    orderNo: {key: 'order.nr'},
                    recipient: {key: 'recipient.name'},
                    count: null,
                    unsatisfied: null,
                    satisfied: null,
                    length: {key: 'cuttingSize.origSize'},
                    MATERIAL_DEMAND_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );

        ListConfigServiceProvider.addDefaultControls('materialByCodeControls', {
            leftControl: '',
            rightControl:
                `<div class=btn-group dropdown-append-to-body uib-dropdown>
                    <button type=button class="btn-with-icon btn btn-default" ng-click="cleanSearchModel()"><i class=ion-code></i>Wyczyść filtry</button>
                    <button class="btn btn-default" type=button uib-dropdown-toggle><span class=caret></span></button>
                    <ul uib-dropdown-menu class="dropdown-menu-right">
                        <li><a class=btn-with-icon href ng-click="customize()"><i class=ion-wrench></i>Konfiguruj</a>
                    </ul>
                </div>`,
            menu: [{
                html: '<a class=btn-with-icon href><i class=ion-link></i>Przypisz</a>',
                click: (item, scope) => scope.$ctrl.assign(item)
            }]
        });

        ListConfigServiceProvider.addDefaultConfig('MaterialByCode',
            {
                vm: '$ctrl',
                id: 'MaterialByCode',
                options: {},
                controls: {name: 'materialByCodeControls'},
                defaultColumns: ['materialCode', 'name', 'particularDimension', 'count', 'leftLengthObj', 'MATERIAL_STATE'],
                availableColumns: {
                    _id: {router: 'stock.material'},
                    particularDimension: {key: 'materialTemplate.particularDimension'},
                    materialCode: {key: 'materialTemplate.materialCode'},
                    count: null,
                    name: {key: 'materialTemplate.materialType.name'},
                    lengthObj: {key: "length", templateOptions: {field: 'length'}},
                    reservedLengthObj: {key: "reservedLength", templateOptions: {field: 'length'}},
                    leftLengthObj: {key: "leftLength", templateOptions: {field: 'length'}},
                    MATERIAL_STATE: null,
                    select_steelGrade: {key: 'materialTemplate.steelGrade.steelGrade'},
                    select_tolerance: {key: 'materialTemplate.tolerance.tolerance'},
                    select_norm: {key: 'materialTemplate.norm.norm'},
                    comments: null,
                    materialTemplateId: {key: 'materialTemplate'},
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                },
                extends: [{
                    title: '',
                    search: '',
                    tbody: '<td><button class="btn-with-icon btn btn-primary" type=button ng-click="$ctrl.assign(item)"><i class=ion-link></i>Przypisz</button></td>'
                }]
            }
        );

        ListConfigServiceProvider.addDefaultConfig('ProviderWithMaterial',
            {
                vm: '$ctrl',
                id: 'ProviderWithMaterial',
                options: {
                    hideSearch: true,
                    hideTfoot: true
                },
                defaultColumns: ['pwm_providerName', 'pwm_materialAssortment'],
                availableColumns: {
                    _id: {router: 'contractors.provider'},
                    pwm_providerName: {
                        id: 'providerName',
                        key: 'providerName',
                        templateOptions: {
                            label: 'Nazwa',
                            placeholder: 'Nazwa',
                            tbody: '{{item.providerName}}'
                        }
                    },
                    pwm_materialAssortment: {
                        id: 'pwm_materialAssortment',
                        key: 'materialAssortment',
                        templateOptions: {
                            label: 'Materiały',
                            placeholder: 'Materiały',
                            tbody:
                                `<div>
                                <span class="col-md-3">Wymiary</span>
                                <span class="col-md-3">Cena</span>
                                <span class="col-md-6">Ilość</span>
                            </div>
                            <div ng-repeat="ma in item.materialAssortment">
                                <span class="col-md-3">{{ma.salesLength.origSize}}</span>
                                <span class="col-md-3">{{ma.salesPrice.amount}} {{ma.salesPrice.currency}} /{{ma.sellUnit}}</span>
                                <span class="col-md-6"><input type="number" min="0" class="form-control" ng-model="ma.count" quantity></span>
                            </div>`,
                        }
                    }
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateMaterialDemand', 'UpdateMaterialDemand'],
            {
                id: 'MaterialDemand',
                options: {},
                fields: [
                    {id: 'materialId', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('MaterialDemand',
            {
                id: 'MaterialDemand',
                options: {},
                availableFields: {
                    name: {key: "materialTemplate.materialType.name"},
                    materialCode: {key: "materialTemplate.materialCode"},
                    particularDimension: {key: "materialTemplate.particularDimension"},
                    count: {templateOptions: {label: 'Wymagana ilość', placeholder: 'Wymagana ilość'}},
                    length: {
                        key: "cuttingSize.origSize",
                        templateOptions: {label: 'Wymagana długość', placeholder: 'Wymagana długość'}
                    },
                    MATERIAL_DEMAND_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'production.materialDemand',
                abstract: true,
                url: '/zapotrzebowaniaMaterialowe',
                title: 'Zapotrzebowanie materiałowe',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 100
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista zapotrzebowań materiałowych',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "MaterialDemandCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (MaterialDemandListModel) {
                                return MaterialDemandListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'MaterialDemand',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Zapotrzebowanie materiałowe',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function (params) {
                            return 'Zapotrzebowanie materiałowe';
                        },
                        resolve: {
                            elementModel: function ($stateParams, MaterialDemandModel) {
                                return MaterialDemandModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/zakupy/zapotrzebowaniaMaterialowe', '/zakupy/zapotrzebowaniaMaterialowe/lista');
    }
})();
