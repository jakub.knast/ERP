/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.production.materialDemand')
        .factory('MaterialDemandModel', MaterialDemandModel)
        .factory('MaterialDemandListModel', MaterialDemandListModel);

    /** @ngInject */
    function MaterialDemandModel(BaseModelClass, MaterialDemandRequestService) {
        return class MaterialDemandClass extends BaseModelClass {
            constructor(materialDemandData) {
                super(MaterialDemandRequestService, materialDemandData);
            }

            satisfyDemands(materialId) {
                return this.runServiceFunc('satisfyDemands', {id: this.data.id, materialId: materialId}, {message: 'MaterialDemand.alert.satisfyDemands', update:true});
            };

            static getById(materialDemandId) {
                return new MaterialDemandClass().getById(materialDemandId);
            };
        }
    }

    /** @ngInject */
    function MaterialDemandListModel(ListModelClass, MaterialDemandRequestService, MaterialDemandModel) {
        return class MaterialDemandsListClass extends ListModelClass {
            constructor() {
                super(MaterialDemandRequestService, MaterialDemandModel);
            }

            static list() {
                return new MaterialDemandsListClass().refresh();
            }
        }
    }
})();
