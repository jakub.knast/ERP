/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class GenerateDocumentCtrl {
        constructor($scope, $window, config, FormlyConfigService, ProductListModel) {
            const that = this;
            this.title = config.title;
            this.type = config.type;
            this.config = FormlyConfigService.getFormlyConfig(config.formlyConfig);
            this.listData = {};

            $scope.$watch('$ctrl.model.recipientId', function (nValue, oValue) {
                var listFunction;
                if(that.type === "WZ"){
                    listFunction = ProductListModel.listProductsForWz;
                }else{
                    listFunction = ProductListModel.listProductsForInvoice;
                }
                if (nValue && nValue !== oValue) {
                    listFunction({
                        recipientId: nValue
                    }).then(function (successData) {
                        that.productListModel = successData;
                        that.listData = {}
                    });
                } else {
                    that.productListModel = null;
                }
            });

            this.queryFromBooleanObjectMap = function (booleanObjectMap) {
                let query = '';
                if (angular.isObject(booleanObjectMap)) {
                    let i = 0;
                    for (let key in booleanObjectMap) {
                        if (booleanObjectMap.hasOwnProperty(key)) {
                            if (booleanObjectMap[key]) {
                                query += "&productsIds[" + (i++) + "]=" + key;
                            }
                        }
                    }
                }
                return query;
            };

            this.generate = function () {
                $window.open(`/api/invoker/${config.url}?sellerId=${that.model.sellerId}&recipientId=${that.model.recipientId}${that.queryFromBooleanObjectMap(that.listData)}`, "_blank")
                $scope.$dismiss();
            };


        }
    }

    angular
            .module('JangraERP.core')
            .controller('GenerateDocumentCtrl', GenerateDocumentCtrl);
})();
