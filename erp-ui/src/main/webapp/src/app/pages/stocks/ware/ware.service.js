/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.stocks.ware')
        .service('WareRequestService', WareRequestService);

    /** @ngInject */
    function WareRequestService(RequestServiceClass) {
        class WareRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Ware', 'ware');
            }
        }

        return new WareRequestServiceClass();
    }
})();
