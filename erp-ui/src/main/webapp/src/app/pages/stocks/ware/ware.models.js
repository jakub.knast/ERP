/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.stocks.ware')
        .factory('WareModel', WareModel)
        .factory('WareListModel', WareListModel);

    /** @ngInject */
    function WareModel(BaseModelClass, WareRequestService) {
        return class WareClass extends BaseModelClass {
            constructor(wareData) {
                super(WareRequestService, wareData);
            }

            static getById(wareId) {
                return new WareClass().getById(wareId);
            };
        }
    }

    /** @ngInject */
    function WareListModel(ListModelClass, WareRequestService, WareModel) {
        return class WaresListClass extends ListModelClass {
            constructor() {
                super(WareRequestService, WareModel);
            }

            static list(preFilter, postFilter) {
                return new WaresListClass().setPreFilter(preFilter).setPostFilter(postFilter).refresh();
            }
        }
    }
})();
