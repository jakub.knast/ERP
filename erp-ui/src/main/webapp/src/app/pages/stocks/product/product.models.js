/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
            .module('JangraERP.pages.stocks.product')
            .factory('ProductModel', ProductModel)
            .factory('ProductListModel', ProductListModel);

    /** @ngInject */
    function ProductModel(BaseModelClass, ProductRequestService) {
        return class ProductClass extends BaseModelClass {
            constructor(productData) {
                super(ProductRequestService, productData);
            }

            static getById(productId) {
                return new ProductClass().getById(productId);
            }

            finishProduct(data) {
                return BaseModelClass.runServiceFunc('finishProduct', ProductRequestService, data, {message: "ProductionOrder.alert.finish"});
            }
        };
    }

    /** @ngInject */
    function ProductListModel(ListModelClass, ProductRequestService, ProductModel) {
        return class ProductsListClass extends ListModelClass {
            constructor() {
                super(ProductRequestService, ProductModel);
            }

            validateAllProductsTemplate() {
                return this.runServiceFunc('validateAllProductsTemplate', {updateValidationErrorsField: true}, {
                    message: 'ProductTemplate.alert.validateAllProductsTemplate',
                    update: false
                });
            }

            generateWZ(data) {
                return this.runServiceFunc('generateWZ', data, {message: "ProductionOrder.alert.generate"});
            }

            generateInvoice(data) {
                return this.runServiceFunc('generateInvoice', data, {message: "ProductionOrder.alert.generate"});
            }

            static list(preFilter, postFilter) {
                return new ProductsListClass().setPreFilter(preFilter).setPostFilter(postFilter).refresh();
            }        

            static listProductsForWz(data) {
                return ProductRequestService.listProductsForWz(data)
                        .then(responseData => {
                            var list = {};
                            list.size = responseData.size;
                            list.elements = responseData.elements.map(element => new ProductModel(element));
                            return list;
                        });
            }
            
            static listProductsForInvoice(data) {
                return ProductRequestService.listProductsForInvoice(data)
                        .then(responseData => {
                            var list = {};
                            list.size = responseData.size;
                            list.elements = responseData.elements.map(element => new ProductModel(element));
                            return list;
                        });
            }
        };
    }
})();
