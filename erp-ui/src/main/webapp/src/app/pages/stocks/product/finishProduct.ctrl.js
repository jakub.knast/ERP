/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class FinishProductCtrl {
        constructor(FormlyConfigService, elementModel) {
//            this.modalTitle = elementModel.getName() + '.remove.title';
            this.modalInclude = 'app/pages/stocks/product/finishProduct.modal.html';
            this.config = FormlyConfigService.getFormlyConfig('FinishProductModalConfig');
            this.element = elementModel;
//            var toDispose = elementModel.data.orderPositions && elementModel.data.orderPositions[0] && elementModel.data.orderPositions[0].toDisposeCount ? elementModel.data.orderPositions[0].toDisposeCount : 0;
//            var disposed = elementModel.data.orderPositions && elementModel.data.orderPositions[0] && elementModel.data.orderPositions[0].disposedCount ? elementModel.data.orderPositions[0].disposedCount : 0;
            this.model = {
                count : 0
            };
        }

        doIt($close) {
            if (this.dispositionForm.$valid) {
                this.dispositionForm.$setSubmitted();
                var requestData = {
                    productId: this.element.id,
                    count: this.model.count
                };
                this.cgBusy = this.element.finishProduct(requestData).then(() => {
//                    this.model.updateTemp();
                    $close();
                }, angular.noop);
            }
        }

    }

    angular
            .module('JangraERP.core')
            .controller('FinishProductCtrl', FinishProductCtrl);
})();
