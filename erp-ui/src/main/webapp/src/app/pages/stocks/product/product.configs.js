    /**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
            .module('JangraERP.pages.stocks.product')
            .config(listConfig)
            .config(formlyConfig)
            .config(previewConfig)
            .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Product',
                {
                    vm: '$ctrl',
                    id: 'Product',
                    options: {selectable: true},
                    controls: {
                        name: 'listControls',
                        rightControl: [{
                                index: 1,
                                template: '<button class="btn-with-icon btn btn-default" type=button  ng-click="$ctrl.modalGenerateWZ(item)"><i class=ion-flash></i>Generuj WZ</button>'
                            }, {
                                index: 2,
                                template: '<button class="btn-with-icon btn btn-default" type=button  ng-click="$ctrl.modalGenerateInvoice(item)"><i class=ion-flash></i>Generuj Fakturę</button>'
                            }],
                        menu: [{
                                index: 3,
                                item: {
                                    html: '<a class=btn-with-icon href><i class=ion-help></i>help</a>',
                                    click: (item, scope) => scope.$ctrl.help()
                                }
                            }, {
                                index: 4,
                                item: {
                                    html: '<a class=btn-with-icon href><i class=ion-flash></i>Zakończ produkcję</a>',
                                    click: (item, scope) => scope.$ctrl.modalFinishProduct(item),
                                    displayed: item => item.data.state === "IN_PRODUCTION"
                                }
                            },{
                                index: 5,
                                item: null
                            }]
//                        ,
//                        tbody: [{
//                                index: 6,
//                                template: '<li><a class=btn-with-icon href ng-click="$ctrl.modalFinishProduct(item)"><i class=ion-flash></i>Zakończ produkcję</a>'
//                            }]
                    },
                    defaultColumns: ['productNo', 'name', 'orderNo', 'productionOrderNo', 'positionOnOrder', 'count', 'PRODUCT_STATE', 'datePicker_deadline', 'recipient', 'deadLine'],
                    availableColumns: {
                        _id: {router: 'stock.product'},
                        invoice: null,
                        wz: null,
                        productNo: {key: 'productTemplate.productNo'},
                        name: {key: 'productTemplate.name'},
                        count: null,
                        recipient: {key: 'recipient.name'},
                        orderNo: null,
                        productionOrderNo: null,
                        positionOnOrder: null,
                        datePicker_deadline: null,
                        comments: null,
                        productTemplateId: {key: 'productTemplate'},
                        PRODUCT_STATE: null,
                        datePicker_creationDate: null,
                        datePicker_lastModificationDate: null
                    }
                }
        );

        ListConfigServiceProvider.addDefaultControls('productByCodeControls', {
            leftControl: '',
            rightControl:
                `<div class=btn-group dropdown-append-to-body uib-dropdown>
                    <button type=button class="btn-with-icon btn btn-default" ng-click="cleanSearchModel()"><i class=ion-code></i>Wyczyść filtry</button>
                </div>`,
            menu: []
        });

        ListConfigServiceProvider.addDefaultConfig('ProductByCode',
                {
                    vm: '$ctrl',
                    id: 'ProductByCode',
                    options: {},
                    controls: {name: 'productByCodeControls'},
                    defaultColumns: ['name', 'count', 'orderNo', 'positionOnOrder', 'checkbox'],
                    availableColumns: {
//                        _id: {router: 'stock.product'},
                        count: null,
                        name: {key: 'productTemplate.name'},
//                        PRODUCT_STATE: null,
                        orderNo: null,
                        positionOnOrder: null,
//                        comments: null,
                        checkbox: {
                            id: 'checkbox',
                            key: 'checkbox',
                            templateOptions: {
                                label: 'Wybierz',
                                tbody: '<label class="checkbox-inline custom-checkbox nowrap"><input type="checkbox" id="{{::options.key}}" ng-model="data[item.id]"><span></span></label>'
                            }
                        }
                    }
                }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateProduct', 'UpdateProduct'],
                {
                    id: 'Product',
                    options: {},
                    fields: [
                        {id: 'productTemplateId', config: {templateOptions: {required: true}}},
                        'count',
//                    'orderNo',
//                    'datePicker_deadline',
//                    'localization',
                        'comments'
                    ]
                }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['GenerateWZModalConfig'],
                {
                    id: 'GenerateWZModalConfig',
                    options: {},
                    fields: [
                        {id: 'sellerId', config: {templateOptions: {required: true}}},
                        {id: 'recipientId', config: {templateOptions: {required: true}}}
                    ]
                }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['GenerateInvoiceModalConfig'],
                {
                    id: 'GenerateInvoiceModalConfig',
                    options: {},
                    fields: [
                        {id: 'sellerId', config: {templateOptions: {required: true}}},
                        {id: 'recipientId', config: {templateOptions: {required: true}}}
                    ]
                }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['FinishProductModalConfig'],
                {
                    id: 'FinishProductModalConfig',
                    options: {},
                    fields: [
                        {id: 'count', config: {templateOptions: {required: true}}}
                    ]
                }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Product',
                {
                    id: 'Product',
                    options: {},
                    availableFields: {
                        productNo: {key: 'productTemplate.productNo'},
                        name: {key: 'productTemplate.name'},
                        count: null,
                        recipient: null,
                        orderNo: null,
                        localization: null,
                        datePicker_deadline: null,
                        PRODUCT_STATE: null,
                        datePicker_creationDate: null,
                        datePicker_lastModificationDate: null
                    }
                }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
                .state({
                    name: 'stocks.product',
                    abstract: true,
                    url: '/produkt',
                    template: '<ui-view />',
                    title: 'Produkty',
                    sidebarMeta: {
                        order: 200
                    },
                    children: [
                        {
                            name: 'list',
                            url: '/lista',
                            title: 'Lista produktów',
                            templateUrl: 'app/core/controllers/base.list.html',
                            controller: "ProductListCtrl",
                            controllerAs: "$ctrl",
                            resolve: {
                                listModel: function (ProductListModel) {
                                    return ProductListModel.list(null, {});
                                },
                                listConfig: function () {
                                    return {
                                        id: 'Product',
                                        removeConsequencesTpl: null
                                    };
                                }
                            }
                        },
                        {
                            name: 'element',
                            abstract: true,
                            url: '/{id:[0-9a-f]{24}}',
                            title: 'Produkt',
                            templateUrl: 'app/core/controllers/base.element.html',
                            controller: 'BaseElementCtrl',
                            controllerAs: "$ctrl",
                            breadcrumb: function () {
                                return 'Produkt';
                            },
                            resolve: {
                                elementModel: function ($stateParams, ProductModel) {
                                    return ProductModel.getById($stateParams.id);
                                }
                            },
                            children: [
                                {
                                    name: 'detail',
                                    url: '/szczegoly',
                                    title: 'Szczegóły',
                                    templateUrl: 'app/core/controllers/base.detail.html',
                                    controller: 'BaseDetailCtrl',
                                    controllerAs: "$ctrl"
                                }
                            ]
                        }
                    ]
                });

        $urlRouterProvider.when('/magazyn/produkt', '/magazyn/produkt/lista');
    }
})();
