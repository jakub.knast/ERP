/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.stocks.material')
        .factory('MaterialModel', MaterialModel)
        .factory('MaterialListModel', MaterialListModel);

    /** @ngInject */
    function MaterialModel(BaseModelClass, MaterialRequestService) {
        return class MaterialClass extends BaseModelClass {
            constructor(materialData) {
                super(MaterialRequestService, materialData);
            }

            static getById(materialId) {
                return new MaterialClass().getById(materialId);
            };
        }
    }

    /** @ngInject */
    function MaterialListModel(ListModelClass, MaterialRequestService, MaterialModel) {
        return class MaterialsListClass extends ListModelClass {
            constructor() {
                super(MaterialRequestService, MaterialModel);
            }

            static list(preFilter, postFilter) {
                return new MaterialsListClass().setPreFilter(preFilter).setPostFilter(postFilter).refresh();
            }
        }
    }
})();
