/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.process.productionOrder')
        .factory('ProductionOrderModel', ProductionOrderModel)
        .factory('ProductionOrderListModel', ProductionOrderListModel);

    /** @ngInject */
    function ProductionOrderModel(BaseModelClass, ProductionOrderRequestService, ProductionJobRequestService, MaterialDemandRequestService) {
        return class ProductionOrderClass extends BaseModelClass {
            constructor(productionOrderData) {
                super(ProductionOrderRequestService, productionOrderData);
            }

            generateProductionJobs () {
                return BaseModelClass.runServiceFunc('generateProductionJobs', ProductionJobRequestService, this.data.id, {message: "ProductionJob.alert.generateProductionJobs"});
            };

            generateMaterialDemands () {
                return BaseModelClass.runServiceFunc('generateMaterialDemands', MaterialDemandRequestService, this.data.id, {message: "MaterialDemand.alert.generateMaterialDemands"});
            };
            
            finishProductionOrder () {
                return BaseModelClass.runServiceFunc('finishProductionOrder', ProductionOrderRequestService, this.data, {message: "ProductionOrder.alert.finish"});
            };

            static getById(productionOrderId) {
                return new ProductionOrderClass().getById(productionOrderId);
            };
        }
    }

    /** @ngInject */
    function ProductionOrderListModel(ListModelClass, ProductionOrderRequestService, ProductionOrderModel) {
        return class ProductionOrdersListClass extends ListModelClass {
            constructor() {
                super(ProductionOrderRequestService, ProductionOrderModel);
            }

            static list() {
                return new ProductionOrdersListClass().refresh();
            }
        }
    }
})();
