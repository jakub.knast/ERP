/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.process.order')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(nestDataListConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Order',
            {
                vm: '$ctrl',
                id: 'Order',
                options: {selectable: true},
                controls: {
                    name: 'listControls',
                    rightControl: [
//                        {
//                        index: 1,
//                        template: '<button class="btn-with-icon btn btn-default" type=button ng-click="$ctrl.cancelAllOrders()"><i class=ion-flash></i>Anuluj wsz. zamówienia</button>'
//                    }, {
//                        index: 2,
//                        template: '<button class="btn-with-icon btn btn-default" type=button ng-click="$ctrl.generateAllNewOrdersProdOrdAndProdJobs()"><i class=ion-flash></i>Gen. wszystko</button>'
//                    }, {
//                        index: 3,
//                        template: '<button class="btn-with-icon btn btn-default" type=button ng-click="$ctrl.regenerateAll()"><i class=ion-flash></i>Regen. wszystko</button>'
//                    }
                ],
                    menu: [{
                        index: 3,
                        item: {
                            html: '<a class=btn-with-icon href><i class=ion-help></i>help</a>',
                            click: (item, scope) => scope.$ctrl.help()
                        }
//                    }, {
//                        index: 4,
//                        item: {
//                            html: '<a class=btn-with-icon href><i class=ion-flash></i>Generuj zamówienie</a>',
//                            click: (item, scope) => scope.$ctrl.generateProductionOrder(item),
//                            displayed: item => !item.data.frameworkContractOrder && item.data.state === "NEW"
//                        }
                    }, {
                        index: 5,
                        item: {
                            html: '<a class=btn-with-icon href><i class=ion-flash></i>Wykonaj dyspozycję</a>',
                            click: (item, scope) => scope.$ctrl.modalGenerateDisposition(item),
                            displayed: item => item.data.frameworkContractOrder && item.data.state === "NEW"
                        }
//                    }, {
//                        index: 6,
//                        item: {
//                            html: '<a class=btn-with-icon href><i class=ion-flash></i>Anuluj zamówienie</a>',
//                            click: (item, scope) => scope.$ctrl.cancelOrder(item)
//                        }
                    }, {
                        index: 7,
                        item: null
                    }]
                },
                defaultColumns: ['nr', 'recipientId', 'ORDER_STATE', 'comments', 'frameworkContractOrder', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'process.order'},
                    nr: null,
                    orderPositionsBriefly: null,
                    comments: null,
                    recipientId: {key: 'recipient'},
                    frameworkContractOrder: null,
                    ORDER_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateOrder'],
            {
                id: 'Order',
                options: {},
                fields: [
                    'frameworkContractOrder',
                    {id: 'orderNo', config: {key: "nr", templateOptions: {required: true}}},
                    {id: 'recipientId', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );
        FormlyConfigServiceProvider.addDefaultConfig(['UpdateOrder'],
            {
                id: 'Order',
                options: {},
                fields: [
                    {id: 'frameworkContractOrder', config: {templateOptions: {readonly: true}}},
                    {id: 'orderNo', config: {key: "nr", templateOptions: {required: true}}},
                    {id: 'recipientId', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );
        FormlyConfigServiceProvider.addDefaultConfig(['GenerateDispositionModalConfig'],
            {
                id: 'GenerateDispositionModal',
                options: {},
                fields: [
                    {id: 'datePicker_deliveryDate', config: {templateOptions: {required: true}}},
                    {
                        id: 'disposedHideableCount',
                        config: {key: "disposedCount", templateOptions: {label: "Zadysponowane", readonly: true}}
                    },
                    {
                        id: 'disposedHideableCount',
                        config: {key: "toDisposeCount", templateOptions: {label: "Do zadysponowania", readonly: true}}
                    },
                    {id: 'count', config: {templateOptions: {label: "Zadysponuj", required: true, min: 1}}}
                ]
            }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['CreateOrderPositions', 'UpdateOrderPositions'],
            {
                id: 'OrderPositions',
                options: {},
                fields: [
                    {id: 'positionOnOrder', config: {templateOptions: {required: true}}},
                    {
                        id: 'saleProductTemplateId',
                        config: {
                            templateOptions: {
                                onlyValid: true,
                                required: true,
                                crossHtml: '{{model[options.key].name}}'
                            }
                        }
                    },
                    {id: 'datePicker_deliveryDate', config: {templateOptions: {required: true}}},
                    {id: 'count', config: {templateOptions: {label: "Ilość", required: true, min: 1}}},

                    {
                        id: 'disposedHideableCount',
                        config: {key: "disposedCount", templateOptions: {label: "Zadysponowane", readonly: true}}
                    },
                    {
                        id: 'toDisposeHideableAutoFill',
                        config: {key: "toDisposeCount", templateOptions: {label: "Do zadysponowania", readonly: true}}
                    },

                    {id: 'productDiscountAutoFilled', config: {templateOptions: {required: true}}},
                    {id: 'productPriceAutoFilled', config: {templateOptions: {required: true}}},
                    {id: 'productPriceWithDiscountAutoFilled', config: {templateOptions: {required: true}}},
                    'calculatedPositionPrice',
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Order',
            {
                id: 'Order',
                options: {},
                availableFields: {
                    frameworkContractOrder: null,
                    nr: null,
                    recipientId: {key: 'recipient'},
                    comments: null,
                    ORDER_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );

        PreviewConfigServiceProvider.addDefaultConfig('OrderPositions',
            {
                id: 'OrderPositions',
                options: {},
                availableFields: {
                    positionOnOrder: null,
                    productTemplateId: {key: 'productTemplate'},
                    datePicker_deliveryDate: null,
                    count: null,
                    discount: null,
                    orderPositionPrice: null,
                    comments: null
                }
            }
        );
    }

    /** @ngInject */
    function nestDataListConfig(NestDataListConfigServiceProvider) {
        NestDataListConfigServiceProvider.addDefaultConfig('OrderPositions',
            {
                id: 'OrderPositions',
                options: {
                    path: 'orderPositions',
                    formlyConfig: 'OrderPositions',
                    previewConfig: 'OrderPositions',
                    viewTemplateUrl: 'app/core/templates/nestDataList/orderPositions.html'
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'process.order',
                abstract: true,
                url: '/zamowienia',
                title: 'Zamówienia',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 100,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista zamówień',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "OrderListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (OrderListModel) {
                                return OrderListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'Order',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Zamówienie',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Zamówienie';
                        },
                        resolve: {
                            elementModel: function ($stateParams, OrderModel) {
                                return OrderModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            },
                            {
                                name: 'positions',
                                url: '/pozycja',
                                title: 'Pozycje',
                                templateUrl: 'app/core/components/nestDataList/nestDataList.base.html',
                                controller: 'BaseNestDataListCtrl',
                                controllerAs: "$ctrl",
                                breadcrumb: function () {
                                    return 'Pozycje na zamówieniu'
                                },
                                resolve: {
                                    configName: function () {
                                        return 'OrderPositions'
                                    }
                                }
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/procesy/zamowienia', '/procesy/zamowienia/lista');
    }
})();
