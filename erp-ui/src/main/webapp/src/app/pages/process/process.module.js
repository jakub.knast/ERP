(function () {
    'use strict';

    angular.module('JangraERP.pages.process', [
        'JangraERP.pages.process.order',
        'JangraERP.pages.process.productionJob',
        'JangraERP.pages.process.productionOrder'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('process', {
                url: '/procesy',
                abstract: true,
                template: '<ui-view />',
                title: 'Procesy',
                sidebarMeta: {
                    icon: 'ion-clipboard',
                    order: 100
                }
            });
    }

})();
