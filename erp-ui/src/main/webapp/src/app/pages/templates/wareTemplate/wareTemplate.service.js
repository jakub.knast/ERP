/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.wareTemplate')
        .service('WareTemplateRequestService', WareTemplateRequestService);

    /** @ngInject */
    function WareTemplateRequestService(RequestServiceClass) {
        class WareTemplateRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('WareTemplate', 'waretemplates');
            }
        }

        return new WareTemplateRequestServiceClass();
    }
})();
