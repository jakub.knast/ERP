/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.wareTemplate')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('WareTemplate',
            {
                vm: '$ctrl',
                id: 'WareTemplate',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['wareCode', 'wareTypeId', 'name', 'select_norm', 'kgToPieceRatio', 'comments', 'wareUsages', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'templates.ware'},
                    wareCode: null,
                    wareTypeId: {key: 'wareType'},
                    name: null,
                    select_norm: null,
                    kgToPieceRatio: null,
                    wareUsages: null,
                    comments: null,
                    WARE_TEMPLATE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateWareTemplate', 'UpdateWareTemplate'],
            {
                id: 'WareTemplate',
                options: {},
                fields: [
                    {id: 'wareTypeId', config: {templateOptions: {required: true}}},
                    {id: 'wareCode', config: {templateOptions: {required: true}}},
                    {id: 'name', config: {templateOptions: {required: true}}},
                    'const_norm',
                    'kgToPieceRatio',
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('WareTemplate',
            {
                id: 'WareTemplate',
                options: {},
                availableFields: {
                    wareCode: null,
                    wareUsages: {
                        key: 'wareUsages',
                        type: 'validationMessageTable',
                        templateOptions: {
                            label: 'Użycia towaru',
                            countLabel: 'Ilość użyć:'
                        }
                    },
                    name: {key: 'wareType.name', templateOptions: {
                        label: 'Typ',
                        placeholder: 'Typ'
                    }},
                    select_norm: null,
                    kgToPieceRatio: null,
                    comments: null,
                    WARE_TEMPLATE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'templates.wareTemplate',
                abstract: true,
                url: '/towary',
                title: 'Sz. towarów',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 400,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista szablonów towarów',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (WareTemplateListModel) {
                                return WareTemplateListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'WareTemplate',
                                    removeConsequencesTpl:
                                        `<div>testowy komunikat</div>
                                        <ul>
                                            <li ng-repeat="leaf in $ctrl.removeConsequences.leafs">
                                                lista leafow
                                            </li>
                                        </ul>`
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Szablon towaru',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Sz. towaru';
                        },
                        resolve: {
                            elementModel: function ($stateParams, WareTemplateModel) {
                                return WareTemplateModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/szablony/towary', '/szablony/towary/lista');
    }
})();
