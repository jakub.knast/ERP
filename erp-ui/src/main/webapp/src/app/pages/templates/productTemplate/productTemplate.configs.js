/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.productTemplate')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('ProductTemplate',
            {
                vm: '$ctrl',
                id: 'ProductTemplate',
                options: {selectable: true},
                controls: {
                    name: 'listControls',
                    menu: [{
                        index: 3,
                        item : {
                            html: '<a class=btn-with-icon href><i class=ion-compose></i>Sprawdź poprawność</a>',
                            click: (item, scope) => scope.$ctrl.validateOne(item),
                        }
//                    }, {
//                        index: 4,
//                        item : {
//                            html: '<a class=btn-with-icon href ng-href="/api/invoker/ProductTemplateService/getProductionGuide?productTemplateId={{::item.data.id}}" target="_blank"><i class=ion-ios-cloud-download-outline></i>Pobierz szablon przewodnik</a>',
//                            compile: true
//                        }

                    }, {
                        index: 4,
                        item: {
                            html: '<a class=btn-with-icon href><i class=ion-ios-cloud-download-outline></i>Pobierz przewodnik</a>',
                            click: function(item){window.open("/api/invoker/ProductTemplateService/getProductionGuide?productTemplateId=" + item.data.id, "_blank");},
                            compile: true
                        }
                    }, {
                        index: 5,
                        item : null
                    }],
                    rightControl: [{
                        index: 1,
                        template: '<button class="btn-with-icon btn btn-default" type=button  ng-click="$ctrl.validateAll()"><i class=ion-compose></i>Spradź poprawność wszystkich</button>'
                    }]
                },
                defaultColumns: ['productNo', 'name', 'salesOffers', 'techDrawingPDF', 'comments', 'ptUsages', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'templates.productTemplate'},
                    productNo: null,
                    name: null,
                    salesOffers: null,
                    techDrawingPDF: null,
                    techDrawingDXF: null,
                    reportTemplate: null,
                    productionPlanList: null,
                    buildingTimeStr: null,
                    totalBuildingTimeStr: null,
                    buildingTime: null,
                    totalBuildingTime: null,
                    validationStatus: null,
                    validationMessage: null,
                    ptUsages: null,
                    comments: null,
                    PRODUCT_TEMPLATE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateProductTemplate', 'UpdateProductTemplate'],
            {
                id: 'ProductTemplate',
                options: {},
                fields: [
                    'techDrawingPDF',
                    'techDrawingDXF',
                    'reportTemplate',
                    {id: 'name', config: {templateOptions: {required: true}}},
                    {id: 'productNo', config: {templateOptions: {required: true}}},
//                    {id: 'salesOffers', config: {templateOptions: {required: true}},},
                    'productTemplateType',
                    'salesOffers',
                    'comments'
                ]
            }
        );

        /*
        ProductionPlan config in src/app/pages/templates/templates.module.js
         */

    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('ProductTemplate',
            {
                id: 'ProductTemplate',
                options: {},
                availableFields: {
                    productNo: null,
                    name: null,
                    price: null,
                    comments: null,
                    validationStatus: {
                        id: 'validationMessage',
                        key: 'validationMessage',
                        type: 'validationMessageTable',
                        templateOptions: {
                            label: 'Blędy w szablonie',
                            countLabel: 'Ilość błędów:'
                        }
                    },
                    ptUsages: {
                        id: 'ptUsages',
                        key: 'ptUsages',
                        type: 'validationMessageTable',
                        templateOptions: {
                            label: 'Użycia szablonu produktu',
                            countLabel: 'Ilość użyć:'
                        }
                    },
                    buildingTimeStr: null,
                    totalBuildingTimeStr: null,
                    techDrawingPDF: null,
                    techDrawingDXF: null,
                    guideForProductTemplate: null,
                    reportTemplate: {
                        key: 'partCard',
                        type: 'limitCardTable',
                        templateOptions: {
                            label: 'Karta limitowa'
                        }
                    },
                    salesOffers: null,
                    PRODUCT_TEMPLATE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                },
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'templates.productTemplate',
                abstract: true,
                url: '/produkty',
                title: 'Sz. produktów',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 300,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista szablonów produktów',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "ProductTemplateListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (ProductTemplateListModel) {
                                return ProductTemplateListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'ProductTemplate',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Szablon produktu',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Sz. produktu'
                        },
                        resolve: {
                            elementModel: function ($stateParams, ProductTemplateModel) {
                                return ProductTemplateModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            },
                            {
                                name: 'material',
                                url: '/materialy',
                                title: 'KL - Materiały',
                                templateUrl: 'app/core/components/nestDataList/nestDataList.base.html',
                                controller: 'BaseNestDataListCtrl',
                                controllerAs: "$ctrl",
                                breadcrumb: function () {
                                    return 'Karta limitowa - Materiały'
                                },
                                resolve: {
                                    configName: function () {
                                        return 'MaterialParts'
                                    }
                                }
                            },
                            {
                                name: 'semiproduct',
                                url: '/polprodukty',
                                title: 'KL - Półprodukty',
                                templateUrl: 'app/core/components/nestDataList/nestDataList.base.html',
                                controller: 'BaseNestDataListCtrl',
                                controllerAs: "$ctrl",
                                breadcrumb: function () {
                                    return 'Karta limitowa - Półprodukty'
                                },
                                resolve: {
                                    configName: function () {
                                        return 'SemiproductParts'
                                    }
                                }
                            },
                            {
                                name: 'ware',
                                url: '/towary',
                                title: 'KL - Towary',
                                templateUrl: 'app/core/components/nestDataList/nestDataList.base.html',
                                controller: 'BaseNestDataListCtrl',
                                controllerAs: "$ctrl",
                                breadcrumb: function () {
                                    return 'Karta limitowa - Towary'
                                },
                                resolve: {
                                    configName: function () {
                                        return 'WareParts'
                                    }
                                }
                            },
                            {
                                name: 'production',
                                url: '/produkcja',
                                title: 'Plan produkcji',
                                templateUrl: 'app/core/components/nestDataList/nestDataList.base.html',
                                controller: 'BaseNestDataListCtrl',
                                controllerAs: "$ctrl",
                                breadcrumb: function () {
                                    return 'Marszruta - plan produkcji'
                                },
                                resolve: {
                                    configName: function () {
                                        return 'ProductionPlan'
                                    }
                                }
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/szablony/produkty', '/szablony/produkty/lista');
    }
})();
