/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.materialTemplate')
        .service('MaterialTemplateRequestService', MaterialTemplateRequestService);

    /** @ngInject */
    function MaterialTemplateRequestService(RequestServiceClass) {
        class MaterialTemplateRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('MaterialTemplate', 'materialtemplates');
            }
        }

        return new MaterialTemplateRequestServiceClass();
    }
})();
