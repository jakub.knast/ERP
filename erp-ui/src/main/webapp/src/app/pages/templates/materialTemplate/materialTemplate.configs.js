/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.materialTemplate')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('MaterialTemplate',
            {
                vm: '$ctrl',
                id: 'MaterialTemplate',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['materialCode', 'materialTypeId', 'particularDimension', 'select_materialKind',
                    'select_tolerance', 'select_steelGrade', 'select_norm', 'materialUsages', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'templates.material'},
                    materialCode: null,
                    removed: null,
                    materialTypeId: null,
                    particularDimension: null,
                    select_materialKind: null,
                    select_tolerance: null,
                    select_steelGrade: null,
                    select_norm: null,
                    materialUsages: null,
                    comments: null,
                    MATERIAL_TEMPLATE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateMaterialTemplate', 'UpdateMaterialTemplate'],
            {
                id: 'MaterialTemplate',
                options: {},
                fields: [
                    {id: 'materialTypeId', config: {templateOptions: {required: true}}},
                    {id: 'materialCode', config: {templateOptions: {required: true}}},
                    {id: 'particularDimension', config: {templateOptions: {required: true}}},
                    {id: 'const_materialKind', config: {templateOptions: {required: true}}},
                    {id: 'const_steelGrade', config: {templateOptions: {required: true}}},
                    'const_norm',
                    'const_tolerance',
                    'meterToKgRatio',
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('MaterialTemplate',
            {
                id: 'MaterialTemplate',
                options: {},
                availableFields: {
                    materialCode: null,
                    materialType: {key: 'materialType.name'},
                    particularDimension: null,
                    select_materialKind: null,
                    select_tolerance: null,
                    meterToKgRatio: null,
                    select_steelGrade: null,
                    select_norm: null,
                    WARE_TEMPLATE_STATE: null,                    
                    materialUsages: {
                        key: 'materialUsages',
                        type: 'validationMessageTable',
                        templateOptions: {
                            label: 'Użycia materiału',
                            countLabel: 'Ilość użyć:'
                        }
                    },
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'templates.materialTemplate',
                abstract: true,
                url: '/materialy',
                title: 'Sz. materiałów',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 100,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista szablonów materiałów',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (MaterialTemplateListModel) {
                                return MaterialTemplateListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'MaterialTemplate',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Szablon materiału',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Sz. materiału';
                        },
                        resolve: {
                            elementModel: function ($stateParams, MaterialTemplateModel) {
                                return MaterialTemplateModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/szablony/materialy', '/szablony/materialy/lista');
    }
})();
