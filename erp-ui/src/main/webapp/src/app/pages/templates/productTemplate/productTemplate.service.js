/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.productTemplate')
        .service('ProductTemplateRequestService', ProductTemplateRequestService);

    /** @ngInject */
    function ProductTemplateRequestService(RequestServiceClass) {
        class ProductTemplateRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('ProductTemplate', 'producttemplates');
            }

            /* Details */

            update(data, isFormData) {
                delete data.partCard;
                delete data.productionPlan;

                return this.requestHandler.send({
                    method: 'updateProductTemplateSummary',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.UpdateProductTemplateSummaryRequest',
                    data: data
                }, isFormData);
            };

            /* MaterialPart */

            addProductTemplateMaterialPart(data) {
                return this.requestHandler.send({
                    method: 'addProductTemplateMaterialPart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.AddProductTemplateMaterialPartRequest',
                    data: data
                });
            };

            updateProductTemplateMaterialPart(data) {
                return this.requestHandler.send({
                    method: 'updateProductTemplateMaterialPart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.UpdateProductTemplateMaterialPartRequest',
                    data: data
                });
            };

            removeProductTemplateMaterialPart(data) {
                return this.requestHandler.send({
                    method: 'removeProductTemplateMaterialPart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.RemoveProductTemplateMaterialPartRequest',
                    data: data
                });
            };

            /* SemiproductPart */

            addProductTemplateProductPart(data) {
                return this.requestHandler.send({
                    method: 'addProductTemplateProductPart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.AddProductTemplateProductPartRequest',
                    data: data
                });
            };

            updateProductTemplateProductPart(data) {
                return this.requestHandler.send({
                    method: 'updateProductTemplateProductPart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.UpdateProductTemplateProductPartRequest',
                    data: data
                });
            };

            removeProductTemplateSemiproductPart(data) {
                return this.requestHandler.send({
                    method: 'removeProductTemplateSemiproductPart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.RemoveProductTemplateSemiproductPartRequest',
                    data: data
                });
            };

            /* WarePart */

            addProductTemplateWarePart(data) {
                return this.requestHandler.send({
                    method: 'addProductTemplateWarePart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.AddProductTemplateWarePartRequest',
                    data: data
                });
            };

            updateProductTemplateWarePart(data) {
                return this.requestHandler.send({
                    method: 'updateProductTemplateWarePart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.UpdateProductTemplateWarePartRequest',
                    data: data
                });
            };

            removeProductTemplateWarePart(data) {
                return this.requestHandler.send({
                    method: 'removeProductTemplateWarePart',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.RemoveProductTemplateWarePartRequest',
                    data: data
                });
            };

            /* ProdPlan */

            addProductTemplateProdPlanItem(data) {
                return this.requestHandler.send({
                    method: 'addProductTemplateProdPlanItem',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.AddProductTemplateProdPlanItemRequest',
                    data: data
                });
            };

            updateProductTemplateProdPlanItem(data) {
                return this.requestHandler.send({
                    method: 'updateProductTemplateProdPlanItem',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.UpdateProductTemplateProdPlanItemRequest',
                    data: data
                });
            };

            removeProductTemplateProdPlanItem(data) {
                return this.requestHandler.send({
                    method: 'removeProductTemplateProdPlanItem',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.RemoveProductTemplateProdPlanItemRequest',
                    data: data
                });
            };

            /* validators */

            validateProductTemplate(data) {
                return this.requestHandler.send({
                    method: 'validateProductTemplate',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.ValidateProductTemplateRequest',
                    data: data
                });
            };

            validateAllProductsTemplate(data) {
                return this.requestHandler.send({
                    method: 'validateAllProductsTemplate',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.producttemplates.ValidateAllProductTemplatesRequest',
                    data: data
                });
            };
        }

        return new ProductTemplateRequestServiceClass();
    }
})();
