/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.materialType')
        .service('MaterialTypeRequestService', MaterialTypeRequestService);

    /** @ngInject */
    function MaterialTypeRequestService(RequestServiceClass) {
        class MaterialTypeRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('MaterialType', 'materialtypes');
            }
        }

        return new MaterialTypeRequestServiceClass();
    }
})();
