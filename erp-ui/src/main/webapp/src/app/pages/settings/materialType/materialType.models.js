/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.materialType')
        .factory('MaterialTypeModel', MaterialTypeModel)
        .factory('MaterialTypeListModel', MaterialTypeListModel);

    /** @ngInject */
    function MaterialTypeModel(BaseModelClass, MaterialTypeRequestService) {
        return class MaterialTypeClass extends BaseModelClass {
            constructor(materialTypeData) {
                super(MaterialTypeRequestService, materialTypeData);
            }

            static getById(materialTypeId) {
                return new MaterialTypeClass().getById(materialTypeId);
            };
        }
    }

    /** @ngInject */
    function MaterialTypeListModel(ListModelClass, MaterialTypeRequestService, MaterialTypeModel) {
        return class MaterialTypesListClass extends ListModelClass {
            constructor() {
                super(MaterialTypeRequestService, MaterialTypeModel);
            }

            static list() {
                return new MaterialTypesListClass().refresh();
            }
        }
    }
})();
