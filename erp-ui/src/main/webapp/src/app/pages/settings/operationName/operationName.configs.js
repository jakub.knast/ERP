/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.operationName')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('OperationName',
            {
                vm: '$ctrl',
                id: 'OperationName',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['name', 'dimension', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'settings.operationName'},
                    name: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateOperationName', 'UpdateOperationName'],
            {
                id: 'OperationName',
                options: {},
                fields: [
                    {id: 'name', config: {templateOptions: {required: true}}}
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('OperationName',
            {
                id: 'OperationName',
                options: {},
                availableFields: {
                    name: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'settings.operationName',
                abstract: true,
                url: '/operacje_produkcyjne',
                title: 'Operacje produkcyjne',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 500,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista operacji produkcyjnych',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (OperationNameListModel) {
                                return OperationNameListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'OperationName',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Typ towaru',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Operacja produkcyjna';
                        },
                        resolve: {
                            elementModel: function ($stateParams, OperationNameModel) {
                                return OperationNameModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/ustawienia/operacje_produkcyjne', '/ustawienia/operacje_produkcyjne/lista');
    }
})();
