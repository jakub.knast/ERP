/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.professionGroup')
        .service('ProfessionGroupRequestService', ProfessionGroupRequestService);

    /** @ngInject */
    function ProfessionGroupRequestService(RequestServiceClass) {
        class ProfessionGroupRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('ProfessionGroup', 'professionGroup');
            }

            listProfessionGroupsWithOperationId(operationNameId) {
                return this.requestHandler.send({
                    method: 'listProfessionGroupsWithOperationId',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.professionGroup.ListProfessionGroupsWithOperationIdRequest',
                    data: {operationNameId: operationNameId}
                });
            };
        }

        return new ProfessionGroupRequestServiceClass();
    }
})();
