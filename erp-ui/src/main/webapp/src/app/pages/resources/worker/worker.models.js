/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.resources.worker')
        .factory('WorkerModel', WorkerModel)
        .factory('WorkerListModel', WorkerListModel);

    /** @ngInject */
    function WorkerModel(BaseModelClass, WorkerRequestService) {
        return class WorkerClass extends BaseModelClass {
            constructor(workerData) {
                super(WorkerRequestService, workerData);
            }

            static getById(workerId) {
                return new WorkerClass().getById(workerId);
            };
            
            fireWorker () {
                return this.runServiceFunc('fireWorker', this.data.id, {message: "Order.alert.cancelOrder"});
            };
            
            hireWorker () {
                return this.runServiceFunc('hireWorker', this.data.id, {message: "Order.alert.cancelOrder"});
            };
            
        }
    }

    /** @ngInject */
    function WorkerListModel(ListModelClass, WorkerRequestService, WorkerModel) {
        return class WorkersListClass extends ListModelClass {
            constructor() {
                super(WorkerRequestService, WorkerModel);
            }

            static list() {
                return new WorkersListClass().refresh();
            }
        }
    }
})();
