/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.resources.machine')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(nestDataListConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Machine',
            {
                vm: '$ctrl',
                id: 'Machine',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['name', 'machineCode', 'workersLinks', 'comments', 'MACHINE_STATE', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'resources.machine'},
                    name: null,
                    machineCode: null,
                    workersLinks: null,
                    comments: null,
                    MACHINE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateMachine', 'UpdateMachine'],
            {
                id: 'Machine',
                options: {},
                fields: [
                    {id: 'name', config: {templateOptions: {required: true}}},
                    {id: 'machineCode', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['CreateMachineWorkers', 'UpdateMachineWorkers'],
            {
                id: 'MachineWorkers',
                options: {},
                fields: [
                    {id: 'ref_worker', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Machine',
            {
                id: 'Machine',
                options: {},
                availableFields: {
                    name: null,
                    machineCode: null,
                    comments: null,
                    MACHINE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );

        PreviewConfigServiceProvider.addDefaultConfig('MachineWorkers',
            {
                id: 'MachineWorkers',
                options: {},
                availableFields: {
                    ref_worker: null,
                    comments: null
                }
            }
        );
    }

    /** @ngInject */
    function nestDataListConfig(NestDataListConfigServiceProvider) {
        NestDataListConfigServiceProvider.addDefaultConfig('MachineWorkers',
            {
                id: 'MachineWorkers',
                options: {
                    path: 'machineWorkers',
                    formlyConfig: 'MachineWorkers',
                    previewConfig: 'MachineWorkers',
                    viewTemplateUrl: 'app/core/templates/nestDataList/machineWorkers.html'
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'resources.machine',
                abstract: true,
                url: '/maszyny',
                title: 'Maszyny',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 100,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista maszyn',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (MachineListModel) {
                                return MachineListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'Machine',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Maszyna',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function(){
                            return 'Maszyna';
                        },
                        resolve: {
                            elementModel: function ($stateParams, MachineModel) {
                                return MachineModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }, {
                                name: 'workers',
                                url: '/pracownicy',
                                title: 'Przypisani pracownicy',
                                templateUrl: 'app/core/components/nestDataList/nestDataList.base.html',
                                controller: 'BaseNestDataListCtrl',
                                controllerAs: "$ctrl",
                                resolve: {
                                    configName: function () {
                                        return 'MachineWorkers';
                                    }
                                }
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/zasoby/maszyny', '/zasoby/maszyny/lista');
    }
})();
