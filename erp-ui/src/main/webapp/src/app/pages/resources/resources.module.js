(function () {
    'use strict';

    angular.module('JangraERP.pages.resources', [
        'JangraERP.pages.resources.machine',
        'JangraERP.pages.resources.worker'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('resources', {
                url: '/zasoby',
                abstract: true,
                template: '<ui-view />',
                title: 'Zasoby',
                sidebarMeta: {
                    icon: 'ion-person-stalker',
                    order: 500,
                },
            });
    }

})();
