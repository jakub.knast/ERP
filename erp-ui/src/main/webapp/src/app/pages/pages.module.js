(function () {
    'use strict';

    angular.module('JangraERP.pages', [
        'ui.router',

        'JangraERP.pages.dashboard',
        'JangraERP.pages.stocks',
        'JangraERP.pages.process',
        'JangraERP.pages.contractors',
        'JangraERP.pages.templates',
        'JangraERP.pages.resources',
        'JangraERP.pages.production',
        'JangraERP.pages.settings',

        'JangraERP.pages.other',

        'JangraERP.pages.form'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($urlRouterProvider) {
        $urlRouterProvider.otherwise('/dashboard');
    }

})();
