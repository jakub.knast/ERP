(function () {
    'use strict';

    angular.module('JangraERP.pages.dashboard')
        .directive('dashboardCalendar', dashboardCalendar);

    /** @ngInject */
    function dashboardCalendar() {
        return {
            restrict: 'E',
            controller: 'DashboardCalendarCtrl',
            templateUrl: 'app/pages/dashboard/dashboardCalendar/dashboardCalendar.html'
        };
    }
})();
