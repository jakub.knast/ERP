/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.dashboard')
        .service('TodoIssuesRequestService', TodoIssuesRequestService);

    /** @ngInject */
    function TodoIssuesRequestService(RequestServiceClass) {
        class TodoIssuesRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('TodoIssues', 'todoIssues');
            }
        }

        return new TodoIssuesRequestServiceClass();
    }
})();
