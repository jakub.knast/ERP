/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.contractors.seller')
        .factory('SellerModel', SellerModel)
        .factory('SellerListModel', SellerListModel);

    /** @ngInject */
    function SellerModel(BaseModelClass, SellerRequestService) {
        return class SellerClass extends BaseModelClass {
            constructor(sellerData) {
                super(SellerRequestService, sellerData);
            }

            static getById(sellerId) {
                return new SellerClass().getById(sellerId);
            };
        }
    }

    /** @ngInject */
    function SellerListModel(ListModelClass, SellerRequestService, SellerModel) {
        return class SellersListClass extends ListModelClass {
            constructor() {
                super(SellerRequestService, SellerModel);
            }

            static list() {
                return new SellersListClass().refresh();
            }
        }
    }
})();
