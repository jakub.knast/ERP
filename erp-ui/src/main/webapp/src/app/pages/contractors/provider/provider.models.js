/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.contractors.provider')
        .factory('ProviderModel', ProviderModel)
        .factory('ProviderListModel', ProviderListModel);

    /** @ngInject */
    function ProviderModel(BaseModelClass, ProviderRequestService) {
        return class ProviderClass extends BaseModelClass {
            constructor(providerData) {
                super(ProviderRequestService, providerData);
            }

            static getById(providerId) {
                return new ProviderClass().getById(providerId);
            };
        }
    }

    /** @ngInject */
    function ProviderListModel(ListModelClass, ProviderRequestService, ProviderModel) {
        return class ProvidersListClass extends ListModelClass {
            constructor() {
                super(ProviderRequestService, ProviderModel);
            }

            static list() {
                return new ProvidersListClass().refresh();
            }
        }
    }
})();
