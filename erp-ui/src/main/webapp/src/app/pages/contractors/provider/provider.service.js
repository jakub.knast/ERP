/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.contractors.provider')
        .service('ProviderRequestService', ProviderRequestService);

    /** @ngInject */
    function ProviderRequestService(RequestServiceClass) {
        class ProviderRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Provider', 'provider');
            }

            findProviderForMaterial(data) {
                return this.requestHandler.send({
                    method: 'findProviderForMaterial',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.' + this.subName + '.FindProviderForMaterialRequest',
                    data: data
                });
            };
        }

        return new ProviderRequestServiceClass();
    }
})();
