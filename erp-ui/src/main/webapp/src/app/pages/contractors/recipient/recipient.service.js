/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.contractors.recipient')
        .service('RecipientRequestService', RecipientRequestService);

    /** @ngInject */
    function RecipientRequestService(RequestServiceClass) {
        class RecipientRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Recipient', 'recipient');
            }
        }

        return new RecipientRequestServiceClass();
    }
})();
