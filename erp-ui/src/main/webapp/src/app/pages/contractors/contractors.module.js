(function () {
    'use strict';

    angular.module('JangraERP.pages.contractors', [
        'JangraERP.pages.contractors.recipient',
        'JangraERP.pages.contractors.provider',
        'JangraERP.pages.contractors.seller'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('contractors', {
                url: '/Kontrahenci',
                abstract: true,
                template: '<ui-view />',
                title: 'Kontrahenci',
                sidebarMeta: {
                    icon: 'ion-earth',
                    order: 400,
                },
            });
    }

})();
