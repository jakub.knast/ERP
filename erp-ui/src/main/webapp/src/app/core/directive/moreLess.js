(function () {
    angular.module('JangraERP.core')
        .filter('subString', SubStringFilter)
        .directive('moreLess', MoreLessDirective);

    function SubStringFilter() {
        return function(str, start, end) {
            if (str !== undefined) {
                return str.substr(start, end);
            }
        }
    }

    function MoreLessDirective() {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                text: '='
            },
            template: '<div><span ng-show="largeText"> {{ text | subString :0 :end }} ...<a href="javascript:;" ng-click="showMore()" ng-show="isShowMore">więcej</a><a href="javascript:;" ng-click="showLess()" ng-hide="isShowMore">mniej</a></span><span ng-hide="largeText">{{ text }}</span></div> ',
            link: function(scope, element, attrs) {
                const limit = ~~attrs.limit || 25;
                scope.text = scope.text || '';

                if(scope.text.length <= limit) {
                    scope.largeText = false;
                }else {
                    scope.end = limit;
                    scope.largeText = true;
                    scope.isShowMore = true;
                }

                scope.showMore = function() {
                    scope.end = scope.text.length;
                    scope.isShowMore = false;
                };

                scope.showLess = function() {
                    scope.end = limit;
                    scope.isShowMore = true;
                };
            }
        };
    }
}());
