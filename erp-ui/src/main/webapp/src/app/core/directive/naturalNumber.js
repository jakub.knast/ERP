(function () {
    'use strict';

    angular.module('JangraERP.core')
        .directive('naturalNumber', naturalNumber);

    function naturalNumber() {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                let def = parseInt(attrs.min) || 0;
                if (!ngModelCtrl) {
                    return;
                }

                $(element).keypress(function (e) {
                    if (e.keyCode >= 44 && e.keyCode <= 46) {
                        e.preventDefault();
                    }
                });

                ngModelCtrl.$parsers.push(function (val) {
                    val = Math.floor(val || def);
                    ngModelCtrl.$setViewValue(val);
                    ngModelCtrl.$render();
                    return val;
                });
            }
        };
    }
}());
