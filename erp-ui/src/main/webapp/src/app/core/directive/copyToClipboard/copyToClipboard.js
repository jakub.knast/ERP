(function () {
    'use strict';

    angular.module('JangraERP.core')
        .directive('copyToClipboard', copyToClipboard);

    function copyToClipboard(AlertService) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function copyToClipboard(event) {
                    let $temp = $("<input>");
                    $("body").append($temp);
                    let text = $(element).text();
                    $temp.val(text).select();
                    document.execCommand("copy");
                    $temp.remove();
                    AlertService.info('Main.actions.copy', 'Kopiowanie', {text : text}, {timeOut: 2000, extendedTimeOut: 500})
                });
            }
        };
    }
}());
