(function () {
    'use strict';

    angular.module('JangraERP.core')
        .directive('oneBindHtmlCompile', oneBindHtmlCompile);

    function oneBindHtmlCompile($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.html(scope.$eval(attrs.oneBindHtmlCompile));
                $compile(element.contents())(scope);
            }
        };
    }
}());
