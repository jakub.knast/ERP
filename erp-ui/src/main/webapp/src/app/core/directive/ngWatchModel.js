(function () {
    angular.module('JangraERP.core')
        .directive('ngWatchModel', NgWatchModel);


    function NgWatchModel() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function($scope, element, attrs, ngModelCtrl) {
                $scope.$watch(function(){
                    return ngModelCtrl.$modelValue;
                }, function(newValue, oldValue){
                    $scope[attrs.ngWatchModel](newValue, oldValue);
                });
            }
        };
    }
}());
