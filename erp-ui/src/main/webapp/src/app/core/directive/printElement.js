(function () {
    'use strict';

    angular.module('JangraERP.core')
        .directive('printElement', printElement);

    function printElement(UtilsService) {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                $scope.print = function () {
                    UtilsService.print(attrs.printElement || element)
                }
            }
        };
    }
}());
