(function () {
    'use strict';

    angular.module('JangraERP.core')
        .directive('decimal', decimal)
        .directive('decimalPrice', decimalPrice);

    function decimal($locale) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                const decimalSeparator = $locale.NUMBER_FORMATS.DECIMAL_SEP;
                const groupSeparator = $locale.NUMBER_FORMATS.GROUP_SEP;

                let min = parseNumberAttr(attrs.min, -Infinity);
                let max = parseNumberAttr(attrs.max, Infinity);
                let precision = parseNumberAttr(Math.abs(attrs.precision), 2);
                let regex = /^(-)?(\d+)?([\,|\.])?(\d*)?$/;
                let lastValidValue;

                if (!ngModelCtrl) {
                    return;
                }

                element.bind('blur', onBlur);
                element.bind('focus', onFocus);
                ngModelCtrl.$parsers.push(parseViewValue);
                ngModelCtrl.$formatters.push(formatViewValue);

                function parseNumberAttr(value, defaultValue){
                    return isNaN(parseInt(value)) ? defaultValue : parseInt(value);
                }

                function onBlur() {
                    let value = ngModelCtrl.$modelValue;
                    if (!angular.isUndefined(value)) {
                        ngModelCtrl.$viewValue = formatViewValue(value);
                        // ngModelCtrl.$setViewValue(formatViewValue(value));
                        ngModelCtrl.$render();
                    }
                }

                function onFocus() {
                    let value = ngModelCtrl.$modelValue;
                    if (!angular.isUndefined(value)) {
                        ngModelCtrl.$viewValue = ("" + value).replace(".", decimalSeparator);
                        // ngModelCtrl.$setViewValue(("" + value).replace(".", decimalSeparator));
                        ngModelCtrl.$render();
                    }
                }

                function parseViewValue(value) {
                    if (angular.isUndefined(value)) {
                        value = 0;
                    }

                    value = ("" + value).replace(decimalSeparator, '.');

                    if (ngModelCtrl.$isEmpty(value)) {
                        lastValidValue = 0;
                    } else {
                        if (regex.test(value)) {
                            if (value > max) {
                                lastValidValue = max;
                                ngModelCtrl.$setViewValue(lastValidValue);
                                ngModelCtrl.$render();
                            }
                            else if (value < min) {
                                lastValidValue = min;
                                ngModelCtrl.$setViewValue(lastValidValue);
                                ngModelCtrl.$render();
                            }
                            else {
                                lastValidValue = parseFloat((parseFloat(value) || 0).toFixed(precision));
                            }
                        }
                        else {
                            ngModelCtrl.$setViewValue(lastValidValue);
                            ngModelCtrl.$render();
                        }
                    }

                    return lastValidValue;
                }

                function formatViewValue(value) {
                    if (angular.isUndefined(value)) {
                        return null;
                    }
                    let parts = value.toFixed(precision).split(decimalSeparator);
                    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, groupSeparator);
                    return parts.join(decimalSeparator);
                }
            }
        };
    }


    function decimalPrice() {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }

                ngModelCtrl.$parsers.push(function (val) {
                    if (!angular.isNumber(val) || val < 0) {
                        let val = 0;
                    }

                    val = Math.round(val * 100) / 100;
                    ngModelCtrl.$setViewValue(val);
                    ngModelCtrl.$render();

                    return val;
                });
            }
        };
    }
}());
