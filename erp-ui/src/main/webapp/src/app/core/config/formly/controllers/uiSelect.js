/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core')
        .controller('UiSelectCtrl', UiSelectCtrl);

    /** @ngInject */
    function UiSelectCtrl($scope, $injector) {
        let page = 0;
        let pageSize = $scope.to.pageSize || 20;
        let size = Infinity;
        let projection = ($scope.to.projection || $scope.to.searchFilter || []).map(field => ({
            field: field,
            include: true
        }));
        let searchCache = null;
        let requestService = $injector.get($scope.to.requestService);
        if (angular.isArray($scope.to.services)) {
            $scope.services = $scope.to.services.reduce((servicesMap, service) => {
                servicesMap[service] = $injector.get(service);
                return servicesMap;
            }, {});
        }

        function generateRequestFilter(search) {
            let filter = {
                constraints: [{
                    value: true,
                    field: 'removed',
                    constraintType: 'NOT_EQUALS'
                }],
                criteriaFilters: [{value: 'OR'}],
                value: 'AND'
            };
            if (search && $scope.to.searchFilter) {
                filter.criteriaFilters[0].constraints = $scope.to.searchFilter.map(field => ({
                    value: search,
                    field: field,
                    constraintType: 'LIKE_IGNORE_CASE'
                }));
            }
            if (angular.isFunction($scope.to.customFilter)) {
                $scope.to.customFilter($scope, filter)
            }
            if (angular.isArray($scope.to.searchFilter)) {
                filter.projection = $scope.to.searchFilter.map(item => {
                    if (angular.isString(item)) {
                        return {visible: true, value: item};
                    } else {
                        return item;
                    }
                });
            }
            return filter;
        }

        $scope.to.requestMoreItems = function () {
            ++page;
            if(page * pageSize <= size){
                return requestService.list({
                    page: page,
                    pageSize: pageSize,
                    projection: projection,
                    criteriaFilters: generateRequestFilter(searchCache)
                }).then(function (responseData) {
                    size = responseData.size;
                    $scope.to.items.push(...responseData.elements);
                });
            }
        };

        $scope.to.refresh = function (search, onRefresh) {
            if (searchCache !== search) {
                page = 0;
                onRefresh && onRefresh();
                return requestService.list({
                    page: page,
                    pageSize: pageSize,
                    projection: projection,
                    criteriaFilters: generateRequestFilter(search)
                }).then(function (responseData) {
                    searchCache = search;
                    size = responseData.size;
                    $scope.to.items = responseData.elements;
                });
            }
        };
    }
})();
