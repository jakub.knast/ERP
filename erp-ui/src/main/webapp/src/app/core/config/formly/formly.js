(function () {
    'use strict';

    angular.module('JangraERP.core')
        .config(formlyConfigurationProvider)
        .run(UploadTypeConfiguration)
        .run(formlyConfiguration);

    /** @ngInject */
    function formlyConfigurationProvider(formlyConfigProvider) {
        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        /**
         * Wrappers
         */
        formlyConfigProvider.setWrapper([
            {
                name: 'staticValidation',
                templateUrl: 'app/core/config/formly/templates/staticValidation.html'
            },
            {
                name: 'horizontalLabel',
                templateUrl: 'app/core/config/formly/templates/horizontalLabel.html'
            },
            {
                name: 'verticalLabel',
                templateUrl: 'app/core/config/formly/templates/verticalLabel.html'
            }
        ]);

        let formlyTypesConfig = [
            {
                name: 'input',
                templateUrl: 'app/core/config/formly/templates/input.html'
            }, {
                name: 'unitable',
                templateUrl: 'app/core/config/formly/templates/unitable.html'
            }, {
                name: 'price',
                templateUrl: 'app/core/config/formly/templates/price.html'
            }, {
                name: 'naturalNumber',
                templateUrl: 'app/core/config/formly/templates/naturalNumber.html'
            }, {
                name: 'maskInput',
                templateUrl: 'app/core/config/formly/templates/maskInput.html'
            }, {
                name: 'phone',
                templateUrl: 'app/core/config/formly/templates/phone.html',
                link: function (scope) {
                    scope.isValid = true;
                }
            }, {
                name: 'postalCode',
                templateUrl: 'app/core/config/formly/templates/postalCode.html',
                link: function (scope) {
                    scope.isValid = true;
                }
            }, {
                name: 'select',
                templateUrl: 'app/core/config/formly/templates/select.html'
            }, {
                name: 'textarea',
                templateUrl: 'app/core/config/formly/templates/textarea.html'
            }, {
                name: 'text',
                templateUrl: 'app/core/config/formly/templates/text.html'
            }, {
                name: 'color',
                templateUrl: 'app/core/config/formly/templates/color.html'
            }, {
                name: 'datepicker',
                templateUrl: 'app/core/config/formly/templates/datepicker.html'
            }, {
                name: 'checkbox',
                template: function (config) {
                    return '<label class="checkbox-inline custom-checkbox nowrap">' +
                        '   <input type="checkbox"  id="{{::options.key}}" ng-model="model[options.key]"' +
                        (config.templateOptions.readonly ? " disabled=\"disabled\"" : '') +
                        (config.templateOptions.trueValue ? " ng-true-value=\"'{{::to.trueValue}}'\"" : '') +
                        (config.templateOptions.falseValue ? " ng-false-value=\"'{{::to.falseValue}}'\"" : '') +
                        '>' +
                        '   <span></span>' +
                        '</label>'
                }
            }, {
                name: 'referenceBase',
                template: referenceTpl,
                controller: 'UiSelectCtrl'
            }, {
                name: 'multiReferenceBase',
                template: multiReferenceTpl,
                controller: 'UiSelectCtrl',
                link: function (scope) {
                    scope.extendModel = function () {
                        scope.model[scope.options.key + 'Ids'] = scope.model[scope.options.key] && scope.model[scope.options.key].map(v => v.id) || [];
                    };
                }
            }, {
                name: 'templateBase',
                template: templateTpl,
                controller: 'UiSelectCtrl'
            }, {
                name: 'template',
                template: templateTpl
            }, {
                name: 'crossBase',
                template: templateTpl,
                controller: 'UiSelectCtrl',
                link: function (scope) {
                    let crossName = scope.options.key + 'Id';

                    scope.change = function (selectedItem) {
                        scope.model[crossName] = selectedItem.id;
                    };
                }
            }, {
                name: 'cross',
                template: templateTpl,
                link: function (scope) {
                    let crossName = scope.options.key + 'Id';

                    scope.change = function (selectedItem) {
                        scope.model[crossName] = selectedItem.id;
                    };
                }
            }, {
                name: 'dimension',
                template: '<dimension ng-model="model[options.key]"></dimension>'
            }, {
                name: 'constans',
                templateUrl: 'app/core/config/formly/templates/constans.html',
                controller: 'ConstansCtrl'
            }, {
                name: 'autoFillInput',
                templateUrl: 'app/core/config/formly/templates/input.html',
                controller: 'AutoFillCtrl'
            }, {
                name: 'autoFillUnitable',
                templateUrl: 'app/core/config/formly/templates/unitable.html',
                controller: 'AutoFillCtrl2'
            }, {
                name: 'autoFillUnitable2',
                templateUrl: 'app/core/config/formly/templates/unitable.html',
                controller: 'AutoFillCtrl2'
            }, {
                // I think it should not be there
//                name: 'tableFormly',
//                templateUrl: 'app/core/config/formly/templates/tableFormly.html',
//                controller: 'AutoFillCtrl'
//            }, {
                name: 'dimensionData',
                templateUrl: 'app/core/config/formly/templates/dimensionData.html'
            }, {
                name: 'repeatSection',
                templateUrl: 'app/core/config/formly/templates/repeatSection.html',
                controller: 'RepeatSectionCtrl'
            }, {
                name: 'tableFormly',
                templateUrl: 'app/core/config/formly/templates/tableFormly.html',
                controller: 'TableFormlyCtrl'
            }
        ];

        formlyTypesConfig.forEach(formlyTypeConfig => {
            formlyConfigProvider.setType(formlyTypeConfig);

            let capTypeName = capitalizeFirstLetter(formlyTypeConfig.name);

            formlyConfigProvider.setType({
                name: 'horizontal' + capTypeName,
                extends: formlyTypeConfig.name,
                wrapper: ['staticValidation', 'horizontalLabel']
            });

            formlyConfigProvider.setType({
                name: 'vertical' + capTypeName,
                extends: formlyTypeConfig.name,
                wrapper: ['staticValidation', 'verticalLabel']
            });
        });

    }

    function UploadTypeConfiguration(formlyConfig, $window) {
        formlyConfig.setType({
            name: 'upload',
            template: `<input id="{{::options.key}}" type="file" class="form-control" ng-model="model[options.key]" placeholder="{{::to.placeholder}}">
                    <span one-bind-html-compile="to.html"></span>`,
            link: function (scope, el, attrs) {
                el.on("change", function (changeEvent) {
                    var file = changeEvent.target.files[0];
                    if (file) {
                        if (file.size > 26214400) {
                            alert("Plik jest za duży. Maksymalna dopuszczalna wielkośc pliku to 25MB.");
                            changeEvent.target.value = '';
                            scope.fc.$setViewValue(undefined);
                        } else {
                            var fd = scope.formOptions.formState.formData = scope.formOptions.formState.formData || new FormData();
                            fd.append(scope.options.key, file);
                            var fileProp = {};
                            for (var properties in file) {
                                if (!angular.isFunction(file[properties])) {
                                    fileProp[properties] = file[properties];
                                }
                            }
                            scope.fc.$setViewValue(fileProp.name);
                        }

                    } else {
                        scope.fc.$setViewValue(undefined);
                    }
                });
                el.on("focusout", function (focusoutEvent) {
                    if ($window.document.activeElement.id === scope.id) {
                        scope.$apply(function (scope) {
                            scope.fc.$setUntouched();
                        });
                    } else {
                        scope.fc.$validate();
                    }
                });

            }
        });

        formlyConfig.setType({
            name: 'horizontalUpload',
            extends: 'upload',
            wrapper: ['staticValidation', 'horizontalLabel']
        });
    }

    /** @ngInject */
    function formlyConfiguration(formlyConfig) {
        formlyConfig.extras.errorExistsAndShouldBeVisibleExpression = 'fc.$touched || form.$submitted';
    }

    function multiReferenceTpl(config) {
        return '<ui-select multiple ng-model="model[options.key]" theme="bootstrap" ng-disabled="to.disabled" title="Reference" on-select="extendModel()" ng-init="extendModel()">' +
            '    <ui-select-match placeholder="{{::to.placeholder}}">{{$item[to.valueProp]}}</ui-select-match>' +
            '    <ui-select-choices repeat="item in to.items" refresh="to.refresh($select.search)" refresh-delay="{{::to.refreshDelay}}">' +
            '        <div ng-bind-html="item[to.valueProp] | highlight: $select.search"></div>' +
            (config.templateOptions.smallHtml ? ('<small>' + config.templateOptions.smallHtml + '</small>') : '') +
            '    </ui-select-choices>' +
            '</ui-select>' +
            (config.templateOptions.crossHtml ? ('<div style="margin-top: 5px">' + config.templateOptions.crossHtml + '</div>') : '');
    }

    function referenceTpl(config) {
        return '<ui-select ng-model="model[options.key]" theme="bootstrap" ng-disabled="to.disabled" title="Reference"' +
            '           on-select="model[options.key + \'Id\'] = model[options.key].id" reach-infinity="to.requestMoreItems()"' +
            '           ng-init="model[options.key + \'Id\'] = model[options.key].id">' +
            '    <ui-select-match placeholder="{{::to.placeholder}}">{{$select.selected[to.valueProp]}}</ui-select-match>' +
            '    <ui-select-choices repeat="item in to.items" refresh="to.refresh($select.search, $select.scrollTop)"' +
            '                       refresh-delay="{{::to.refreshDelay}}">' +
            '        <div ng-bind-html="item[to.valueProp] | highlight: $select.search"></div>' +
            (config.templateOptions.smallHtml ? ('<small>' + config.templateOptions.smallHtml + '</small>') : '') +
            '    </ui-select-choices>' +
            '</ui-select>' +
            (config.templateOptions.crossHtml ? ('<div style="margin-top: 5px">' + config.templateOptions.crossHtml + '</div>') : '');
    }

    function templateTpl(config) {
        return '<ui-select ng-model="model[options.key]" theme="bootstrap" ng-disabled="to.disabled" title="Template"' +
            '           on-select="to.select($model)" reach-infinity="to.requestMoreItems()">' +
            '    <ui-select-match placeholder="{{::to.placeholder}}">' +
            '        <div>' + config.templateOptions.selectedItemHtml + '</div>' +
            '    </ui-select-match>' +
            '    <ui-select-choices repeat="item.id as item in to.items" refresh="to.refresh($select.search, $select.scrollTop)" refresh-delay="{{::to.refreshDelay}}">' +
            '        <div ng-bind-html="item[to.valueProp] | highlight: $select.search"></div>' +
            (config.templateOptions.smallHtml ? ('<small>' + config.templateOptions.smallHtml + '</small>') : '') +
            '    </ui-select-choices>' +
            '</ui-select>' +
            (config.templateOptions.crossHtml ? ('<div style="margin-top: 5px">' + config.templateOptions.crossHtml + '</div>') : '');
    }
})();
