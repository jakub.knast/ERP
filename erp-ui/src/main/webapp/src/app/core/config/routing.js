(function () {
    'use strict';

    angular.module('JangraERP.core')
        .run(RoutingConfig);

    /** @ngInject */
    function RoutingConfig($rootScope, /*$location, $state, $stateParams, */$uibModalStack) {

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            //console.warn('$stateChangeSuccess', event, toState, toParams, fromState, fromParams);
            $uibModalStack.dismissAll()
        });

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
            //console.warn('$stateChangeStart', event, toState, toParams, fromState, fromParams, options);
        });

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            //console.warn('$stateChangeError', event, toState, toParams, fromState, fromParams, error);
        });
    }

})();
