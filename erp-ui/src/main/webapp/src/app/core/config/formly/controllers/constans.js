/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core')
        .controller('ConstansCtrl', ConstansCtrl);

    /** @ngInject */
    function ConstansCtrl($scope, SystemService) {
        $scope.getConsts = function (search) {
            let newOptions = $scope.to.items.slice();
            if (search && newOptions.indexOf(search) === -1) {
                newOptions.unshift(search);
            }
            return newOptions;
        };

        $scope.selectConst = function (select, model) {
            if (model && $scope.to.items.indexOf(model) === -1 && $scope.to.constName) {
                SystemService.addConst($scope.to.constName, model);
            }
        };

        $scope.clear = function (select) {
            select.search = '';
            select.select(undefined);
        };
    }
})();
