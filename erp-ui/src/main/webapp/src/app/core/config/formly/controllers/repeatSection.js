/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core')
        .controller('RepeatSectionCtrl', RepeatSectionCtrl);

    /** @ngInject */
    function RepeatSectionCtrl($scope, FormlyConfigService) {
        let unique = 1;
        $scope.formOptions = {formState: $scope.formState};
        $scope.addNew = addNew;

        $scope.copyFields = copyFields;

        //$scope.formlyConfig = FormlyConfigService.getFormlyConfig(options.formlyConfig || config.id);

        function copyFields(fields) {
            fields = angular.copy(fields);
            addRandomIds(fields);
            return fields;
        }

        function addNew() {
            $scope.model[$scope.options.key] = $scope.model[$scope.options.key] || [];
            $scope.model[$scope.options.key].push({});
        }

        function addRandomIds(fields) {
            unique++;
            angular.forEach(fields, function (field, index) {
                if (field.fieldGroup) {
                    addRandomIds(field.fieldGroup);
                    return; // fieldGroups don't need an ID
                }

                if (field.templateOptions && field.templateOptions.fields) {
                    addRandomIds(field.templateOptions.fields);
                }

                field.id = field.id || (field.key + '_' + index + '_' + unique + getRandomInt(0, 9999));
            });
        }

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }
    }
})();
