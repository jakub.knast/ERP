/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.list')
        .directive('list', ListComponent)
        .directive('multiInput', MultiInput);

    /** @ngInject */
    function MultiInput() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attr, ngModelCtrl) {

                function parse(value) {
                    if (value) {
                        let keys = attr.multiInput.split('|');
                        keys.value = value;
                        return keys;
                    }
                    delete keys.value;
                    return null;
                }

                ngModelCtrl.$parsers.push(parse);
            }
        }
    }

    /** @ngInject */
    function ListComponent($compile, $templateCache, ListConfigService, $uibModal, SystemService) {
        return {
            restrict: 'E',
            scope: {
                data: "=?",
                model: "=",
                config: "="
            },
            template: '',
            link: function (scope, element, attr) {
                const postId = "ListComponent";
                let config = (typeof scope.config === 'string') ? ListConfigService.getListConfig(scope.config) : scope.config;

                // compile template from cache or generate it
                compileTemplate($templateCache.get(config.id + postId) || genarateTemplate(config));

                let entityStateMap = SystemService.getEntityStateMap( config.id + 'Entity');
                scope.options = SystemService.getAllOptions();

                // vm||scope from parent controller
                scope.$ctrl = config.$ctrl ? scope.$parent[config.$ctrl] : scope.$parent;
                scope.availableColumns = config.availableColumns;

                scope.maxSize = 5;

                scope.sortBy = function (event, key) {
                    scope.model.sortBy(key);
                    $(event.target).removeClass('sort-asc sort-desc').addClass(scope.model.sort.sortOrder === 'DESCENDING' ? 'sort-desc' : "sort-asc");
                    scope.listPromise = scope.model.refresh();
                };

                scope.changePageSize = function () {
                    scope.listPromise = scope.model.refresh();
                };

                scope.changePage = function () {
                    scope.listPromise = scope.model.refresh();
                };

                scope.search = function () {
                    scope.listPromise = scope.model.refresh();
                };
                scope.searchDate = function (value) {
                    if (value && value.startDate && value.endDate) {
                        scope.listPromise = scope.model.refresh();
                    }
                };

                scope.datepickerOptions = {
                    startingDay: 1,
                    closeText: "Zamknij"
                };

                scope.customize = function () {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'app/core/components/list/list.customize.modal.html',
                        size: 'md',
                        backdrop: 'static',
                        controller: 'ListCustomizeCtrl',
                        resolve: {
                            listConfig: config,
                            listGenerate: function () {
                                return function (config) {
                                    compileTemplate(genarateTemplate(config));
                                }
                            }
                        }
                    }).result.then(angular.noop, angular.noop);
                };

                scope.cleanSearchModel = function () {
                    scope.model.resetFilter();
                    scope.listPromise = scope.model.refresh();
                };

                if (config.options.selectable) {
                    scope.selected = [];
                    scope.selectedHashMap = {};
                    scope.selectedAll = false;

                    function clearSelectModels() {
                        scope.selected = [];
                        scope.selectedHashMap = {};
                        scope.selectedAll = false;
                    }

                    clearSelectModels();

                    scope.select = function (key, value) {
                        let index = scope.selected.indexOf(key);
                        if (value && index === -1) {
                            scope.selected.push(key);
                        } else if (!value && index > -1) {
                            scope.selected.splice(index, 1);
                        }
                        scope.selectedHashMap[key] = value;
                    };

                    scope.selectOne = function (key) {
                        clearSelectModels();
                        scope.select(key, true)
                    };

                    scope.selectAll = function () {
                        scope.model.elements.forEach(element => {
                            scope.select(element.id, scope.selectedAll)
                        })
                    };
                }

                scope.menuOptions = function (element) {
                    let item = element.item;
                    config.options.selectable && scope.selectOne(item.id);

                    let menu = config.controls.menu.map(menuOption => {
                        if (menuOption) {
                            return {
                                text: menuOption.text || null,
                                html: menuOption.html || null,
                                compile: menuOption.compile || false,
                                click: menuOption.click ? function ($itemScope) {
                                    return menuOption.click($itemScope.item, scope)
                                } : null,
                                displayed: menuOption.displayed ? function ($itemScope) {
                                    return menuOption.displayed($itemScope.item, scope)
                                } : null
                            }
                        }
                        return null;
                    });

                    if(entityStateMap){
                        let state = item.data.state;
                        let dynamicMenuConfig = entityStateMap.verticlesHashMap[state];

                        if(dynamicMenuConfig){
                            if(angular.isArray(dynamicMenuConfig.adjacents) && dynamicMenuConfig.adjacents.length){
                                menu = menu.concat(null, dynamicMenuConfig.adjacents.map(c => {
                                    return {
                                        html: '<a class=btn-with-icon href><i class=ion-flash></i>' + c.methodVisibeName + '</a>',
                                        click: function(){scope.$ctrl[c.method](item)}
                                    }
                                }))
                            }

                            if(dynamicMenuConfig.canBeRemoved){
                                menu.push(null, {
                                    html: '<a class=btn-with-icon href><i class=ion-trash-b></i>Usuń</a>',
                                    click: function(){scope.$ctrl.modalRemove(item)}
                                })
                            }

                            if(dynamicMenuConfig.canBeRestored){
                                menu.push(null, {
                                    html: '<a class=btn-with-icon href><i class=ion-trash-b></i>Przywróć</a>',
                                    click: function(){item.restore()}
                                })
                            }
                        }
                    }

                    return menu;
                };

                function genarateTemplate(listConfig) {
                    let title = '';
                    let search = '';
                    let tbody = '';
                    let controls = '';

                    let options = listConfig.options || {};

                    if (options.selectable) {
                        title += '<th class="short-column">#</th>';
                        search += '<th><label class="checkbox-inline custom-checkbox nowrap">' +
                            '   <input type="checkbox" ng-model="selectedAll" ng-change="selectAll()"><span></span>' +
                            '</label></th>';
                        tbody +=
                            '<td ng-click="$event.stopPropagation();"><label class="checkbox-inline custom-checkbox nowrap">' +
                            '   <input type="checkbox" ng-model="selectedHashMap[item.id]" ng-change="select(item.id, selectedHashMap[item.id])"><span></span>' +
                            '</label></td>';
                    }

                    listConfig.columns.forEach(function (column, index) {
                        title += '<th class="columnSortable ' + (column.sortList ? column.sortList : '') + (column.short ? ' short-column' : '') + '" ng-click="::sortBy($event, \'' + column.key + '\')" ng-class="{\'active\': model.sort.field === \'' + column.key + '\'}">' + column.templateOptions.label + '</th>';
                        tbody += column.tbody;
                        search += column.search;
                    });

                    listConfig.extends.forEach(function (extend) {
                        title += extend.title || '<th></th>';
                        search += extend.search || '<th></th>';
                        tbody += extend.tbody || '<td></td>';
                    });

                    if (listConfig.controls) {
                        controls +=
                            `<div class="listControls">
                                <div class="left">${listConfig.controls.leftControl || ''}</div>
                                <div class="right">${listConfig.controls.rightControl || ''}</div>
                            </div>`;
                    }

                    title = '<tr class="listSortable">' + title + '</tr>';
                    search = '<tr>' + search + '</tr>';

                    let thead = '<thead>' + title + (listConfig.options.hideSearch ? '' : search) + '</thead>';
                    tbody = '<tbody><tr context-menu="menuOptions" context-menu-empty-text="\'brak elementów\'" ng-repeat="item in model.elements"' + (options.selectable ? 'ng-class="{\'selected\': selectedHashMap[item.id], \'removed\': item.data.removed}" ng-click="selectOne(item.id)"' : '') + '>' + tbody + '</tr></tbody>';
                    let tfoot = '<tfoot><tr><td colspan="' + (config.columns.length + (listConfig.controls ? 1 : 0)) + '"><div class="col-md-3"><select class="form-control selectpicker show-tick" title="Rows on page" selectpicker ng-model="model.pageSize" ng-options="i for i in model.pageSizeList" ng-change="changePageSize()"></select></div><div class="text-center col-md-6"><ul uib-pagination total-items="model.size" items-per-page="model.pageSize" ng-model="model.page" max-size="maxSize" boundary-links="true" boundary-link-numbers="true" rotate="true" force-ellipses="true" ng-change="changePage()" first-text="&laquo;" previous-text="&lsaquo;" next-text="&rsaquo;" last-text="&raquo;"></ul></div></td></tr></tfoot>';
                    let table = '<table id="' + (listConfig.id || 'formly') + 'ListTable" cg-busy="listPromise" class="table ' + (listConfig.controls ? 'erp-table' : '') + '">' + thead + tbody + (listConfig.options.hideTfoot ? '' : tfoot) + '</table>';

                    return $templateCache.put(listConfig.id + postId, controls + table);
                }

                function compileTemplate(template) {
                    $(element).empty(); // TODO: destroy bind
                    element.append($compile(template)(scope));
                }
            }
        };
    }
})();
