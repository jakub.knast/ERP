(function () {
    'use strict';

    angular.module('JangraERP.core.components')
            .directive('myTable', myTable);

    /** @ngInject */
    function myTable($compile) {

        var controllerFunction = function ($scope, BaseModel, DeliveryOrderService, ProviderService) {
            console.log($scope.inputModel);

            $scope._addDeliveryOrder = function (addData) {
                DeliveryOrderService.addDeliveryOrder(addData).then(
                        function (data) {
                            let model = $scope.inputModel;
                            Object.keys(model).forEach(function (key) {
                                model[key].count = '';
                            });
                        },
                        function (data) {
                            console.log("error: " + data)
                        });
            };

            $scope._findMaterialAtProviders = function (materialTemplateId, materialCode) {
                ProviderService.findProviderForMaterial(materialTemplateId, materialCode).then(
                        function (responseData) {
                            $scope.assortmentForMaterial = responseData.assortmentForMaterialResponse;
                           $scope.genarateTemplate($scope.assortmentForMaterial);                           
                            BaseModel.AlertService.success('Zakończono pobieranie asortymentu');
                        },
                        function (responseData) {
                            console.log("error: " + responseData);                            
                            BaseModel.AlertService.error('Błąd podczas pobierania asortymentu ' + responseData.message.message);
                        });
            };
        };

        var linkFunction = function (scope, element, attr, controller) {
            function init() {
                scope.assortmentForMaterial = [];
                genarateTemplate(scope.assortmentForMaterial);
            }
            init();

            scope.findMaterialAtProviders = function () {
                console.log(scope.item);
                let materialTemplateId = scope.item.data.materialTemplate.id;
                let materialCode = scope.item.data.materialTemplate.materialCode;
                scope._findMaterialAtProviders(materialTemplateId, materialCode);
            };

            scope.addToShoppingCart = function () {
                let addData = {};
                addData.providerId = '';
                addData.providerName = "";
                addData.positions = [];

                let datas = {};

                let model = scope.inputModel;
                console.table(model);
                Object.keys(model).forEach(function (key) {
                    if (model[key] && model[key].count && model[key].count !== '') {
                        console.log("has filled property: " + key + " : " + model[key].count);

                        let providerId = model[key].providerId;
                        if (datas[providerId]) {
                            datas[providerId].positions.push({assortmentId: key, count: model[key].count});
                        } else {
                            datas[providerId] = {};
                            datas[providerId].providerId = model[key].providerId;
                            datas[providerId].providerName = model[key].providerName;
                            datas[providerId].positions = [];
                            datas[providerId].positions.push({assortmentId: key, count: model[key].count});
                        }
                    }
                });

                Object.keys(datas).forEach(function (providerId) {
                    scope._addDeliveryOrder(datas[providerId]);
                });

            };

            function genarateTemplate(model) {
                let thead = '<thead>' + '<tr>' + "Targowisko" + '</tr>' + '</thead>' +
                        '<tr><th>firma</th><th>długość</th><th>cena</th><th>ilość</th></tr>';
                let tbody = '';
                let i = 0;
                var inputModel = {};
                scope.inputModel = inputModel;
                model.forEach(function (obj, index) {
                    let providerName = obj.providerName;
                    if (obj.materialAssortment) {
                        let span = obj.materialAssortment.length;

                        let id = obj.materialAssortment[0].id;
                        inputModel[id] = {}
                        inputModel[id].count = '';
                        inputModel[id].providerId = obj.providerId;
                        inputModel[id].providerName = obj.providerName;
                        tbody += '<tr>';
                        tbody += '<td rowspan="' + span + '">' + providerName + '</td>';
                        tbody += '<td>' + obj.materialAssortment[0].salesLength.origSize + '</td>';
                        tbody += '<td>' + obj.materialAssortment[0].salesPrice + '</td>';
                        tbody += '<td><input id="' + id + '" ng-model="inputModel[' + "'" + id + "'" + '].count">' + '</td>';
                        tbody += '</tr>';
                        i++;

                        obj.materialAssortment.forEach(function (assor, index) {
                            if (index >= 1) {
                                let id = assor.id;
                                inputModel[id] = {}
                                inputModel[id].count = '';
                                inputModel[id].providerId = obj.providerId;
                                inputModel[id].providerName = obj.providerName;
                                tbody += '<tr>';
                                tbody += '<td>' + assor.salesLength.origSize + '</td>';
                                tbody += '<td>' + assor.salesPrice + '</td>';
                                tbody += '<td><input id="' + id + '" ng-model="inputModel[' + "'" + id + "'" + '].count">' + '</td>';
                                tbody += '</tr>';
                                i++;
                            }
                        });
                    }
                });
                let table = "";
                table += '<button type="button" class="btn btn-default btn-with-icon" ng-click="findMaterialAtProviders()"><i class="ion-ios-plus-empty"></i>findMaterialAtProviders</button>';
                table += '<br><table id="' + 'myTableListTable" class="table">' + thead + tbody + '</table>';
                table += '<button type="button" class="btn btn-default btn-with-icon" ng-click="addToShoppingCart()"><i class="ion-ios-plus-empty"></i>Dodaj do koszyka</button>';
                element.html('');
                element.append($compile(table)(scope));
            }
            scope.genarateTemplate = genarateTemplate;
        };

        return {
            restrict: 'E',
            transclude: true,
            scope: {
                item: "="
            },
            template: '',
            link: linkFunction,
            controller: controllerFunction
        };
    }
})();
