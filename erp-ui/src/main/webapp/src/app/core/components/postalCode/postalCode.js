
(function () {
    angular
        .module('JangraERP.core.components.postalCode', [])
        .directive('postalCode', PostalCodeComponent);

    function PostalCodeComponent($timeout) {

        if (typeof (bcCountries) === 'undefined') {
            throw new Error('bc-countries not found, did you forget to load the Javascript?');
        }

        function getPreferredCountries(preferredCodes) {
            let preferredCountries = [];

            for (let i = 0; i < preferredCodes.length; i++) {
                let country = bcCountries.getCountryByIso2Code(preferredCodes[i]);
                if (country) {
                    preferredCountries.push(country);
                }
            }

            return preferredCountries;
        }

        return {
            templateUrl: 'app/core/components/postalCode/postalCode.html',
            require: 'ngModel',
            scope: {
                preferredCountriesCodes: '@preferredCountries',
                defaultCountryCode: '@defaultCountry',
                selectedCountry: '=?',
                isValid: '=?',
                ngModel: '=',
                ngChange: '&',
                ngDisabled: '=',
                name: '@',
                label: '@'
            },
            link: function (scope, element, attrs, ctrl) {
                scope.selectedCountry = bcCountries.getCountryByIso2Code(scope.defaultCountryCode || 'us');
                scope.allCountries = bcCountries.getAllCountries();
                scope.number = scope.ngModel;
                scope.mask = '99-999';
                scope.changed = function () {
                    scope.ngChange();
                };

                if (scope.preferredCountriesCodes) {
                    var preferredCodes = scope.preferredCountriesCodes.split(' ');
                    scope.preferredCountries = getPreferredCountries(preferredCodes);
                }

                scope.selectCountry = function (country) {
                    scope.selectedCountry = country;
                    if(country.iso2Code === 'pl'){
                        scope.mask = '99-999';
                    } else {
                        scope.mask = '99999';
                    }
                };

                scope.isCountrySelected = function (country) {
                    return country.iso2Code == scope.selectedCountry.iso2Code;
                };

                scope.resetCountry = function () {
                    let defaultCountryCode = scope.defaultCountryCode;

                    if (defaultCountryCode) {
                        let defaultCountry = bcCountries.getCountryByIso2Code(defaultCountryCode);
                        let number = bcCountries.changeDialCode(scope.number, defaultCountry.dialCode);

                        scope.selectedCountry = defaultCountry;
                        scope.ngModel = number;
                        scope.number = number;
                    }
                };

                scope.resetCountry();
            }
        };
    }
})();
