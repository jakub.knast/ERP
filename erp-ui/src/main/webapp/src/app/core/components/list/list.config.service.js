/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.list')
        .provider('ListConfigService', ListConfigService);

    /** @ngInject */
    function ListConfigService() {
        const defaultControls = {
            listControls: {
                leftControl:
                    `<span ng-if="selected.length === 1">
                        <!--one select-->
                    </span>
                    <span ng-if="selected.length > 1">
                        <!--multi select-->
                    </span>`,
                rightControl:
                    `<button class="btn-with-icon btn btn-default" type=button ng-click="$ctrl.modalCreate()"><i class=ion-plus></i>Dodaj</button>
                    <button type=button class="btn-with-icon btn btn-default" ng-model="model.filter.removed" ng-change="$ctrl.refresh()" uib-btn-checkbox><i class="fa" ng-class="model.filter.removed ? 'fa-eye-slash' : 'fa-eye'" aria-hidden="true"></i>Pokaż usunięte</button>                      
                    <div class=btn-group dropdown-append-to-body uib-dropdown>
                        <button type=button class="btn-with-icon btn btn-default" ng-click="cleanSearchModel()"><i class=ion-code></i>Wyczyść filtry</button>
                        <button class="btn btn-default" type=button uib-dropdown-toggle><span class=caret></span></button>
                        <ul uib-dropdown-menu class="dropdown-menu-right">
                            <li><a class=btn-with-icon href ng-click="$ctrl.print()"><i class=ion-printer></i>Drukuj</a>
                            <li><a class=btn-with-icon href ng-click="$ctrl.refresh()"><i class=ion-refresh></i>Odśwież</a>
                            <li><a class=btn-with-icon href ng-click="customize()"><i class=ion-wrench></i>Konfiguruj</a>
                        </ul>
                    </div>`,
                menu: [
                    {
                        html: '<a class="btn-with-icon" href ng-click="$parent.$ctrl.modalPreview(item)"><i class=ion-eye></i>Podgląd</a>',
                        compile: true
                    },
                    {
                        html: '<a class=btn-with-icon href ui-sref="^.element.detail({id: item.data.id})"><i class=ion-edit></i>Edytuj</a>',
                        compile: true
                    }
                ]
            },
            listControlsWithoutAdd: {
                leftControl:
                    `<span ng-if="selected.length === 1">
                        <!--one select-->
                    </span>
                    <span ng-if="selected.length > 1">
                        <!--multi select-->
                    </span>`,
                rightControl:
                    `<button type=button class="btn-with-icon btn btn-default" ng-model="model.filter.removed" ng-change="$ctrl.refresh()" uib-btn-checkbox><i class="fa" ng-class="model.filter.removed ? 'fa-eye-slash' : 'fa-eye'" aria-hidden="true"></i>Pokaż usunięte</button>                      
                    <div class=btn-group dropdown-append-to-body uib-dropdown>
                        <button type=button class="btn-with-icon btn btn-default" ng-click="cleanSearchModel()"><i class=ion-code></i>Wyczyść filtry</button>
                        <button class="btn btn-default" type=button uib-dropdown-toggle><span class=caret></span></button>
                        <ul uib-dropdown-menu class="dropdown-menu-right">
                            <li><a class=btn-with-icon href ng-click="$ctrl.print()"><i class=ion-printer></i>Drukuj</a>
                            <li><a class=btn-with-icon href ng-click="$ctrl.refresh()"><i class=ion-refresh></i>Odśwież</a>
                            <li><a class=btn-with-icon href ng-click="customize()"><i class=ion-wrench></i>Konfiguruj</a>
                        </ul>
                    </div>`,
                menu: [
                    {
                        html: '<a class="btn-with-icon" href ng-click="$parent.$ctrl.modalPreview(item)"><i class=ion-eye></i>Podgląd</a>',
                        compile: true
                    },
                    {
                        html: '<a class=btn-with-icon href ui-sref="^.element.detail({id: item.data.id})"><i class=ion-edit></i>Edytuj</a>',
                        compile: true
                    }
                ]
            }
        };
        const defaultConfigs = {};

        function genListControls(listName) {
            let listConfigControls = defaultConfigs[listName] && defaultConfigs[listName].controls;
            if (listConfigControls && listConfigControls.name) {
                let defaultControl = defaultControls[listConfigControls.name];
                if (defaultControl) {
                    let leftControl = defaultControl.leftControl;
                    if (listConfigControls.leftControl && listConfigControls.leftControl.length) {
                        leftControl = leftControl.split('\n');
                        listConfigControls.leftControl.forEach(element => {
                            leftControl.splice(element.index, 0, element.template)
                        });
                        leftControl = leftControl.join('');
                    }
                    
                    let rightControl = defaultControl.rightControl;
                    if (listConfigControls.rightControl && listConfigControls.rightControl.length) {
                        rightControl = rightControl.split('\n');
                        listConfigControls.rightControl.forEach(element => {
                            rightControl.splice(element.index, 0, element.template)
                        });
                        rightControl = rightControl.join('');
                    }

                    let menu = defaultControl.menu.slice(0);
                    if (listConfigControls.menu && listConfigControls.menu.length) {
                        listConfigControls.menu.forEach(element => {
                            menu.splice(element.index, 0, element.item)
                        });
                    }

                    return {
                        leftControl: leftControl,
                        rightControl: rightControl,
                        menu: menu
                    }
                }
            }
            return null;
        }

        this.addDefaultControls = function (controlsName, config) {
            defaultControls[controlsName] = config;
        };

        this.addDefaultConfig = function (configName, config) {
            defaultConfigs[configName] = config;
        };

        this.$get = function (FieldsService) {
            let listConfigs = {};
            let fields = FieldsService.getFields();

            let searchTemplate = {
                text: column => `<th><input type="{{::type}}" class="form-control" ng-model="model.filter.${column.key}" ng-model-options="{ debounce: 500 }" ng-change="search()"/></th>`,
                number: column =>
                    `<th><search-input type="number" ng-model="model.filter.${column.key}" change="search()"></search-input></th>`,
                select: column =>
                    `<th><input type="text" class="form-control" ng-model="model.filter.${column.key}.value" ng-model-options="{ debounce: 500 }" ng-change="search()"></th>`,
                uiSelect: column => `<th><input type="{{::type}}" class="form-control" ng-model="model.filter.${column.key}.${column.templateOptions.valueProp}" ng-model-options="{ debounce: 500 }" ng-change="search()"/></th>`,
            };

            function generateColumn(fieldName, mergeOptions) {
                let column = mergeOptions ? angular.merge({}, fields[fieldName] || {}, mergeOptions) : angular.copy(fields[fieldName]);

                switch (column.type) {
                    case 'horizontalInput':
                        column.sortList = 'sort-alpha';
                        column.tbody = '<td><span copy-to-clipboard>{{::item.data.' + column.key + '}}</span></td>';
                        column.search = searchTemplate.text(column);
                        break;
                    case 'horizontalEmail':
                        column.sortList = 'sort-alpha';
                        column.tbody = '<td><a href="mailto:{{::item.data.' + column.key + '}}">{{::item.data.' + column.key + '}}</a></td>';
                        column.search = searchTemplate.text(column);
                        break;
                    case 'horizontalTextarea':
                        column.sortList = 'sort-alpha';
                        column.tbody = '<td><button class="btn btn-primary btn-xs" ng-if="::item.data.' + column.key + '" uib-popover="{{item.data.' + column.key + '}}"  popover-title="' + column.templateOptions.label + '" popover-trigger="outsideClick">{{item.data.' + column.key + ' | limitTo:25}}{{::(item.data.' + column.key + '.length > 25 ? "..." : "")}}</button></td>';
                        column.search = searchTemplate.text(column);
                        break;
                    case 'horizontalText':
                        column.sortList = 'sort-alpha';
                        column.tbody = '<td><button class="btn btn-primary btn-xs" ng-if="::item.data.' + column.key + '" uib-popover-html="item.data.' + column.key + '"  popover-title="' + column.templateOptions.label + '" popover-trigger="outsideClick" popover-placement="auto bottom" popover-trigger="\'outsideClick\'">Pokaż</button></td>';
                        column.search = searchTemplate.text(column);
                        break;
                    case 'horizontalDatepicker':
                        column.sortList = 'sort-numeric';
                        if (column.templateOptions.type === 'time') {
                            column.tbody = '<td><div><span><i class="fa fa-clock-o"></i> {{::item.data.' + column.key + ' | date:"mediumTime"}}</span></div></td>';
                        } else if (column.templateOptions.type === 'date') {
                            column.tbody = '<td><div><span><i class="fa fa-calendar"></i> {{::item.data.' + column.key + ' | date:"mediumDate"}}</span></div></td>';
                        } else {
                            column.tbody = '<td><div><span><i class="fa fa-clock-o"></i> {{::item.data.' + column.key + ' | date:"mediumTime"}}</span></div><div><span><i class="fa fa-calendar"></i> {{::item.data.' + column.key + ' | date:"mediumDate"}}</span></div></td>';
                        }
                        column.search = `<th><input date-range-picker class="form-control date-picker" readonly type="text" ng-model="model.filter.${column.key}" autocomplete="off" options="options.rangeDatePicker" ng-init="model.filter.${column.key} = {startDate: null, endDate: null}" ng-watch-model="searchDate"/></th>`;
                        break;
                    case 'horizontalSelect':
                        column.sortList = 'sort-alpha';
                        if (column.templateOptions.type === 'const') {
                            column.tbody = '<td><span>{{::item.data.' + column.key + '}}</span></td>';
                        } else {
                            column.tbody = `<td><span class="label label-default" copy-to-clipboard ng-style="::{'background-color': availableColumns['${column.id}'].templateOptions.items[item.data.${column.key}].backgroundColor, 'color': availableColumns['${column.id}'].templateOptions.items[item.data.${column.key}].fontColor}">{{::availableColumns['${column.id}'].templateOptions.items[item.data.${column.key}].translation || ""}}</span></td>`;
                        }
                        column.search = `<th>
                                            <select class="form-control selectpicker show-tick" title="" selectpicker ng-model="model.filter['${column.key}']" ng-change="search()"
                                            ng-options="${column.templateOptions.options || 'item for item in'} ::availableColumns[\'${column.id}\'].templateOptions.items" data-selected-text-format="count > 2" multiple></select>
                                        </th>`;

                        break;
                    case 'horizontalTemplateBase':
                        column.tbody = '<td><a ui-sref="' + column.templateOptions.uiSref + '.element.detail({ id: item.data.' + column.key + '.id })">{{::item.data.' + column.key + '.' + column.templateOptions.valueProp + '}}</a></td>';
                        column.search = searchTemplate.uiSelect(column);
                        break;
                    case 'horizontalTemplate':
                        column.tbody = '<td><a ui-sref="' + column.templateOptions.uiSref + '.element.detail({ id: item.data.' + column.key + '.id })">{{::item.data.' + column.key + '.' + column.templateOptions.valueProp + '}}</a></td>';
                        column.search = searchTemplate.uiSelect(column);
                        break;
                    case 'horizontalReferenceBase':
                        column.tbody = '<td><a ui-sref="' + column.templateOptions.uiSref + '.element.detail({ id: item.data.' + column.key + '.id })">{{::item.data.' + column.key + '.' + column.templateOptions.valueProp + '}}</a></td>';
                        column.search = searchTemplate.uiSelect(column);
                        break;
                    case 'horizontalUpload':
                        column.tbody = '<td><a ng-if="::item.data.techDrawingPDFdto.resourceId" ng-href="/api/invoker/ResourceInfoService/getResourceById?id={{::item.data.' + column.key + 'dto.resourceId}}" target="_blank">{{::item.data.techDrawingPDFdto.resourceName}}</a></td>';
                        column.search = column.search ? column.search : '<th></th>';
                        break;
                    case 'horizontalPhone':
                        //todo move to def field
                        column.tbody = '<td><span copy-to-clipboard>{{::item.data.' + column.key + '}}</span></td>';
                        column.search = searchTemplate.text(column);
                        break;
                    case '_id':
                        column.tbody = '<td><a ui-sref="' + column.router + '.element.detail({ id: item.data.id })">{{::item.data.id}}</a></td>';
                        column.search = searchTemplate.text(column);
                        column.short = true;
                        break;
                    case 'cuttingSize':
                        column.tbody = '<td><span copy-to-clipboard>{{item.data.' + column.key + '.origSize}}</span></td>';
                        column.search = '<th><input type="text" class="form-control" ng-model="model.filter[\'' + column.key + '\'].value" ng-model-options="{ debounce: 500 }" ng-change="search()"></th>';
                        break;
                    case 'horizontalNaturalNumber':
                        column.tbody = '<td><span copy-to-clipboard>{{item.data.' + column.key + '}}</span></td>';
                        column.search = searchTemplate.number(column);
                        break;
                    case 'horizontalLink':
                        column.tbody = '<td><a ng-if="::item.data.' + column.key + '" ng-href="' + column.templateOptions.url + '?' + (column.templateOptions.fieldName || 'id') + '={{::item.data.' + column.key + '}}" target="_blank">Pobierz</a></td>';
                        column.search = '<th></th>';
                        break;
                    case 'horizontalCheckbox':
                        column.tbody = '<td>{{("_." + item.data.' + column.key + ') | translate}}</td>';
                        column.search = `<th>
                                            <select class="form-control selectpicker show-tick" title="" selectpicker ng-model="model.filter['${column.key}']" ng-change="search()">
                                                <option></option>
                                                <option ng-value="${column.templateOptions.trueValue | true}">{{"_.true" | translate}}</option>
                                                <option ng-value="${column.templateOptions.falseValue | false}">{{"_.false" | translate}}</option>
                                            </select>
                                        </th>`;
                        break;

                    default:
                        column.tbody = '<td>' + ((column.templateOptions && column.templateOptions.tbody) || '<span copy-to-clipboard>{{::item.data.' + column.key + '}}</span>') + '</td>';
                        column.search = '<th>' + ((column.templateOptions && column.templateOptions.search) || '') + '</th>';
                }

                return column;
            }

            class ListConfigClass {
                constructor(defaultConfig, controls) {
                    this.id = defaultConfig.id;
                    this.name = this.id + 'ListConfig';
                    this.$ctrl = defaultConfig.$ctrl || '$ctrl';
                    this.options = defaultConfig.options;
                    this.defaultColumns = defaultConfig.defaultColumns;
                    this.controls = controls;
                    this.columns = [];
                    this.extends = defaultConfig.extends || [];
                    this.availableColumns = {};

                    let defaultAvailableColumns = defaultConfig.availableColumns;
                    for (let columnName in defaultAvailableColumns) {
                        if (defaultAvailableColumns.hasOwnProperty(columnName)) {
                            this.availableColumns[columnName] = generateColumn(columnName, defaultAvailableColumns[columnName])
                        }
                    }

                    let userColumns = localStorage.getItem(this.name);
                    userColumns = userColumns ? userColumns.split("|") : defaultConfig.defaultColumns;

                    this.init(userColumns);
                }

                init(columns) {
                    this.columns = columns.reduce((columns, columnName) => {
                        if (this.availableColumns[columnName]) {
                            columns.push(this.availableColumns[columnName]);
                        }
                        return columns;
                    }, []);
                }

                switchOnColumn(column) {
                    this.columns.push(column);
                    this.saveInLocalStorage();
                }

                switchOffColumn(column) {
                    this.columns.splice(this.columns.indexOf(column), 1);
                    this.saveInLocalStorage();
                }

                saveInLocalStorage() {
                    localStorage.setItem(this.name, this.getColumnsIdMap());
                }

                getColumnsIdMap() {
                    return this.columns.map(column => column.id).join('|');
                }

                getCustomizeObject() {
                    let cusObject = {
                        availableColumns: {},
                        columns: []
                    };

                    for (let columnName in this.availableColumns) {
                        if (this.availableColumns.hasOwnProperty(columnName)) {
                            cusObject.availableColumns[columnName] = {
                                id: this.availableColumns[columnName].id,
                                label: this.availableColumns[columnName].templateOptions.label
                            }
                        }
                    }

                    cusObject.columns = this.columns.map(column => {
                        cusObject.availableColumns[column.id].enabled = true;
                        return cusObject.availableColumns[column.id]
                    });

                    return cusObject;
                }

                setColumnsFromMap(columnsMap) {
                    this.init(columnsMap.map(column => column.id))
                }

                static getFields() {
                    return fields;
                }
            }

            function getListConfig(listName) {
                if (!listConfigs[listName]) {
                    return listConfigs[listName] = new ListConfigClass(defaultConfigs[listName], genListControls(listName))
                }
                return listConfigs[listName];
            }

            return {
                getListConfig: getListConfig
            }
        };
    }
})();
