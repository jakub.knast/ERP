/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.preview')
        .directive('preview', PreviewComponent)
        .directive('pdfPreview', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    let url = attrs.src;
                    element.append(`<object type="application/pdf" data="${url}" width="100%" style="height: 90vh"></object>`);
                }
            };
        })
        .directive('nestTablePreview', function () {
            return {
                scope: {
                    model: '=',
                    templateId: '@'
                },
                restrict: 'E',
                template: "<div style='padding: 0 0 8px; text-align: right' print-element='#nestTablePreview'><button class='btn btn-primary btn-sm btn-with-icon' ng-click='print()'><i class=ion-printer></i>Drukuj</button></div><div id='nestTablePreview' ng-include=\"'' + templateId + ''\"></div>"
            }
        });

    /** @ngInject */
    function PreviewComponent($compile, $templateCache, $uibModal, PreviewConfigService) {
        return {
            restrict: 'E',
            scope: {
                model: "=",
                config: "="
            },
            template: '',
            link: function (scope, element, attr, controller) {
                const postId = "PreviewComponent";
                let config = (typeof scope.config === 'string') ? PreviewConfigService.getPreviewConfig(scope.config) : scope.config;

                // compile template from cache or generate it
                compileTemplate($templateCache.get(config.id + postId) || genarateTemplate(config));

                function genarateTemplate(previewConfig) {
                    let tbody = '';
                    previewConfig.fields.forEach(function (field) {
                        tbody += field.tbody;
                    });

                    return $templateCache.put(previewConfig.id + postId, `<table class="previewTable"><tbody>${tbody}</tbody></table><div ng-if="preview" one-bind-html-compile="preview"></div>`);
                }

                scope.showPreview = function (fieldKey) {
                    if (scope.config.availableFields[fieldKey].preview) {
                        $uibModal.open({
                            // animation: false,
                            size: 'auto',
                            backdrop: false,
                            windowClass: 'modalPreview',
                            template: '' + scope.config.availableFields[fieldKey].preview,
                            windowTemplateUrl: 'app/core/components/preview/preview.template.html',
                            controller: function ($scope, model, UtilsService) {
                                $scope.model = model;

                                $scope.print = function () {
                                    UtilsService.print('#limitCardPreview');
                                };
                            },
                            resolve: {
                                model: scope.model
                            }
                        }).result.then(angular.noop, angular.noop)
                    }
                };


                function compileTemplate(template) {
                    $(element).empty(); // TODO: destroy bind
                    element.append($compile(template)(scope));
                }
            }
        };
    }
})();
