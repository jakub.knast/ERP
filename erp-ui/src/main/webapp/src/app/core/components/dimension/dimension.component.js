/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular.module('JangraERP.core.components')
        .directive('dimension', dimension);

    /** @ngInject */
    function dimension() {
        return {
            restrict: 'E',
            templateUrl: 'app/core/components/dimension/dimension.component.html',
            require: 'ngModel',
            link: function ($scope, $element, $attrs, ngModelCtrl) {
                let separator = 'x';
                let unitSeparator = ' x ';
                $scope.params = [];
                $scope.values = [];

                ngModelCtrl.$formatters.push(function (value) {
                    if (angular.isString(value)) {
                        $scope.values = value.split(separator).map(Number);
                    }
                    return value;
                });


                $scope.$watch('model.' + $scope.to.dep, function (newVal, oldVal) {
                    if(newVal){
                        if (angular.isString(newVal.baseDimensionTemplate)) {
                            $scope.params = newVal.baseDimensionTemplate.split(separator).map(key => {
                                return {
                                    key: key
                                }
                            });
                            $scope.values.length = $scope.params.length;
                        }
                        if (angular.isString(newVal.baseDimensionUnit)) {
                            newVal.baseDimensionUnit.split(unitSeparator).forEach((unit, index) => {
                                $scope.params[index].unit = '[' + unit + ']';
                            });
                        }
                    }
                });

                $scope.$watch('values', function (newVal, oldVal) {
                    if (newVal && newVal.length && newVal.every(num => num > 0)){
                        $scope.options.value(newVal.join(separator));
                    } else {
                        $scope.options.value(null);
                    }
                }, true);
            }
        };
    }
})();
