/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class BaseNestDataListCtrl {
        constructor(elementModel, configName) {
            this.model = elementModel.updateTemp();
            this.config = configName;
        }
    }

    angular
        .module('JangraERP.core.components.nestDataList')
        .controller('BaseNestDataListCtrl', BaseNestDataListCtrl);
})();
