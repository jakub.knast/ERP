/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular.module('JangraERP.core.components')
        .directive('dimensionData', dimensionData);

    /** @ngInject */
    function dimensionData(MaterialTemplateRequestService) {
        return {
            restrict: 'E',
            templateUrl: 'app/core/components/dimensionData/dimensionData.component.html',
            require: 'ngModel',
            link: function ($scope, $element, $attrs, ngModelCtrl) {
                $scope.mode = null;
                $scope.tempModel = null;
                $scope.ngModelCtrl = ngModelCtrl;

                let modelWatch = null;

                function reset() {
                    $scope.mode = null;
                    ngModelCtrl.$parsers.length = 0;
                    ngModelCtrl.$formatters.length = 0;

                    if (modelWatch) {
                        modelWatch();
                    }
                }

                function init(mode) {
                    $scope.mode = mode;

                    switch (mode) {
                        case 1:
                            modelWatch = $scope.$watch('model.salesLength.length', function (newVal, oldVal) {
                                if (newVal !== null && newVal !== undefined) {
                                    ngModelCtrl.$setViewValue({
                                        cuttingDimension: 'dł',
                                        isOneDimensial: true,
                                        length: newVal,
                                        origSize: newVal.toString()
                                    });
                                    ngModelCtrl.$setValidity('required', true);
                                } else {
                                    ngModelCtrl.$setValidity('required', false);
                                }
                            });

                            break;

                        case 2:
                            modelWatch = $scope.$watch('model.salesLength.area', function (newVal, oldVal) {
                                if (newVal !== null && newVal !== undefined && angular.isNumber(newVal.a) && angular.isNumber(newVal.b)) {
                                    ngModelCtrl.$setViewValue({
                                        area: {
                                            a: newVal.a,
                                            b: newVal.b
                                        },
                                        cuttingDimension: 'AxB',
                                        isOneDimensial: false,
                                        origSize: newVal.a + 'x' + newVal.b
                                    });
                                    ngModelCtrl.$setValidity('required', true);
                                } else {
                                    ngModelCtrl.$setValidity('required', false);
                                }
                            }, true);

                            break;

                        default:
                            reset()
                    }
                }

                $scope.$watch('formOptions.data.model.' + $scope.to.dep, function (newVal, oldVal) {
                    if (newVal) {
                        MaterialTemplateRequestService.getById(newVal).then(materialTemplateData => {
                            reset();
                            init(materialTemplateData.materialType.cuttingDimensionTemplate === "dł" ? 1 : 2);
                        });
                    }
                });
            }
        };
    }
})();
