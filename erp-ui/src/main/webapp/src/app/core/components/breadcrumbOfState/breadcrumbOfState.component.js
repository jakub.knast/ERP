/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular.module('JangraERP.core.components')
        .directive('breadcrumbOfState', breadcrumbOfState);

    /** @ngInject */
    function breadcrumbOfState($state, $stateParams) {
        return {
            restrict: 'E',
            templateUrl: 'app/core/components/breadcrumbOfState/breadcrumbOfState.component.html',
            scope: {},
            link: function ($scope) {
                $scope.params = $stateParams;
                $scope.breadcrumbList = null;

                $scope.goTo = function(state){
                    if(state.abstract){
                        $state.go(state.children[0].name);
                    }else {
                        $state.go(state.name);
                    }
                };

                function getBreadcrumbList(state) {
                    $scope.breadcrumbList = parseState(state).reverse();
                }

                function parseState(state, stateList) {
                    stateList = stateList || [];
                    stateList.push(state);
                    if (state.parent) {
                        parseState(state.parent, stateList)
                    }

                    return stateList;
                }

                $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                        getBreadcrumbList(toState)
                    });

                getBreadcrumbList($state.current)
            }
        };
    }
})();
