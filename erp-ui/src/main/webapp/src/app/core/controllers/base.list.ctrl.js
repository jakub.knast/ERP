/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class BaseListCtrl {
        constructor($uibModal, UtilsService, listModel, listConfig) {
            this.listModel = listModel;
            this.listConfig = listConfig;

            this.modalPreview = function (element) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/core/controllers/base.preview.modal.html',
                    size: 'auto',
                    backdrop: 'static',
                    controller: 'BasePreviewCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        elementModel: element
                    }
                }).result.then(angular.noop, angular.noop);
            };

            this.modalCreate = function (element) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/core/controllers/base.create.modal.html',
                    size: 'md',
                    backdrop: 'static',
                    controller: 'BaseCreateCtrl',
                    controllerAs: '$ctrl',
                    bindToController: true,
                    resolve: {
                        elementModel: element || new this.listModel.elementClass(),
                    }
                }).result.then(() => {
                    this.refresh()
                }, () => {
                    this.refresh()
                });
            };

            this.modalRemove = function (element) {
                element.remove(false).then(responseData => {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'app/core/controllers/base.remove.modal.html',
                        size: 'auto',
                        backdrop: 'static',
                        controller: 'BaseRemoveCtrl',
                        controllerAs: '$ctrl',
                        resolve: {
                            listConfig, listConfig,
                            elementModel: element,
                            removeConsequences: responseData.removeConsequences
                        }
                    }).result.then(() => {
                        element.remove().then(() => {
                            this.refresh();
                        })
                    }, () => {
                        this.refresh()
                    })
                })
            };

            this.print = function () {
                UtilsService.print('#' + (this.listConfig.id || 'Base') + 'ListTable');
            };

        }

        refresh() {
            this.listModel.refresh()
        }
    }

    angular
        .module('JangraERP.core')
        .controller('BaseListCtrl', BaseListCtrl);
})();
