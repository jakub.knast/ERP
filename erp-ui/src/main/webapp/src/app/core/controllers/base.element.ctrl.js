/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class BaseElementCtrl {
        constructor($state, $uibModal, UtilsService, elementModel) {
            this.model = elementModel;
            this.elementState = $state.get($state.current.parent.name);

            this.modalRemove = function () {
                this.model.remove(false).then(responseData => {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'app/core/controllers/base.remove.modal.html',
                        size: 'auto',
                        backdrop: 'static',
                        controller: 'BaseRemoveCtrl',
                        controllerAs: '$ctrl',
                        resolve: {
                            elementModel: this.model
                        }
                    }).result.then(() => {
                        this.model.remove().then(() => {
                            $state.go('^.^.list');
                        })
                    }, () => {
                        this.model.refresh()
                    })
                })
            };

            this.print = function () {
                UtilsService.print('#baseElement');
            };
        }

        reset() {
            this.model.updateTemp()
        }
    }

    angular
        .module('JangraERP.core')
        .controller('BaseElementCtrl', BaseElementCtrl);
})();
