/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class BaseCreateCtrl {
        constructor(FormlyConfigService, elementModel, $state, $scope) {
            this.modalTitle = elementModel.getName() + '.element.new';
            this.model = elementModel.updateTemp();
            this.config = FormlyConfigService.getFormlyConfig('Create' + elementModel.getName());
            if($state.params && $state.params.typeId){
                this.config.options.data = {
                    typeId: $state.params.typeId
                };
            }

            this.saveAndGo = function () {
                this.detailForm.$setSubmitted();
                if (this.detailForm.$valid) {
                    this.cgBusy = this.model.save(true, this.config.options.formState.formData).then(responseData => {
                        $state.go('^.element.detail', {id: responseData.id});
                        $scope.$close();
                    }, angular.noop)
                }
            };
        }

        save ($close) {
            this.detailForm.$setSubmitted();
            if (this.detailForm.$valid) {
                this.cgBusy = this.model.save(true, this.config.options.formState.formData).then(() => {
                    this.model.updateTemp();
                    $close();
                }, angular.noop)
            }
        };

        saveAndClean () {
            this.detailForm.$setSubmitted();
            if (this.detailForm.$valid) {
                this.cgBusy = this.model.save(true, this.config.options.formState.formData).then(() => {
                    this.config.options.resetModel();
                    this.model.clean();
                }, angular.noop)
            }
        };
    }

    angular
        .module('JangraERP.core')
        .controller('BaseCreateCtrl', BaseCreateCtrl);
})();
