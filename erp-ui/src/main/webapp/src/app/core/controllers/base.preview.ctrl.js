/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class BasePreviewCtrl {
        constructor(PreviewConfigService, elementModel) {
            this.modalTitle = elementModel.getName() + '.element.title';
            this.model = elementModel;
            this.config = PreviewConfigService.getPreviewConfig(elementModel.getName());
        }
    }

    angular
        .module('JangraERP.core')
        .controller('BasePreviewCtrl', BasePreviewCtrl);
})();
