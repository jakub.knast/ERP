(function () {
    'use strict';

    angular.module('JangraERP.core')
        .service('SystemService', SystemService)
        .service('ConfigService', ConfigService)
        .run(function (SystemService) {
            SystemService.init();
        });

    function ConfigService() {
        const constObject = {};

        this.setConst = function (key, value) {
            constObject[key] = value;
        };

        this.getConst = function (key) {
            return constObject[key];
        }
    }

    /** @ngInject */
    function SystemService($locale, requestHandler, UtilsService, ConfigService, AlertService) {
        const service = 'SystemService';
        let entityStates = {};
        let states = {};
        let constants = {};
        let options = {
            datePicker: {
                format: $locale.DATETIME_FORMATS.mediumDate,
                singleDatePicker: true,
                locale: {
                    daysOfWeek: $locale.DATETIME_FORMATS.SHORTDAY,
                    monthNames: $locale.DATETIME_FORMATS.MONTH
                }
            },
            rangeDatePicker: {
                format: $locale.DATETIME_FORMATS.mediumDate,
                locale: {
                    applyLabel: "Zatwierdź",
                    fromLabel: "od",
                    toLabel: "Do",
                    cancelLabel: 'Anuluj',
                    customRangeLabel: 'Własne',
                    daysOfWeek: $locale.DATETIME_FORMATS.SHORTDAY,
                    monthNames: $locale.DATETIME_FORMATS.MONTH
                },
                ranges: {
                    'Ten tydzień': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
                    'Ten miesiąc': [moment().startOf('month'), moment().endOf('month')],
                    'Ten rok': [moment().startOf('year'), moment().endOf('year')],
                    'Poprzedni tydzień': [moment().subtract(1, 'weeks').startOf('isoWeek'), moment().subtract(1, 'weeks').endOf('isoWeek')],
                    'Poprzedni miesiąc': [moment().subtract(1, 'months').startOf('month'), moment().subtract(1, 'months').endOf('month')],
                    'Poprzedni rok': [moment().subtract(1, 'years').startOf('year'), moment().subtract(1, 'years').endOf('year')]
                }
            }
        };

        const filterOperators = {
            EQUALS: {
                type: 'EQUALS',
                icon: 'fa-exchange'
            },
            EQUALS_IGNORE_CASE: {
                type: 'EQUALS_IGNORE_CASE',
                icon: 'fa-exchange'
            },
            NOT_EQUALS: {
                type: 'NOT_EQUALS',
                icon: 'fa-random'
            },
            GRATHER_THAN: {
                type: 'GRATHER_THAN',
                icon: 'fa-angle-right'
            },
            GRATHER_THAN_OR_EQUAL: {
                type: 'GRATHER_THAN_OR_EQUAL',
                icon: 'fa-angle-right'
            },
            LESS_THAN: {
                type: 'LESS_THAN',
                icon: 'fa-angle-left'
            },
            LESS_THAN_OR_EQUAL: {
                type: 'LESS_THAN_OR_EQUAL',
                icon: 'fa-angle-left'
            },
            LIKE: {
                type: 'LIKE',
                icon: 'fa-thumbs-up'
            },
            LIKE_IGNORE_CASE: {
                type: 'LIKE_IGNORE_CASE',
                icon: 'fa-thumbs-up'
            },
            BETWEEN: {
                type: 'BETWEEN',
                icon: 'fa-arrows-h'
            }
        };

        this.getFilterOperators = function () {
            return filterOperators
        };

        function getProgramConts() {
            return requestHandler.send({
                method: 'getProgramConts',
                service: service
            });
        }

        function addConst(type, value) {
            return requestHandler.send({
                method: 'addConst',
                service: service,
                class: 'regiec.knast.erp.api.dto.system.AddConstRequestDTO',
                data: {type: type, value: value}
            });
        }

        this.validate = function (entityType, field, value, id) {
            let data = {entityType, field, value};
            if (id) {
                data.myId = id
            }
            return requestHandler.send({
                method: 'validate',
                service: service,
                class: 'regiec.knast.erp.api.dto.ValidateUniquenessRequest',
                data: data
            });
        };

        this.validate2 = function (entityType, field1, value1, field2, value2, id) {
            let data = {entityType, field1, value1, field2, value2};
            if (id) {
                data.myId = id
            }
            return requestHandler.send({
                method: 'validate2',
                service: service,
                class: 'regiec.knast.erp.api.dto.Validate2UniquenessRequest',
                data: data
            });
        };

        this.init = function () {
            getProgramConts().then(function (responseData) {
                states = responseData.statesMap;

                angular.forEach(responseData.entityStatesMap, entityState => {
                    if (angular.isArray(entityState.verticles)) {
                        entityState.verticlesHashMap = UtilsService.getHashMap(entityState.verticles, 'name')
                    }
                });
                entityStates = responseData.entityStatesMap;

                angular.forEach(responseData, (value, key) => {
                    if (angular.isArray(value)) {
                        constants[key] = responseData[key]
                    } else if (typeof value === "boolean") {
                        ConfigService.setConst(key, value);
                    }
                });
            });
        };

        this.getConts = function (constName) {
            return constants[constName];
        };

        this.getStateMap = function (state) {
            return states[state];
        };

        this.getStatesMap = function () {
            return states;
        };

        this.getEntityStateMap = function (state) {
            return entityStates[state];
        };

        this.getEntityStatesMap = function () {
            return entityStates;
        };

        this.addConst = function (type, value) {
            addConst(type, value).then(function (responseData) {
                AlertService.success('Const.alert.create', null, {type, value});
                angular.extend(constants[type], responseData[type]);
            })
        };

        this.getOptions = function (name) {
            return options[name];
        };

        this.getAllOptions = function () {
            return options;
        };
    }
})();
