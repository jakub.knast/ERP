(function () {
    'use strict';

    angular.module('JangraERP.core')
        .service('UtilsService', UtilsService);

    /** @ngInject */
    function UtilsService($q) {
        this.requestQueue = function (request, dataArray) {
            let q = $q.defer();
            let queue = dataArray;
            let response = {
                resolve: [],
                reject: []
            };
            let execNext = function () {
                if (queue.length) {
                    let requestData = queue[0];
                    request(requestData).then(responseSuccessData => {
                        queue.shift();
                        response.resolve.push(responseSuccessData);
                        execNext()
                    }, responseErrorData => {
                        queue.shift();
                        response.reject.push(responseErrorData);
                        execNext()
                    })
                } else {
                    q.resolve(response)
                }
            };

            execNext();

            return q.promise;
        };

        this.objectToPathMap = function objectToPathMap(data, array, parentKey, type) {
            array = array || [];
            for (let i in data) {
                let key = i;

                if (parentKey) {
                    switch (type) {
                        case 'o': {
                            key = parentKey + '.' + i;
                            break
                        }
                        case 'a': {
                            key = parentKey + '[' + i + ']';
                            break
                        }
                    }
                }

                if (data.hasOwnProperty(i)) {
                    if (Object.prototype.toString.call(data[i]) === '[object Array]') {
                        objectToPathMap(data[i], array, key, 'a')
                    }
                    else if (Object.prototype.toString.call(data[i]) === '[object Object]') {
                        objectToPathMap(data[i], array, key, 'o')
                    }

                    else {
                        array.push({
                            key: key,
                            value: data[i]
                        });
                    }
                }
            }
            return array;
        };

        this.toFormData = function jsonToFormData(formData, data, parentKey, type) {
            formData = formData || new FormData();
            for (let i in data) {
                let key = i;

                if (parentKey) {
                    switch (type) {
                        case 'o': {
                            key = parentKey + '.' + i;
                            break
                        }
                        case 'a': {
                            key = parentKey + '[' + i + ']';
                            break
                        }
                    }
                }

                if (data.hasOwnProperty(i)) {
                    if (Object.prototype.toString.call(data[i]) === '[object Array]') {
                        jsonToFormData(formData, data[i], key, 'a')
                    }
                    else if (Object.prototype.toString.call(data[i]) === '[object Object]') {
                        jsonToFormData(formData, data[i], key, 'o')
                    }

                    else if (!formData.has(i)) {
                        formData.append(key, data[i]);// TODO or set ???
                    }
                }
            }
            return formData;
        };

        this.print = function (selector) {
            $(selector).printThis({
                debug: false,               // show the iframe for debugging
                importCSS: true,            // import page CSS
                importStyle: false,         // import style tags
                printContainer: true,       // grab outer container as well as the contents of the selector
                loadCSS: "development/css/print.css",  // path to additional css file - use an array [] for multiple
                pageTitle: "",              // add title to print page
                removeInline: false,        // remove all inline styles from print elements
                printDelay: 333,            // variable print delay; depending on complexity a higher value may be necessary
                header: null,               // prefix to html
                footer: null,               // postfix to html
                base: false,               // preserve the BASE tag, or accept a string for the URL
                formValues: true,           // preserve input/form values
                canvas: false,              // copy canvas elements (experimental)
                doctypeString: "...",       // enter a different doctype for older markup
                removeScripts: false        // remove script tags from print content
            });
        };

        this.capitalizeFirstLetter = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        };

        this.objFromPath = function (obj, path) {
            let paths = path.split('.'),
                current = obj;

            for (let i = 0; i < paths.length; ++i) {
                if (current[paths[i]] === undefined) {
                    return undefined;
                } else {
                    current = current[paths[i]];
                }
            }
            return current;
        };

        this.getHashMap = function (array, indexBy) {
            if (angular.isArray(array) && angular.isString(indexBy)) {
                return array.reduce((hashMap, element) => {
                    hashMap[element[indexBy]] = element;
                    return hashMap;
                }, {})
            }
        }
    }
})();
