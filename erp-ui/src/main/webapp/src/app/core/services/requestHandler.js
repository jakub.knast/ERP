(function () {
    'use strict';

    angular.module('JangraERP.core')
        .service('requestHandler', requestHandler);

    /** @ngInject */
    function requestHandler($http, $q, AlertService, ConfigService) {

        let formHeaders = {
            withCreadential: true,
            headers: {
                'Content-Type': undefined,
                'Access-Control-Allow-Methods': 'adjust your need',
                'Access-Control-Allow-Origin': 'adjust your need',
            }
        };

        function getUrl(requestModel) {
            return window.location.origin + '/api/invoker/' + requestModel.service + '/' + requestModel.method
        }

        function generateRequestOptions(requestModel) {
            let requestOptions = {
                url: getUrl(requestModel),
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            if (requestModel.class) {
                let requestData = {
                    inputType: 'JSON'
                };
                requestData[requestModel.class] = JSON.stringify(requestModel.data || {});
                requestOptions.data = $.param(requestData);
            }

            return requestOptions;
        }

        /**
         * @typedef {Object} requestModel
         * @property {string} service - API service.
         * @property {string} method - API method.
         * @property {string} class - API data class.
         * @property {{}} data - request data.
         */

        /**
         * Send request to API
         * @param {requestModel} requestModel
         * @param {boolean} toFormData
         */
        this.send = function (requestModel, toFormData) {
            let def = $q.defer();
            let requestOptions = generateRequestOptions(requestModel);
            let request = null;
            if (!toFormData) {
                request = $http(requestOptions);
            } else {
                requestModel.data.append("inputType", 'PROP2JSON');
                request = $http.post(getUrl(requestModel), requestModel.data, formHeaders);
            }
            request
                .then(function (successObj) {
                    if (successObj.data && successObj.data.dto) {
                        def.resolve(successObj.data.dto.responseObject);
                        } else {
                        console.error('err', successObj);
                    }
                }, function (errorObj) {
                    let exception = errorObj.data.exception;
                    //noinspection JSAnnotator

                    /* start-dev-code */
                    if(ConfigService.getConst('doSendExceptionEmail')){
                        html2canvas(document.body).then(canvas => {
                            let reportRequestModel = {
                                method: 'sendEmailWithExcepitonInfo',
                                service: 'SystemService',
                                class: 'regiec.knast.erp.api.dto.system.SendEmailWithExcepitonInfoRequest',
                                data: {
                                    request: JSON.stringify(requestModel),
                                    response: JSON.stringify(exception),
                                    screenshot: canvas.toDataURL()
                                }
                            };

                            $http(generateRequestOptions(reportRequestModel));
                        });
                    }
                    /* end-dev-code */

                    // noinspection JSAnnotator
                    if(exception.arguments && exception.arguments.length){
                        let errArg = {};
                        //noinspection JSAnnotator
                        exception.arguments.forEach(arg => {errArg[arg.key] = arg.value});
                        if(errArg.code){
                            AlertService.error(`ERROR.${errArg.code}`, null, errArg.params);
                        } else if (errArg.messageInfo){
                            AlertService.error(errArg.messageInfo);
                        }
                    } else {
                        AlertService.error("ERROR.0");
                    }
                    def.reject(exception)
                });

            return def.promise;
        };

        this.getResouceLink = function(id, forceApplicationDataContentType){
            return window.location.origin + '/api/invoker/ResourceInfoService/getResourceById?id=' + id + '&forceApplicationDataContentType=' + (forceApplicationDataContentType ? 'TRUE' : 'FALSE')
        };

        this.getUrl = getUrl
    }
})();
