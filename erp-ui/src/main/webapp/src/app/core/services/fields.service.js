(function () {
    'use strict';

    angular.module('JangraERP.core')
        .service('FieldsService', FieldsService);

    /** @ngInject */
    function FieldsService(SystemService) {

        this.getFields = function () {
            return fields;
        };

        this.getField = function (name) {
            return fields[name];
        };

        /**
         * Generate simple field
         * @param {String} type
         * @param {String|Array} idAndKey
         * @param {String|Array} labelAndPlaceholder
         * @param {Boolean=} required
         * @param mergeOptions
         * @returns {{templateOptions: {label: String, placeholder: String, required: Boolean}, id: String, type: String, key: String}}
         */
        function generateSimpleField(type, idAndKey, labelAndPlaceholder, required, mergeOptions) {
            let field = {
                type: type
            };

            let templateOptions = {
                required: required
            };

            if (angular.isString(idAndKey)) {
                field.id = field.key = idAndKey
            } else if (angular.isArray(idAndKey)) {
                field.id = idAndKey[0];
                field.key = idAndKey[1];
            }

            if (angular.isString(labelAndPlaceholder)) {
                templateOptions.label = templateOptions.placeholder = labelAndPlaceholder
            } else if (angular.isArray(labelAndPlaceholder)) {
                templateOptions.label = labelAndPlaceholder[0];
                templateOptions.placeholder = labelAndPlaceholder[1];
            }

            field.templateOptions = templateOptions;

            return mergeOptions ? angular.merge(field, mergeOptions) : field;
        }

        function generateHorizontalInputField(idAndKey, labelAndPlaceholder, required = false, mergeOptions) {
            return generateSimpleField('horizontalInput', idAndKey, labelAndPlaceholder, required = false, mergeOptions);
        }

        function addField(field) {
            fields[field.id] = field;
        }

        function addFields(fields) {
            fields.forEach(function (field) {
                addField(field)
            })
        }

        let fields = {
            /**
             * reference type
             */
            ref_materialTemplate: {
                id: 'ref_materialTemplate',
                key: 'materialTemplate',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    label: 'Szablon materiału',
                    placeholder: 'Szablon materiału',
                    valueProp: 'materialCode',
                    uiSref: 'templates.materialTemplate',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['materialCode', 'particularDimension', 'materialType.name'],
                    requestService: 'MaterialTemplateRequestService',
                    smallHtml: 'Wymiar: <span ng-bind-html="item.particularDimension | highlight: $select.search"></span> ({{::item.materialType.name}})'
                }
            },
            ref_wareTemplate: {
                id: 'ref_wareTemplate',
                key: 'wareTemplate',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    label: 'Szablon towaru',
                    placeholder: 'Szablon towaru',
                    valueProp: 'wareCode',
                    uiSref: 'templates.wareTemplate',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['wareCode', 'name', 'wareType.name'],
                    requestService: 'WareTemplateRequestService',
                    smallHtml: 'Nazwa: <span ng-bind-html="item.name | highlight: $select.search"></span> ({{::item.wareType.name}})'
                }
            },
            ref_productTemplate: {
                id: 'ref_productTemplate',
                key: 'productTemplate',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    label: 'Szablon produktu',
                    placeholder: 'Szablon produktu',
                    valueProp: 'productNo',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['productNo', 'name'],
                    requestService: 'ProductTemplateRequestService',
                    smallHtml: 'Nazwa: <span ng-bind-html="item.name | highlight: $select.search"></span>'
                }
            },
            ref_worker: {
                id: 'ref_worker',
                key: 'worker',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    label: 'Pracownik',
                    placeholder: 'Wybierz pracownika',
                    valueProp: 'secondName',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['firstName', 'secondName'],
                    requestService: 'WorkerRequestService',
                    smallHtml: '<span ng-bind-html="item.firstName | highlight: $select.search"></span>'
                }
            },
            materialTypeId: {
                id: 'materialTypeId',
                key: 'materialType',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    uiSref: 'settings.materialType',
                    label: 'Typ materiału',
                    placeholder: 'Typ materiału',
                    valueProp: 'name',
                    items: [],
                    refreshDelay: 0,
                    projection: ['name', 'baseDimensionTemplate', 'baseDimensionUnit'],
                    searchFilter: ['name', 'baseDimensionTemplate'],
                    requestService: 'MaterialTypeRequestService',
                    smallHtml: '<div>Wymiar: {{item.baseDimensionTemplate}} [{{item.baseDimensionUnit}}]</div>'
                }
            },
            operationNameId: {
                id: 'operationNameId',
                key: 'operationName',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    uiSref: 'settings.operationName',
                    label: 'Operacja',
                    placeholder: 'Operacja',
                    valueProp: 'name',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['name'],
                    requestService: 'OperationNameRequestService',
                    smallHtml: ''
                }
            },
            /**
             * template type
             */
            recipientId: {
                id: 'recipientId',
                key: 'recipientId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    label: 'Odbiorca',
                    placeholder: 'Wybierz odbiorce',
                    valueProp: 'name',
                    labelProp: 'name',
                    uiSref: 'contractors.recipient',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['name'],
                    services: ['$q', 'SystemService'],
                    requestService: 'RecipientRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}',
                },
                asyncValidators: {
                    uniqueOrderNoAndRecipient: {
                        expression: function ($viewValue, $modelValue, scope) {
                            if (scope.model.nr) {
                                //recipientId
                                // assume that $viewValue.id exist becouse $viewValue is object RecipientDTO
                                return scope.services['SystemService'].validate2('ORDER', "recipient", $viewValue.id || $viewValue, "nr", scope.model.nr, scope.model.id).then(function (valid) {
                                    if (!valid) {
                                        throw new Error('uniqueOrderNoAndRecipient');
                                    }
                                });
                            } else {
                                return scope.services['$q'].resolve();
                            }
                        }
                    }
                },
                modelOptions: {
                    updateOn: 'default'
                }
            },
            providerId: {
                id: 'providerId',
                key: 'providerId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    label: 'Dostawca',
                    placeholder: 'Wybierz dostawce',
                    valueProp: 'name',
                    labelProp: 'name',
                    uiSref: 'contractors.provider',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['name'],
                    requestService: 'ProviderRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}'
                }
            },
            sellerId: {
                id: 'sellerId',
                key: 'sellerId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    label: 'Sprzedawca',
                    placeholder: 'Wybierz sprzedawcę',
                    valueProp: 'name',
                    labelProp: 'name',
                    uiSref: 'contractors.seller',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['name'],
                    requestService: 'SellerRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}'
                }
            },
            materialTemplateId: {
                id: 'materialTemplateId',
                key: 'materialTemplateId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    label: 'Szablon materiału',
                    placeholder: 'Szablon materiału',
                    valueProp: 'materialCode',
                    uiSref: 'templates.materialTemplate',
                    items: [],
                    refreshDelay: 0,
                    projection: ['materialCode', 'particularDimension', 'materialType.name', 'steelGrade'],
                    searchFilter: ['materialCode', 'particularDimension', 'materialType.name'],
                    customFilter: function ($scope, filter) {
                        if ($scope.formOptions.data && $scope.formOptions.data.typeId) {
                            filter.constraints.push({
                                value: $scope.formOptions.data.typeId,
                                field: 'materialType.id',
                                constraintType: 'EQUALS'
                            });
                        }
                    },
                    requestService: 'MaterialTemplateRequestService',
                    selectedItemHtml: '{{$select.selected.materialType.name}} {{$select.selected.particularDimension}} ({{$select.selected[to.valueProp]}}, {{$select.selected.steelGrade.steelGrade}})',
                    smallHtml: 'Wymiar: <span ng-bind-html="item.particularDimension | highlight: $select.search"></span>\
                            (<span ng-bind-html="item.materialType.name | highlight: $select.search"></span>{{::item.steelGrade.steelGrade}})'
                }
            },
            materialId: {
                id: 'materialId',
                key: 'materialId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    label: 'Materiał',
                    placeholder: 'Materiał',
                    valueProp: 'materialTemplateId',
                    uiSref: 'material.preview',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: [],
                    customFilter: function ($scope, filter) {
                        filter.constraints.push({
                            value: $scope.model.materialTemplate.materialCode,
                            field: 'materialTemplate.materialCode',
                            constraintType: 'EQUALS'
                        });
                        filter.constraints.push({
                            value: $scope.model.cuttingSize.length,
                            field: 'leftLength.length',
                            constraintType: 'GRATHER_THAN_OR_EQUAL'
                        });
                    },
                    requestService: 'MaterialRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}',
                    smallHtml: 'Ilość: <span ng-bind-html="item.count | highlight: $select.search"></span>, Długość: {{::item.length}}, Pozostała długość: {{::item.leftLength}}'
                }
            },
            wareTemplateId: {
                id: 'wareTemplateId',
                key: 'wareTemplateId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    label: 'Szablon towaru',
                    placeholder: 'Szablon towaru',
                    valueProp: 'wareCode',
                    uiSref: 'templates.wareTemplate',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['wareCode', 'name', 'wareType.name'],
                    customFilter: function ($scope, filter) {
                        if ($scope.formOptions.data && $scope.formOptions.data.typeId) {
                            filter.constraints.push({
                                value: $scope.formOptions.data.typeId,
                                field: 'wareType.id',
                                constraintType: 'EQUALS'
                            });
                        }
                    },
                    requestService: 'WareTemplateRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}',
                    smallHtml: 'Nazwa: <span ng-bind-html="item.name | highlight: $select.search"></span> ({{::item.wareType.name}})'
                }
            },
            productTemplateId: {
                id: 'productTemplateId',
                key: 'productTemplateId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    label: 'Szablon produktu',
                    onlyValid: false,
                    placeholder: 'Szablon produktu',
                    valueProp: 'productNo',
                    uiSref: 'templates.productTemplate',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['productNo', 'name'],
                    customFilter: function ($scope, filter) {
                        if ($scope.to.onlyValid) {
                            filter.constraints.push({
                                value: "tak",
                                field: 'validationStatus',
                                constraintType: 'EQUALS'
                            });
                        }
                    },
                    requestService: 'ProductTemplateRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}',
                    smallHtml: 'Nazwa: <span ng-bind-html="item.name | highlight: $select.search"></span>'
                }
            },
            saleProductTemplateId: {
                id: 'saleProductTemplateId',
                key: 'productTemplate',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    label: 'Szablon produktu',
                    onlyValid: false,
                    placeholder: 'Szablon produktu',
                    valueProp: 'productNo',
                    uiSref: 'templates.product',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['productNo', 'name'],
                    projection: ['name', 'productNo', 'salesOffers'],//,'name', 'baseDimensionTemplate', 'baseDimensionUnit'],
                    requestService: 'ProductTemplateRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}',
                    smallHtml: 'Nazwa: <span ng-bind-html="item.name | highlight: $select.search"></span>'
                }
            },
            productId: {
                id: 'productId',
                key: 'productTemplateId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    label: 'Produkt nadrzędny',
                    placeholder: 'Wybierz produkt',
                    valueProp: 'productNo',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['productNo', 'name'],
                    requestService: 'ProductTemplateRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}',
                    smallHtml: 'Nazwa: <span ng-bind-html="item.name | highlight: $select.search"></span> ({{::option.name}})'
                }
            },
            wareTypeId: {
                id: 'wareTypeId',
                key: 'wareTypeId',
                type: 'horizontalTemplateBase',
                templateOptions: {
                    uiSref: 'settings.wareType',
                    label: 'Typ towaru',
                    placeholder: 'Typ towaru',
                    valueProp: 'name',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['name'],
                    requestService: 'WareTypeRequestService',
                    selectedItemHtml: '{{$select.selected[to.valueProp]}}',
                    smallHtml: ''
                }
            },
            allowOperationsIds: {
                id: 'allowOperationsIds',
                key: 'allowOperations',
                type: 'horizontalMultiReferenceBase',
                templateOptions: {
                    uiSref: 'settings.operationName',
                    label: 'Dozwolone operacje',
                    placeholder: 'Wybierz operacje',
                    valueProp: 'name',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['name'],
                    requestService: 'OperationNameRequestService',
                    smallHtml: ''
                }
            },
            professionGroupId: {
                id: 'professionGroupId',
                key: 'professionGroup',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    uiSref: 'settings.professionGroup',
                    label: 'Grupa zawodowa',
                    placeholder: 'Grupa zawodowa',
                    valueProp: 'name',
                    items: [],
                    refreshDelay: 0,
                    smallHtml: '<div>Kod: {{item.code}}</div>',
                    requestService: 'ProfessionGroupRequestService',
                },
                expressionProperties: {
                    'templateOptions.disabled': '!model.operationNameId'
                },
                // DEV
                // watcher: {
                //     expression: 'model.operationNameId',
                //     listener: function (field, newValue, oldValue, $scope) {
                //         if (newValue !== oldValue) {
                //             $scope.model.professionGroup = undefined;
                //             $scope.model.professionGroupId = undefined;
                //             $scope.options.resetModel();
                //         }
                //     }
                // },
                controller: /* @ngInject */ function ($scope, ProfessionGroupRequestService) {
                    $scope.to.refresh = function (search) {
                        if ($scope.model && !$scope.model.operationNameId) {
                            return;
                        } else {
                            ProfessionGroupRequestService.listProfessionGroupsWithOperationId($scope.model.operationNameId).then(function (responseData) {
                                $scope.to.items = responseData.elements;
                            });
                        }

                        // let filter = {
                        //     constraints: [{
                        //         value: 'REMOVED',
                        //         field: 'state',
                        //         constraintType: 'NOT_EQUALS'
                        //     }],
                        //     criteriaFilters: [],
                        //     value: 'AND'
                        // };
                        // if ($scope.model && !$scope.model.operationNameId) {
                        //     return
                        // } else {
                        //     filter.criteriaFilters.push({
                        //         constraints: [{
                        //             value: $scope.model.operationNameId,
                        //             field: 'allowOperations',
                        //             constraintType: 'EQUALS'
                        //         }],
                        //         value: 'AND'
                        //     });
                        // }
                        //
                        // if (search) {
                        //     filter.criteriaFilters.push({
                        //         constraints: [{
                        //             value: search,
                        //             field: 'name',
                        //             constraintType: 'LIKE_IGNORE_CASE'
                        //         }, {
                        //             value: search,
                        //             field: 'code',
                        //             constraintType: 'LIKE_IGNORE_CASE'
                        //         }],
                        //         value: 'OR'
                        //     });
                        // }
                        // return ProfessionGroupRequestService.list({
                        //     page: 0,
                        //     pageSize: 0,
                        //     criteriaFilters: filter
                        // }).then(function (responseData) {
                        //     $scope.to.items = responseData.elements;
                        // });
                    };

                    $scope.$watch('model.operationNameId', function (newVal, oldValue) {
                        if (newVal !== oldValue && !oldValue) {
                            $scope.model.professionGroup = undefined;
                            $scope.options.resetModel();
                        }
                    });
                }
            },
            professionGroupId2: {
                id: 'professionGroupId',
                key: 'professionGroup',
                type: 'horizontalReferenceBase',
                templateOptions: {
                    uiSref: 'settings.professionGroup',
                    label: 'Grupa zawodowa',
                    placeholder: 'Grupa zawodowa',
                    valueProp: 'name',
                    items: [],
                    refreshDelay: 0,
                    searchFilter: ['name'],
                    requestService: 'ProfessionGroupRequestService',
                    smallHtml: '<div>Kod: {{item.code}}</div>',
                }
            },

            /**
             * Datapicker type
             */
            datePicker_creationDate: {
                id: 'datePicker_creationDate',
                key: 'creationDate',
                type: 'horizontalDatepicker',
                templateOptions: {
                    label: 'Data utworzenia',
                    placeholder: 'Data utworzenia',
                    options: SystemService.getOptions('datePicker')
                }
            },
            datePicker_lastModificationDate: {
                id: 'datePicker_lastModificationDate',
                key: 'lastModificationDate',
                type: 'horizontalDatepicker',
                templateOptions: {
                    label: 'Data ostatniej modyfikacji',
                    placeholder: 'Data ostatniej modyfikacji',
                    options: SystemService.getOptions('datePicker')
                }
            },
            datePicker_orderCreationDate: {
                id: 'datePicker_orderCreationDate',
                key: 'orderCreationDate',
                type: 'horizontalDatepicker',
                templateOptions: {
                    type: 'date',
                    label: 'Termin płatności',
                    placeholder: 'Termin płatności',
                    options: SystemService.getOptions('datePicker')
                }
            },
            datePicker_deliveryDate: {
                id: 'datePicker_deliveryDate',
                key: 'deliveryDate',
                type: 'horizontalDatepicker',
                templateOptions: {
                    type: 'date',
                    label: 'Termin dostawy',
                    placeholder: 'Termin dostawy',
                    options: SystemService.getOptions('datePicker')
                },
                hideExpression: function ($viewValue, $modelValue, scope) {
                    return scope.options && scope.options.data && scope.options.data.mainModel && scope.options.data.mainModel.data && scope.options.data.mainModel.data.frameworkContractOrder === true;
                }
            },
            datePicker_deadline: {
                id: 'datePicker_deadline',
                key: 'deadline',
                type: 'horizontalDatepicker',
                templateOptions: {
                    type: 'date',
                    label: 'Termin',
                    placeholder: 'Termin',
                    options: {
                        format: "$locale.DATETIME_FORMATS.mediumDate",
                        singleDatePicker: true
                    }
                }
            },

            /**
             * email type
             */
            email: {
                id: 'email',
                key: 'email',
                type: 'horizontalInput',
                templateOptions: {
                    type: 'email',
                    label: 'Email',
                    placeholder: 'Email'
                }
            },

            /**
             * price type
             */
            priceFloat: {
                id: 'priceFloat',
                key: 'price',
                type: 'horizontalPrice',
                templateOptions: {
                    min: 0,
                    type: 'Number',
                    label: 'Cena',
                    placeholder: 'Cena'
                }
            },
            orderPositionPrice: {
                id: 'orderPositionPrice',
                key: 'orderPositionPrice',
                type: 'horizontalPrice',
                templateOptions: {
                    min: 0,
                    type: 'Number',
                    label: 'Cena za sztukę',
                    placeholder: 'Cena za sztukę'
                }
            },
            discount: {
                id: 'discount',
                key: 'discount',
                type: 'horizontalPrice',
                defaultValue: 0,
                templateOptions: {
                    min: 0,
                    max: 100,
                    type: 'Number',
                    label: 'Rabat',
                    placeholder: 'Rabat'
                }
            },

            /**
             * number type
             */
            count: {
                id: 'count',
                key: 'count',
                type: 'horizontalNaturalNumber',
                templateOptions: {
                    min: 1,
                    type: 'Number',
                    label: 'Ilość',
                    placeholder: 'Ilość'
                }
            },
            unsatisfied: {
                id: 'unsatisfied',
                key: 'unsatisfied',
                type: 'horizontalNaturalNumber',
                templateOptions: {
                    min: 0,
                    type: 'Number',
                    label: 'Pozostało',
                    placeholder: 'Pozostało'
                }
            },
            satisfied: {
                id: 'satisfied',
                key: 'satisfied',
                type: 'horizontalNaturalNumber',
                templateOptions: {
                    min: 0,
                    type: 'Number',
                    label: 'Zaspokojono',
                    placeholder: 'Zaspokojono'
                }
            },
            meterToKgRatio: {
                id: 'meterToKgRatio',
                key: 'meterToKgRatio',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0000000001,
                    type: 'Number',
                    label: 'Metr na kg',
                    placeholder: 'Metr na kg'
                }
            },
            kgToPieceRatio: {
                id: 'kgToPieceRatio',
                key: 'kgToPieceRatio',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0000000001,
                    type: 'Number',
                    label: 'Szt. w kg.',
                    placeholder: 'Szt. w kg.'
                }
            },
            reserved: {
                id: 'reserved',
                key: 'reserved',
                type: 'horizontalNaturalNumber',
                templateOptions: {
                    min: 0,
                    type: 'Number',
                    label: 'Zarezerwowane szt.',
                    placeholder: 'Zarezerwowane szt.'
                }
            },
            free: {
                id: 'free',
                key: 'free',
                type: 'horizontalNaturalNumber',
                templateOptions: {
                    min: 0,
                    type: 'Number',
                    label: 'Wolne szt.',
                    placeholder: 'Wolne szt.'
                }
            },
            itemsCount: {
                id: 'itemsCount',
                key: 'itemsCount',
                type: 'horizontalNaturalNumber',
                templateOptions: {
                    min: 1,
                    type: 'Number',
                    label: 'Ilość',
                    placeholder: 'Ilość elementów'
                }
            },
            countInKg: {
                id: 'countInKg',
                key: 'countInKg',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0001,
                    type: 'Number',
                    label: 'Przelicznik',
                    placeholder: 'Przelicznik'
                }
            },
            daysForPayment: {
                id: 'daysForPayment',
                key: 'daysForPayment',
                type: 'horizontalNaturalNumber',
                templateOptions: {
                    min: 0,
                    type: 'Number',
                    label: 'Liczba dni na płatność',
                    placeholder: 'Liczba dni na płatność'
                }
            },
            leftLength: {
                id: 'leftLength',
                key: 'leftLength',
                type: 'horizontalInput',
                templateOptions: {
                    type: 'Number',
                    label: 'Pozostała długość',
                    placeholder: 'Pozostała długość'
                }
            },
            reservedLength: {
                id: 'reservedLength',
                key: 'reservedLength',
                type: 'horizontalInput',
                templateOptions: {
                    type: 'Number',
                    label: 'Zarezerwowana długość',
                    placeholder: 'Zarezerwowana długość'
                }
            },
            buildingTime: {
                id: 'buildingTime',
                key: 'buildingTime',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0001,
                    type: 'Number',
                    label: 'Czas wytw. detalu',
                    placeholder: 'Wyliczane automatycznie'
                }
            },
            totalBuildingTime: {
                id: 'totalBuildingTime',
                key: 'totalBuildingTime',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0001,
                    type: 'Number',
                    label: 'Czas wytw. detalu i podzespołów',
                    placeholder: 'Wyliczane automatycznie'
                }
            }, transportTime: {
                id: 'transportTime',
                key: 'transportTime',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0001,
                    type: 'Number',
                    label: 'Czas transp. [godziny]',
                    placeholder: 'Czas transportu'
                }
            }, preparationTime: {
                id: 'preparationTime',
                key: 'preparationTime',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0001,
                    type: 'Number',
                    label: 'Czas przyg.-zak. [godziny]',
                    placeholder: 'Czas przygotowawczo-zakończeniowy'
                }
            }, operationTime: {
                id: 'operationTime',
                key: 'operationTime',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0001,
                    type: 'Number',
                    label: 'Czas jedn. [godziny]',
                    placeholder: 'Czas wytwarzania jednej sztuki'
                }
            },
            operationNumber: {
                id: 'operationNumber',
                key: 'operationNumber',
                type: 'horizontalInput',
                templateOptions: {
                    type: 'operationNumber',
                    label: 'Numer operacji',
                    placeholder: 'Numer operacji'
                }
            },

            /*****************************************************************
             * ****************************************************************
             * ****************************************************************
             * ****************************************************************
             * string type
             * ****************************************************************
             * ****************************************************************
             * ****************************************************************
             */
            disposedHideableCount: {
                id: 'disposedHideableCount',
                key: 'disposedHideableCount',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'To change'
                },
                hideExpression: function ($viewValue, $modelValue, scope) {
                    return scope.options && scope.options.data && scope.options.data.mainModel && scope.options.data.mainModel.data && scope.options.data.mainModel.data.frameworkContractOrder === false;
                }
            },
            machineCode: {
                id: 'machineCode',
                key: 'machineCode',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Kod',
                    placeholder: 'Kod maszyny w systemie',
                    onBlur: function (value, options) {
                        options.validation.show = null;
                    }
                },
                controller: /* @ngInject */ function ($scope, SystemService) {
                    $scope.validate = SystemService.validate;
                },
                asyncValidators: {
                    uniqueTemplate: {
                        expression: function ($viewValue, $modelValue, scope) {
                            return scope.validate('MACHINE', scope.options.key, $viewValue, scope.model.id).then(function (valid) {
                                if (!valid) {
                                    throw new Error('uniqueMachineCode');
                                }
                            });
                        }
                    }
                },
                modelOptions: {
                    updateOn: 'blur'
                }
            },
            workerCode: {
                id: 'workerCode',
                key: 'workerCode',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Kod',
                    placeholder: 'Kod pracownika w systemie',
                    onBlur: function (value, options) {
                        options.validation.show = null;
                    }
                },
                controller: /* @ngInject */ function ($scope, SystemService) {
                    $scope.validate = SystemService.validate;
                },
                asyncValidators: {
                    uniqueTemplate: {
                        expression: function ($viewValue, $modelValue, scope) {
                            return scope.validate('WORKER', scope.options.key, $viewValue, scope.model.id).then(function (valid) {
                                if (!valid) {
                                    throw new Error('uniqueMaterialCode');
                                }
                            });
                        }
                    }
                },
                modelOptions: {
                    updateOn: 'blur'
                }
            },
            materialCode: {
                id: 'materialCode',
                key: 'materialCode',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Kod',
                    placeholder: 'kod',
                    onBlur: function (value, options) {
                        options.validation.show = null;
                    }
                },
                controller: /* @ngInject */ function ($scope, SystemService) {
                    $scope.validate = SystemService.validate;
                },
                asyncValidators: {
                    uniqueTemplate: {
                        expression: function ($viewValue, $modelValue, scope) {
                            return scope.validate('MATERIAL_TEMPLATE', scope.options.key, $viewValue, scope.model.id).then(function (valid) {
                                if (!valid) {
                                    throw new Error('uniqueMaterialCode');
                                }
                            });
                        }
                    }
                },
                modelOptions: {
                    updateOn: 'blur'
                }
            },
            stateDescription: {
                id: 'stateDescription',
                key: 'stateDescription',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Info o stanie',
                    placeholder: 'Info o stanie'
                }
            },
            productNo: {
                id: 'productNo',
                key: 'productNo',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Nr detalu',
                    placeholder: 'Nr detalu'
                },
                controller: /* @ngInject */ function ($scope, SystemService) {
                    $scope.validate = SystemService.validate;
                },
                asyncValidators: {
                    uniqueTemplate: {
                        expression: function ($viewValue, $modelValue, scope) {
                            return scope.validate('PRODUCT_TEMPLATE', scope.options.key, $viewValue, scope.model.id).then(function (valid) {
                                if (!valid) {
                                    throw new Error('uniqueProductNo');
                                }
                            });
                        }
                    }
                },
                modelOptions: {
                    updateOn: 'blur'
                }
            },
            orderNo: {
                id: 'orderNo',
                key: 'orderNo',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Nr zamówienia',
                    placeholder: 'Nr zamówienia'
                },
                controller: /* @ngInject */ function ($scope, SystemService, $q) {
                    $scope.q = $q;
                    $scope.validate = SystemService.validate2;
                },
                asyncValidators: {
                    uniqueOrderNoAndRecipient: {
                        expression: function ($viewValue, $modelValue, scope) {
                            if (scope.model.recipientId) {
                                //nr
                                return scope.validate('ORDER', scope.options.key, $viewValue, "recipient", scope.model.recipientId, scope.model.id).then(function (valid) {
                                    if (!valid) {
                                        throw new Error('uniqueOrderNoAndRecipient');
                                    }
                                });
                            } else {
                                return scope.q.resolve();
                            }
                        }
                    }
                },
                modelOptions: {
                    updateOn: 'blur'
                }
            },
            orderId: {
                id: 'orderId',
                key: 'orderNo',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Nr zamówienia',
                    placeholder: 'Nr zamówienia'
                }
            },
            particularDimension: {
                id: 'particularDimension',
                key: 'particularDimension',
                type: 'horizontalDimension',
                templateOptions: {
                    label: 'Wymiar',
                    placeholder: 'Wymiar',
                    dep: 'materialType',
                    search: '<input type="text" class="form-control" ng-model="model.filter.particularDimension" ng-model-options="{ debounce: 500 }" ng-change="search()"/>'
                }
            },
            wareCode: {
                id: 'wareCode',
                key: 'wareCode',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Kod',
                    placeholder: 'kod'
                },
                controller: /* @ngInject */ function ($scope, SystemService) {
                    $scope.validate = SystemService.validate;
                },
                asyncValidators: {
                    uniqueTemplate: {
                        expression: function ($viewValue, $modelValue, scope) {
                            return scope.validate('WARE_TEMPLATE', scope.options.key, $viewValue, scope.model.id).then(function (valid) {
                                if (!valid) {
                                    throw new Error('uniqueWareCode');
                                }
                            });
                        }
                    }
                },
                modelOptions: {
                    updateOn: 'blur'
                }
            },
            price: {
                id: 'price',
                key: 'price',
                type: 'horizontalInput',
                templateOptions: {
                    min: 0.0001,
                    label: 'Cena',
                    placeholder: 'Cena'
                }
            },
            productionOrderNo: {
                id: 'productionOrderNo',
                key: 'productionOrderNo',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Nr zlecenia produkcyjnego',
                    placeholder: 'Nr zlecenia produkcyjnego'
                }
            },
            productionPlanList: {
                id: 'productionPlanList',
                key: 'productionPlan',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'productionPlan',
                    placeholder: 'productionPlan'
                }
            },
            validationStatus: {
                id: 'validationStatus',
                key: 'validationStatus',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Poprawny?',
                    placeholder: 'Tu się nic nie wpisuje'
                }
            },
            validationMessage: {
                id: 'validationMessage',
                key: 'validationMessage',
                templateOptions: {
                    label: 'Błędy w szablonie',
                    tbody: '<button class="btn btn-info btn-xs" ng-if="::item.data.validationMessage.length"  uib-popover-template="\'app/core/templates/popover/validationMessage.html\'" popover-trigger="outsideClick">{{::(item.data.validationMessage.length === 1 ? "1 Błąd" : item.data.validationMessage.length + " Błędów" )}}</button>'
                }
            },
            ptUsages: {
                id: 'ptUsages',
                key: 'ptUsages',
                templateOptions: {
                    label: 'Użycia',
                    tbody: '<button class="btn btn-info btn-xs" ng-if="::item.data.ptUsages.length"  uib-popover-template="\'app/core/templates/popover/ptUsages.html\'" popover-trigger="outsideClick">{{::(item.data.ptUsages.length === 1 ? "1 użycie" : item.data.ptUsages.length + " Użycia" )}}</button>'
                }
            },
            materialUsages: {
                id: 'materialUsages',
                key: 'materialUsages',
                templateOptions: {
                    label: 'Użycia',
                    tbody: '<button class="btn btn-info btn-xs" ng-if="::item.data.materialUsages.length"  uib-popover-template="\'app/core/templates/popover/materialUsages.html\'" popover-trigger="outsideClick">{{::(item.data.materialUsages.length === 1 ? "1 użycie" : item.data.materialUsages.length + " Użycia" )}}</button>'
                }
            },
            wareUsages: {
                id: 'wareUsages',
                key: 'wareUsages',
                templateOptions: {
                    label: 'Użycia',
                    tbody: '<button class="btn btn-info btn-xs" ng-if="::item.data.wareUsages.length"  uib-popover-template="\'app/core/templates/popover/wareUsages.html\'" popover-trigger="outsideClick">{{::(item.data.wareUsages.length === 1 ? "1 użycie" : item.data.wareUsages.length + " Użycia" )}}</button>'
                }
            },
            wareType: {
                id: 'wareType',
                key: 'wareType',
                type: 'horizontalInput',
                templateOptions: {
                    label: 'Typ',
                    placeholder: 'Typ'
                }
            },

            /**
             * textarea
             */
            comment: {
                id: 'comment',
                key: 'comment',
                type: 'horizontalText',
                templateOptions: {
                    label: 'Uwagi',
                    placeholder: 'Uwagi',
                    taOptions: {
                        toolbar: [
                            ['bold', 'italics', 'underline', 'strikeThrough'],
                            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
                            ['ul', 'ol', 'insertLink'],
                            ['redo', 'undo', 'clear']
                        ]
                    }
                }
            },
            comments: {
                id: 'comments',
                key: 'comments',
                type: 'horizontalText',
                templateOptions: {
                    label: 'Uwagi',
                    placeholder: 'Uwagi',
                    taOptions: {
                        toolbar: [
                            ['bold', 'italics', 'underline', 'strikeThrough'],
                            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
                            ['ul', 'ol', 'insertLink'],
                            ['redo', 'undo', 'clear']
                        ]
                    }
                }
            },

            /**
             * Document generator
             */
            guideForProductTemplate: {
                id: 'guideForProductTemplate',
                key: 'guideForProductTemplate',
                type: 'docGenerator',
                templateOptions: {
                    label: 'Szablon przewodnika',
                    tbody: '<a ng-href="/api/invoker/ProductTemplateService/getProductionGuide?productTemplateId={{::item.data.id}}" target="_blank">Pobierz</a>',
                    prevBody: '<a ng-href="/api/invoker/ProductTemplateService/getProductionGuide?productTemplateId={{::model.id}}" target="_blank">Pobierz</a>'
                }
            },
            guideForProductionOrder: {
                id: 'guideForProductTemplate',
                key: 'guideForProductTemplate',
                type: 'docGenerator',
                templateOptions: {
                    label: 'Przewodnik',
                    tbody: '<a ng-href="/api/invoker/ProductionOrderService/getProductionGuide?productionOrderId={{::item.data.id}}" target="_blank">Pobierz</a>',
                    prevBody: '<a ng-href="/api/invoker/ProductionOrderService/getProductionGuide?productionOrderId={{::model.id}}" target="_blank">Pobierz</a>'
                }
            },

            /**
             * auto fill
             */
            autoFillTest: {
                id: 'autoFillTest',
                key: 'autoFillTest',
                type: 'horizontalAutoFillInput',
                templateOptions: {
                    label: 'autoFillTest',
                    type: 'text',
                    dependancies: ['comments', 'materialCode', 'particularDimension']
                }
            },
            //discount
            productDiscountAutoFilled: {
                id: 'productDiscountAutoFilled',
                key: 'discount',
                type: 'horizontalAutoFillInput',
                templateOptions: {
                    label: 'Rabat %',
                    type: 'number',
                    max: 100,
                    placeholder: 'Rabat %',
                    readonly: 'false',
                    dependancies: [{m: 'productTemplate.salesOffers', r: 'salesOffers'}],
                    additionalArguments: ['orderPositionId'],
                    service: 'OrderService',
                    method: 'productDiscountAutoFilled',
                    clazz: 'regiec.knast.erp.api.dto.order.ProductDiscountAutoFilledRequest'
                }
            },
//            autofill price
            productPriceAutoFilled: {
                id: 'productPriceAutoFilled',
                key: 'piecePrice',
                type: 'horizontalAutoFillUnitable',
                templateOptions: {
                    min: 0,
                    precision: 2,
                    label: 'Cena za szt.',
                    placeholder: 'Cena za szt.',
                    keyInput: 'amount',
                    keyUnit: 'currency',
                    items: ['PLN', 'EUR', 'USD'],
                    readonly: 'false',
                    dependancies: [{m: 'productTemplate.salesOffers', r: 'salesOffers'}],
                    additionalArguments: ['orderPositionId'],
                    service: 'OrderService',
                    method: 'productPriceAutoFilled',
                    clazz: 'regiec.knast.erp.api.dto.order.ProductPriceAutoFilledRequest'
                }
            },
            //            autofill price
            productPriceWithDiscountAutoFilled: {
                id: 'productPriceWithDiscountAutoFilled',
                key: 'piecePriceWithDiscount',
                type: 'horizontalAutoFillUnitable2',
                templateOptions: {
                    min: 0,
                    precision: 2,
                    keyInput: 'amount',
                    keyUnit: 'currency',
                    items: ['PLN', 'EUR', 'USD'],
                    label: 'Cena za szt. z rabatem',
                    placeholder: 'Cena za szt. z rabatem',
                    readonly: 'true',
                    dependancies: ['discount', 'piecePrice'],
                    mandatoryValues: ['discount', 'piecePrice.amount', 'piecePrice.currency'],
                    service: 'OrderService',
                    method: 'productPriceWithDiscountAutoFilled',
                    clazz: 'regiec.knast.erp.api.dto.order.OrderPositionPiecePriceWithDiscountRequest'
                }
            },
            //            autofill text
            calculatedPositionPrice: {
                id: 'calculatedPositionPrice',
                key: 'calculatedPositionPrice',
                type: 'horizontalAutoFillInput',
                templateOptions: {
                    label: 'Wyliczona wartość',
                    type: 'text',
                    placeholder: 'Wyliczona wartość',
                    readonly: 'true',
                    dependancies: ['count', 'discount', 'piecePrice'],
                    service: 'OrderService',
                    method: 'calculateOrderPositionPrice',
                    clazz: 'regiec.knast.erp.api.dto.order.CalculateOrderPositionPriceRequest'
                    //fieldsMap: [{key: 'productTemplate.count', value: 'count'}]
                }
            },

            toDisposeHideableAutoFill: {
                id: 'toDisposeHideableAutoFill',
                key: 'toDisposeHideableAutoFill',
                type: 'horizontalAutoFillInput',
                templateOptions: {
                    label: 'To change',
//                    type: 'number',
                    placeholder: 'To change',
                    readonly: 'true',
                    dependancies: ['count'],
                    additionalArguments: ['orderPositionId'],
                    service: 'OrderService',
                    method: 'calculateToDispose',
                    clazz: 'regiec.knast.erp.api.dto.order.CalculateToDisposeRequest'
                },
                hideExpression: function ($viewValue, $modelValue, scope) {
                    return scope.options && scope.options.data && scope.options.data.mainModel && scope.options.data.mainModel.data && scope.options.data.mainModel.data.frameworkContractOrder === false;
                }
            },
            /*
             *
             id: 'price',
             key: 'price',
             type: 'verticalUnitable',
             className: 'col-md-8',
             templateOptions: {
             required: true,
             min: 0,
             precision: 2,
             keyInput: 'amount',
             keyUnit: 'currency',
             items: ['PLN', 'EUR', 'USD'],
             label: 'Cena bez rabatu',
             placeholder: 'Cena bez rabatu'
             }
             */

            /**
             * custom table fields
             */
            smallAddress: {
                id: 'smallAddress',
                key: 'smallAddress',
                templateOptions: {
                    label: 'Pełny adres',
                    placeholder: 'Pełny adres',
                    tbody: '<div copy-to-clipboard>{{::item.data.postalCode}} {{::item.data.city}} <br/>{{::item.data.street}}</div>',
                    search: '<input multi-input="postalCode|city|street" type="text" class="form-control" ng-model="model.filter[\'smallAddress\']" ng-model-options="{ debounce: 200 }" ng-change="search()">'
                }
            },

            /**
             *
             */
            workersLinks: {
                id: 'workersLinks',
                key: 'workersLinks',
                templateOptions: {
                    label: 'Pracownicy',
                    placeholder: 'Pracownicy',
                    tbody: '<button class="btn btn-info btn-xs" ng-if="::item.data.machineWorkers.length"  uib-popover-template="\'app/core/templates/popover/workers.html\'" popover-trigger="outsideClick">{{::(item.data.machineWorkers.length === 1 ? "1 pracownik" : item.data.machineWorkers.length + " pracowników" )}}</button>'
                }
            },
            deliveryOrderPositions: {
                id: 'deliveryOrderPositions',
                key: 'workersLinks',
                templateOptions: {
                    label: 'Prozycje',
                    placeholder: 'Prozycje',
                    tbody: '<button class="btn btn-info btn-xs" ng-if="::item.data.positions.length"  uib-popover-template="\'app/core/templates/popover/deliveryOrderPositions.html\'" popover-trigger="outsideClick">{{::(item.data.positions.length === 1 ? "1 pozycja" : item.data.positions.length + " pozycji" )}}</button>'
                }
            },
            orderPositionsBriefly: {
                id: 'orderPositionsBriefly',
                key: 'orderPositionsBriefly',
                templateOptions: {
                    label: 'Zawartość zamowienia',
                    tbody: '<button class="btn btn-info btn-xs" ng-if="::item.data.orderPositions.length"  uib-popover-template="\'app/core/templates/popover/orderPositions.html\'" popover-trigger="outsideClick">Ilość: {{::item.data.orderPositions.length}}</button>'
                }
            },

            /**
             * sectionRepeat
             */
            materialAssortmentPositions: {
                id: 'materialAssortmentPositions',
                key: 'materialAssortmentPositions',
                type: 'horizontalRepeatSection',
                templateOptions: {
                    label: 'Dostępne wielkości',
                    fields: [
                        {
                            id: 'salesLength',
                            key: 'salesLength',
                            type: 'verticalDimensionData',
                            className: 'col-md-6',
                            templateOptions: {
                                min: 0,
                                required: true,
                                dep: 'materialTemplateId',
                                label: 'Wymiary',
                                placeholder: 'Wymiary'
                            }
                        },
                        {
                            id: 'salesPrice',
                            key: 'salesPrice',
                            type: 'verticalUnitable',
                            className: 'col-md-6',
                            templateOptions: {
                                required: true,
                                min: 0,
                                precision: 2,
                                keyInput: 'amount',
                                keyUnit: 'currency',
                                items: ['PLN', 'EUR', 'USD'],
                                label: 'Cena',
                                placeholder: 'Cena'
                            }
                        },
                        {
                            id: 'sellUnit',
                            key: 'sellUnit',
                            type: 'verticalConstans',
                            className: 'col-md-12',
                            templateOptions: {
                                required: true,
                                label: 'Cena za',
                                placeholder: 'mb, kg, tona, arkusz',
                                constName: 'sellUnit',
                                items: SystemService.getConts('sellUnit')
                            }
                        }
                    ]
                }
            },
            salesOffers: {
                id: 'salesOffers',
                key: 'salesOffers',
                type: 'horizontalRepeatSection',
                templateOptions: {
                    tbody: '<button class="btn btn-info btn-xs" ng-if="::item.data.salesOffers.length"  uib-popover-template="\'app/core/templates/popover/salesOffers.html\'" popover-trigger="outsideClick">{{::(item.data.salesOffers.length === 1 ? "1 oferta" : item.data.salesOffers.length + " ofert" )}}</button>',
                    prevBody: `<div ng-repeat="s in ::model.salesOffers"><a ui-sref="contractors.recipient.element.detail({ id: s.recipient.id })">{{s.recipient.name}}</a></div>`,
                    label: 'Oferty sprzedaży',
                    fields: [
                        {
                            id: 'recipient',
                            key: 'recipient',
                            type: 'verticalReferenceBase',
                            className: 'col-md-12',
                            templateOptions: {
                                label: 'Odbiorca',
                                placeholder: 'Wybierz odbiorce',
                                required: true,
                                valueProp: 'name',
                                uiSref: 'contractors.recipient',
                                items: [],
                                refreshDelay: 0,
                                searchFilter: ['name'],
                                requestService: 'RecipientRequestService'
                            }
                        },
                        {
                            id: 'price',
                            key: 'price',
                            type: 'verticalUnitable',
                            className: 'col-md-8',
                            templateOptions: {
                                required: true,
                                min: 0,
                                precision: 2,
                                keyInput: 'amount',
                                keyUnit: 'currency',
                                items: ['PLN', 'EUR', 'USD'],
                                label: 'Cena bez rabatu',
                                placeholder: 'Cena bez rabatu'
                            }
                        },
                        {
                            id: 'discount',
                            key: 'discount',
                            type: 'verticalPrice',
                            className: 'col-md-4',
                            templateOptions: {
                                required: true,
                                min: 0,
                                max: 100,
                                type: 'Number',
                                label: 'Rabat',
                                placeholder: 'Rabat'
                            }
                        }
                    ]
                }
            },

            /**
             * formly group
             */
            group_test: {
                "className": "row",
                "fieldGroup": []
            },

            /**
             * html
             */
            hr: {
                template: "<hr />"
            },

            reservedLengthObj: {
                id: 'reservedLengthObj',
                key: 'reservedLength',
                type: 'cuttingSize',
                templateOptions: {
                    field: 'length_or_area_or_sth',
                    label: 'Zarezerwowana długość',
                    placeholder: 'Zarezerwowana długość'
                }
            },
            leftLengthObj: {
                id: 'leftLengthObj',
                key: 'leftLength',
                type: 'cuttingSize',
                templateOptions: {
                    field: 'length_or_area_or_sth',
                    label: 'Pozostała długość',
                    placeholder: 'Pozostała długość'
                }
            },
            lengthObj: {
                id: 'lengthObj',
                key: 'length',
                type: 'cuttingSize',
                templateOptions: {
                    field: 'length_or_area_or_sth',
                    label: 'Długość',
                    placeholder: 'Długość'
                }
            }
        };

        addFields([
            generateHorizontalInputField('name', 'Nazwa'),
            generateHorizontalInputField('firstName', 'Imię'),
            generateHorizontalInputField('secondName', 'Nazwisko'),
            generateHorizontalInputField('city', 'Miasto'),
            generateHorizontalInputField('postalCode', 'Kod pocztowy'),
            generateHorizontalInputField('street', ['Adres', 'Ulica, numer budynku / lokalu']),
            generateHorizontalInputField('nr', 'Numer'),
            generateHorizontalInputField('no', 'Numer'),
            generateHorizontalInputField('nip', 'NIP'),
            generateHorizontalInputField('regon', 'REGON'),
            generateHorizontalInputField('swift', 'SWIFT'),
            generateHorizontalInputField('bankNamePLN', ['Bank PLN', 'Nazwa banku PLN']),
            generateHorizontalInputField('bankAccountPLN', ['Konto PLN', 'Nr konta PLN']),
            generateHorizontalInputField('bankNameEUR', ['Bank EUR', 'Nazwa banku EUR']),
            generateHorizontalInputField('bankAccountEUR', ['Konto EUR', 'Nr konta EUR']),
            generateHorizontalInputField('code', 'Kod'),
            generateHorizontalInputField('state', 'Stan'),
            generateHorizontalInputField('norm', 'Norma'),
            generateHorizontalInputField('label', 'Etykieta'),
            generateHorizontalInputField(['length', 'length.origSize'], 'Długość'),
            generateHorizontalInputField('localization', 'Lokalizacja'),
            generateHorizontalInputField('recipient', 'Odbiorca'),
            generateHorizontalInputField('provider', 'Dostawca'),
            generateHorizontalInputField('materialType', 'Typ'),
            generateHorizontalInputField('deliveryDate', 'Data dostawy'),
            generateHorizontalInputField('positionOnTechDraw', 'Pozycja na rysunku'),
            generateHorizontalInputField('positionOnOrder', ['Pozycja na zam.', 'Pozycja']),
            generateHorizontalInputField(['providerOffers', 'offers'], 'Ofery'),
            generateHorizontalInputField('assortmentId', 'assortmentId'),
            generateHorizontalInputField('productName', 'Nazwa produktu'),
            generateHorizontalInputField('detalNo', 'Numer detalu'),
            generateHorizontalInputField('detalName', 'Nazwa detalu'),
            generateHorizontalInputField('cuttedSectionSize', ['Ciętka', 'Długość/powierzchnia ciętki']),
            generateHorizontalInputField('cuttedSectionSizeWithBorders', ['Limit ciętki', 'Długość/powierzchnia ciętki z naddatkiem']),
            generateHorizontalInputField('baseDimensionUnit', 'Jednostka wymiaru'),
            generateHorizontalInputField('cuttingDimensionUnit', 'Jednostka ciętki'),
            generateHorizontalInputField('buildingTimeStr', ['Czas wytw. detalu', 'Wyliczane automatycznie']),
            generateHorizontalInputField('totalBuildingTimeStr', ['Czas wytw. detalu i podzespołów', 'Wyliczane automatycznie']),
            generateHorizontalInputField('workerMinorNo', 'Numer w grupie'),
            generateHorizontalInputField('removed', 'Usunięty'),
        ]);

        addFields([
            // id
            generateSimpleField('_id', '_id', 'ID', false, {router: '', sortList: 'sort-alpha'}),
            // horizontalId table fields
            generateSimpleField('horizontalId', ['horizontalId', 'id'], 'ID', false, {router: ''}),
            // color
            generateSimpleField('horizontalColorInput', 'color', 'Color', false, {templateOptions: {colorOptions: {}}}),
            // phone
            generateSimpleField('horizontalPhone', 'phone', 'Telefon'),
            // postalCode
            generateSimpleField('horizontalPostalCode', 'postalCodeDev', 'Kod pocztowy'),
            // textarea
            generateSimpleField('horizontalTextarea', 'positionsStr', 'Pozycje'),
            // checkbox
            generateSimpleField('horizontalCheckbox', 'frameworkContractOrder', 'Zamówienie ramowe'),
            generateSimpleField('horizontalCheckbox', ['productTemplateType', 'type'], 'Produkt niesprzedawany', null, {
                templateOptions: {
                    trueValue: 'NO_SALE',
                    falseValue: 'SALE',
                }
            }),
            // table formly
            generateSimpleField('tableFormly', 'productionPlan', 'Plan produkcji', null, {templateOptions: {fields: []}}),
            generateSimpleField('tableFormly', 'orderPositions', 'Pozycje na zamówieniu', null, {templateOptions: {fields: []}}),
            // Upload
            generateSimpleField('horizontalUpload', 'techDrawingPDF', 'Rys. techniczny PDF', null, {
                templateOptions: {
                    previewType: 'pdf',
                    html: '<a ng-if="model.techDrawingPDFdto" ng-href="/api/invoker/ResourceInfoService/getResourceById?id={{model.techDrawingPDFdto.resourceId}}" target="_blank">Nazwa: {{model.techDrawingPDFdto.resourceName}}, data dodania: {{model.techDrawingPDFdto.creationDate | date:"medium"}}</a>'
                }
            }),
            generateSimpleField('horizontalUpload', 'techDrawingDXF', 'Rys. techniczny DXF', null, {
                templateOptions: {
                    html: '<a ng-if="model.techDrawingDXFdto" ng-href="/api/invoker/ResourceInfoService/getResourceById?id={{model.techDrawingDXFdto.resourceId}}" target="_blank">Nazwa: {{model.techDrawingDXFdto.resourceName}}, data dodania: {{model.techDrawingDXFdto.creationDate | date:"medium"}}</a>'
                }
            }),
            generateSimpleField('horizontalUpload', 'reportTemplate', 'Szablon raportu', null, {
                templateOptions: {
                    previewType: 'pdf',
                    html: '<a ng-if="model.reportTemplatedto" ng-href="/api/invoker/ResourceInfoService/getResourceById?id={{model.reportTemplatedto.resourceId}}" target="_blank">Nazwa: {{model.reportTemplatedto.resourceName}}, data dodania: {{model.reportTemplatedto.creationDate | date:"medium"}}<a/>'
                }
            }),
            // link
            generateSimpleField('horizontalLink', ['invoice', 'invoiceId'], 'Faktura', null, {
                templateOptions: {
                    url: '/api/invoker/ProductService/getMyPdfDocument',
                    fieldName: 'documentId'
                }
            }),
            generateSimpleField('horizontalLink', ['wz', 'wzId'], 'WZ', null, {
                templateOptions: {
                    url: '/api/invoker/ProductService/getMyPdfDocument',
                    fieldName: 'documentId'
                }
            }),
            // unitable
            generateSimpleField('horizontalUnitable', 'unitable', 'unitable', null, {
                templateOptions: {
                    keyInput: 'amount',
                    keyUnit: 'currency',
                    items: ['PLN', 'EUR', 'USD']
                }
            }),
            // Constans
            generateSimpleField('horizontalConstans', ['const_materialKind', 'materialKind.materialKind'], 'Rodzaj', null, {
                templateOptions: {
                    type: 'const',
                    constName: 'materialKind',
                    items: SystemService.getConts('materialKind')
                }
            }),
            generateSimpleField('horizontalConstans', ['const_norm', 'norm.norm'], 'Norma', null, {
                templateOptions: {
                    type: 'const',
                    constName: 'norm',
                    items: SystemService.getConts('norm')
                }
            }),
            generateSimpleField('horizontalConstans', ['const_steelGrade', 'steelGrade.steelGrade'], 'Gatunek materiału', null, {
                templateOptions: {
                    type: 'const',
                    constName: 'steelGrade',
                    items: SystemService.getConts('steelGrade')
                }
            }),
            generateSimpleField('horizontalConstans', ['const_tolerance', 'tolerance.tolerance'], 'Tolerancja', null, {
                templateOptions: {
                    type: 'const',
                    constName: 'tolerance',
                    items: SystemService.getConts('tolerance')
                }
            }),
            generateSimpleField('horizontalConstans', ['operationName', 'operationName.operationName'], 'Operacja', null, {
                templateOptions: {
                    type: 'const',
                    constName: 'operationName',
                    items: SystemService.getConts('operationName')
                }
            }),
            generateSimpleField('horizontalConstans', 'operationName', 'Operacja', null, {
                templateOptions: {
                    type: 'const',
                    constName: 'operationName',
                    items: SystemService.getConts('operationName')
                }
            }),
            generateSimpleField('verticalConstans', 'sellUnit', ['Cena za', 'mb, kg, tona, arkusz'], null, {
                templateOptions: {
                    type: 'const',
                    constName: 'sellUnit',
                    items: SystemService.getConts('sellUnit')
                }
            }),
            // select
            generateSimpleField('horizontalSelect', ['select_materialKind', 'materialKind.materialKind'], 'Rodzaj', null, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('materialKind')
                }
            }),
            generateSimpleField('horizontalSelect', ['select_tolerance', 'tolerance.tolerance'], 'Tolerancja', null, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('tolerance')
                }
            }),
            generateSimpleField('horizontalSelect', ['select_norm', 'norm.norm'], 'Norma', null, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('norm')
                }
            }),
            generateSimpleField('horizontalSelect', ['select_steelGrade', 'steelGrade.steelGrade'], 'Gatunek materiału', null, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('steelGrade')
                }
            }),
            generateSimpleField('horizontalSelect', ['select_unit', 'unit'], 'Jednostka', null, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('materialUnit')
                }
            }),
            generateSimpleField('horizontalSelect', 'sizeType', 'Jednostka zakupowa', true, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('sizeType')
                }
            }),
            generateSimpleField('horizontalSelect', 'sizeType', 'Jednostka zakupowa', true, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('sizeType')
                }
            }),
            generateSimpleField('horizontalSelect', 'dimension', 'Wymiar', true, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('dimension')
                }
            }),
            generateSimpleField('horizontalSelect', 'cuttingDimensionTemplate', 'Wymiar ciętki', true, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('dimension')
                }
            }),
            generateSimpleField('horizontalSelect', 'baseDimensionTemplate', 'Podstawowy wymiar', true, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('dimension')
                }
            }),
            generateSimpleField('horizontalSelect', ['wareDimension', 'dimension'], 'Wymiar', true, {
                templateOptions: {
                    type: 'const',
                    items: SystemService.getConts('wareDimension')
                }
            }),
        ]);

        //Generated
        (function () {
            let statesMap = SystemService.getStatesMap();

            for (const stateName in statesMap) {
                if (statesMap.hasOwnProperty(stateName)) {
                    fields[stateName + '_STATE'] = {
                        id: stateName + '_STATE',
                        key: 'state',
                        type: 'horizontalSelect',
                        templateOptions: {
                            label: 'Stan',
                            placeholder: 'Stan',
                            options: 'item.value as item.translation for item in',
                            items: statesMap[stateName]
                        }
                    };
                }
            }
        })();
    }
})();
