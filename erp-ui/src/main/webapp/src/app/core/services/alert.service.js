(function () {
    'use strict';

    angular.module('JangraERP.core')
        .service('AlertService', AlertService);

    /** @ngInject */
    function AlertService($filter, toastr) {
        let toastrConfig = {
            timeOut: 10000,
            extendedTimeOut: 500
        };

        this.info = function (message, title, data, config) {
            toastr.info($filter('translate')(message, data), title, config || toastrConfig)
        };

        this.success = function (message, title, data, config) {
            toastr.success($filter('translate')(message, data), title, config || toastrConfig)
        };

        this.warning = function (message, title, data, config) {
            toastr.warning($filter('translate')(message, data), title, config || toastrConfig)
        };

        this.error = function (message, title, data, config) {
            toastr.error($filter('translate')(message, data), title, config || toastrConfig)
        };
    }
})();
