let gulp = require('gulp');
require('./gulp/depTasks');
require('./gulp/devTasks');
require('./gulp/relTasks');

/*
 * Default task
 */
gulp.task('default', () => null);
