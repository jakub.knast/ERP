let del = require('del');
let gulp = require('gulp');
let config = require('./config');

let plugins = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
    scope: ['devDependencies']
});

/**
 * Development - main task
 */
gulp.task('dev', ['dev_fonts', /*'dev_js',*/ 'dev_scss', 'dev_copy_html', 'dev_copy_assets'], () =>
    gulp.start('dev_html')
);

/*
 * Development - clean task
 */
gulp.task('dev_clean', () => del(config.paths.dev));

/**
 * Development - fonts task
 */
gulp.task('dev_fonts', () =>
    gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe(gulp.dest(`${config.paths.dev}/fonts`))
);

/**
 * Development - javaScript task
 */
gulp.task('dev_js', () => null);

/*
 * Development - scss task
 */
gulp.task('dev_scss', () =>
    gulp.src([`${config.paths.src}/main.scss`, `${config.paths.src}/print.scss`])
        .pipe(plugins.inject(
            gulp.src([
                `${config.paths.src}/scss/**/*.scss`,
                `!${config.paths.src}/scss/theme/conf/**/*.scss`,
                `!${config.paths.src}/scss/404.scss`,
                `!${config.paths.src}/scss/auth.scss`
            ], {read: false}), {
                transform: function (filePath) {
                    return `@import '${filePath}';`;
                },
                starttag: '// injector',
                endtag: '// endinjector',
                ignorePath: [config.paths.src],
                addRootSlash: false
            }
        ))
        .pipe(plugins.sass({
            sourceComments: true,
            sourceMap: true,
            sourceMapRoot: '../../',
            sourceMapEmbed: true,
            sourceMapContents: true
        }).on('error', plugins.sass.logError))
        .pipe(plugins.csscomb())
        .pipe(gulp.dest(`${config.paths.dev}/css`))
        .pipe(plugins.livereload())
);

/*
 * Development - html task
 */
gulp.task('dev_html', () =>
    gulp.src(`${config.paths.src}/index.html`)
        .pipe(plugins.data(config.data))
        .pipe(plugins.nunjucksRender())
        .pipe(plugins.inject(
            gulp.src([
                `${config.paths.dev}/css/main.css`
            ], {read: false}), {
                ignorePath: config.paths.dev,
                addRootSlash: false
            }
        ))
        .pipe(plugins.inject(
            gulp.src([
                `${config.paths.src}/assets/js/*.js`,
                `${config.paths.src}/app/**/*.module.js`,
                `${config.paths.src}/app/app.js`,
                `${config.paths.src}/app/**/*.js`
            ], {read: false}), {
                relative: false,
                ignorePath: [config.paths.dev],
                addRootSlash: false,
                addPrefix: '..'
            }
        ))
        .pipe(plugins.inject(
            gulp.src(plugins.mainBowerFiles(
                (err, fileArray) => fileArray.push('!**/require.js')
            ), {read: false}), {
                name: 'bower',
                addRootSlash: false,
                addPrefix: '..'
            }))
        .pipe(gulp.dest(config.paths.dev))
        .pipe(plugins.livereload())
);

/**
 * Development - copy HTML task
 */
gulp.task('dev_copy_html', () =>
    gulp.src([
        `${config.paths.src}/**/*.html`,
        `!${config.paths.src}/404.html`,
        `!${config.paths.src}/auth.html`,
        `!${config.paths.src}/reg.html`
    ])
        .pipe(gulp.dest(`${config.paths.dev}`))
        .pipe(plugins.livereload())
);

/**
 * Development - copy assets task
 */
gulp.task('dev_copy_assets', () =>
    gulp.src(`${config.paths.src}/assets/**/*`)
        .pipe(gulp.dest(`${config.paths.dev}/assets`))
);

/**
 * Development - reload task
 */
gulp.task('dev_reload', () =>
    gulp.src('', {read: false})
        .pipe(plugins.livereload())
);

/*
 * Development - watch task
 */
gulp.task('dev_watch', () => {
    plugins.livereload.listen();
    gulp.watch(`${config.paths.src}/**/*.js`, ['dev_reload']);
    gulp.watch(`${config.paths.src}/**/*.scss`, ['dev_scss']);
    gulp.watch(`${config.paths.src}/index.html`, ['dev_html']);
    gulp.watch(`${config.paths.src}/**/*.html`, ['dev_copy']);
});
