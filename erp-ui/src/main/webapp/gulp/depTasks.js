let del = require('del');
let gulp = require('gulp');
let config = require('./config');

let plugins = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
    scope: ['devDependencies']
});

/*
 * Dependencies - clean task
 */
gulp.task('dep_clean', () => del([config.paths.node, config.paths.bower]));

/*
 * Dependencies - log plugins name
 */
gulp.task('dep_log', () =>
    console.log(plugins)
);
