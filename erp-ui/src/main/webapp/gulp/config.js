let gutil = require('gulp-util');
let data = require('../config.json');

/**
 *  The main paths project
 */
exports.paths = {
    src: 'src',
    e2e: 'e2e',
    tmp: 'temp',
    dist: 'release',
    dev: 'development',
    node: 'node_modules',
    bower: 'bower_components'
};

/**
 *  Project data
 */
exports.data = data;
