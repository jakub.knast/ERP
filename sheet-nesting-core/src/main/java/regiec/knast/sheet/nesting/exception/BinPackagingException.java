/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.sheet.nesting.exception;

/**
 *
 * @author fbrzuzka
 */
public class BinPackagingException extends RuntimeException{

    public BinPackagingException() {
    }

    public BinPackagingException(String message) {
        super(message);
    }

    public BinPackagingException(String message, Throwable cause) {
        super(message, cause);
    }

    public BinPackagingException(Throwable cause) {
        super(cause);
    }

    public BinPackagingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
