package regiec.knast.sheet.nesting.core;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import regiec.knast.sheet.nesting.exception.BinPackagingException;
import regiec.knast.sheet.nesting.primitives.MArea;

public class BinPacking {

    private static int BIN_COUNT_LIMIT = 100;

    /**
     * Entry point for the application. Applies the packing strategies to the
     * provided pieces.
     *
     * @param pieces pieces to be nested inside the bins.
     * @param binDimension dimensions for the generated bins.
     * @return list of generated bins.
     */
    public static Bin[] BinPackingStrategy(List<MArea> piecesList, Dimension binDimension) {
        MArea[] pieces = piecesList.toArray(new MArea[piecesList.size()]);
        return BinPackingStrategy(pieces, binDimension);
    }

    /**
     * Entry point for the application. Applies the packing strategies to the
     * provided pieces.
     *
     * @param pieces pieces to be nested inside the bins.
     * @param binDimension dimensions for the generated bins.
     * @return list of generated bins.
     */
    public static Bin[] BinPackingStrategy(MArea[] pieces, Dimension binDimension) {
        System.out.println(".............Started computation of bin placements.............");
        ArrayList<Bin> bins = new ArrayList<Bin>();
        int nbin = 0;
        boolean stillToPlace = true;
        MArea[] notPlaced = pieces;
        double startTime = System.currentTimeMillis();
        while (stillToPlace) {
            stillToPlace = false;
            Bin bin = new Bin(binDimension);
            notPlaced = bin.BBCompleteStrategy(notPlaced);

            bin.compress();

            notPlaced = bin.dropPieces(notPlaced);

            System.out.println("Bin " + (++nbin) + " generated");
            if(nbin > BIN_COUNT_LIMIT){
                throw new BinPackagingException("Bin limit reached");
            }
            bins.add(bin);
            if (notPlaced.length > 0) {
                stillToPlace = true;
            }
        }
        double stopTime = System.currentTimeMillis();
        System.out.println();
        System.out.println("Number of used bins: " + nbin);
        System.out.println("Computation time:" + (stopTime - startTime)  + " miliseconds");
        System.out.println();
        return bins.toArray(new Bin[0]);
    }
    
    public static int fillBinToFull(Bin sheetBin, List<MArea> piecesList) {
        System.out.println(".............Started computation of bin placements.............");
        MArea[] notPlaced = piecesList.toArray(new MArea[piecesList.size()]);
        double startTime = System.currentTimeMillis();       
        notPlaced = sheetBin.boundingBoxPacking(notPlaced, true);        
        double stopTime = System.currentTimeMillis();
        System.out.println();
        System.out.println("Number of placed: " + (piecesList.size() - notPlaced.length));
        System.out.println("Number of not placed: " + notPlaced.length);
        System.out.println("Computation time:" + (stopTime - startTime)  + " miliseconds");
        System.out.println();
        return notPlaced.length;
    }
    
//    static  class FillBinToFullResponse{
//        public Bin sheetBin;
//        
//    }

}
