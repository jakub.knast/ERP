/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.sheet.nesting.utils;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import regiec.knast.sheet.nesting.core.Bin;
import regiec.knast.sheet.nesting.primitives.MArea;

/**
 *
 * @author fbrzuzka
 */
public class ResultDrawer {

    
    private String outDir = "example_out";
    
    public ResultDrawer(String outDir) {
        this.outDir = outDir;        
        ensureOutDirExist();
    }   

    public ResultDrawer drawbinToFile(Bin[] bins, Dimension viewPortDimension) throws IOException {
        for (int i = 0; i < bins.length; i++) {

            MArea[] areasInThisbin = bins[i].getPlacedPieces();
            ArrayList<MArea> areas = new ArrayList<MArea>();
            for (MArea area : areasInThisbin) {
                areas.add(area);
            }
            Utils.drawMAreasToFile(areas, viewPortDimension, bins[i].getDimension(), (outDir + File.separator + "Bin-" + String.valueOf(i + 1)));
            System.out.println("Generated image for bin " + String.valueOf(i + 1));
        }
        return this;
    }

    public ResultDrawer createOutputFiles(Bin[] bins) throws IOException {
        for (int i = 0; i < bins.length; i++) {

            PrintWriter writer = new PrintWriter(outDir + File.separator + "Bin-" + String.valueOf(i + 1) + ".txt", "UTF-8");
            writer.println(bins[i].getPlacedPieces().length);
            MArea[] areasInThisbin = bins[i].getPlacedPieces();
            for (MArea area : areasInThisbin) {
                double offsetX = area.getBoundingBox2D().getX();
                double offsetY = area.getBoundingBox2D().getY();
                writer.println(area.getID() + " " + area.getRotation() + " " + offsetX + "," + offsetY);
            }
            writer.close();
            System.out.println("Generated points file for bin " + String.valueOf(i + 1));
        }
        return this;
    }

    private void ensureOutDirExist() {

        File example_out = new File(outDir);
        if (example_out.exists()) {
            if (!example_out.isDirectory()) {
                example_out.delete();
            }
        } else {
            example_out.mkdir();
        }
        System.out.println("Resutl destination directory: " + example_out.getAbsolutePath());
    }
}
