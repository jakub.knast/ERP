/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils.scheduler.insert;

import com.google.common.collect.Lists;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author fbrzuzka
 */
public class InsJobTest {

    public List<Integer> operationsToDo = Lists.newArrayList(1, 1, 1, 1);
    public int doneOperations = 0;

    @Test
    public void t1() {
        doneOperations = 0;
        int result = calculateRemainingTime();
        assertEquals(result, 4);
    }
    
    @Test
    public void t2() {
        doneOperations = 1;
        int result = calculateRemainingTime();
        assertEquals(result, 3);
    }
    
    @Test
    public void t4() {
        doneOperations = 4;
        int result = calculateRemainingTime();
        assertEquals(result, 0);
    }

    public Integer calculateRemainingTime() {
        Integer remainingTime = 0;
        for (int i = this.doneOperations; i < this.operationsToDo.size(); i++) {
            remainingTime += operationsToDo.get(i);
        }
        return remainingTime;
    }
}
