/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class GeneratePermutationTest {

    public List<String> perm5 = new ArrayList();
    public List<String> perm4 = new ArrayList();
    public List<String> perm3 = new ArrayList();
    public List<String> perm2 = new ArrayList();

    Random rand = new Random();

    @Before
    public void setUp() {
        perm5.add("A");
        perm5.add("B");
        perm5.add("C");
        perm5.add("D");
        perm5.add("E");

        perm4.add("A");
        perm4.add("B");
        perm4.add("C");
        perm4.add("D");

        perm3.add("A");
        perm3.add("B");
        perm3.add("C");

        perm2.add("A");
        perm2.add("B");
    }

    @Test
    public void shall_createNewPermutation5A() {  // |A|   B    C    D    E
        List<String> expected = Lists.newArrayList("B", "A", "C", "D", "E");
        List<String> result = createNewPermutation(perm5, "A", null, null);
        assertThat(result).containsExactlyElementsOf(expected);
    }

    @Test
    public void shall_createNewPermutation5B() {    // A   |B|   C    D    E
        List<String> expected1 = Lists.newArrayList("B", "A", "C", "D", "E");
        List<String> expected2 = Lists.newArrayList("A", "C", "B", "D", "E");
        List<String> result = createNewPermutation(perm5, "B", null, null);
        boolean equals = result.equals(expected1) || result.equals(expected2);
        assertThat(equals).isTrue();
    }

    @Test
    public void shall_createNewPermutation5C() {    // A    B   |C|   D    E
        List<String> expected1 = Lists.newArrayList("A", "C", "B", "D", "E");
        List<String> expected2 = Lists.newArrayList("A", "B", "D", "C", "E");
        List<String> result = createNewPermutation(perm5, "C", null, null);
        boolean equals = result.equals(expected1) || result.equals(expected2);
        assertThat(equals).isTrue();
    }

    @Test
    public void shall_createNewPermutation5D() {    // A    B    C   |D|   E
        List<String> expected1 = Lists.newArrayList("A", "B", "D", "C", "E");
        List<String> expected2 = Lists.newArrayList("A", "B", "C", "E", "D");
        List<String> result = createNewPermutation(perm5, "D", null, null);
        boolean equals = result.equals(expected1) || result.equals(expected2);
        assertThat(equals).isTrue();
    }

    @Test
    public void shall_createNewPermutation5E() {   // A    B    C    D   |E|
        List<String> expected = Lists.newArrayList("A", "B", "C", "E", "D");
        List<String> result = createNewPermutation(perm5, "E", null, null);
        assertThat(result).containsExactlyElementsOf(expected);
    }
    // ----4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4------
    // ----4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4--4------

    @Test
    public void shall_createNewPermutation4A() {  // |A|   B    C    D 
        List<String> expected = Lists.newArrayList("B", "A", "C", "D");
        List<String> result = createNewPermutation(perm4, "A", null, null);
        assertThat(result).containsExactlyElementsOf(expected);
    }

    @Test
    public void shall_createNewPermutation4B() {    // A   |B|   C    D  
        List<String> expected1 = Lists.newArrayList("B", "A", "C", "D");
        List<String> expected2 = Lists.newArrayList("A", "C", "B", "D");
        List<String> result = createNewPermutation(perm4, "B", null, null);
        boolean equals = result.equals(expected1) || result.equals(expected2);
        assertThat(equals).isTrue();
    }

    @Test
    public void shall_createNewPermutation4C() {    // A    B   |C|   D 
        List<String> expected1 = Lists.newArrayList("A", "C", "B", "D");
        List<String> expected2 = Lists.newArrayList("A", "B", "D", "C");
        List<String> result = createNewPermutation(perm4, "C", null, null);
        boolean equals = result.equals(expected1) || result.equals(expected2);
        assertThat(equals).isTrue();
    }

    @Test
    public void shall_createNewPermutation4D() { // A    B    C   |D|
        List<String> expected = Lists.newArrayList("A", "B", "D", "C");
        List<String> result = createNewPermutation(perm4, "D", null, null);
        assertThat(result).containsExactlyElementsOf(expected);
    }

    // ----3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3------
    // ----3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3------
    @Test
    public void shall_createNewPermutation3A() {// |A|   B    C 
        List<String> expected = Lists.newArrayList("B", "A", "C");
        List<String> result = createNewPermutation(perm3, "A", null, null);
        assertThat(result).containsExactlyElementsOf(expected);
    }

    @Test
    public void shall_createNewPermutation3B() {  // A   |B|   C
        List<String> expected1 = Lists.newArrayList("B", "A", "C");
        List<String> expected2 = Lists.newArrayList("A", "C", "B");
        List<String> result = createNewPermutation(perm3, "B", null, null);
        boolean equals = result.equals(expected1) || result.equals(expected2);
        assertThat(equals).isTrue();
    }

    @Test
    public void shall_createNewPermutation3C() { // A    B   |C|
        List<String> expected = Lists.newArrayList("A", "C", "B");
        List<String> result = createNewPermutation(perm3, "C", null, null);
        assertThat(result).containsExactlyElementsOf(expected);
    }

    // ----2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2------
    // ----2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2------
    @Test
    public void shall_createNewPermutation2A() {// |A|   B  
        List<String> expected = Lists.newArrayList("B", "A");
        List<String> result = createNewPermutation(perm2, "A", null, null);
        assertThat(result).containsExactlyElementsOf(expected);
    }

    @Test
    public void shall_createNewPermutation2B() {   // A   |B|
        List<String> expected = Lists.newArrayList("B", "A");
        List<String> result = createNewPermutation(perm2, "B", null, null);
        assertThat(result).containsExactlyElementsOf(expected);
    }

    private List<String> createNewPermutation(List<String> permutation, String v, List<Integer> criticalPath, DisjunctiveGraph graph) {
//        int v1IndexOnCriticalPath = criticalPath.indexOf(v.vertexNo);
        int vToChangeIndexinPermutation = permutation.indexOf(v);
//        int vOpositeIndexOnCriticalPath = -1;
//        int vOpositeIndexinPermutation = -1;
        List<String> ret = new ArrayList();

        //perm is >= 2 for sure
        //jeśli pierwszy w permutacji
        List<String> pre = new ArrayList(permutation.subList(0, vToChangeIndexinPermutation));
        List<String> post = new ArrayList(permutation.subList(vToChangeIndexinPermutation + 1, permutation.size()));
        List<String> prepre = new ArrayList();
        List<String> postpost = new ArrayList();
        if (pre.size() >= 2) {
            prepre = new ArrayList(pre.subList(0, pre.size() - 2));
            pre.removeAll(prepre);
        }
        if (post.size() >= 2) {
            postpost = new ArrayList(post.subList(2, post.size()));
            post.removeAll(postpost);
        }

        ret.addAll(prepre);
        if (pre.isEmpty()) { // post should has 1 or 2
            String vPost = post.get(0);
            ret.addAll(pre);
            ret.add(vPost);
            ret.add(v);
            if (post.size() == 2) { // has size 2     
                ret.add(post.get(1));
            }
        } else if (post.isEmpty()) {
            String vPre = pre.get(pre.size() - 1);
            if (pre.size() == 2) { // has size 2   
                ret.add(pre.get(0));
            }
            ret.add(v);
            ret.add(vPre);
        } else if (pre.size() == 1) { // post should has 1 or 2
            String vPre = pre.get(0);
            String vPost = post.get(0);
            addInChangedRandomOrder(vPre, v, vPost, ret);
            if (post.size() == 2) { // has size 2      
                ret.add(post.get(1));
            }
        } else if (post.size() == 1) {
            String vPre = pre.get(1);
            String vPost = post.get(0);
            if (pre.size() == 2) { // has size 2   
                ret.add(pre.get(0));
            }
            addInChangedRandomOrder(vPre, v, vPost, ret);
        } else if (pre.size() == 2 && post.size() == 2) {
            String vPre = pre.get(1);
            String vPost = post.get(0);
            ret.add(pre.get(0));
            addInChangedRandomOrder(vPre, v, vPost, ret);
            ret.add(post.get(1));
        }
        ret.addAll(postpost);
        return ret;
    }

    private void addInChangedRandomOrder(String vPre, String v, String vPost, List<String> ret) {
        if (rand.nextBoolean()) {
            add_v_vPre_vPost(v, vPre, vPost, ret);
        } else {
            add_vPre_vPost_v(vPre, vPost, v, ret);
        }
    }

    private void add_v_vPre_vPost(String v, String vPre, String vPost, List<String> newPerm) {
        newPerm.add(v);
        newPerm.add(vPre);
        newPerm.add(vPost);
    }

    private void add_vPre_vPost_v(String vPre, String vPost, String v, List<String> newPerm) {
        newPerm.add(vPre);
        newPerm.add(vPost);
        newPerm.add(v);
    }

}
