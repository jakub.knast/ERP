/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class UpdateEdgesTest {

    public List<String> perm5 = new ArrayList();
    public List<String> perm4 = new ArrayList();
    public List<String> perm3 = new ArrayList();
    public List<String> perm2 = new ArrayList();

    Random rand = new Random();

    @Before
    public void setUp() {
        perm5.add("A");
        perm5.add("B");
        perm5.add("C");
        perm5.add("D");
        perm5.add("E");

        perm4.add("A");
        perm4.add("B");
        perm4.add("C");
        perm4.add("D");

        perm3.add("A");
        perm3.add("B");
        perm3.add("C");

        perm2.add("A");
        perm2.add("B");
    }
    

    public void removeEdges(List<String> oldPerm, DisjunctiveGraph graph){
        for (int i = 1; i < oldPerm.size(); i++) {
            System.out.println("graph.removeEdge(findEdge(" + oldPerm.get(i-1)+ ", " + oldPerm.get(i)+ ", graph))");
        }
//            Edge edgeAB = findEdge(vA, vB, graph);
//            Edge edgeBC = findEdge(vB, vC, graph);
//            graph.removeEdge(edgeAB);
//            graph.removeEdge(edgeBC);
    }
    
    @Test
    public void shall_removeEdges5A() {  // |A|   B    C    D    E
        removeEdges(perm5, null);
    }
    
    @Test
    public void shall_randPropercriticalPathVerticle() {  // |A|   B    C    D    E
        List<Integer> criticalPath = Lists.newArrayList(0, 1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> result = new ArrayList();
        for (int i = 0; i < 50; i++) {
            result.add(criticalPath.get(rand.nextInt(criticalPath.size() - 2) + 1));
        }
        Map<Integer, Long> groupped = result.stream().collect(Collectors.groupingBy(p -> p, Collectors.counting()));
        System.out.println(groupped);
    }
            
    
    public void addDisjunctiveEdgesToVerticles(List<String> newPerm, DisjunctiveGraph graph) {
        for (int i = 1; i < newPerm.size(); i++) {
            System.out.println("graph.addEdge(new Edge(nextEdgeNo(), " + newPerm.get(i-1) + ".operationDuration, true), " + newPerm.get(i-1)+ ", " + newPerm.get(i)+ ", EdgeType.DIRECTED);");
        }
//        graph.addEdge(new Edge(nextEdgeNo(), v1.operationDuration, true), v1, v2, EdgeType.DIRECTED);
//        graph.addEdge(new Edge(nextEdgeNo(), v2.operationDuration, true), v2, v3, EdgeType.DIRECTED);
//        return new Permutation(v1, v2, v3);
    }

    @Test
    public void shall_addDisjunctiveEdgesToVerticles() {  // |A|   B    C    D    E
        addDisjunctiveEdgesToVerticles(perm5, null);
    }
}