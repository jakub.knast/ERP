/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.charts.utils;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.map.LazyMap;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Edge;
import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author fbrzuzka
 */
/*
 * Copyright (c) 2005, the JUNG Project and the Regents of the University of
 * California All rights reserved.
 *
 * This software is open-source under the BSD license; see either "license.txt"
 * or http://jung.sourceforge.net/license.txt for a description.
 *
 * Created on Jul 9, 2005
 */
public class DisjunctiveGraphLayout implements Layout<Operation, Edge> {

    protected Dimension size = new Dimension(600, 100);
    protected DisjunctiveGraph graph;
    protected Map<Operation, Integer> basePositions = new HashMap();
    protected Operation root;
    protected Map<Operation, Point2D> locations
            = LazyMap.decorate(new HashMap(), (Operation arg0) -> new Point2D.Double());

    protected transient Set<Operation> alreadyDone = new HashSet<Operation>();

    /**
     * The default horizontal vertex spacing. Initialized to 50.
     */
    public static int DEFAULT_DISTX = 100;

    /**
     * The default vertical vertex spacing. Initialized to 50.
     */
    public static int DEFAULT_DISTY = 100;

    /**
     * The horizontal vertex spacing. Defaults to {@code DEFAULT_XDIST}.
     */
    protected int distX = 50;

    /**
     * The vertical vertex spacing. Defaults to {@code DEFAULT_YDIST}.
     */
    protected int distY = 50;

    protected transient Point m_currentPoint = new Point();

    public DisjunctiveGraphLayout(DisjunctiveGraph g, Operation root) {
        this(g, root, DEFAULT_DISTX, DEFAULT_DISTY);
    }

    public DisjunctiveGraphLayout(DisjunctiveGraph g, Operation root, int distx) {
        this(g, root, distx, DEFAULT_DISTY);
    }

    public DisjunctiveGraphLayout(DisjunctiveGraph g, Operation root, int distx, int disty) {
        if (g == null) {
            throw new IllegalArgumentException("Graph must be non-null");
        }
        if (distx < 1 || disty < 1) {
            throw new IllegalArgumentException("X and Y distances must each be positive");
        }
        this.graph = g;
        this.distX = distx;
        this.distY = disty;
        this.root = root;
        buildTree();
    }

    protected void buildTree() {
        this.m_currentPoint = new Point(0, size.height/2);
        calculateDimensionY(root);
        m_currentPoint.y += this.basePositions.get(root) / 2 + this.distY;
        buildTree(root, this.m_currentPoint.y);
    }

    protected void buildTree(Operation v, int y) {

        if (!alreadyDone.contains(v)) {
            alreadyDone.add(v);

            //go one level further down
            this.m_currentPoint.x += this.distX;
            this.m_currentPoint.y = y;

            this.setCurrentPositionFor(v);//todo

            int sizeYofCurrent = basePositions.get(v);

            int lastY = y - sizeYofCurrent / 2;

            int sizeYofChild;
            int startYofChild;
            
            List<Operation> li = graph.getChildrenWithoutDisjunctives(v).stream().sorted((Operation o1, Operation o2) -> o1.vertexNo.compareTo(o2.vertexNo)).collect(Collectors.toList());
            for (Operation element : li) {
                sizeYofChild = this.basePositions.get(element);
                startYofChild = lastY + sizeYofChild / 2;
                buildTree(element, startYofChild);
                lastY = lastY + sizeYofChild + distY;
            }
            this.m_currentPoint.x -= this.distX;
        }
    }

    private int calculateDimensionY(Operation v) {

        int size = 0;
        Collection<Operation> children = graph.getChildrenWithoutDisjunctives(v);
        if (!children.isEmpty()) {            
            List<Operation> li = children.stream().sorted((Operation o1, Operation o2) -> o1.vertexNo.compareTo(o2.vertexNo)).collect(Collectors.toList());
            for (Operation element : li) {
                size += calculateDimensionY(element) + distY;
            }
        }
        size = Math.max(0, size - distY);
        basePositions.put(v, size);

        return size;
    }

    /**
     * This method is not supported by this class. The size of the layout is
     * determined by the topology of the tree, and by the horizontal and
     * vertical spacing (optionally set by the constructor).
     */
    public void setSize(Dimension size) {
        throw new UnsupportedOperationException("Size of TreeLayout is set"
                + " by vertex spacing in constructor");
    }

    protected void setCurrentPositionFor(Operation vertex) {
        int y = m_currentPoint.y;
        int x = m_currentPoint.x;
        if (y < 0) {
            size.height -= y;
        }

        if (y > size.height - distY) {
            size.height = y + distY;
        }

        if (x < 0) {
            size.width -= x;
        }
        if (x > size.width - distX) {
            size.width = x + distX;
        }
        locations.get(vertex).setLocation(m_currentPoint);

    }

    public Graph getGraph() {
        return graph;
    }

    public Dimension getSize() {
        return size;
    }

    public void initialize() {

    }

    public boolean isLocked(Operation v) {
        return false;
    }

    public void lock(Operation v, boolean state) {
    }

    public void reset() {
    }

    public void setGraph(Graph graph) {
            throw new IllegalArgumentException("unsupported");
    }

    public void setInitializer(Transformer<Operation, Point2D> initializer) {
    }

    /**
     * Returns the center of this layout's area.
     */
    public Point2D getCenter() {
        return new Point2D.Double(size.getWidth() / 2, size.getHeight() / 2);
    }

    public void setLocation(Operation v, Point2D location) {
        locations.get(v).setLocation(location);
    }

    public Point2D transform(Operation v) {
        return locations.get(v);
    }
}
