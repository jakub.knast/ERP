/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.charts.utils;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.transform.shape.GraphicsDecorator;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import org.jfree.chart.ChartColor;
import regiec.knast.erp.api.scheduler.model.Edge;
import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author fbrzuzka
 */
public class MyVertexRenderer implements Renderer.Vertex<Operation, Edge> {

    boolean cyclic;
    private Map<String, Color> colorsMap = new HashMap();

    private static Color colors[] = new Color[]{
        new Color(0xFF, 0x55, 0x55),
//        new Color(0x55, 0x55, 0xFF),
        new Color(0x55, 0xFF, 0x55),
        new Color(0xFF, 0xFF, 0x55),
        new Color(0xFF, 0x55, 0xFF),
        new Color(0x55, 0xFF, 0xFF),
        Color.pink,
        Color.gray,
        ChartColor.LIGHT_RED,
        ChartColor.LIGHT_BLUE,
        ChartColor.LIGHT_GREEN,
        ChartColor.LIGHT_YELLOW,
        ChartColor.LIGHT_MAGENTA,
        ChartColor.LIGHT_CYAN,
        Color.lightGray,
//        ChartColor.DARK_RED,
//        ChartColor.DARK_BLUE,
        ChartColor.DARK_GREEN,
        ChartColor.DARK_YELLOW,
        ChartColor.DARK_MAGENTA,
        ChartColor.DARK_CYAN,
//        Color.darkGray,
        ChartColor.VERY_DARK_RED,
//        ChartColor.VERY_DARK_BLUE,
        ChartColor.VERY_DARK_GREEN,
        ChartColor.VERY_DARK_YELLOW,
        ChartColor.VERY_DARK_MAGENTA,
        ChartColor.VERY_DARK_CYAN,
        ChartColor.VERY_LIGHT_RED,
        ChartColor.VERY_LIGHT_BLUE,
        ChartColor.VERY_LIGHT_GREEN,
        ChartColor.VERY_LIGHT_YELLOW,
        ChartColor.VERY_LIGHT_MAGENTA,
        ChartColor.VERY_LIGHT_CYAN
    };

    public MyVertexRenderer(List<String> workerLabels) {
        for (int i = 0; i < workerLabels.size(); i++) {
            colorsMap.put(workerLabels.get(i), colors[i]);
        }
    }

    private Paint chooseColor(Operation v) {
        if (v.worker != null && colorsMap.containsKey(v.worker.workerLabel)) {
            return colorsMap.get(v.worker.workerLabel);
        }
        return ChartColor.VERY_LIGHT_CYAN;
    }

    public void paintVertex(RenderContext<Operation, Edge> rc, Layout<Operation, Edge> layout, Operation v) {
        Graph<Operation, Edge> graph = layout.getGraph();
        if (rc.getVertexIncludePredicate().evaluate(Context.<Graph<Operation, Edge>, Operation>getInstance(graph, v))) {
            boolean vertexHit = true;
            // get the shape to be rendered
            Shape shape = rc.getVertexShapeTransformer().transform(v);

            Point2D p = layout.transform(v);
            p = rc.getMultiLayerTransformer().transform(Layer.LAYOUT, p);

            float x = (float) p.getX();
            float y = (float) p.getY();

            // create a transform that translates to the location of
            // the vertex to be rendered
            AffineTransform xform = AffineTransform.getTranslateInstance(x, y);
            // transform the vertex shape with xtransform
            shape = xform.createTransformedShape(shape);

            vertexHit = vertexHit(rc, shape);
            //rc.getViewTransformer().transform(shape).intersects(deviceRectangle);

            if (vertexHit) {
                paintShapeForVertex(rc, v, shape);
            }
        }
    }

    protected boolean vertexHit(RenderContext<Operation, Edge> rc, Shape s) {
        JComponent vv = rc.getScreenDevice();
        Rectangle deviceRectangle = null;
        if (vv != null) {
            Dimension d = vv.getSize();
            deviceRectangle = new Rectangle(
                    0, 0,
                    d.width, d.height);
        }
        return rc.getMultiLayerTransformer().getTransformer(Layer.VIEW).transform(s).intersects(deviceRectangle);
    }

    protected void paintShapeForVertex(RenderContext<Operation, Edge> rc, Operation v, Shape shape) {
        GraphicsDecorator g = rc.getGraphicsContext();
        Paint oldPaint = g.getPaint();
        Rectangle r = shape.getBounds();
        float y2 = (float) r.getMaxY();
        if (cyclic) {
            y2 = (float) (r.getMinY() + r.getHeight() / 2);
        }

        Paint fillPaint = chooseColor(v);

        if (fillPaint != null) {
            g.setPaint(fillPaint);
            g.fill(shape);
            g.setPaint(oldPaint);
        }
        Paint drawPaint = rc.getVertexDrawPaintTransformer().transform(v);
        if (drawPaint != null) {
            g.setPaint(drawPaint);
        }
        Stroke oldStroke = g.getStroke();
        Stroke stroke = rc.getVertexStrokeTransformer().transform(v);
//        Stroke ss = null;
        if (stroke != null) {
            if (v.isOnCriticalPath) {
                g.setStroke(new BasicStroke(5));
            } else {
                g.setStroke(stroke);
            }
        }
        g.draw(shape);
        g.setPaint(oldPaint);
        g.setStroke(oldStroke);
    }
}
