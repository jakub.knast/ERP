/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlan;
import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.model.DetalTemplateEntity;
import regiec.knast.erp.api.model.TreeNodeType;


/**
 *
 * @author fbrzuzka
 */
public class Job {
    
    private boolean WITH_TRANSPORT;
    
    public Operation actualOperation;
    public List<Operation> operationsToDo;
    
    public Job parent;
    public List<Job> children = new ArrayList();
    protected List<ProductionPlan> productionPlan;
    public Integer doneOperations = 0;
    public Integer totalDetalBuildingTime;
    public String jobNo;
    public String detalName;
    public String detalNo;
    public String parentJobNo;
    public int count;
    
    public int childCount;    
    public Boolean hasNoChildren;    
    public Integer estimatedJobDuration;
    public List<String> childJobsToDo = new ArrayList();
    public Map<String, Boolean> childJobsDoneStatus = new HashMap();
    public TreeNodeType treeNodeType;    
    public JobState state;
    
    protected Job (){        
    }
        
    public Job(ProductionJobEntity productionJobEntity, Job parent, boolean withTransport, int transportTime) {
        DetalTemplateEntity dte = productionJobEntity.getDetalEntity().getDetalTemplateEntity();
        WITH_TRANSPORT = withTransport;
        if (parent == null) {
            this.treeNodeType = TreeNodeType.ROOT;
        }
        this.count = productionJobEntity.getDetalEntity().getCount();
        this.totalDetalBuildingTime =(int)(this.count *
                productionJobEntity.getDetalEntity().getDetalTemplateEntity().getTotalBuildingTime()* 60);
        this.jobNo = productionJobEntity.getNo();
        this.detalName = dte.getName();
        this.detalNo = dte.getNo();

        this.parentJobNo = productionJobEntity.getParentNo();
        this.parent = parent;
        this.productionPlan = trimProductionPlanList(productionJobEntity.getDetalEntity().getDetalTemplateEntity().getProductionPlan());
        this.childCount = productionJobEntity.getChildren() == null ? 0 : productionJobEntity.getChildren().size();
        this.hasNoChildren = this.childCount == 0;
        if (!hasNoChildren && productionJobEntity.getChildren() != null) {
            for (ProductionJobEntity child : productionJobEntity.getChildren()) {
                this.childJobsDoneStatus.put(child.getNo(), false);
                this.childJobsToDo.add(child.getNo());
                this.children.add(new Job(child, this, withTransport, transportTime));
            }
        }
        this.operationsToDo = createOperationsWithTransportFromPlan(productionJobEntity.getDetalEntity().getDetalTemplateEntity(), 10, withTransport); 
        this.estimatedJobDuration = primitiveCalculateJobDuration();
    }

    public List<Job> getChildren(){
        return children;
    }
    
    public void tellParentJobThatImComplete(List<Job> jobLists) {
        if (!Strings.isNullOrEmpty(this.parentJobNo)) { // assume that im the root
            Job parent = jobLists.stream().filter(j -> j.jobNo.equals(this.parentJobNo)).findFirst().get();
            if (!parent.childJobsDoneStatus.containsKey(this.jobNo)) {
                throw new ConversionError("parent job map dont contain theri child;/");
            }
            parent.childJobsDoneStatus.put(this.jobNo, true);
            if (!parent.childJobsToDo.contains(this.jobNo)) {
                throw new ConversionError("parent job map dont contain theri child;/");
            }
            parent.childJobsToDo.remove(this.jobNo);
        }
    }

    public List<String> getNotDoneChildrenJobs() {
        return childJobsToDo;
    }
    
    public Operation getNextOperation() {
        return operationsToDo.get(doneOperations);
    }

    public void updateOperationsStates() {
        for (Operation o : operationsToDo) {
            o.operationState = o.defineOperationState(this);
        }
    }
    
    protected List<Operation> createOperationsWithTransportFromPlan(DetalTemplateEntity dte, int transportTime, boolean withTransport) {
        int operationIndex = 0;
        List<Operation> operations = new ArrayList();
        for (int i = 0; i < dte.getProductionPlan().size(); i++) {
            ProductionPlan pp = dte.getProductionPlan().get(i);
            if (withTransport) {
                Operation tr = Operation.newTransportOperation(this, transportTime, operationIndex++);
                operations.add(tr);
            }

            Operation o = new Operation(this, pp, operationIndex++, null);
            while (i + 1 < dte.getProductionPlan().size() && pp.hasSameProffesionGroup(dte.getProductionPlan().get(i + 1))) {
                ProductionPlan nextOp = dte.getProductionPlan().get(i + 1);
                o.operationNames.add(nextOp.getOperationName().toString());
                o.preparationTime += (int)(nextOp.getPreparationTime() * 60);
                o.operationTime += (int)(nextOp.getOperationTime() * 60);
                o.isMultipleOperation = true;
                o.calculateOperationDuration();
                i++;
            }
            operations.add(o);
        }
        for (Operation operation : operations) {
            operation.operationsInJob = operations.size();
        }        
        return operations;
    }

    public List<Operation> getPreOp(int opIndex) {
        List<Operation> ret = new ArrayList();
        ret.addAll(this.operationsToDo.subList(0, opIndex));
        for (Job child : this.children) {
            ret.addAll(child.getPreOp());
        }
        return ret;
    }

    public List<Operation> getPreOp() {
        List<Operation> ret = new ArrayList();
        ret.addAll(this.operationsToDo);
        for (Job child : this.children) {
            ret.addAll(child.getPreOp());
        }
        return ret;
    }
    
    protected Integer primitiveCalculateJobDuration() {
        Integer jobDuration = 0;
        for (Operation op : this.operationsToDo) {
            jobDuration += op.cost;
        }
        return jobDuration;
    }    
    
    public int getOperationsCount() {
        if (this.operationsToDo == null) {
            if (WITH_TRANSPORT) {
                return this.productionPlan.size() * 2;
            } else {
                return this.productionPlan.size();
            }
        } else {
            return this.operationsToDo.size();
        }
    }
    
    public Integer calculateRemainingTime() {
        Integer remainingTime = 0;
        for (int i = this.doneOperations; i < this.operationsToDo.size(); i++) {
            remainingTime += operationsToDo.get(i).cost;
        }
        return remainingTime;
    }
    
    protected List<ProductionPlan> trimProductionPlanList(List<ProductionPlan> pps) {
        for (ProductionPlan pp : pps) {
            if (pp.getProfessionGroup() == null) {
                throw new RuntimeException();
            }
            pp.getProfessionGroup().setAllowOperations(null);
        }
        return pps;
    }

    @Override
    public String toString() {
        return jobNo;
    }

}
