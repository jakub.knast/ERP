/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model.interfaces;

/**
 *
 * @author fbrzuzka
 */
public interface UpdatePosition {

    String getPositionId();

    void setPositionId(String positionId);
}
