/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.zzzother;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.scheduler.ScheduleLauncher;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Operation;

public class DijkstraCriticalPathDetectorSlow {

    public static final int INFINITE_DISTANCE = Integer.MAX_VALUE / 2;

    public static void main(String[] args) throws ObjectCreateError {
        ScheduleLauncher.main(null);
    }

    private final Comparator<Operation> shortestDistanceComparator = new Comparator<Operation>() {

        public int compare(Operation left, Operation right) {
            int result = getLongestDistance(right) - getLongestDistance(left);
            return (result == 0) ? right.cost.compareTo(left.cost) : result;
//            return  left.operationDuration.compareTo(right.operationDuration);
        }
    };

    private final DisjunctiveGraph graph;
    private final PriorityQueue<Operation> unsettledNodes = new PriorityQueue(shortestDistanceComparator);
    private final Set settledNodes = new HashSet();
    private final Map<Operation, Integer> longestDistances = new HashMap();
    private final Map predecessors = new HashMap();

    private Operation startVertex;
    private Operation endVertex;

    public DijkstraCriticalPathDetectorSlow(DisjunctiveGraph graph) {
        this.graph = graph;
    }

    private void init(Operation start) {
        this.startVertex = start;
//        settledNodes.clear();
        unsettledNodes.clear();

        longestDistances.clear();
        predecessors.clear();

        // add source
        setLongestDistance(start, 0);
        unsettledNodes.add(start);
    }
    public static long duration = 0;

    public static void markOperationsOnCriticalPath(DisjunctiveGraph graph, List<Integer> criticalPath) {
        for (Operation vertice : graph.getVertices()) {
            vertice.isOnCriticalPath = criticalPath.contains(vertice.vertexNo);
        }
    }

    public DijkstraCriticalPathDetectorSlow execute() {
        Operation startV = this.graph.getVertices().stream().filter(v -> v.operationName.equals("zero")).findFirst().get();
        Operation endV = this.graph.getVertices().stream().filter(v -> v.operationName.equals("last")).findFirst().get();
        endVertex = endV;
        execute(startV, endV);
//        System.out.println("DijkstraLongest        " + this.getPathToEndVertex());
//        System.out.println("DijkstraLongest        " + this.getLongestDistance(endV));
        return this;
    }

    public void execute(Operation start, Operation destination) {
        long startTime = System.currentTimeMillis();
        init(start);

        // the current node
        Operation u;

        // extract the node with the shortest distance
        while ((u = extractMax()) != null) {
            assert !isSettled(u);

            // destination reached, stop
//            if (u == destination) {
//                break;
//            }
            markSettled(u);
            relaxNeighbors(u);
        }
        duration += (System.currentTimeMillis() - startTime);
    }

    private Operation extractMax() {
        if (unsettledNodes.isEmpty()) {
            return null;
        }
//        System.out.println("unsettled: " + unsettledNodes);
        Operation max = (Operation) unsettledNodes.poll();
//        System.out.print("wybieram do przejrzenia ");
//        System.out.println("first: " + (OperationBase) unsettledNodes.first());
//        System.out.println("last: " + (OperationBase) unsettledNodes.last());
//        unsettledNodes.remove(max);

        return max;
    }

    private void relaxNeighbors(Operation u) {
//        for (Iterator i = map.getDestinations(u).iterator(); i.hasNext();) {
//            OperationBase v = (OperationBase) i.next();
        for (Operation v : graph.getChildren(u)) {

            // skip node already settled
//            if (isSettled(v)) {
//                continue;
//            }
//            System.out.print("parent " + u + " dziecko " + v);
            if (getLongestDistance(v) <= getLongestDistance(u) + u.cost) {
                // assign new longest distance and mark unsettled
//                System.out.println(" nowy dystans " + (getLongestDistance(u) + u.operationDuration));
//                printDistances();
                setLongestDistance(v, getLongestDistance(u) + u.cost);
//                System.out.println(longestDistances);

                // assign predecessor in shortest path
                setPredecessor(v, u);
            }
        }
//        System.out.println("");
    }

    private void markSettled(Operation u) {
        settledNodes.add(u);
    }

    private boolean isSettled(Operation v) {
        return settledNodes.contains(v);
    }

    public int getLongestDistance(Operation city) {
        Integer d = (Integer) longestDistances.get(city);
        return (d == null) ? 0 : d.intValue();
    }

    public int getLongestDistanceToEnd() {
        Integer d = (Integer) longestDistances.get(endVertex);
        return (d == null) ? 0 : d.intValue();
    }
    public int ii = 0;

    private void setLongestDistance(Operation city, int distance) {
        // this crucial step ensure no duplicates will be created in the queue
        // when an existing unsettled node is updated with a new shortest distance
//        System.out.println("\nii: " + ii++);
        ii++;
//        System.out.println("unsettledNodes : " + unsettledNodes);
//        System.out.println("remove : " + city);
        unsettledNodes.remove(city);
//        System.out.println("unsettledNodes : " + unsettledNodes);

        longestDistances.put(city, new Integer(distance));

        // re-balance the sorted set according to the new shortest distance found
        // (see the comparator the set was initialized with)
        unsettledNodes.add(city);

//        System.out.println("unsettledNodes : " + unsettledNodes);
//        for (OperationBase op : unsettledNodes) {
//            System.out.print(op.operationDuration + ", ");
//            System.out.print(getLongestDistance(op) + ", ");
//        }
    }

    public Operation getPredecessor(Operation city) {
        return (Operation) predecessors.get(city);
    }

    public List<Integer> getPathToEndVertex() {
        return getPathTo(endVertex);
    }

    public List<Integer> getPathTo(Operation endVertex) {
        List<Integer> criticalPath = new ArrayList();
        Operation end = endVertex;
        while (true) {
            criticalPath.add(end.vertexNo);
            Operation next = getPredecessor(end);
            if (next.vertexNo == startVertex.vertexNo) {
                criticalPath.add(next.vertexNo);
                break;
            }
            end = next;
        }
        return Lists.reverse(criticalPath);
    }

    private void setPredecessor(Operation a, Operation b) {
        predecessors.put(a, b);
    }

    public Result getResult() {
        return new Result(getLongestDistanceToEnd(), getPathToEndVertex(), longestDistances);
    }

    public class Result {

        public Integer makespan;
        public List<Integer> criticalPath;
        public Map<Operation, Integer> longestDistances;

        public Result(Integer makespan, List<Integer> criticalPath, Map<Operation, Integer> longestDistances) {
            this.makespan = makespan;
            this.criticalPath = criticalPath;
            this.longestDistances = longestDistances;
        }

        public void markOperationsOnCriticalPath(DisjunctiveGraph graph) {
            for (Operation vertice : graph.getVertices()) {
                vertice.isOnCriticalPath = criticalPath.contains(vertice.vertexNo);
            }
        }

        public void markDistances(DisjunctiveGraph g) {
            for (Operation op : g.getVertices()) {
                if (!longestDistances.containsKey(op)) {
                    throw new ConversionError("łoooo, powinien być wierzchołek w liście ale nie ma");
                }
                Integer distanceFromZero = longestDistances.get(op);
                op.operationStartTime = distanceFromZero;
                op.operationEndTime = distanceFromZero + op.cost;
            }
        }
    }

}
