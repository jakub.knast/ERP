/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.charts;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
import com.google.common.collect.Lists;
import java.awt.Color;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.IntervalCategoryItemLabelGenerator;
import org.jfree.chart.labels.IntervalCategoryToolTipGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.urls.StandardCategoryURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.ui.TextAnchor;
import regiec.knast.erp.api.scheduler.ScheduleLauncher;
import regiec.knast.erp.api.scheduler.charts.model.MultiTask;
import regiec.knast.erp.api.scheduler.charts.model.MyTask;
import regiec.knast.erp.api.scheduler.charts.model.MyTaskSeriesCollection;
import regiec.knast.erp.api.scheduler.charts.model.OperationGantKey;
import regiec.knast.erp.api.scheduler.charts.utils.MyGanttRenderer;
import regiec.knast.erp.api.scheduler.model.Operation;
import regiec.knast.erp.api.scheduler.model.Schedule;

public class GanttChartPainter extends ApplicationFrame {

    public static void main(String[] args) throws ObjectCreateError {
        ScheduleLauncher.main(null);
    }

    public static void ganttFromSchedule(String title, Schedule schedule, boolean includeLegend) {
        GanttChartPainter ganttSchedule = new GanttChartPainter(title, schedule, includeLegend);
        ganttSchedule.pack();
        RefineryUtilities.centerFrameOnScreen(ganttSchedule);
        ganttSchedule.setVisible(true);
    }

    public GanttChartPainter(String title, Schedule schedule, boolean includeLegend) {

        super(title);

        MyTaskSeriesCollection dataset = createDatasetFromSchedule(schedule);
        final JFreeChart chart = createChart(dataset, title, includeLegend);

        // add the chart to a panel...
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1880, 950));
        setContentPane(chartPanel);

    }

    private JFreeChart createChart(final MyTaskSeriesCollection dataset, String title, boolean includeLegend) {
        final JFreeChart chart = createGanttChart(
                title, // chart title
                "Operations", // domain axis label
                "Date", // range axis label
                dataset, // data
                includeLegend, // include legend
                true, // tooltips
                false // urls
        );
//        chart.getCategoryPlot().getDomainAxis().setMaxCategoryLabelWidthRatio(10.0f);
        return chart;
    }

    public static JFreeChart createGanttChart(String title,
            String categoryAxisLabel, String dateAxisLabel,
            MyTaskSeriesCollection dataset, boolean legend, boolean tooltips,
            boolean urls) {

        CategoryAxis categoryAxis = new CategoryAxis(categoryAxisLabel);
        DateAxis dateAxis = new DateAxis(dateAxisLabel);

        MyGanttRenderer renderer = new MyGanttRenderer();
//        CategoryItemRenderer renderer = new GanttRenderer();
        if (tooltips) {
            renderer.setBaseToolTipGenerator(
                    new IntervalCategoryToolTipGenerator(
                            "{3} - {4}", DateFormat.getDateInstance()));
        }
        if (urls) {
            renderer.setBaseItemURLGenerator(
                    new StandardCategoryURLGenerator());
        }
        renderer.setBaseItemLabelGenerator(new IntervalCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBaseItemLabelPaint(Color.BLACK);
//        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.INSIDE6, TextAnchor.TOP_CENTER));
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE9, TextAnchor.CENTER_LEFT));
        renderer.setBaseItemLabelGenerator(new CategoryItemLabelGenerator() {
            @Override
            public String generateRowLabel(CategoryDataset categoryDataset, int row) {
                return "Your Row Text  " + row;
            }

            @Override
            public String generateColumnLabel(CategoryDataset categoryDataset, int column) {
                return "Your Column Text  " + column;
            }

            @Override
            public String generateLabel(CategoryDataset categoryDataset, int row, int column) {
                Task task = dataset.getTask(row, column);
                return generateForMyTask(task);
            }

            private String generateForMyTask(Task task) {
                if (task instanceof MyTask) {
                    MyTask myTask = (MyTask) task;
                    return myTask.isIsOnCriticalPath() ? "  XXX-" + myTask.getVertexNo() : "  " + myTask.getVertexNo().toString();
                } else {
                    throw new ConversionError("Task is not a MyTask, cannot write is on critical path");
                }
            }
        });
        CategoryPlot plot = new CategoryPlot(dataset, categoryAxis, dateAxis, renderer);
        plot.setDomainGridlinesVisible(true);
        plot.setOrientation(PlotOrientation.HORIZONTAL);

        JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT,
                plot, legend);

        return chart;

    }

    public MyTaskSeriesCollection createDatasetFromSchedule(Schedule schedule) {

        Map<OperationGantKey, List<Operation>> multipleValueMap2 = prepareMultipleValueMapList(schedule);

        final MyTaskSeriesCollection series = prepareSeries(schedule);
        for (Map.Entry<OperationGantKey, List<Operation>> entry : multipleValueMap2.entrySet()) {
            final Operation op = entry.getValue().get(0);
            TaskSeries s = series.getSeries(op.job.jobNo);

            if (entry.getValue().size() == 1) {
                s.add(new MyTask(op.worker.workerLabel, date1(op), date2(op), op.isOnCriticalPath, op.vertexNo));
            } else {
                Operation jFirst = findFirstJob(entry.getValue());
                Operation jLast = findLastJob(entry.getValue());
                Task superTask = new MultiTask(op.worker.workerLabel, date1(jFirst), date2(jLast), op.isOnCriticalPath);
                s.add(superTask);
                for (Operation subOp : entry.getValue()) {
                    superTask.addSubtask(new MyTask(subOp.worker.workerLabel, date1(subOp), date2(subOp), subOp.isOnCriticalPath, subOp.vertexNo));
                }
            }
        }
        series.setKeyss(sortKeys(schedule));
        return series;
    }

    private Date date1(Operation op) {
        return new Date(op.operationStartTime * 1000 * 60);
    }

    private Date date2(Operation op) {
        return new Date(op.operationEndTime * 1000 * 60);
    }

    private Map<OperationGantKey, List<Operation>> prepareMultipleValueMapList(Schedule schedule) {
        Map<OperationGantKey, List<Operation>> map = new TreeMap();
        for (Operation job : schedule) {
            if (map.containsKey(job.key)) {
                List l = map.get(job.key);
                l.add(job);
                map.put(job.key, l);
            } else {
                map.put(job.key, Lists.newArrayList(job));
            }
            //    System.out.println("jobId: " + job.getTaskNo() + "-" + job.operationNo + " worker: " + job.worker.workerLabel);
        }
//        for (Map.Entry<JobGantKey, List<JobInterface>> entry : map.entrySet()) {
//            System.out.println("key: " + entry.getKey() + " value size: " + entry.getValue().size());
//        }
        return map;

    }

    private MyTaskSeriesCollection prepareSeries(Schedule schedule) {
        final MyTaskSeriesCollection series = new MyTaskSeriesCollection();
        Set<String> treeset = new TreeSet();
        for (Operation op : schedule) {
            treeset.add(op.job.jobNo);
        }
        treeset.remove("transport");
        //always should be first on task series list
        series.add(new TaskSeries("transport"));
        for (String string : treeset) {
            series.add(new TaskSeries(string));
            //  System.out.println(string);
        }
        return series;
    }

    public static List<String> sortKeys(Collection<? extends Operation> schedule) {
        Map<String, String> m = new TreeMap();
        for (Operation job : schedule) {
            if (job.worker != null) {
                m.put(job.worker.code, job.worker.workerLabel);
            }
        }
        return new ArrayList(m.values());
    }

    private Operation findFirstJob(List<Operation> jobs) {
        return Collections.min(jobs, new Comparator<Operation>() {
            @Override
            public int compare(Operation o1, Operation o2) {
                return o1.operationStartTime.compareTo(o2.operationStartTime);
            }
        });
    }

    private Operation findLastJob(List<Operation> jobs) {
        return Collections.max(jobs, new Comparator<Operation>() {
            @Override
            public int compare(Operation o1, Operation o2) {
                return o1.operationEndTime.compareTo(o2.operationEndTime);
            }
        });
    }

}
