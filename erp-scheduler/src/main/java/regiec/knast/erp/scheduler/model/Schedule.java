/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import java.util.ArrayList;

/**
 *
 * @author fbrzuzka
 */
public class Schedule extends ArrayList<Operation> {

    private String algName;
    private DisjunctiveGraph graph = null;
    
    public Schedule(DisjunctiveGraph graph, String algName) {
        for (Operation v : graph.getVertices()) {
            if (!v.operationName.equals("zero") && !v.operationName.equals("last")) {
                this.add(v);
            }
        }
        this.algName = algName;
        this.graph = graph;
    }

    public Schedule(String algName) {
        this.algName = algName;
    }

    public String getAlgName() {
        return algName;
    }    

    public DisjunctiveGraph getGraph(){
        return graph;
    }

    public void setGraph(DisjunctiveGraph graph) {
        this.graph = graph;
    }
        
    public boolean hasGraph() {
        return graph != null;
    }

    public void setAlgName(String algName) {
        this.algName = algName;
    }
    
}
