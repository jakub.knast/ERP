/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import java.util.Objects;

/**
 *
 * @author fbrzuzka
 */
public class OperationLast extends Operation {

    public OperationLast() {
        super();
        this.vertexNo = Integer.MAX_VALUE;
        this.operationName = "last";
        this.professionGroup = null;
        this.cost = 0;
    }

    @Override
    public String toString() {
        return this.vertexNo == null ? "last" : "last " + this.vertexNo;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.hash);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OperationLast other = (OperationLast) obj;
        if (!Objects.equals(this.hash, other.hash)) {
            return false;
        }
        return true;
    }
}
