/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import edu.uci.ics.jung.graph.SparseGraph;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import regiec.knast.erp.api.scheduler.utils.CriticalPathDetector;

/** 
 *
 * @author fbrzuzka
 */
public class DisjunctiveGraph extends SparseGraph<Operation, Edge> {

    public String name;
    public Map<String, List<EdgeWrapper>> disjunctiveEdgesMap;
    public Map<String, List<Operation>> disjunctiveOperations;
    public Map<String, Boolean> permutationOnOnlyOneGraphLevel = new HashMap();
    public final Map<String, List<Worker>> grupedWorkers;

    public DisjunctiveGraph(Map<String, List<Worker>> grupedWorkers) {
        this.grupedWorkers = grupedWorkers;
    }
    
    public Collection<Operation> getChildren(Operation vertex) {
        if (!containsVertex(vertex)) {
            return null;
        }
        Collection<Operation> succs = new HashSet(vertex_maps.get(vertex)[OUTGOING].keySet());
        return Collections.unmodifiableCollection(succs);
    }

    public Collection<Operation> getParents(Operation vertex) {
        if (!containsVertex(vertex)) {
            return null;
        }
        return Collections.unmodifiableCollection(vertex_maps.get(vertex)[INCOMING].keySet());
    }

    public Collection<Edge> getInDirectedEdges(Operation vertex) {
        if (!containsVertex(vertex)) {
            return null;
        }
        Collection<Edge> in = new HashSet(vertex_maps.get(vertex)[INCOMING].values());
        return Collections.unmodifiableCollection(in);
    }

    public Collection<Edge> getOutDirectedEdges(Operation vertex) {
        if (!containsVertex(vertex)) {
            return null;
        }
        Collection<Edge> out = new HashSet(vertex_maps.get(vertex)[OUTGOING].values());
        return Collections.unmodifiableCollection(out);
    }

    public Collection<Edge> getOutDirectedDisjunctiveEdges(Operation vertex) {
        return getOutDirectedEdges(vertex).stream().filter(e -> e.isDisjunctive()).collect(Collectors.toList());
    }

    public Collection<Edge> getDirectedEdges() {
        Collection<Edge> edges = directed_edges.keySet();
        return Collections.unmodifiableCollection(edges);
    }

    public Collection<Edge> getUndirectedEdges() {
        Collection<Edge> edges = undirected_edges.keySet();
        return Collections.unmodifiableCollection(edges);
    }

    public Collection<Edge> getDirectedEdgesWithoutDisjunctives() {
        Collection<Edge> edges = directed_edges.keySet().stream().filter(e -> !e.isDisjunctive()).collect(Collectors.toList());
        return Collections.unmodifiableCollection(edges);
    }

    public Collection<Edge> getDirectedEdgesDisjunctives() {
        Collection<Edge> edges = directed_edges.keySet().stream().filter(e -> e.isDisjunctive()).collect(Collectors.toList());
        return Collections.unmodifiableCollection(edges);
    }

    public Collection<Edge> getOutEdgesOnlyDirected(Operation vertex) {
        if (!containsVertex(vertex)) {
            return null;
        }
        return Collections.unmodifiableCollection(vertex_maps.get(vertex)[OUTGOING].values());
    }

    public Collection<Operation> getChildrenWithoutDisjunctives(Operation vertex) {
        List<Operation> childrenWithoutDisjunctivesEdges = this.getOutEdgesOnlyDirected(vertex)
                .stream()
                .filter(e -> !e.isDisjunctive())
                .collect(Collectors.mapping(e -> this.getOpposite(vertex, e), Collectors.toList()));
        return Collections.unmodifiableCollection(childrenWithoutDisjunctivesEdges);
    }

    public Collection<Operation> getVertices() {
        return vertex_maps.keySet();
    }

    public CriticalPathDetector.Result calculateResult() {
        return new CriticalPathDetector(this).execute().getResult();
    }

}
