/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.zzzother;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
import regiec.knast.erp.api.scheduler.charts.utils.MyGanttRenderer;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.labels.IntervalCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.urls.StandardCategoryURLGenerator;
import org.jfree.data.category.IntervalCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class GanttChartDemo extends ApplicationFrame {


    /**
     * Starting point for the demonstration application.
     *
     * @param args ignored.
     */
    public static void main(final String[] args) {

        final GanttChartDemo demo = new GanttChartDemo("Gantt Chart Demo 1");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }
    
    private JFreeChart createChart(final IntervalCategoryDataset dataset) {
        final JFreeChart chart = createGanttChart(
                "Gantt Chart Demo", // chart title
                "Operations", // domain axis label
                "Date", // range axis label
                dataset, // data
                true, // include legend
                true, // tooltips
                false // urls
        );
//        chart.getCategoryPlot().getDomainAxis().setMaxCategoryLabelWidthRatio(10.0f);
        return chart;
    }

    public static JFreeChart createGanttChart(String title,
            String categoryAxisLabel, String dateAxisLabel,
            IntervalCategoryDataset dataset, boolean legend, boolean tooltips,
            boolean urls) {

        CategoryAxis categoryAxis = new CategoryAxis(categoryAxisLabel);
        DateAxis dateAxis = new DateAxis(dateAxisLabel);

        CategoryItemRenderer renderer = new MyGanttRenderer();
        // renderer.set
        if (tooltips) {
            renderer.setBaseToolTipGenerator(
                    new IntervalCategoryToolTipGenerator(
                            "{3} - {4}", DateFormat.getDateInstance()));
        }
        if (urls) {
            renderer.setBaseItemURLGenerator(
                    new StandardCategoryURLGenerator());
        }

        CategoryPlot plot = new CategoryPlot(dataset, categoryAxis, dateAxis,
                renderer);
        plot.setDomainGridlinesVisible(true);

        plot.setOrientation(PlotOrientation.HORIZONTAL);
        JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT,
                plot, legend);
        // currentTheme.apply(chart);
        return chart;

    }

    public GanttChartDemo(final String title) {

        super(title);

        final IntervalCategoryDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);

        // add the chart to a panel...
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);

    }

    private static Date date(final int day, final int month, final int year) {

        final Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        final Date result = calendar.getTime();
        return result;

    }

    public static IntervalCategoryDataset createDataset() {

        final TaskSeries s1 = new TaskSeries("Scheduled");
        s1.add(new Task("A",
                new SimpleTimePeriod(date(1, Calendar.APRIL, 2001),
                        date(2, Calendar.APRIL, 2001))));
        s1.add(new Task("A",
                new SimpleTimePeriod(date(3, Calendar.APRIL, 2001),
                        date(4, Calendar.APRIL, 2001))));

        final TaskSeries s2 = new TaskSeries("Scheduled2");
        s2.add(new Task("C",
                new SimpleTimePeriod(date(1, Calendar.APRIL, 2001),
                        date(2, Calendar.APRIL, 2001))));
        s2.add(new Task("D",
                new SimpleTimePeriod(date(3, Calendar.APRIL, 2001),
                        date(4, Calendar.APRIL, 2001))));

        s2.add(new Task("E",
                new SimpleTimePeriod(date(4, Calendar.APRIL, 2001),
                        date(5, Calendar.APRIL, 2001))));
        final TaskSeries s3 = new TaskSeries("Scheduled3332");
        Task eTask = new Task("E",
                new SimpleTimePeriod(date(1, Calendar.APRIL, 2001),
                        date(1, Calendar.APRIL, 2001)));
        s3.add(eTask);
        eTask.addSubtask(new Task("R",
                new SimpleTimePeriod(date(1, Calendar.APRIL, 2001),
                        date(2, Calendar.APRIL, 2001))));
        eTask.addSubtask(new Task("E",
                new SimpleTimePeriod(date(3, Calendar.APRIL, 2001),
                        date(4, Calendar.APRIL, 2001))));

        final TaskSeriesCollection collection = new TaskSeriesCollection();
        collection.add(s1);
        collection.add(s2);
        collection.add(s3);

        return collection;
    }
}
