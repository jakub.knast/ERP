/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.zzzother;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.scheduler.ScheduleLauncher;
import regiec.knast.erp.api.scheduler.model.Operation;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;

public class DijkstraEngine {

    public static final int INFINITE_DISTANCE = Integer.MAX_VALUE;

    
    public static void main(String[] args) throws ObjectCreateError {
        ScheduleLauncher.main(null);
    }
    
    /**
     * This comparator orders cities according to their shortest distances, in
     * ascending fashion. If two cities have the same shortest distance, we
     * compare the cities themselves.
     */
    private final Comparator shortestDistanceComparator = new Comparator() {
        public int compare(Object left, Object right) {
            assert left instanceof Operation && right instanceof Operation : "invalid comparison";
            return compare((Operation) left, (Operation) right);
        }

        private int compare(Operation left, Operation right) {
            // note that this trick doesn't work for huge distances, close to Integer.MAX_VALUE
            int result = getShortestDistance(left) - getShortestDistance(right);

            return (result == 0) ? left.cost.compareTo(right.cost) : result;
        }
    };

    /**
     * The graph.
     */
    private final DisjunctiveGraph map;

    /**
     * The working set of cities, kept ordered by shortest distance.
     */
    private final SortedSet unsettledNodes = new TreeSet(shortestDistanceComparator);

    /**
     * The set of cities for which the shortest distance to the source has been
     * found.
     */
    private final Set settledNodes = new HashSet();

    /**
     * The currently known shortest distance for all cities.
     */
    private final Map shortestDistances = new HashMap();

    /**
     * Predecessors list: maps a city to its predecessor in the spanning tree of
     * shortest paths.
     */
    private final Map predecessors = new HashMap();

    /**
     * Constructor.
     */
    public DijkstraEngine(DisjunctiveGraph map) {
        this.map = map;
    }

    /**
     * Initialize all data structures used by the algorithm.
     *
     * @param start the source node
     */
    private void init(Operation start) {
        settledNodes.clear();
        unsettledNodes.clear();

        shortestDistances.clear();
        predecessors.clear();

        // add source
        setShortestDistance(start, 0);
        unsettledNodes.add(start);
    }
//    public static long duration = 0;

    public void execute(Operation start, Operation destination) {
//        long startTime = System.currentTimeMillis();
        init(start);

        // the current node
        Operation u;

        // extract the node with the shortest distance
        while ((u = extractMin()) != null) {
            assert !isSettled(u);

            // destination reached, stop
            if (u == destination) {
                break;
            }
            markSettled(u);
            relaxNeighbors(u);
        }
//        duration += (System.currentTimeMillis() - startTime);
    }

    private Operation extractMin() {
        if (unsettledNodes.isEmpty()) {
            return null;
        }

//        System.out.println("unsettled: " + unsettledNodes);
//        System.out.print("wybieram do przejrzenia ");
//        System.out.println("first: " + (OperationBase) unsettledNodes.first());
        Operation min = (Operation) unsettledNodes.first();
        unsettledNodes.remove(min);

        return min;
    }

    private void relaxNeighbors(Operation u) {
//        for (Iterator i = map.getDestinations(u).iterator(); i.hasNext();) {
//            OperationBase v = (OperationBase) i.next();
        for (Operation v : map.getChildren(u)) {

//             skip node already settled
            if (isSettled(v)) {
                continue;
            }

//            System.out.print("parent " + u + " dziecko " + v + " ");
            if (getShortestDistance(v) > getShortestDistance(u) + u.cost) {
                // assign new shortest distance and mark unsettled
//                System.out.println("nowy dystans " + (getShortestDistance(u) + u.operationDuration));
                setShortestDistance(v, getShortestDistance(u) + u.cost);

//                System.out.println(shortestDistances);
                // assign predecessor in shortest path
                setPredecessor(v, u);
            }
        }
//        System.out.println("");
    }

    /**
     * Mark a node as settled.
     *
     * @param u the node
     */
    private void markSettled(Operation u) {
        settledNodes.add(u);
    }

    /**
     * Test a node.
     *
     * @param v the node to consider
     *
     * @return whether the node is settled, ie. its shortest distance has been
     * found.
     */
    private boolean isSettled(Operation v) {
        return settledNodes.contains(v);
    }

    /**
     * @return the shortest distance from the source to the given city, or
     * {@link DijkstraEngine#INFINITE_DISTANCE} if there is no route to the
     * destination.
     */
    public int getShortestDistance(Operation city) {
        Integer d = (Integer) shortestDistances.get(city);
        return (d == null) ? INFINITE_DISTANCE : d.intValue();
    }

    /**
     * Set the new shortest distance for the given node, and re-balance the set
     * according to new shortest distances.
     *
     * @param city the node to set
     * @param distance new shortest distance value
     */
    private void setShortestDistance(Operation city, int distance) {
        // this crucial step ensure no duplicates will be created in the queue
        // when an existing unsettled node is updated with a new shortest distance
        unsettledNodes.remove(city);

        shortestDistances.put(city, new Integer(distance));

        // re-balance the sorted set according to the new shortest distance found
        // (see the comparator the set was initialized with)
        unsettledNodes.add(city);
    }

    /**
     * @return the city leading to the given city on the shortest path, or
     * <code>null</code> if there is no route to the destination.
     */
    public Operation getPredecessor(Operation city) {
        return (Operation) predecessors.get(city);
    }

    private void setPredecessor(Operation a, Operation b) {
        predecessors.put(a, b);
    }

}
