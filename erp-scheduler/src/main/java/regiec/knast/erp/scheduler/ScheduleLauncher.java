/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.business.apitests.GuiceInjector;
import regiec.knast.erp.api.dao.interfaces.ProductionJobDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WorkerDAOInterface;
import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.entities.WorkerEntity;
import regiec.knast.erp.api.entities.containers.ProductionJobEntitiesList;
import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;
import regiec.knast.erp.api.scheduler.charts.ResultBarChartPainter;
import regiec.knast.erp.api.scheduler.construct.ConstructFirstComeFirstServed;
import regiec.knast.erp.api.scheduler.construct.ConstructLargestNumberOfRemainingTasks;
import regiec.knast.erp.api.scheduler.construct.ConstructLongestJobProcessTime;
import regiec.knast.erp.api.scheduler.construct.ConstructLongestJobRemainingProcessTime;
import regiec.knast.erp.api.scheduler.construct.ConstructLongestPrOrderProcessTime;
import regiec.knast.erp.api.scheduler.construct.ConstructLongestTaskTime;
import regiec.knast.erp.api.scheduler.construct.ConstructRandom;
import regiec.knast.erp.api.scheduler.construct.ConstructShortestJobProcessTime;
import regiec.knast.erp.api.scheduler.construct.ConstructShortestJobRemainingProcessTime;
import regiec.knast.erp.api.scheduler.construct.ConstructShortestPrOrderProcessTime;
import regiec.knast.erp.api.scheduler.construct.ConstructShortestTaskTime;
import regiec.knast.erp.api.scheduler.construct.ConstructSmalestNumberOfRemainingTasks;
import regiec.knast.erp.api.scheduler.model.AlgorithmResult;
import regiec.knast.erp.api.scheduler.model.LauncherTask;
import regiec.knast.erp.api.scheduler.model.LauncherTaskList;
import regiec.knast.erp.api.scheduler.model.TestCase;
import regiec.knast.erp.api.scheduler.model.TestResult;
import regiec.knast.erp.api.scheduler.model.TestResultsList;
import regiec.knast.erp.api.scheduler.sa.SimulatedAnnealing;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ScheduleLauncher extends GuiceInjector {

    @Inject
    private WorkerDAOInterface workerDAO;
    @Inject
    private ProductionJobDAOInterface productionJobDAO;

    private Gson g = new GsonBuilder().setPrettyPrinting().create();
    private Map<Integer, TestCase> casesMap;
    private ConstructFirstComeFirstServed foo = null;
    private SimulatedAnnealing bar = null;

    public static void main(String[] args) throws ObjectCreateError {
        GuiceInjector.init();
        Object create = ObjectBuilderFactory.instance().create(ScheduleLauncher.class);
        ScheduleLauncher scheduling = (ScheduleLauncher) create;
        try {
            scheduling.run();
        } catch (Exception ex) {
            Logger.getLogger(ScheduleLauncher.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            scheduling.cleanUp();
        }
    }

    public void run() {
        prepareCases();

        LauncherTaskList test = new LauncherTaskList();
        test.add(new LauncherTask(ConstructFirstComeFirstServed.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructFirstComeFirstServed.class));
        test.add(new LauncherTask(ConstructShortestTaskTime.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructShortestTaskTime.class));
        test.add(new LauncherTask(ConstructLongestTaskTime.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructLongestTaskTime.class));
        test.add(new LauncherTask(ConstructShortestJobRemainingProcessTime.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructShortestJobRemainingProcessTime.class));
        test.add(new LauncherTask(ConstructLongestJobRemainingProcessTime.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructLongestJobRemainingProcessTime.class));
        test.add(new LauncherTask(ConstructShortestJobProcessTime.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructShortestJobProcessTime.class));
        test.add(new LauncherTask(ConstructLongestJobProcessTime.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructLongestJobProcessTime.class));
        test.add(new LauncherTask(ConstructShortestPrOrderProcessTime.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructShortestPrOrderProcessTime.class));
        test.add(new LauncherTask(ConstructLongestPrOrderProcessTime.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructLongestPrOrderProcessTime.class));
        test.add(new LauncherTask(ConstructSmalestNumberOfRemainingTasks.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructSmalestNumberOfRemainingTasks.class));
        test.add(new LauncherTask(ConstructLargestNumberOfRemainingTasks.class, 1, null));
        test.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructLargestNumberOfRemainingTasks.class));

        LauncherTaskList testConstruct = new LauncherTaskList();
        testConstruct.add(new LauncherTask(ConstructRandom.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructFirstComeFirstServed.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructShortestTaskTime.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructLongestTaskTime.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructShortestJobRemainingProcessTime.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructLongestJobRemainingProcessTime.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructShortestJobProcessTime.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructLongestJobProcessTime.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructShortestPrOrderProcessTime.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructLongestPrOrderProcessTime.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructSmalestNumberOfRemainingTasks.class, 1, null));
        testConstruct.add(new LauncherTask(ConstructLargestNumberOfRemainingTasks.class, 1, null));

        LauncherTaskList runnerSA = new LauncherTaskList();
        runnerSA.add(new LauncherTask(ConstructLongestJobProcessTime.class, 1, null));
//        runnerSA.add(new LauncherTask(SimulatedAnnealing.class, 1, ConstructShortestPrOrderProcessTime.class));

//        runCases(test);
        runCases(testConstruct);
//        runCases(runnerSA);
    }

    private static final boolean RUN_IN_PARALLEL = true;
    public static final boolean ENABLE_PRINTING = true;

    public static final int TRANSPORT_WORKERS_COUNT = 2;
    public static final int TRANSPORT_TIME = 20;
    public static final boolean WITH_TRANSPORT = false;
    public static final boolean MANY_WORKERS_IN_GROUP = false;
    public static final boolean DRAW_DISJUNCTIVE = true;
    public static final boolean DRAW_GANTT = true;
    public static final boolean DRAW_RESULT_TIMES = false;
    public static final boolean DRAW_RESULT_POINTS = false;

    private void runCases(LauncherTaskList launcherTaskList) {
        long start = System.currentTimeMillis();
        TestResultsList testResults = new TestResultsList();
        final WorkerEntitiesList workers = workerDAO.list(WorkerEntity.class);
        if (!MANY_WORKERS_IN_GROUP) {
            Map<String, List<WorkerEntity>> groupedWorkersMap = workers.stream().collect(Collectors.groupingBy(w -> w.getProfessionGroup().getCode()));
            List<WorkerEntity> workersList = groupedWorkersMap.values().stream().map(list -> list.get(0)).collect(Collectors.toList());
            workers.clear();
            workers.addAll(workersList);
        }
        for (Map.Entry<Integer, TestCase> entry : casesMap.entrySet()) {
          
//            if (entry.getKey() >= 10) {
//                break;
//            }
//            if (entry.getKey() < 10) {
//                continue;
//            }
//              if (entry.getKey() >= 30) {
//                break;
//            }
//            if (entry.getKey() == 3) { // cały case
//            if (entry.getKey() == 2) { // cały case
            if (entry.getKey() == 100) { // klasyczny od Węglarza
//            if (true) { // wszystkie casy    
                System.out.println("case: " + entry.getKey());
                TestResult testResult = new TestResult(entry.getKey().toString());
                testResults.add(testResult);
                if (RUN_IN_PARALLEL) {
                    ExecutorService exector = Executors.newFixedThreadPool(6);
                    Set<Future<AlgorithmResult>> set = new HashSet();
                    for (LauncherTask launcherTask : launcherTaskList) {
                        TestCase testCase = entry.getValue();
                        Callable<AlgorithmResult> callable = new ParallelScheduleTaskRunner(launcherTask, testCase, new WorkerEntitiesList(workers));
                        Future<AlgorithmResult> future = exector.submit(callable);
                        set.add(future);
                    }
                    for (Future<AlgorithmResult> future : set) {
                        try {
                            testResult.addValue(future.get());
                        } catch (InterruptedException | ExecutionException ex) {
                            throw new ConversionError("Interrupt exception ;/ ", ex);
                        }
                    }
                    exector.shutdown();
                } else {
                    for (LauncherTask launcherTask : launcherTaskList) {
                        TestCase testCase = entry.getValue();
                        ScheduleTaskRunner scheduleTaskRunner = new ScheduleTaskRunner(launcherTask, testCase, new WorkerEntitiesList(workers));
                        AlgorithmResult result = scheduleTaskRunner.run();
                        testResult.addValue(result);
                    }
                }
                

            }
        }
        if (DRAW_RESULT_TIMES) {
            ResultBarChartPainter.fromDataset(testResults.toTimeDataset());
        }
        if (DRAW_RESULT_POINTS) {
            ResultBarChartPainter.fromDataset(testResults.toPointsDataset());
        }
        saveResults(testResults);

//        System.out.println("dijkstra trwał: " + DijkstraCriticalPathDetectorSlow.duration);
//        System.out.println("dijkstra3 trwał: " + CriticalPathDetector.duration);
        System.out.println("całośc trwała: " + (System.currentTimeMillis() - start) / 1000 + " sekund");
        System.out.println("całośc trwała: " + (System.currentTimeMillis() - start) / 60000 + " minut");

        //  ResultBarChart.fromDataset(results1.countPoints());
        //  ResultBarChart.fromDataset(dataset);
    }
    private void saveResults(TestResultsList testResults) {
        String testResultsString = g.toJson(testResults);
        String fileName = "wyniki"
                + Calendar.getInstance().get(Calendar.YEAR) + "-"
                + Calendar.getInstance().get(Calendar.MONTH) + "-"
                + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "--"
                + Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + "-"
                + Calendar.getInstance().get(Calendar.MINUTE) + "-"
                + Calendar.getInstance().get(Calendar.SECOND) + ".json";
        File resultFile = new File("/opt/" + fileName);
        try (FileOutputStream os = new FileOutputStream(resultFile)) {
            IOUtils.write(testResultsString, os);
        } catch (Exception e) {
            throw new ConversionError("", e);
        }
    }

    public Map<Integer, TestCase> prepareCases() {
        casesMap = new HashMap();
        ProductionJobEntitiesList list = productionJobDAO.list(ProductionJobEntity.class);
        for (ProductionJobEntity pte : list) {
            for (Integer caseNo : pte.getTestCasesNumbers()) {
                if (!casesMap.containsKey(caseNo)) {
                    casesMap.put(caseNo, new TestCase());
                }
                casesMap.get(caseNo).add(pte);
            }
        }
        return casesMap;
    }

}
