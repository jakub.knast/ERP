/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import java.util.Objects;

/**
 *
 * @author fbrzuzka
 */
public class OperationZero extends Operation {

    public OperationZero() {
        super();
        this.vertexNo = 0;
        this.operationName = "zero";
        this.professionGroup = null;
        this.cost = 0;
    }

    @Override
    public String toString() {
        return this.vertexNo == null ? "zero" : "zero " + this.vertexNo;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.hash);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OperationZero other = (OperationZero) obj;
        if (!Objects.equals(this.hash, other.hash)) {
            return false;
        }
        return true;
    }
}
