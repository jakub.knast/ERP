/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler;

import com.rits.cloning.Cloner;
import java.util.Collections;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;
import static regiec.knast.erp.api.scheduler.ScheduleLauncher.DRAW_DISJUNCTIVE;
import static regiec.knast.erp.api.scheduler.ScheduleLauncher.DRAW_GANTT;
import static regiec.knast.erp.api.scheduler.ScheduleLauncher.TRANSPORT_TIME;
import static regiec.knast.erp.api.scheduler.ScheduleLauncher.TRANSPORT_WORKERS_COUNT;
import static regiec.knast.erp.api.scheduler.ScheduleLauncher.WITH_TRANSPORT;
import regiec.knast.erp.api.scheduler.charts.DisjunctiveGraphPainter;
import regiec.knast.erp.api.scheduler.charts.GanttChartPainter;
import regiec.knast.erp.api.scheduler.exceptions.CreatesSchedulerException;
import regiec.knast.erp.api.scheduler.model.AlgorithmResult;
import regiec.knast.erp.api.scheduler.model.LauncherTask;
import regiec.knast.erp.api.scheduler.model.Operation;
import regiec.knast.erp.api.scheduler.model.Schedule;
import regiec.knast.erp.api.scheduler.model.TestCase;
import regiec.knast.erp.api.scheduler.model.interfaces.SaSchedulingInterface;
import regiec.knast.erp.api.scheduler.model.interfaces.SchedulingInterface;
import regiec.knast.erp.api.scheduler.utils.CycleDetector;

/**
 *
 * @author fbrzuzka
 */
public class ScheduleTaskRunner {

    protected LauncherTask launcherTask;
    protected TestCase testCase;
    protected WorkerEntitiesList workers;

    public ScheduleTaskRunner(LauncherTask launcherTask, TestCase testCase, WorkerEntitiesList workers) {
        this.launcherTask = launcherTask;
        this.testCase = testCase;
        this.workers = workers;
    }

    public AlgorithmResult run() {      
        Schedule schedule;
        if (launcherTask.getHowManyTrials() > 1) {
            schedule = runWithProgress(launcherTask, testCase, workers);
        } else {
            Schedule initSchedule = null;
            SchedulingInterface initializer = null;
            if (launcherTask.getInitializationAlgorithmClass() != null) {
                initializer = createScheduler(launcherTask.getInitializationAlgorithmClass());
                initSchedule = initializer.run("initializer " + initializer.getName(), testCase, workers, WITH_TRANSPORT, TRANSPORT_TIME, TRANSPORT_WORKERS_COUNT);
            }
            SchedulingInterface scheduler = createScheduler(launcherTask.getAlgorithmClass());
            if (initializer != null) {
                Schedule cloneOfInitSchedule = (Schedule) new Cloner().deepClone(initSchedule);
                schedule = ((SaSchedulingInterface) scheduler).run(initializer.getName() + " " + scheduler.getName(), testCase, workers, cloneOfInitSchedule, WITH_TRANSPORT, TRANSPORT_TIME, TRANSPORT_WORKERS_COUNT);
            } else {
                schedule = scheduler.run(scheduler.getName(), testCase, workers, WITH_TRANSPORT, TRANSPORT_TIME, TRANSPORT_WORKERS_COUNT);
            }
        }
        final String algorithmName = schedule.getAlgName();
        int makespan = calculateHowLong(schedule);
//                    DijkstraLongest.Result result = schedule.getGraph().calculateMakespan();
//        testResult.addValue(makespan, algorithmName);
        if (DRAW_GANTT) {
            GanttChartPainter.ganttFromSchedule(algorithmName + " " + String.valueOf(makespan), schedule, false);
        }
        if (DRAW_DISJUNCTIVE && schedule.hasGraph()) {
            if (new CycleDetector().detectCycleInGraph(schedule.getGraph())) {
                throw new ConversionError(algorithmName + " has cycle!!!");
            }
            DisjunctiveGraphPainter disjunctiveGraph = new DisjunctiveGraphPainter(schedule.getGraph(), algorithmName, makespan);
        }
//        System.out.println("uszeregowanie " + algorithmName + " zajmie " + makespan + " minut");
        return new AlgorithmResult(makespan, algorithmName);
    }
    
    private Schedule runWithProgress(LauncherTask launcherTask, TestCase testCase, WorkerEntitiesList workers) {
        Schedule initSchedule = null;
        SchedulingInterface initializer = null;
        if (launcherTask.getInitializationAlgorithmClass() != null) {
            initializer = createScheduler(launcherTask.getInitializationAlgorithmClass());
            initSchedule = initializer.run("initializer " + initializer.getName(), testCase, workers, WITH_TRANSPORT, TRANSPORT_TIME, TRANSPORT_WORKERS_COUNT);
        }
        Schedule bestSchedule = null;
        int minMakeSpan = Integer.MAX_VALUE;
        Integer errors = 0;
        String algorithmName = "";
        for (int i = 0; i < launcherTask.getHowManyTrials(); i++) {
            SchedulingInterface scheduler = createScheduler(launcherTask.getAlgorithmClass());
            algorithmName = scheduler.getName();

            final String algorithmNameIter = algorithmName + "_" + i;
            Schedule schedule;
            try {
                if (initializer != null) {
                    Schedule cloneOfInitSchedule = (Schedule) new Cloner().deepClone(initSchedule);
                    schedule = ((SaSchedulingInterface) scheduler).run(initializer.getName() + " " + algorithmNameIter, testCase, workers, cloneOfInitSchedule, WITH_TRANSPORT, TRANSPORT_TIME, TRANSPORT_WORKERS_COUNT);
                } else {
                    schedule = scheduler.run(algorithmNameIter, testCase, workers, WITH_TRANSPORT, TRANSPORT_TIME, TRANSPORT_WORKERS_COUNT);
                }
            } catch (ConversionError e) {
                errors++;
                continue;
            }
            int makespan = calculateHowLong(schedule);
            if (makespan < minMakeSpan) {
                bestSchedule = schedule;
                minMakeSpan = makespan;
            }
        }
        System.out.println("errors in " + algorithmName + ": " + errors.toString());
        return bestSchedule;
    }

    private int calculateHowLong(Schedule s1) {
        Operation o = Collections.max(s1, (Operation o1, Operation o2) -> o1.operationEndTime.compareTo(o2.operationEndTime));
        return o.operationEndTime;
    }

    public SchedulingInterface createScheduler(Class<? extends SchedulingInterface> schedulerClass) {
        try {
            return schedulerClass.getConstructor().newInstance();
        } catch (ReflectiveOperationException | IllegalArgumentException ex) {
            throw new CreatesSchedulerException("Cannot initialize scheduling class: " + schedulerClass.getSimpleName());
        }
    }

}
