/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

/**
 *
 * @author fbrzuzka
 */
public class Edge{
    public Integer no;
    public Integer weight;
    public boolean isDisjunctive;

    public Edge(Integer no, Integer weight, boolean isDisjunctive) {
        this.no = no;
        this.weight = weight;
        this.isDisjunctive = isDisjunctive;
    }

    public Edge(Integer no, Integer weight) {
        this.no = no;
        this.weight = weight;
        this.isDisjunctive = false;
    }

    public boolean isDisjunctive() {
        return isDisjunctive;
    }

    @Override
    public String toString() {
        return "Edge{" + "no=" + no + ", isDisjunctive=" + isDisjunctive + '}';
    }
    
    
}
