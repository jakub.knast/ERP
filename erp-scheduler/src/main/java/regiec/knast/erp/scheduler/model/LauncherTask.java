/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import regiec.knast.erp.api.scheduler.model.interfaces.SchedulingInterface;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
public class LauncherTask {
    private Class<? extends SchedulingInterface> algorithmClass;
    private Integer howManyTrials;
    private Class<? extends SchedulingInterface> initializationAlgorithmClass;

    public LauncherTask(Class<? extends SchedulingInterface> algorithmClass, Integer howManyRuns, Class<? extends SchedulingInterface> initializationAlgorithmClass) {
        this.algorithmClass = algorithmClass;
        this.howManyTrials = howManyRuns;
        this.initializationAlgorithmClass = initializationAlgorithmClass;
    }   
    
}
