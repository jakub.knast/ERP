/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.charts.model;

import java.util.Date;
import org.jfree.data.gantt.Task;
import org.jfree.data.time.TimePeriod;

/**
 *
 * @author fbrzuzka
 */
public class MultiTask extends Task {

    private boolean isOnCriticalPath;
    private Integer vertexNo;

    public MultiTask(String description, TimePeriod duration) {
        super(description, duration);
    }

    public MultiTask(String description, Date start, Date end, boolean isOnCriticalPath) {
        super(description, start, end);
        this.isOnCriticalPath = isOnCriticalPath;
    }
    
    public MultiTask(String description, Date start, Date end, boolean isOnCriticalPath, Integer vertexNo) {
        super(description, start, end);
        this.isOnCriticalPath = isOnCriticalPath;
        this.vertexNo = vertexNo;
    }

    public MultiTask(String description, TimePeriod duration, boolean isOnCriticalPath) {
        super(description, duration);
    }

    public Integer getVertexNo() {
        return vertexNo;
    }

    public void setVertexNo(Integer vertexNo) {
        this.vertexNo = vertexNo;
    }

    public boolean isIsOnCriticalPath() {
        return isOnCriticalPath;
    }

    public void setIsOnCriticalPath(boolean isOnCriticalPath) {
        this.isOnCriticalPath = isOnCriticalPath;
    }

}
