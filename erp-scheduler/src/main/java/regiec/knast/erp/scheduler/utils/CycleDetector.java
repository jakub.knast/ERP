/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.utils;

import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author fbrzuzka
 */
public class CycleDetector {

    private DisjunctiveGraph graph;
    
    public boolean detectCycleInGraph(DisjunctiveGraph graph) {
        this.graph = graph;
        Operation start = graph.getVertices().stream().filter(v -> v.vertexNo == 0).findFirst().get();
        boolean visited[] = new boolean[graph.getVertexCount()];
        boolean recStack[] = new boolean[graph.getVertexCount()];
        return isCyclic(start, visited, recStack);
    }

    private boolean isCyclic(Operation op, boolean[] visited, boolean[] recursionStack) {
        int v = op.vertexNo;
        if (visited[v] == false) {
            visited[v] = true;
            recursionStack[v] = true;

            for (Operation child : graph.getChildren(op)) {                
                if (!visited[child.vertexNo] && isCyclic(child, visited, recursionStack)) {
                    return true;
                } else if (recursionStack[child.vertexNo]) {
                    return true;
                }
            }
        }
        recursionStack[v] = false; 
        return false;
    }
}
