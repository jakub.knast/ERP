/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.zzzother;

import java.util.ArrayList;
import regiec.knast.erp.api.scheduler.model.Operation;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;

/**
 *
 * @author fbrzuzka
 */
public class LongestPathAlgo {

    public int run(DisjunctiveGraph graph) {
        Operation start = graph.getVertices().stream().filter(v -> v.vertexNo == 0).findFirst().get();
        Operation end = graph.getVertices().stream().filter(v -> v.operationName.equals("last")).findFirst().get();
        return run(graph, start, end.vertexNo);
    }

    public int run(DisjunctiveGraph graph, Operation startVertex, int endVertextIndex) {
        ArrayList verticles = new ArrayList(graph.getVertices());
        int verticlesCount = verticles.size();
//        long starttime = System.nanoTime();
        boolean visited[] = new boolean[endVertextIndex + 1];
        initialize(verticlesCount, visited);

        int max = longestPath(startVertex, visited, graph);
//        long runtime = System.nanoTime() - starttime;
//        System.out.println("Runtime =" + runtime + " nano seconds");
        System.out.println("Longest Path Length = " + max);
        return max;
    }

    public void initialize(int verticlesCount, boolean visited[]) {
        for (int i = 0; i < verticlesCount; i++) {
            visited[i] = false;
        }
    }

    int longestPath(Operation v, boolean visited[], DisjunctiveGraph graph) {
        int dist, max = 0;
        visited[v.vertexNo] = true;
        for (Operation op : graph.getChildren(v)) {
            if (!visited[op.vertexNo]) {
                dist = op.cost + longestPath(op, visited, graph);
//                op.operationStartTime = dist - op.operationDuration;
//                op.operationEndTime = dist;
                if (dist > max) {
                    max = dist;
                }
            }
        }
        visited[v.vertexNo] = false;
        return max;
    }
}
