/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.construct;

import edu.uci.ics.jung.graph.util.EdgeType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.business.apitests.GuiceInjector;
import regiec.knast.erp.api.entities.WorkerEntity;
import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;
import regiec.knast.erp.api.model.TreeNodeType;
import regiec.knast.erp.api.scheduler.ScheduleLauncher;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Edge;
import regiec.knast.erp.api.scheduler.model.EdgeWrapper;
import regiec.knast.erp.api.scheduler.model.Job;
import regiec.knast.erp.api.scheduler.model.JobState;
import regiec.knast.erp.api.scheduler.model.Operation;
import regiec.knast.erp.api.scheduler.model.OperationLast;
import regiec.knast.erp.api.scheduler.model.OperationState;
import regiec.knast.erp.api.scheduler.model.OperationZero;
import regiec.knast.erp.api.scheduler.model.Schedule;
import regiec.knast.erp.api.scheduler.model.TestCase;
import regiec.knast.erp.api.scheduler.model.Worker;
import regiec.knast.erp.api.scheduler.model.interfaces.SchedulingInterface;
import regiec.knast.erp.api.scheduler.sa.SaUtil;
import regiec.knast.erp.api.scheduler.utils.CriticalPathDetector;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ConstructFirstComeFirstServed extends GuiceInjector implements SchedulingInterface {

    public static int TRANSPORT_WORKERS_COUNT = 1;
    public static Integer TRANSPORT_TIME = 60;
    public static boolean WITH_TRANSPORT = true;
    public static String SAVED_TRANSPORT = "Zaoszczędzony transport";

    private SaUtil saUtil = new SaUtil();

    private List<Job> jobs = new ArrayList();
    private List<Operation> operations = new ArrayList();
    private Map<String, List<Worker>> grupedWorkers = new HashMap();
    private WorkerEntitiesList workers;
    private Worker savedTransportWorker;
    private Map<String, List<EdgeWrapper>> disjunctiveEdgesMap;
    private Map<String, List<Operation>> disjunctiveOperationsMap;

    public static void main(String[] args) throws ObjectCreateError {
        ScheduleLauncher.main(null);
    }

    public DisjunctiveGraph toGraph() {
        List<Job> rootJobs = jobs.stream()
                .filter(j -> j.treeNodeType == TreeNodeType.ROOT)
                .collect(Collectors.toList());

        DisjunctiveGraph graph = new DisjunctiveGraph(grupedWorkers);

        Operation zeroV = new OperationZero();
        Operation lastV = new OperationLast();
        graph.addVertex(zeroV);
        saUtil.addRootsOfProductionJobs(rootJobs, graph, zeroV, lastV);

        List<Operation> disjunctiveOperations = operations.stream()
                .filter(o -> o.tempPreviousDisjunctiveOperation != null)
                .collect(Collectors.toList());
        int edgeNo = graph.getEdgeCount();

        for (Operation o : disjunctiveOperations) {
            for (Operation preOp : o.tempPreviousDisjunctiveOperation) {
                Edge edge = new Edge(edgeNo++, preOp.cost, true);
                graph.addEdge(edge, preOp, o, EdgeType.DIRECTED);
                disjunctiveEdgesMap.get(o.worker.workerLabel).add(new EdgeWrapper(edge, preOp, o));
            }
        }
        graph.disjunctiveEdgesMap = new HashMap();
        graph.disjunctiveOperations = new HashMap();
        for (Map.Entry<String, List<EdgeWrapper>> entry : this.disjunctiveEdgesMap.entrySet()) {
            if (entry.getValue().size() > 0) {
                graph.disjunctiveEdgesMap.put(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry<String, List<Operation>> entry : this.disjunctiveOperationsMap.entrySet()) {
            if (entry.getValue().size() >= 2) {
                graph.disjunctiveOperations.put(entry.getKey(), entry.getValue());
            }
        }
        CriticalPathDetector dijkstra = new CriticalPathDetector(graph).execute();
        CriticalPathDetector.Result result = dijkstra.getResult();
        result.markOperationsOnCriticalPath(graph);

//        System.out.println("makespan: " + result.makespan);
        ArrayList<Operation> verticles = new ArrayList(graph.getVertices());
        Collections.sort(verticles, new Comparator<Operation>() {
            @Override
            public int compare(Operation o1, Operation o2) {
                return o1.vertexNo.compareTo(o2.vertexNo);
            }
        });
        
        Operation last = graph.getVertices().stream().filter(v -> v.operationName.equals("last")).findFirst().get();
        last.vertexNo = graph.getVertexCount()-1;
        System.out.println("verticles " + (graph.getVertexCount()-2));
//        System.out.println("jobs: " + jobs.size());
        return graph;
    }

    private Schedule doSchedule() {
        int time = 0;
        int maxTime = 100000;
        Schedule ret = new Schedule(this.getName());

        while (time < maxTime && getCompleteOperations().size() < operations.size()) {
            for (Operation o : getOperationsInProduction()) {
                if (o.operationEndTime == time) {
                    o.worker.doingOperation = false;
                    o.job.doneOperations++;
                    o.operationState = OperationState.operationCompleted;
                    if (o.isLast()) {
                        o.job.state = JobState.jobCompleted;
                        o.job.tellParentJobThatImComplete(jobs);
                    } else {
                        o.getNextOperation().operationState = OperationState.operationReady;
                    }
                }
            }
            for (Job j : getNeedSemiJobs()) {
                if (j.getNotDoneChildrenJobs().isEmpty()) {
                    j.state = JobState.jobReady;
                    j.updateOperationsStates();
                }
            }

            for (Operation readyOperation : getReadyOperations()) {
                tryToFindWorkerForOperation(readyOperation, time, ret);
            }
            time++;
        }
        if (time == maxTime) {
            System.out.println("Time is left");
            throw new ConversionError("Time is left");
        }
        return ret;
    }

    private void tryToFindWorkerForOperation(Operation operation, int time, Schedule ret) {
        String groupCode = operation.professionGroup.getCode();

        //sprawdź czy jakiś pracownik jest wolny, jesli tak to daj mu tę operację.
        for (Worker worker : grupedWorkers.get(groupCode)) {
            if (!worker.doingOperation) {
                giveOperationToWorker(time, operation, worker);
                ret.add(operation);
                break;
            }
        }
    }

    private void giveOperationToWorker(int time, Operation operation, Worker worker) {
        worker.doingOperation = true;
        disjunctiveOperationsMap.get(worker.workerLabel).add(operation);
        operation.worker = worker;
        if (worker.doneOperation != null) {
            operation.tempPreviousDisjunctiveOperation.add(worker.doneOperation);
        }
        operation.worker.doneOperation = operation;
        operation.operationStartTime = time;
        operation.operationEndTime = operation.calculateOperationEndTime(time);
        operation.operationState = OperationState.operationInProduction;
        operation.createKey();
    }

    @Override
    public Schedule run(String title, TestCase casee, WorkerEntitiesList workers, boolean withTransport, int transportTime, int transportWorkerCount) {
        this.workers = workers;
        TRANSPORT_TIME = transportTime;
        TRANSPORT_WORKERS_COUNT = transportWorkerCount;
        WITH_TRANSPORT = withTransport;
        List<Job> jobs = caseToJobks(casee);
        initLists(jobs);
        Schedule schedule = doSchedule();
        schedule.setGraph(toGraph());
//        System.out.println(this.getClass().getSimpleName() + " skończył");
        return schedule;
    }

    private List<Job> caseToJobks(TestCase casee) {
        List<Job> rootJobs = casee.stream()
                .filter(pt -> pt.getTreeNodeType() == TreeNodeType.ROOT)
                .collect(Collectors.mapping(pt -> new Job(pt, null, WITH_TRANSPORT, TRANSPORT_TIME), Collectors.toList()));

        List<Job> jjjjj = new ArrayList();
        for (Job root : rootJobs) {
            for (Job job : getChildren(root)) {
                jjjjj.add((Job) job);
            }
        }
        return jjjjj;
    }

    private List<Job> getChildren(Job job) {
        List<Job> li = new ArrayList();
        li.add(job);
        for (Job jobBase : job.children) {
            li.addAll(getChildren((Job) jobBase));
        }
        return li;
    }

    private void initLists(List<Job> jobss) {
        for (Job j : jobss) {
            if (j.hasNoChildren) {
                j.state = JobState.jobReady;
            } else {
                j.state = JobState.jobWaitingForDependentJobs;
            }
            jobs.add(j);
        }
        for (WorkerEntity workerEntty : workers) {
            String code = workerEntty.getProfessionGroup().getCode();
            if (!grupedWorkers.containsKey(code)) {
                grupedWorkers.put(code, new ArrayList());
            }
            grupedWorkers.get(code).add(new Worker(workerEntty));
        }
        ProfessionGroup transportGroup = ProfessionGroup.getTransportGroup();
        List<Worker> transportWorkers = new ArrayList();
        for (int i = 0; i < TRANSPORT_WORKERS_COUNT; i++) {
            transportWorkers.add(Worker.newTransportWorker(i));
        }
        savedTransportWorker = Worker.newTransportWorker(123);
        savedTransportWorker.workerLabel = SAVED_TRANSPORT;
        grupedWorkers.put(transportGroup.getCode(), transportWorkers);

        for (Job j : jobss) {
            for (Operation operation2 : j.operationsToDo) {
                operations.add(operation2);
            }
        }

        disjunctiveEdgesMap = new HashMap();
        disjunctiveOperationsMap = new HashMap();
        for (List<Worker> value : grupedWorkers.values()) {
            for (Worker w : value) {
                disjunctiveEdgesMap.put(w.workerLabel, new ArrayList());
                disjunctiveOperationsMap.put(w.workerLabel, new ArrayList());
            }
        }
    }

    public ArrayList<Operation> getReadyOperations() {
        return getOperationsInState(OperationState.operationReady);
    }

    public ArrayList<Operation> getOperationsInProduction() {
        return getOperationsInState(OperationState.operationInProduction);
    }

    private ArrayList<Operation> getCompleteOperations() {
        return getOperationsInState(OperationState.operationCompleted);
    }

    private ArrayList<Job> getNeedSemiJobs() {
        return getJobsInState(JobState.jobWaitingForDependentJobs);
    }

    private ArrayList<Job> getJobsInState(JobState state) {
        ArrayList<Job> ret = new ArrayList();
        for (Job j : jobs) {
            if (j.state == state) {
                ret.add(j);
            }
        }
        return ret;
    }

    private ArrayList<Operation> getOperationsInState(OperationState state) {
        ArrayList<Operation> ret = new ArrayList();
        for (Operation o : operations) {
            if (o.operationState == state) {
                ret.add(o);
            }
        }
        return ret;
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

}
