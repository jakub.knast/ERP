/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.sa;

import com.google.inject.Singleton;
import com.rits.cloning.Cloner;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.scheduler.ScheduleLauncher;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Edge;
import regiec.knast.erp.api.scheduler.model.EdgeWrapper;
import regiec.knast.erp.api.scheduler.model.Job;
import regiec.knast.erp.api.scheduler.model.Operation;
import regiec.knast.erp.api.scheduler.model.Permutation;
import regiec.knast.erp.api.scheduler.utils.CycleDetector;
import regiec.knast.erp.api.scheduler.utils.CriticalPathDetector;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class SaUtil {
    
    public static void main(String[] args) throws ObjectCreateError {
        ScheduleLauncher.main(null);
    }
    
    private Integer vertexNo = 1;
    private Integer edgeNo = 0;
    private Random rand = new Random();

    public Integer nextVertexNo() {
        return vertexNo++;
    }

    public Integer nextEdgeNo() {
        return edgeNo++;
    }

    public void resetVertexNo() {
        this.vertexNo = 1;
    }

    public void resetEdgeNo() {
        this.edgeNo = 0;
    }
//    public void cloneGraph

    public void removeEdges(Permutation oldPerm, DisjunctiveGraph graph) {
        for (int i = 1; i < oldPerm.size(); i++) {
            Edge edgeAB = findEdge(oldPerm.get(i - 1), oldPerm.get(i), graph);
            if(edgeAB == null){
                throw new ConversionError("dijsunctive edge to remove not found. From: " + oldPerm.get(i - 1) + " to " + oldPerm.get(i));
            }
            graph.removeEdge(edgeAB);
//            graph.removeEdge(findEdge(oldPerm.get(i-1), oldPerm.get(i), graph));
//            System.out.println("graph.removeEdge(findEdge(" + oldPerm.get(i-1)+ ", " + oldPerm.get(i)+ ", graph))");
        }
    }

    public Permutation addEdgesWithOrder(DisjunctiveGraph graph, Operation v1, Operation v2, Operation v3) {
        graph.addEdge(new Edge(nextEdgeNo(), v1.cost, true), v1, v2, EdgeType.DIRECTED);
        graph.addEdge(new Edge(nextEdgeNo(), v2.cost, true), v2, v3, EdgeType.DIRECTED);
        return new Permutation(v1, v2, v3);
    }

    public void addDisjunctiveEdgesToVerticles(Permutation newPerm, DisjunctiveGraph graph) {
        for (int i = 1; i < newPerm.size(); i++) {
            graph.addEdge(new Edge(nextEdgeNo(), newPerm.get(i - 1).cost, true), newPerm.get(i - 1), newPerm.get(i), EdgeType.DIRECTED);
        }
//        graph.addEdge(new Edge(nextEdgeNo(), v1.operationDuration, true), v1, v2, EdgeType.DIRECTED);
    }

    public Edge findEdge(Operation v1, Operation v2, DisjunctiveGraph graph) {
        Collection<Edge> outEdges = graph.getOutDirectedDisjunctiveEdges(v1);
        for (Edge outEdge : outEdges) {
            if (graph.getOpposite(v1, outEdge).equals(v2)) {
                return outEdge;
            }
        }
        return null;
    }

    public void updateEdgesToNewPermutation(Permutation newPerm, Permutation oldPerm, DisjunctiveGraph nextSolution) {
        Permutation toChange = findDifferencesInPermutations(newPerm, oldPerm);
        removeEdges(toChange, nextSolution);
        addDisjunctiveEdgesToVerticles(newPerm, nextSolution);
    }

    private Permutation findDifferencesInPermutations(Permutation newPerm, Permutation oldPerm) {
        return oldPerm;
    }

    public Permutation createNewPermutationWthConstraint(Permutation permutation, Operation vToChange, List<Integer> criticalPath, DisjunctiveGraph nextSolution) {
        return null;
    }
    
    public Permutation createNewPermutation(List<Operation> permutation, Operation v, List<Integer> criticalPath, DisjunctiveGraph graph) {
//        int v1IndexOnCriticalPath = criticalPath.indexOf(v.vertexNo);
        int vToChangeIndexinPermutation = permutation.indexOf(v);
//        int vOpositeIndexOnCriticalPath = -1;
//        int vOpositeIndexinPermutation = -1;
        List<Operation> ret = new ArrayList();

        //perm is >= 2 for sure
        //jeśli pierwszy w permutacji
        List<Operation> pre = new ArrayList(permutation.subList(0, vToChangeIndexinPermutation));
        List<Operation> post = new ArrayList(permutation.subList(vToChangeIndexinPermutation + 1, permutation.size()));
        List<Operation> prepre = new ArrayList();
        List<Operation> postpost = new ArrayList();
        if (pre.size() >= 2) {
            prepre = new ArrayList(pre.subList(0, pre.size() - 2));
            pre.removeAll(prepre);
        }
        if (post.size() >= 2) {
            postpost = new ArrayList(post.subList(2, post.size()));
            post.removeAll(postpost);
        }

        ret.addAll(prepre);
        if (pre.isEmpty()) { // post should has 1 or 2
            Operation vPost = post.get(0);
            ret.addAll(pre);
            ret.add(vPost);
            ret.add(v);
            if (post.size() == 2) { // has size 2     
                ret.add(post.get(1));
            }
        } else if (post.isEmpty()) {
            Operation vPre = pre.get(pre.size() - 1);
            if (pre.size() == 2) { // has size 2   
                ret.add(pre.get(0));
            }
            ret.add(v);
            ret.add(vPre);
        } else if (pre.size() == 1) { // post should has 1 or 2
            Operation vPre = pre.get(0);
            Operation vPost = post.get(0);
            addInChangedRandomOrder(vPre, v, vPost, ret);
            if (post.size() == 2) { // has size 2      
                ret.add(post.get(1));
            }
        } else if (post.size() == 1) {
            Operation vPre = pre.get(1);
            Operation vPost = post.get(0);
            if (pre.size() == 2) { // has size 2   
                ret.add(pre.get(0));
            }
            addInChangedRandomOrder(vPre, v, vPost, ret);
        } else if (pre.size() == 2 && post.size() == 2) {
            Operation vPre = pre.get(1);
            Operation vPost = post.get(0);
            ret.add(pre.get(0));
            addInChangedRandomOrder(vPre, v, vPost, ret);
            ret.add(post.get(1));
        }
        ret.addAll(postpost);
        return new Permutation(ret);
    }

    private void addInChangedRandomOrder(Operation vPre, Operation v, Operation vPost, List<Operation> ret) {
        if (rand.nextBoolean()) {
            add_v_vPre_vPost(v, vPre, vPost, ret);
        } else {
            add_vPre_vPost_v(vPre, vPost, v, ret);
        }
    }

    private void add_v_vPre_vPost(Operation v, Operation vPre, Operation vPost, List<Operation> newPerm) {
        newPerm.add(v);
        newPerm.add(vPre);
        newPerm.add(vPost);
    }

    private void add_vPre_vPost_v(Operation vPre, Operation vPost, Operation v, List<Operation> newPerm) {
        newPerm.add(vPre);
        newPerm.add(vPost);
        newPerm.add(v);
    }

    public String addspace(int i, String str) {
        StringBuilder str1 = new StringBuilder();
        str1.append(str);
        for (int j = 0; j < i; j++) {
            str1.append(" ");
        }
        return str1.toString();
    }

    public String disjOpToString(Collection<List<Operation>> permCol) {
        StringBuilder s = new StringBuilder();
        s.append("[");
        for (List<Operation> perm : permCol) {
            s.append("(");
            for (int i = 0; i < perm.size() - 1; i++) {
                s.append(perm.get(i).vertexNo.toString());
                s.append(", ");
            }
            s.append(perm.get(perm.size() - 1).vertexNo.toString());
            s.append(") ");
        }
        s.append("]");
        return s.toString();
    }

    public void randInitialSolution(DisjunctiveGraph graph, Map<String, List<Operation>> grouppedDisjunctiveOperations) {

        Map<String, Permutation> shuffledPermutations = new HashMap();
        Map<String, List<EdgeWrapper>> disjunctiveEdgesMap = new HashMap();
        int iter = 1;
        while (true) {
//            System.out.println("\niteracja: " + iter);
            for (Map.Entry<String, List<Operation>> entry : grouppedDisjunctiveOperations.entrySet()) {
                Permutation perm = Permutation.prepareBetterShuffledPermutation(new Permutation(entry.getValue()));
                shuffledPermutations.put(entry.getKey(), perm);
            }

            for (Map.Entry<String, Permutation> entry : shuffledPermutations.entrySet()) {
                Permutation perm = entry.getValue();
                List<EdgeWrapper> edges2 = new ArrayList();
                disjunctiveEdgesMap.put(entry.getKey(), edges2);
                for (int i = 1; i < perm.size(); i++) {
//                    if (!perm.get(i - 1).jobNo.equals(perm.get(i).jobNo)) {
                    edges2.add(new EdgeWrapper(new Edge(nextEdgeNo(), perm.get(i - 1).cost, true), perm.get(i - 1), perm.get(i)));
//                    }
                }
            }
            graph.disjunctiveEdgesMap = disjunctiveEdgesMap;
            graph.disjunctiveOperations = grouppedDisjunctiveOperations;
            for (Map.Entry<String, List<EdgeWrapper>> entry : disjunctiveEdgesMap.entrySet()) {
                for (EdgeWrapper e : entry.getValue()) {
                    graph.addEdge(e.e, e.o1, e.o2, EdgeType.DIRECTED);
                }
            }

            boolean hasCycle = new CycleDetector().detectCycleInGraph(graph);
            if (!hasCycle) {
                System.out.println("Znalazł w ieracji: " + iter);
                break;
            } else {
                for (Map.Entry<String, List<EdgeWrapper>> entry : disjunctiveEdgesMap.entrySet()) {
                    for (EdgeWrapper e : entry.getValue()) {
                        graph.removeEdge(e.e);
                    }
                }
            }
            shuffledPermutations.clear();
            disjunctiveEdgesMap.clear();
            if (iter++ > 100000) {
                throw new ConversionError("iteration limit exceed");
            }
        }
    }

    public CriticalPathDetector.Result cloneResult(CriticalPathDetector.Result o) { 
        Cloner cloner = new Cloner();
        return cloner.deepClone(o);
    }

    public DisjunctiveGraph cloneGraph(DisjunctiveGraph graph) {
        Cloner cloner = new Cloner();
        return cloner.deepClone(graph);
    }

    public void addRootsOfProductionJobs(List<? extends Job> roots, DisjunctiveGraph graph, Operation zeroV, Operation lastV) {
        for (Job root : roots) {
            addRoot(root, graph, zeroV, lastV);
        }
    }

    private void addRoot(Job job, DisjunctiveGraph graph, Operation firstOp, Operation lastV) {
        List<Operation> operations = job.operationsToDo;
        Operation prevOp;
        if (operations.size() < 1) {
            throw new ConversionError("job should has at least one operation");
        }
        Operation localFirstOp = null;
        int tempI = 0;
        if (job.hasNoChildren) {
            localFirstOp = firstOp;
            prevOp = firstOp;
        } else {
            operations.get(0).vertexNo = nextVertexNo();
            prevOp = operations.get(0);
            localFirstOp = prevOp;
            tempI = 1;
        }
        for (int i = tempI; i < operations.size(); i++) {
            operations.get(i).vertexNo = nextVertexNo();
            Operation nextOp = operations.get(i);
            graph.addEdge(new Edge(edgeNo++, prevOp.cost), prevOp, nextOp, EdgeType.DIRECTED);
            prevOp = nextOp;
        }
        graph.addEdge(new Edge(edgeNo++, prevOp.cost), prevOp, lastV, EdgeType.DIRECTED);
        if (!job.hasNoChildren) {
            for (Job childJob : (List<Job>) job.children) {
                addRoot(childJob, graph, firstOp, localFirstOp);
            }
        }
    }

//    public void addDisjunctiveArcs(SparseGraph<OperationBase, Edge> graph, List<SimulatedAnnealing_trial1.Touple> disjunctiveVertexes) {
//        for (SimulatedAnnealing_trial1.Touple t : disjunctiveVertexes) {
//            if (!isOnPath(t)) {
//                graph.addEdge(new Edge(edgeNo++, t.o1.operationDuration, true), t.o1, t.o2);
//            }
//        }
//    }

//    public boolean isOnPath(SimulatedAnnealing_trial1.Touple t) {
//        return t.o1.getPre().contains(t.o2) || t.o2.getPre().contains(t.o1) || t.o1.equals(t.o2);
//    }

//    public Map<String, List<OperationBase>> grouppDisjunctiveArcsByWorkerGroup(SparseGraph<OperationBase, Edge> graph) {
//        Collection<OperationBase> vertexes = graph.getVertices();
//        Map<String, List<OperationBase>> grouped = vertexes.stream()
//                .collect(Collectors.groupingBy(v -> v.professionGroup.getLabel()));
//        Map<String, List<OperationBase>> filtered = grouped.entrySet().stream().filter(e -> e.getValue().size() >= 2).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
//        return filtered;
//    }
    public Map<String, List<Operation>> grouppDisjunctiveArcsByWorkerLabel(SparseGraph<Operation, Edge> graph) {
        Collection<Operation> vertexes = graph.getVertices();
        vertexes.stream().filter(v -> v.worker == null).forEach(System.out::println);
        Map<String, List<Operation>> grouped = vertexes.stream()
                .filter(v -> !v.operationName.equals("zero") && !v.operationName.equals("last"))
                .collect(Collectors.groupingBy(v -> v.worker.workerLabel));
        Map<String, List<Operation>> filtered = grouped.entrySet().stream().filter(e -> e.getValue().size() >= 2).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
        return filtered;
    }

    public boolean detectCycles(DisjunctiveGraph graph) {
        boolean areCycles = new CycleDetector().detectCycleInGraph(graph);
        System.out.println("----------------------------------------");
        System.out.println("are cycle? : " + areCycles);
        System.out.println("----------------------------------------");
        return areCycles;
    }
//    public List<SimulatedAnnealing_trial1.Touple> prepareDisjunctiveArcs(Map<String, List<SaOperation>> grouped) {
//        List<SimulatedAnnealing_trial1.Touple> touples = new ArrayList();
//        for (List<SaOperation> list : grouped.values()) {
//            List<SimulatedAnnealing_trial1.Touple> t = Sets.cartesianProduct(ImmutableList.of(ImmutableSet.copyOf(list), ImmutableSet.copyOf(list)))
//                    .stream()
//                    .filter(vt -> !vt.get(0).vertexNo.equals(vt.get(1).vertexNo))
//                    .collect(Collectors.mapping(vt -> new SimulatedAnnealing_trial1.Touple(vt.get(0), vt.get(1)), Collectors.toList()));
//            touples.addAll(t);
//        }
//        return touples;
//    }

    public void aa(DisjunctiveGraph graph) {
        Collection<Edge> disjunctiveEgges = graph.getUndirectedEdges();
        List<Edge> edgesToRemove = new ArrayList();
        for (Edge de : disjunctiveEgges) {
            Pair<Operation> es = graph.getEndpoints(de);
            System.out.println(es.getFirst().vertexNo + " -> " + es.getSecond().vertexNo);
            edgesToRemove.add(de);
            graph.addEdge(new Edge(edgeNo++, es.getFirst().cost, true), es, EdgeType.DIRECTED);
        }
        for (Edge edge : edgesToRemove) {
            graph.removeEdge(edge);

        }
//        SaOperation v0 = graph.getVertices().stream().filter(v -> v.vertexNo == 0).findFirst().get();
//        SaOperation v1 = graph.getVertices().stream().filter(v -> v.vertexNo == 1).findFirst().get();
//        SaOperation v2 = graph.getVertices().stream().filter(v -> v.vertexNo == 2).findFirst().get();
//        SaOperation v3 = graph.getVertices().stream().filter(v -> v.vertexNo == 3).findFirst().get();
//        SaOperation v4 = graph.getVertices().stream().filter(v -> v.vertexNo == 4).findFirst().get();
//        SaOperation v5 = graph.getVertices().stream().filter(v -> v.vertexNo == 5).findFirst().get();
//        SaOperation v6 = graph.getVertices().stream().filter(v -> v.vertexNo == 6).findFirst().get();
//        SaOperation v7 = graph.getVertices().stream().filter(v -> v.vertexNo == 7).findFirst().get();
//        SaOperation v8 = graph.getVertices().stream().filter(v -> v.vertexNo == 8).findFirst().get();
//
////        graph.addEdge(new Edge(edgeNo++, v3.operationDuration, true), new Pair(v3, v6), EdgeType.DIRECTED);
////        graph.addEdge(new Edge(edgeNo++, v5.operationDuration, true), new Pair(v5, v6), EdgeType.DIRECTED);
////        graph.addEdge(new Edge(edgeNo++, v3.operationDuration, true), new Pair(v3, v4), EdgeType.DIRECTED);
////        graph.addEdge(new Edge(edgeNo++, v2.operationDuration, true), new Pair(v2, v5), EdgeType.DIRECTED);
////        graph.addEdge(new Edge(edgeNo++, v4.operationDuration, true), new Pair(v4, v8), EdgeType.DIRECTED);
//        graph.addEdge(new Edge(edgeNo++, v1.operationDuration, true), new Pair(v1, v7), EdgeType.DIRECTED);
//        graph.addEdge(new Edge(edgeNo++, v2.operationDuration, true), new Pair(v2, v6), EdgeType.DIRECTED);
//        graph.addEdge(new Edge(edgeNo++, v6.operationDuration, true), new Pair(v6, v5), EdgeType.DIRECTED);
//        graph.addEdge(new Edge(edgeNo++, v3.operationDuration, true), new Pair(v3, v4), EdgeType.DIRECTED);
//        graph.addEdge(new Edge(edgeNo++, v4.operationDuration, true), new Pair(v4, v8), EdgeType.DIRECTED);
    }
}
