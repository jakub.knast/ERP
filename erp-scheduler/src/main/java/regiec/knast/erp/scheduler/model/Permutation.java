/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.scheduler.sa.SaUtil;

/**
 *
 * @author fbrzuzka
 */ 
public class Permutation extends ArrayList<Operation> {

    static SaUtil saUtil = new SaUtil();
    static Random rand = new Random();

    public static Permutation prepareNeightbourPermutation(final List<Operation> disjunctiveOperations) {
//        List<List<SaOperation>> ll = Lists.newArrayList();
//        ll.add(disjunctiveOperations);
//        String old = saUtil.disjOpToString(ll);
//        System.out.print("\n | old: " + old);
        Permutation newPerm = new Permutation(disjunctiveOperations);
        Operation firstOp = newPerm.get(0);
        newPerm.remove(firstOp);
        newPerm.add(firstOp);

//        List<List<SaOperation>> lldfg = Lists.newArrayList();
//        lldfg.add(newPerm);
//        String nnn = saUtil.disjOpToString(lldfg);
//        System.out.println(" | new: " + nnn);
        return newPerm;
    }

    public static Permutation prepareBetterShuffledPermutation(final Permutation disjunctiveOperations) {
        Permutation newPerm = new Permutation(disjunctiveOperations);
        int limit = 100;
        while (newPerm.equals(disjunctiveOperations)) {
            int shifts = rand.nextInt(disjunctiveOperations.size());
            boolean turn = rand.nextBoolean();

            newPerm = new Permutation(disjunctiveOperations);
            for (int i = 0; i < shifts; i++) {
                Operation firstOp = newPerm.get(0);
                newPerm.remove(firstOp);
                newPerm.add(firstOp);
            }
            if (turn) {
                Collections.reverse(newPerm);
            }
            if (limit-- < 0) {
                throw new ConversionError("shufle permitation limit exceed");
            }
        }
        return newPerm;
    }

    public static boolean areEqual(List<Operation> newPerm, List<Operation> disjunctiveOperations) {
        List<Integer> a = newPerm.stream().map(p -> p.vertexNo).collect(Collectors.toList());
        List<Integer> b = disjunctiveOperations.stream().map(p -> p.vertexNo).collect(Collectors.toList());
        return a.equals(b);
    }

    public static Permutation prepareShuffledPermutation(final List<Operation> disjunctiveOperations) {
        List<Operation> newperm = new ArrayList(disjunctiveOperations);
        int limit = 100;
        while (newperm.equals(disjunctiveOperations)) {
            Collections.shuffle(newperm);
            if (limit-- < 0) {
                throw new ConversionError("shufle permitation limit exceed");
            }
        }
        return new Permutation(newperm);
    }

    public Permutation() {
    }

    public Permutation(Operation op1) {
        super();
        this.add(op1);
    }

    public Permutation(Operation op1, Operation op2) {
        super();
        this.add(op1);
        this.add(op2);
    }

    public Permutation(Operation op1, Operation op2, Operation op3) {
        super();
        this.add(op1);
        this.add(op2);
        this.add(op3);
    }

    public Permutation(Operation op1, Operation op2, Operation op3, Operation op4) {
        super();
        this.add(op1);
        this.add(op2);
        this.add(op3);
        this.add(op4);
    }

    public Permutation(Collection<? extends Operation> c) {
        super(c);
    }

}
